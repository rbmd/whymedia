<?php
date_default_timezone_set('Europe/Moscow');
$start = microtime(true);

header('Content-type: text/html; charset=utf-8');

mb_internal_encoding('UTF-8');
mb_regex_encoding('UTF-8');



switch($_SERVER['YII_ENV'])
{
	case 'development':

		$yii = '/home/yii/yii-1.1.15.022a51/framework/yii.php';

		define('DEBUG', true);

		require realpath(dirname(__FILE__).'/../secure/db.php');

		$config = dirname(__FILE__).'/protected/config/dev.php';
		
	break;

	case 'production':
		#die('closed');
		$yii = '/home/yii/yii-1.1.15.022a51/framework/yii.php';

		require dirname(__FILE__).'/../../shared/secure/db.php';

		$config = dirname(__FILE__).'/protected/config/main.php'; 
		
	break;

	default:
		$yii = '/Users/gl0om/Sites/yii/framework/yii.php';
		require realpath(dirname(__FILE__).'/../secure/db.php');
		$config = dirname(__FILE__).'/protected/config/dev.php'; 
	break;
}

define('DEBUG', true);

//Если хотим дебажить на проде
if(isset($_COOKIE['bgsee']))
{
	define('DEBUG', true);
}

if(defined('DEBUG') && DEBUG == true)
{
	ini_set('display_errors', 1);
	error_reporting(-1);
	defined('YII_DEBUG') or define('YII_DEBUG',true);
	defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);
}


require_once($yii);
Yii::createWebApplication($config)->run();

$end = microtime(true);

// echo $_SERVER['HTTP_HOST'];
echo '<!--' .($end - $start) . '-->';
