var tag = document.createElement('script');
tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

$(function() {

    var player;
    var playerEl = $('#mini-player');
    window.onYouTubeIframeAPIReady = function() {
        player = new YT.Player('mini-player', {
            height: '140',
            width: '240',
            videoId: playerEl.attr('data-id'),
            playerVars: { 'controls': 0 },
            events: {
                'onReady': onPlayerReady,
                'onStateChange': onPlayerStateChange
            }
        });

        $('a.expand-player').on('click', function(e) {
            e.preventDefault();
            player.stopVideo();
            var popup = $('.popup-container');
            var player2 = new YT.Player('big-player', {
                height: '100%',
                width: '100%',
                videoId: playerEl.attr('data-id'),
                playerVars: { 'controls': 0 },
                events: {
                    'onReady': onPlayerReady,
                    'onStateChange': onPlayerStateChange
                }
            });
            popup.show();
            $('body').addClass('no-scroll');
        });

        $('.popup a.close').on('click', function(e) {
            e.preventDefault();
            $(this).closest('.popup-container').hide();
            $('body').removeClass('no-scroll');
        });

        $('.popup-container').on('click', function(e) {
            var el = $(e.target);
            if (el.hasClass('popup-overlay')) {
                $(this).closest('.popup-container').hide();
                $('body').removeClass('no-scroll');
            }
        })
    }

    window.onPlayerReady = function(event) {
        event.target.playVideo();
    };

    var done = false;
    window.onPlayerStateChange = function(event) {
        if (event.data == YT.PlayerState.PLAYING && !done) {
            setTimeout(stopVideo, 6000);
            done = true;
        }
    };
    function stopVideo() {
        player.stopVideo();
    }
});
