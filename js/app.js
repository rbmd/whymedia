var RES_MAX = 1200, RES_MID = 1024;

$(function() {
    var body = $('body');
    var rightCol = $('.col.right');
    $('.big-card').hover(function() {
        $(this).find('.hidden').slideToggle(300);
    });

    $('.social-buttons').socialLikes({

    });

    $('.right-menu li').on('click', function(e) {
        e.preventDefault();
        var href = $(this).data('href');
        if (!href) return false;
        //спагетти
        if ($(this).hasClass('remote')) {
            var win = window.open(href, '_blank');
            win.focus();
        } else {
            location.href = href;
        }
    });

    rightCol.on('click', '.extender', function() {
        rightCol.toggleClass('opened')
    });

    $('.card .desc').each(function() {
        $clamp(this, {clamp: 4});
    });


});

$(window).load(function() {
    var body = $('body');
    var initialScrollTop = body.scrollTop();
    var grid = $('.grid');
    var rightCol = grid.find('.col.right');
    var rightColOffset = rightCol.offset();
    var initialRightColHeight = rightCol.height();
    if (window.innerWidth >= RES_MAX) {
        moveRightCol(
            initialScrollTop, rightCol, rightColOffset,
            initialScrollTop + window.innerHeight, grid.outerHeight() + grid.offset().top, initialRightColHeight);
    }
    //навешиваем класс "sticky" на правую колонку, когда доскролливаем до нее
    $(document).scroll(function() {
        if (window.innerWidth < RES_MAX) {
            return false;
        }
        var $this = $(this);
        var st = $this.scrollTop();
        var stBottom = st + window.innerHeight;
        var contentBottom = grid.outerHeight() + grid.offset().top;
        moveRightCol(st, rightCol, rightColOffset, stBottom, contentBottom, initialRightColHeight);
    });

    function moveRightCol(st, rightCol, rightColOffset, stBottom, contentBottom, height) {
        if (st >= rightColOffset.top && contentBottom - st > height) {
            rightCol.addClass('sticky').removeClass('bottom');
        } else if (contentBottom - st <= height) {
            rightCol.addClass('bottom').removeClass('sticky');
        } else {
            rightCol.removeClass('sticky bottom');
        }
    }
});