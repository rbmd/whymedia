var gulp = require('gulp'),
	sass = require('gulp-sass'),
	autoprefixer = require('gulp-autoprefixer'),
	minifycss = require('gulp-minify-css'),
	plumber = require('gulp-plumber'),
	notify = require('gulp-notify'),
	browserSync = require('browser-sync'),
    reload = browserSync.reload;

gulp.task('scss', function() {
	return gulp.src('static/scss/**/*.scss')
		.pipe(plumber({errorHandler: onError}))
		.pipe(sass({ style: 'expanded', includePaths: 'scss/'}))
		.pipe(autoprefixer())
		//.pipe(minifycss())
		.pipe(gulp.dest('static/css/'))
		.pipe(reload({stream:true}));
});


gulp.task('browser-sync', function() {
    browserSync({
    	//proxy: '192.168.1.179:8000'
    	proxy: 'localhost:8000'
    });
});


gulp.task('default', ['scss', 'browser-sync'], function() {
	gulp.watch('static/scss/**/*.scss', ['scss']);
});


var onError = function(err) {
	notify
		.onError({
			title:    "Gulp",
			subtitle: "Failure!",
			message:  "Error: <%= error.message %>",
			sound:    "Beep"
		})(err);

	this.emit('end');
};
