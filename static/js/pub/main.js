var popup;
$.fn.eauth = function(options) {
	options = $.extend({
		id: '',
		popup: {
			width: 450,
			height: 380
		}
	}, options);

	return this.each(function() {
		 var el = $(this);
		 el.click(function() {
				if (popup !== undefined)
					 popup.close();

				var redirect_uri,
				url = redirect_uri = this.href;

			url += url.indexOf('?') >= 0 ? '&' : '?';
			if (url.indexOf('redirect_uri=') === -1)
				url += 'redirect_uri=' + encodeURIComponent(redirect_uri) + '&';
			url += 'js';

				var centerWidth = ($(window).width() - options.popup.width) / 2,
				centerHeight = ($(window).height() - options.popup.height) / 2;

			popup = window.open(url, "yii_eauth_popup", "width=" + options.popup.width + ",height=" + options.popup.height + ",left=" + centerWidth + ",top=" + centerHeight + ",resizable=yes,scrollbars=no,toolbar=no,menubar=no,location=no,directories=no,status=yes");
				popup.focus();

				return false;
		  });
	});
};

$(function () {
	$(".b-head-auth__dropdown__button, .b-head-auth__dropdown__button").eauth();

	// Layout changer (900 / 1200) & head switcher (static / floating)
	var root = $('.l-root');
	var head = $('.l-head');
	var totop = $('.b-to-page-top');
	$(window).on({
		'resize': function (e) {
			// Hide or show specific for this width elements
			if (document.body.offsetWidth >= 1240 && false == root.hasClass('l-root_w-1200')) {
				root.removeClass('l-root_w-900').addClass('l-root_w-1200');
			} else if (document.body.offsetWidth < 1240 && false == root.hasClass('l-root_w-900')) {
				root.removeClass('l-root_w-1200').addClass('l-root_w-900');
			}
			// Recalculate shift for scrollable chapters
			var slider = $('.b-article-element-nav_all');
			if (slider.find('.b-article-element-nav__viewport').length > 0) {
				var shift = $('body').hasClass('l-root_w-900') ? 135 : 165;
				var pos = parseInt(slider.find('.b-article-element-nav__scroller').attr('data-current-position')) + 1;
				var left = '-' + (((pos-1) * shift)) + 'px';
				slider.find('.b-article-element-nav__layout').css({'left': left});
			}
		},
		'scroll': function (e) {
			//console.log(totop);
			var s = window.pageYOffset || document.documentElement.scrollTop;
			if (s >= 400 && false == head.hasClass('l-head_floating')) {
				head.find('.b-head-logo').toggleClass('b-head-logo_large b-head-logo_small');
				head.css({'opacity': 0}).stop(true, true, true).addClass('l-head_floating').animate({'opacity': 1}, 1000);
				totop.css({'opacity': 1});
			} else if (s < 400 && true == head.hasClass('l-head_floating')) {
				head.stop(true).animate({'opacity': 0}, 200, 'linear', function () {
					head.removeClass('l-head_floating').css({'opacity': 1});
					head.find('.b-head-logo').toggleClass('b-head-logo_large b-head-logo_small');
				});
				totop.css({'opacity': 0});
			}
		}
	});
	$(window).trigger('resize');

	// Best articles accordeon
	$(document).on('mouseover', '.b-preview_type_best-articles .b-preview__item', function (e) {
		$(this).parents('.b-preview_type_best-articles').find('.b-preview__item').removeClass('g-active g-highlight-bg');
		$(this).addClass('g-active');// g-highlight-bg');
	});
	$(document).on('mouseleave', '.b-preview_type_best-articles .b-preview__item', function (e) {
		$(this).parents('.b-preview_type_best-articles').find('.b-preview__item');//.removeClass('g-highlight-bg');
	});

	// Head menu
	$('.b-head-menu-holder').on('mouseenter', function () {
		if ($('.l-root').hasClass('l-root_w-900')) {
			$(this).addClass('b-head-menu-holder_state_extended');
			$('.l-head__right .b-head-element').not('.b-head-search').hide();
			$('.g-icon_triangle_left').show();
			$('.l-head__right .b-head-element .b-head-search__input').hide();
		}
	});
	$('.b-head-menu-holder').on('mouseleave', function () {
		$(this).removeClass('b-head-menu-holder_state_extended');
		$('.l-head__right .b-head-element').not('.b-head-social').show();
		$('.l-head__right .b-head-element .b-head-search__input').show();
		$('.g-icon_triangle_left').hide();
	});
	$('.b-head-search').on('mouseenter', function () {
		if ($('.l-root').hasClass('l-root_w-900')) {
			//$(this).find('.b-head-search__input').css({'display': 'inline'});
			//$('.b-head-menu-holder').hide();
		}
	});
	$('.b-head-search').on('mouseleave', function () {
		if ($('.l-root').hasClass('l-root_w-900')) {
			//$(this).find('.b-head-search__input').css({'display': 'none'});
		}
//		$('.b-head-menu-holder').show();
	});
	$('.b-head-auth__dropdown__history-switcher').on('click', function (e) {
		e.preventDefault();
		if ($(this).hasClass('b-head-auth__dropdown__history-switcher_state_on')) {
			$(this).removeClass('b-head-auth__dropdown__history-switcher_state_on')
				.addClass('b-head-auth__dropdown__history-switcher_state_off');
			$(this).parent().find('span').html('История включена');
		} else if ($(this).hasClass('b-head-auth__dropdown__history-switcher_state_off')) {
			$(this).removeClass('b-head-auth__dropdown__history-switcher_state_off')
				.addClass('b-head-auth__dropdown__history-switcher_state_on');
			$(this).parent().find('span').html('История выключена');
		}
		return false;
	});

	// Chapters scroller
	$('.b-article-element-nav__scroller__control_type_left, .b-article-element-nav__scroller__control_type_right').on('click', function (e) {
		e.preventDefault();
		var scroller = $(this).parent();
		var pos = parseInt(scroller.attr('data-current-position'));
		var count = parseInt(scroller.attr('data-chapters-count'));
		var shift = $('body').hasClass('l-root_w-900') ? 135 : 165;
		// Left
		if ($(this).hasClass('b-article-element-nav__scroller__control_type_left')) {
			if (pos <= 0) {
				return false;
			}
			pos--;
		}
		// Right
		else {
			if (pos >= count - 4) {
				return false;
			}
			pos++;
		}
		// Move
		left = '-' + ((shift * pos) + 1) + 'px';
		scroller.attr('data-current-position', pos);
		$(this).parents('.b-article-element-nav_all').find('.b-article-element-nav__layout').css({'left': left});
		// Arrows appearance
		if (pos <= 0) {
			scroller.find('.b-article-element-nav__scroller__control_type_left').removeClass('b-article-element-nav__scroller__control_state_active').addClass('b-article-element-nav__scroller__control_state_inactive');
		} else {
			scroller.find('.b-article-element-nav__scroller__control_type_left').removeClass('b-article-element-nav__scroller__control_state_inactive').addClass('b-article-element-nav__scroller__control_state_active');
		}
		if (pos >= count - 4) {
			scroller.find('.b-article-element-nav__scroller__control_type_right').removeClass('b-article-element-nav__scroller__control_state_active').addClass('b-article-element-nav__scroller__control_state_inactive');
		} else {
			scroller.find('.b-article-element-nav__scroller__control_type_right').removeClass('b-article-element-nav__scroller__control_state_inactive').addClass('b-article-element-nav__scroller__control_state_active');
		}
		return false;
	});

	$('.b-recommendations .b-preview__head a').on('click', function (e) {
		//console.log($(this).attr('data-network'));
		e.preventDefault();
		$(this).parent().find('a').removeClass('g-active');
		$(this).addClass('g-active');
		switch ($(this).attr('data-network')) {
			case 'vkontakte':
				$(this).parents('.b-recommendations').find('#vk_recommended').css({'opacity': 1, 'visibility': 'visible', 'z-index': 1});
				$(this).parents('.b-recommendations').find('.fb_iframe_widget').css({'opacity': 0, 'visibility': 'none', 'z-index': -1});
				break;
			case 'facebook':
				$(this).parents('.b-recommendations').find('.fb_iframe_widget').css({'opacity': 1, 'visibility': 'visible', 'z-index': 1});
				$(this).parents('.b-recommendations').find('#vk_recommended').css({'opacity': 0, 'visibility': 'none', 'z-index': -1});
				break;
		}
		return false;
	});

    $('#search_input_box').on('keyup', function(e) {
        var text = $(this).val();

        if(text.length > 3)
        {
            $.ajax({
                type: 'POST',
                url: '/search/quick/',
                data: {'text': text},
                dataType: 'json',
                beforeSend: function(jqXHR,settings){
                },
                complete: function(){},
                success: function(data, textStatus, jqXHR){

                    if(data.status == 1)
                    {
                        $('#quick_search_results').html( data.result );
                        $('#quick_search_results').show();
                        $(".b-head-search").addClass("open");
                    }
                    else
                    {
                        $('#quick_search_results').html( "" );
                        $('#quick_search_results').hide();
                        $(".b-head-search").removeClass("open");
                    }

                },
                error: function(XHR, textStatus, errorThrown) {

                    console.log( "Произошла непредвиденная ошибка! " + XHR.responseText );

                }
            });
        }
        else
        {
            $('#quick_search_results').html( "" );
            $('#quick_search_results').hide();
        }
    });

	$('.b-head-search a.g-icon_search').on('click', function (e) {
		e.preventDefault();
		$(this).parents('form').submit();
		return false;
	});


	if (typeof _gaq == 'object')
	{
//		$(".readalso a").click(function() {
//			time = parseInt(new Date().getTime() / 1000 - time_create);
//			_gaq.push(['_trackEvent', 'rcol_readAlso', 'click', 'time', time, true]);
//		});
//
//		$(".mostread a").click(function() {
//			time = parseInt(new Date().getTime() / 1000 - time_create);
//			_gaq.push(['_trackEvent', 'rcol_mostRead', 'click', 'time', time, true]);
//		});


		$(".best a").click(function() {
			time = parseInt(new Date().getTime() / 1000 - time_create);
			_gaq.push(['_trackEvent', 'rcol_best', 'click', 'time', time, true]);
		});

		$(".views-by-week a").click(function() {
			time = parseInt(new Date().getTime() / 1000 - time_create);
			_gaq.push(['_trackEvent', 'rcol_viewByWeek', 'click', 'time', time, true]);
		});


		$(".views-by-month a").click(function() {
			time = parseInt(new Date().getTime() / 1000 - time_create);
			_gaq.push(['_trackEvent', 'rcol_viewByMonth', 'click', 'time', time, true]);
		});


		$(".new-readalso-col1 a").click(function() {
			time = parseInt(new Date().getTime() / 1000 - time_create);
			_gaq.push(['_trackEvent', 'articleBottom_readAlso', 'click', 'time', time, true]);
		});

		$(".to-comments-btn, to-comments-text").click(function() {
			time = parseInt(new Date().getTime() / 1000 - time_create);
			_gaq.push(['_trackEvent', 'toCommentsLink', 'click', 'time', time, true]);
		});
	}
});

$(document).ready(function()
{
	//Глобальные параметры
	siteParams = new function(){
		//Страница на главной
		this.ajax_page = 2;
	}


	//Получение доп статей
	$('.l-main__grid__more').click(function(){
		var section = $(this).attr('data-section');
		window.end_of_section = false;

		if(!window.end_of_section)
		{
			$.ajax({
				url: '/articles/ajaxgetarticles/',
				data: {section: section, page: siteParams.ajax_page},
				error: function() {

				},
				success: function(data) {
					//console.log(data.length);
					if(data.length > 0)
					{
						siteParams.ajax_page++;
						$('.l-main-articles__left').append(data);
						$('.l-main__grid__more').detach().appendTo('.l-main-articles__left');
					}
					else
					{
						window.end_of_section = true;
						$('.l-main__grid__more').hide();
					}
				}
			});
		}
	});


	//Отображение карты гидов
	$('.guidgmap_show').live('click', function(e){
		//Если отображение всех глав одновременно, то показываем карту один раз
		if(window.show_all_chapters !== undefined)
		{
			$('#guidegmap_canvas_wrap').show();
			window.location = '#guidegmap';
		}
		else
			$('#guidegmap_canvas_wrap').toggle();

		GuideGMapPub.initialize();

		//Отмечаем на карте выбранную точку
		GuideGMapPub.highlightMarker( $(this).attr('data-key') );
	});

	//Карта гидов
	GuideGMapPub = new function() {

		//Объект карты
		this.map;

		//Массив маркеров
		this.markers = {};

		//Инициализирована ли карта
		var initialized;

		this.initialize = function () {
			if(GuideGMapPub.initialized == undefined)
			{

				//Центровка карты на Москву
				var latlng = new google.maps.LatLng(55.75, 37.66);

				var myOptions = {
					zoom: 11,
					center: latlng,
					mapTypeId: google.maps.MapTypeId.ROADMAP
				};

				var bounds = new google.maps.LatLngBounds();

				map = new google.maps.Map(document.getElementById("guidegmap_canvas"), myOptions);

				//Отображаем маркеры на карте
				$.each(GuideGMapPub.markers, function(key, value){
					GuideGMapPub.markers[key].marker.setMap(map);

					var latlng = new google.maps.LatLng(GuideGMapPub.markers[key].marker.getPosition().lat(), GuideGMapPub.markers[key].marker.getPosition().lng());
					bounds.extend(latlng);

				});

				map.fitBounds(bounds);
			}

			GuideGMapPub.initialized = true;
		}

		//Добавление маркера на карту
		this.addMarker = function (params, position)
		{
			//Точка содержит все параметры и объект googlemap -  marker
			var dot = {};

			var marker = new google.maps.Marker({
				position: position,
				icon: '/static/css/pub/i/g-icon_map-marker_inactive.png'
				//map: map,
			});



			//При клике должна открываться страница заданой главы или перелистывать к якорю главы
			if(params.chapter_order_num > 0)
			{
				google.maps.event.addListener(marker, 'click', function() {

					GuideGMapPub.highlightMarker(params.key);

					if(window.show_all_chapters !== undefined)
					{
						window.location = "#chapter" + params.chapter_order_num;
					}
					else
					{
						if(params.chapter_order_num == 1)
							window.location = '//' + location.host + location.pathname;
						else
							window.location = "?chapter=" + params.chapter_order_num;
					}

				});
			}

			dot.marker = marker;
			dot.title = params.title;
			dot.key = params.key;
			dot.chapter_order_num = params.chapter_order_num;

			//Добавляем в массив маркер
			GuideGMapPub.markers[params.key] = dot;
		}

		this.resetMarkersIcon = function () {
			$.each(GuideGMapPub.markers, function(key, value) {
				GuideGMapPub.markers[key].marker.setIcon('/static/css/pub/i/g-icon_map-marker_inactive.png');
			});
		}

		this.highlightMarker = function (key) {
			GuideGMapPub.resetMarkersIcon();
			GuideGMapPub.markers[key].marker.setIcon('/static/css/pub/i/g-icon_map-marker_active.png');
		}
	}

	/* element gallery EVENTS*/
    $('.b-photogallery__canvas .g-active').next('div').css('opacity',0)
	$(document).on('click', '.b-announce .b-photogallery__controls__prev, .b-announce .b-photogallery__controls__next', function (e) {
		e.preventDefault();

		var gallery = $(this).parents('.b-photogallery'),
			viewport = gallery.find('.b-photogallery__viewport'),
			canvas = gallery.find('.b-photogallery__canvas'),
			direction = $(this).hasClass('b-photogallery__controls__next') ? 'R' : 'L',
			images = viewport.find('.b-photogallery__slide'),
			count = images.length,
			index = viewport.find('.b-photogallery__slide.g-active').index(),
			resort = 'N';

		if (gallery.attr('data-animation') == 'true') {
			return;
		}
		gallery.attr('data-animation', 'true');

		if (direction == 'L') {
			if (index <= 0) {
				viewport.find('.b-photogallery__slide').not('.g-active').detach().prependTo(canvas);
				canvas.css({'left': '-' + ((count - 1) * 100) + '%'});
				index = count - 2;
				resort = 'L';
			} else {
				index -= 1;
			}
		} else if (direction == 'R') {
			if (index + 1 >= count) {
				viewport.find('.b-photogallery__slide').not('.g-active').detach().appendTo(canvas);
				canvas.css({'left': 0});
				index = 1;
				resort = 'R';
			} else {
				index += 1;
			}
		}

		canvas.stop(true, true).animate({'left': '-' + (index * 100) + '%'}, 500, function () {
			var dummy = index;
			switch (resort) {
				case 'L':
					viewport.find('.b-photogallery__slide').last().detach().prependTo(canvas);
					canvas.css({'left': '-' + ((index + 1) * 100) + '%'});
					dummy++;
				break;
				case 'R':
					viewport.find('.b-photogallery__slide').first().detach().appendTo(canvas);
					canvas.css({'left': '-' + ((index - 1) * 100) + '%'});
					dummy--;
				break;
			}
			gallery.attr('data-animation', 'false');

			$('.b-announce__controls__item').removeClass('g-active');
			var num = parseInt(viewport.find('.b-photogallery__slide').eq(dummy).attr('data-num'));
			$('.b-announce__controls__item').eq(num).addClass('g-active');
		});
		viewport.find('.b-photogallery__slide').removeClass('g-active');
		viewport.find('.b-photogallery__slide').eq(index).addClass('g-active');
		viewport.find('.b-photogallery__slide').not('.g-active').stop(true, true).animate({'opacity': 0}, 500);
		viewport.find('.b-photogallery__slide').eq(index).stop(true, true).animate({'opacity': 1}, 500);
	});

	$('.b-announce').on({
		'mouseenter': function (e) {
			$(this).addClass('g-hover');
		},
		'mouseleave': function (e) {
			$(this).removeClass('g-hover');
		}
	});

	var autoRotateAnnounce = setInterval(function () {
		if ($('.b-announce').hasClass('g-hover')) {
			return false;
		}
		$('.b-announce .b-photogallery__controls__next').click();
		return true;
	}, 7000);

	$(document).on('click', '.b-announce__controls__item', function (e) {
		e.preventDefault();
		var item = $(this),
			index = item.index();
		if (item.hasClass('g-active')) {
			return false;
		}

		var gallery = $(this).parents('.b-photogallery'),
			viewport = gallery.find('.b-photogallery__viewport'),
			canvas = gallery.find('.b-photogallery__canvas'),
			images = viewport.find('.b-photogallery__slide'),
			count = images.length;

		if (gallery.attr('data-animation') == 'true') {
			return;
		}
		gallery.attr('data-animation', 'true');

		canvas.stop(true, true).animate({'left': '-' + (index * 100) + '%'}, 500, function () {
			gallery.attr('data-animation', 'false');
		});
		viewport.find('.b-photogallery__slide').removeClass('g-active');
		viewport.find('.b-photogallery__slide').eq(index).addClass('g-active');
		viewport.find('.b-photogallery__slide').not('.g-active').stop(true, true).animate({'opacity': 0}, 500);
		viewport.find('.b-photogallery__slide').eq(index).stop(true, true).animate({'opacity': 1}, 500);

		$('.b-announce__controls__item').removeClass('g-active');
		item.addClass('g-active');
	});

	$(document).on('click', '.b-article-element-photo .b-photogallery__controls__prev, .b-article-element-photo .b-photogallery__controls__next', function (e) {
		e.preventDefault();

		var $parent = $(this).closest('div[data-gallery_id]');
		var $img = $parent.find('.b-photogallery__viewport img');
		var gallery_id = $parent.attr('data-gallery_id');
		var current_photo = gallery.galleries[gallery_id].current_photo;

		var direction = $(this).hasClass('b-photogallery__controls__next') ? 'R' : 'L';

		if (direction == 'L')
		{
			if(gallery.galleries[gallery_id].current_photo > 0)
				gallery.galleries[gallery_id].current_photo--;
			else gallery.galleries[gallery_id].current_photo = gallery.galleries[gallery_id].photo_count - 1;
		}
		else if (direction == 'R')
		{
			if(gallery.galleries[gallery_id].current_photo < gallery.galleries[gallery_id].photo_count - 1 )
				gallery.galleries[gallery_id].current_photo++;
			else gallery.galleries[gallery_id].current_photo = 0;
		}

		if(current_photo != gallery.galleries[gallery_id].current_photo)
		{
			var photo = gallery.galleries[gallery_id].photos[gallery.galleries[gallery_id].current_photo];
			$img.removeClass('g-active');
			if ($img.timeout != undefined) {
				clearTimeout($img.timeout);
			}
			$img.timeout = setTimeout(function () {
				$img.attr('src', photo.img);
				$img.attr('title', photo.copyright);
				$img.attr('alt', photo.copyright);
				$img.unbind('load');
				$img.bind('load', function (e) {
					$img.addClass('g-active');
				});
				$parent.find('span.b-photogallery__controls__current').html(gallery.galleries[gallery_id].current_photo + 1);

				$parent.find('p.b-article-element-photo__author').html(photo.copyright);
				$parent.find('p.b-article-element-photo__description').html(photo.detail_text);
				clearTimeout($img.timeout);
			}, 500);
		}


		/*
		var gallery = $(this).parents('.b-photogallery'),
			viewport = gallery.find('.b-photogallery__viewport'),
			direction = $(this).hasClass('b-photogallery__controls__next') ? 'R' : 'L',
			images = viewport.find('.b-photogallery__slide'),
			count = images.length,
			index = viewport.find('.b-photogallery__slide.g-active').index();

		if (direction == 'L') {
			if (index <= 0) {
				index = count - 1;
			} else {
				index -= 1;
			}
		} else if (direction == 'R') {
			if (index + 1 >= count) {
				index = 0;
			} else {
				index += 1;
			}
		}
		images.css({'left': '-' + (index * 100) + '%'});
		images.removeClass('g-active');
		images.eq(index).addClass('g-active');
		gallery.find('.b-photogallery__controls__current').html(index + 1);
		*/
	});
	/* element gallery EVENTS END*/
});

/* element gallery*/

gallery = new function Gallery()
{
	//Список добавленных галерей
	this.galleries = {};

	//Добавляем галерею в список
	this.add = function(placeholder, gallery_id, photo_count, photos)
	{
		this.galleries[gallery_id] = {gallery_id: gallery_id, photo_count: photo_count, photos: photos, current_photo: 0};

		var first_photo = gallery.galleries[gallery_id].photos[0];
		var image_holder = $(placeholder).find('.b-photogallery__viewport img');
		$(image_holder).attr('src', first_photo.img);
		$(image_holder).addClass('g-active');
	}

	//Создаем галерею из параметров элемента
	this.place = function(gallery_id, size)	{
		$.ajax({
			url: '/gallery/place/?id='+gallery_id + '&size=' + size,
			success: function(data){
				$('div#gallery_'+gallery_id).replaceWith(data);
			}
		});
	}
}
/* element gallery END*/


/*element poll*/
poll = new function Poll()
{
	this.polls = {};

	//Добавляем голосование в список
	this.add = function(poll_id)
	{
		this.polls[poll_id] = {poll_id: poll_id};
	}


	this.place = function(poll_id)
	{
		$.ajax({
				url: '/polls/place/?poll_id='+poll_id,
				success: function(data){
					$('div#poll_'+poll_id).html(data);
				}
		});
	}
}
/* element poll END*/



var setMinHeight = function(divs) {

	if ($('.l-root').hasClass('l-root_w-1200')) {
		setCount = 3;
	} else {
		setCount = 2;
	}

	var $container = $('.l-main-articles__left'),
			$divs = $(".l-main-articles__left .b-preview");

	$container
		.find('.b-preview:not(.b-recommendations, .b-preview_type_best-articles) .b-preview__item')
		.css('height', 'auto');

  for(var i = 0; i < $divs.length; i+=setCount) {

    var curSet = $divs.slice(i, i+setCount), 
        height = 0;

    curSet
	    .each(function(i) { 
	    	if (($(this).hasClass('b-preview_type_best-articles')) || ($(this).hasClass('b-recommendations'))  ) {
	    		return true;
	    	} else {
	    		$(this).find('.b-preview__item').height()
	    		height = Math.max(height, $(this).find('.b-preview__item').height()); 
	    	}
	    });

	  curSet
  		.each(function(i) { 
	    	if (($(this).hasClass('b-preview_type_best-articles')) || ($(this).hasClass('b-recommendations'))  ) {
	    		return true
	    	} else {
	    		$(this).find('.b-preview__item').css('height', height);
	    	}
	    });


  }
  return this;
};


// $(function () {


$(window).load(function () {
	if ($('.l-main-articles__left').length) {
		setMinHeight();

		$(window).resize(function () {
			setMinHeight();
		})

	}
})


// })



 