$(function() {

	// Форма ответа на комментарий
	function appendReplyForm($comment) {

   var $form,
       rootId;
		$('form.comment-form.static').clone().appendTo($comment);
		$form = $comment.find('form.comment-form');
		$form.removeClass('static');

		if ($comment.attr('data-parent-id') == 0) {
    rootId = $comment.attr('data-id');
		} else {
    rootId = $comment.attr('data-root-id');
		}

		$form.find('input.comment_parent_id').val( $comment.attr('data-id') );
		$form.find('input.comment_level').val( parseInt($comment.attr('data-level'))+1 );
		$form.find('input.comment_root_id').val( rootId );
		$form.find('input.comment_entity_id').val( $comment.attr('data-entity-id') );
		$form.find('input.comment_entity_name').val( $comment.attr('data-entity-name') );
	}

	function resetForms() {
		$('.comment-form').each(function() {
    var $form = $(this);
    $form[0].reset();
    $form.find('.errorMessage').html('');
		});
	}


 // Подгрузка большего кол-ва комментариев

	$('.comments-loadmore').click(function(e) {
   e && e.preventDefault();

   var $this = $(this),
       data;

   data = {
     load_more: {
      'entity_id': $this.attr('data-entity-id'),
      'entity_name': $this.attr('data-entity-name'),
      'limit': $this.attr('data-limit'),
      'level': $this.attr('data-level'),
      'date_from': $this.attr('data-date-from')
     }
   };

   $.ajax({
     type: 'POST',
     url: '/comments/loadmore/',
     data: data,
     dataType: 'json',
     success: function(res) {
       $(res.comments).insertBefore($this);
       if (res.is_more_available) {
         $this.attr('data-date-from', res.date_from);
       } else {
         $this.remove();
       }
     }
   });

	});

  // Ответ на комментарий

	$('[role="comments"]').on('click', '[role="comment-reply"]', function(e) {
   e && e.preventDefault();

   var $replyButton = $(this),
       $comment = $replyButton.parent('[role="comment"]'),
       $mainReplyButton = $('.main-reply-button');

		$('.comment-form').hide();

		if ($replyButton.parent().hasClass('main-reply-button')) {
   $mainReplyButton.hide();
			$('.comment-form.static').show();
		} else {
   $mainReplyButton.show();
  }

  resetForms();

		if ($comment.find('.comment-form').length == 0) {
			appendReplyForm($comment);
			resetForms();
		}

   $comment.find('.comment-form').show();
	});

 // ajax-валидация при сабмите
 $('[role="comments"]').on('click', '[role="comment-send"]', function(e) {
  e && e.preventDefault();

  var submitDelay;

  var $form = $(this).parents('form.comment-form');
  if (!submitDelay) {
    $.ajax({
      type: 'POST',
      url: '/comments/add/',
      data: $form.serialize(),
      dataType: 'json',
      success: function(res) {
         if (res.message !== undefined && res.message.length > 0) {
            $form.find('.errorMessage').html( res.message.join('<br/>') )
            return false;
         }
         $form[0].reset();
         $form.not('.static').hide();
         if (parseInt($form.find('input.comment_parent_id').val()) > 0) {
          $(res).insertAfter($form.parent('[role="comment"]'));
         } else {
          $('[role="comments-list"]').append(res);
          $('.b-nocomments').remove();
         }
      }
     });
  }

 });

});
