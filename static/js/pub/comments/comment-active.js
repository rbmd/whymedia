/*
 * Активация/Деактивация комментария администратором
 */

$('[role="comments"]').on('click', '[role="comment-toggle"]', function(e) {
 e && e.preventDefault();

 var $toggleButton = $(this),
     $comment = $toggleButton.parents('[role="comment"]'),
     commentId = $comment.attr('data-id'),
     commentActiveState = $comment.attr('data-active');

	$.ajax({
		url: "/admin/comments/setcommentsactive/",
		data: {
    id: commentId,
    active: commentActiveState
  },
		cache: false,
		success: function(data){
			if(commentActiveState == 1) {
    $toggleButton.html('Активировать');
    $comment.attr('data-active', 0);
			} else {
    $toggleButton.html('Деактивировать');
    $comment.attr('data-active', 1);
			}
		}
	});
});