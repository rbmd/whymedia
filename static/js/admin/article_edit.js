//Текущий идентификатор черновика статьи
//Нужен для предотвращения сохранения неактуальной версии статьи
var current_article_draft_id = 0;

$(document).ready(function() {

	//Если статья заблокирована, блокируем все формы редактирования
	if (typeof lockarticle !== 'undefined' && lockarticle == true)
	{
		blockForm();
	}

	current_article_draft_id = $("#article_draft_id").text();

	//Отображение контролов элемента при наведении
	$('.element_holder').live('mouseover', function() {
		$(this).find('a.remove_btn').addClass('active');
	});

	$('.element_holder').live('mouseout', function() {
		$(this).find('a.remove_btn').removeClass('active');
	});

	/*Просмотр источника для элемента wysiwyg*/
	//element_source
	$('.wysiwyg_source').live('click', function() {
		$('#screen_fog').show();
		$holder = chapter.getElementHolder($(this));

		$holder.find('.wysiwyg_source_block').show();

		//текстареа для просмотра исходника
		var $textarea = $holder.find('textarea.wysiwyg_source_area');
		var html = $holder.find('div[contenteditable="true"]').html();

		//Добавляем переносы строк
		html = html.replace(/>/gm, ">\r\n");
		html = html.replace(/<\//gm, "\r\n</");

		$textarea.val(html);

		wysiwyg_source_codemirror = CodeMirror.fromTextArea(document.getElementById($textarea.attr('id')), {mode: 'text/html', tabMode: 'indent', lineNumbers: true});
	});

	$('.wysiwyg_source_close').live('click', function() {
		$('#screen_fog').hide();
		wysiwyg_source_codemirror.toTextArea();
		$holder = chapter.getElementHolder($(this));
		var html = $holder.find('textarea.wysiwyg_source_area').val();

		//Убираем переносы строк
		html = html.replace(/(\r\n|\r|\n)/gm, "");

		$holder.find('div[contenteditable="true"]').html(html);
		$holder.find('textarea.wysiwyg_source_area').val('');

		$holder.find('.wysiwyg_source_block').hide();
	});
	/*Просмотр источника для элемента wysiwyg КОНЕЦ*/


	//Клик для редактирование inline-edit элементов
	/*!!!!!ВРЕМЕННО MOUSEOVER нужно подумать над другим алгоритмом, если будет тормозить*/
	$('.element_holder[data-edit="inline"]').live('mouseover', function() {
		$(this).attr('data-mode', 'admin');
		wysiwygManager.replaceEditor( $(this) );		
	});

  //Редактирование элементов по двойному клику
  $('.element_holder[data-edit="basic"]').live('dblclick doubletap', function(e) {
    // e.preventDefault();
    var $basicElement = $(this);
    $.scrollTo( $basicElement, 500, { offset: { top: -107 }} );
    chapter.editElement($basicElement);
    window.getSelection().removeAllRanges();
  });


	//При нажатии на мышь в стороне от элементов необходимо производить сохранение элемента
	$(document).mousedown(function(event) {
		if ( !$(event.target).hasClass('sort_btn') &&  $(event.target).closest(".element_holder[data-mode='admin_inline'], .element_holder[data-mode='admin'], .cke_dialog, .cke, #corrector_mode").length)
		{
			return;
		}
		// Режим корректора
		if ($("#corrector_mode:checked").length == 1)
		{
			return;
		}
		//Сохраняем все элементы
		chapter.saveAllElements();
	});

	//Изменение названия главы должно сразу отображаться в правой панели глав
	$('.chapter_name').live('keyup', function() {
		$('.chapters_menu[data-chapter_id=' + $(this).attr('data-chapter_id') + ']').html($(this).val());
	});

	//Переключение между главами
	$('.chapters_menu').live('click', function() {

		$('.chapters_menu').parent().removeClass('active');
		$(this).parent().addClass('active');

		$('.chapter_holder').removeClass('active');
		$('.chapter_holder[data-chapter_id="' + $(this).attr('data-chapter_id') + '"]').addClass('active');


	});

	//Включаем первую главу сразу после загрузки
	$('.chapters_menu:first').trigger('click');


	//Выбор закладки при редактировании статьи
	$('.article_control_menu button').click(function() {

		//Дописываем в адресную строку хеш
		window.location.hash = '#pane=' + $(this).attr('pane');

		$('.article_control_menu button').removeClass('active');
		$('.tab-pane').removeClass('active');

		$(this).addClass('active');
		$('#' + $(this).attr('pane')).addClass('active');


		//console.log(1);
	});


	//Добавление элемента
	$('.js-article-menu__element').on('click', function() {
		chapter.addElement($(this).attr('data-type'));
	});

	//Добавление главы
	$('#chapter_add_btn').click(function() {
		chapter.add();
	});






	//Сохранение статьи без перезагрузки
	$('.article_ajax_save').click(function() {
		if ($(this).hasClass('disabled'))
		{
			return false;
		}
		var btn = $(this);
		btn.addClass('disabled');
		lockarticle = true;

		var arr = $('#articles-form').serializeArray();

		//Сохраняем все элементы
		chapter.saveAllElements();

		arr.push({name: 'id', value: btn.attr('data-id')});
		arr.push({name: 'createpage', value: createpage});
		arr.push({name: 'article_draft_id', value: current_article_draft_id});

		$(arr).each(function() {
			var tmp_name = $(this)[0].name;
			tmp_name = tmp_name.split('[').join('_');
			tmp_name = tmp_name.split(']').join('');

			//Проверяем, есть ли инстанс визредактора с таким именем
			//Если есть - берем значение из визредактора
			if (CKEDITOR.instances[tmp_name])
			{
				$(this)[0].value = CKEDITOR.instances[tmp_name].getData();
			}
		});

		var url = '/admin/articles/save/';
		//var url = '/admin/articles/edit/?id=' + btn.attr('data-id');

			$.ajax({
						 url       : url,
						 data      : arr,
						 dataType  : 'json',
						 type      : 'post',
						 beforeSend: function() {
							 btn.val('Сохранение');
							 $("#cancel_btn").attr("disabled", "disabled");
						 },
						 error     : function(data) {
							 //btn.val('Ошибка');
							 alert(data);
							 btn.removeClass('disabled');
						 },
						 success   : function(data) {
							 if (data.result == 'lock')
							 {
								 blockForm();
								 alert(data.message);
								 alert_msg(data.message);
							 }
							 else if (data.result == 'error')
								 alert(data.message);
							 else if (data.result == 'success')
							 {
								 current_article_draft_id = data.article_draft_id;
								 lockarticle = false;
							 }
							 else alert(data);

							 btn.removeClass('disabled').val('Сохранить');
							 $("#cancel_btn").removeAttr('disabled');
							 unMarkEdited();
						 }
					 });
	});

	$('.js-article-menu__element')
		.draggable({
			containment: $(".chapters_list div.chapter_holder.active div.chapter_elements"),
			distance: 10,
			helper: "clone",
			drag: function(event, ui) {

				$(ui.helper).addClass('b-admin__article-menu__element_drag');

				var top = ui.offset.top - $(".chapter_holder.active .chapter_elements").offset().top;
				var margin = 0;

				var $chapterElements = $(".chapter_holder.active .chapter_elements .element_holder");
				$(".element_placeholder").remove();
				// Если в главе еще нет элементов, добавляем в самое начало
				if (!$chapterElements.length) {
					pos = null;
					$('.chapter_elements').append("<div class='element_placeholder'></div>");
				}
				else
				{
					$chapterElements.each(function(i) {
						$(".element_placeholder").remove();
						margin += $(this).height();
						if (margin > top)
						{
							pos = top <= 0 ? 0 : i;
							$(".chapter_holder.active div.element_holder").eq(pos).before("<div class='element_placeholder'></div>");
						}
						else
						{
							pos = 'end';
							$(".chapter_holder.active div.element_holder").last().after("<div class='element_placeholder'></div>");
						}
					});
				}
			},
			stop       : function(event, ui) {
				var type = ui.helper.attr("data-type");
				$(".element_placeholder").remove();
				chapter.addElement(type, null, null, pos);
			}
		});
});


$(window).load(function() {
    	//Определяем, нет ли параметров в хеше НАЧАЛО
	var uri_hash = window.location.hash.replace('#', '');
	var hash_array = uri_hash.split(';');
	for (k in hash_array)
	{
		var val = hash_array[k].split('=');

		//Если выбрана конкретная глава
		if (val[0] == 'chapter_id')
		{
			$('.chapters_menu[data-chapter_id="' + val[1] + '"]').trigger('click');
		}

		//Если указана закладка
		if (val[0] == 'pane')
		{
            //console.log(val);
			$('.article_control_menu button[pane="' + val[1] + '"]').trigger('click');
		}
	}
	window.location.hash = uri_hash;
	//Определяем, нет ли параметров в хеше КОНЕЦ
});


//Работа с главами и элементами
var chapter = new function Chapter() {

	this.add = function() {
		if (lockarticle) return false;
		// id любой главы. Нужен для определения текущего article_draft_id/
		// article_draft_id не можем взять из DOM, т.к. он меняется после сохранения всей статьи.
		var article_id = $("#article_id").text();
		//Получаем шаблон главы и добавляем его
		$.ajax({
					 url    : '/admin/chapters/getchapterform/?article_id=' + article_id,
					 success: function(data) {
						 $('#chapters_list').append(data);
						 var last_chapter = $(".chapter_holder:last");
						 //По умолчанию добавляем визредактор
						 chapter.addElement('wysiwyg', last_chapter.attr("data-chapter_order_num"), last_chapter.attr("data-chapter_id"));
						 $('.js-article-menu-chapters').append('<li class="b-admin__article-menu__chapters__item">\
						 	<span title="Удалить" class="b-admin__article-menu__chapters__item__remove btn btn-mini btn-danger remove_chapter"><i class="icon-remove-sign"></i></span>\
						 	<a class="b-admin__article-menu__chapters__item__name chapters_menu" href="#pane=chapters;chapter_id=' + last_chapter.attr("data-chapter_id") + '" data-chapter_id=' + last_chapter.attr("data-chapter_id") + ' data-chapter_order_num="' + last_chapter.attr("data-chapter_order_num") + '">Новая глава</a></li>');
					 }
				 });
	}

	this.addElement = function(element_type, chapter_order_num, chapter_id, position) {
		if (lockarticle) return false;
		this.sortable_chapter_elements();
		if (chapter_order_num == undefined)
		//Добавляем элемент в текущую выбранную главу
			chapter_order_num = $('.chapters_nav li.active a').attr('data-chapter_order_num');

		if (chapter_id == undefined)
		{
			chapter_id = $('.chapters_nav li.active a').attr('data-chapter_id');
		}

		//Получаем шаблон элемента и добавляем его
		$.ajax({
					 url    : '/admin/elements/getelementform/',
					 data   : {
						 element_type: element_type,
						 mode        : 'admin',
						 chapter_id  : chapter_id
					 },
					 error  : function(data) {
						 alert('Error: ' + data);
					 },
					 success: function(data) {

						 if (position == 'end')
						 {
							 $('#chapter_' + chapter_order_num + '_elements .element_holder').last().after(data);
						 }
						 if (position == -1)
						 {
							 $('#chapter_' + chapter_order_num + '_elements').prepend(data);
						 }
						 else if (position != null)
						 {
							 $('#chapter_' + chapter_order_num + '_elements .element_holder').eq(position).before(data);
						 }
						 else
						 {
							 $('#chapter_' + chapter_order_num + '_elements').append(data);
						 }
						 chapter.sortable_chapter_elements_exec($(".chapter_holder.active .chapter_elements"));

					 }
				 });
	}

	// Редактировать элемент
	this.editElement = function(obj) {
		if (lockarticle == true) return false;
		var $holder = obj;
		//Если уже в режиме редактирования
		if ($holder.attr('data-mode') != 'admin')
		{
			$.ajax({
						 url    : '/admin/elements/getelementform/',
						 data   : {
							 element_type: $holder.attr('data-element_type'),
							 element_id  : $holder.attr('data-element_id'),
							 mode        : 'admin'
						 },
						 type   : 'get',
						 error  : function() {
						 },
						 success: function(data) {
							 $holder.replaceWith(data);
								$('.js-autosize').autosize(); // Вешаем авторесайз для элементов
						 }
					 });
		}
	}

	//Сохранение inline-элемента (визредактор, цитата)
	this.saveInlineElement = function(obj) {
		var $holder = chapter.getElementHolder(obj);

		//Находим внутри заданого элемента все инлайн элементы
		$inline = $holder.find('*[contenteditable="true"]');

		var post = {};

		//Обходим все элементы и генерируем массив
		$($inline).each(function() {
			//console.log($(this).attr('data-name') );
			post[$(this).attr('data-name')] = $(this).html();
		});

		$.ajax({
					 url       : '/admin/elements/saveinline/?id=' + $holder.attr('data-element_id') + '&type=' + $holder.attr('data-element_type'),
					 data      : post,
					 type      : 'post',
					 async     : false,
					 beforeSend: function() {
					 },
					 error     : function(data) {
					 },
					 success   : function(data) {
						 markEdited();
					 }
				 });

		$holder.attr('data-mode', 'pub');
	}

	// Валидация элементов
	this.validateElement = function(object)	{
		var validate = true;
		object.find('select, input, textarea').each(function()
		{
			if ($(this).hasClass('required') && $(this).val() == '')
			{
				validate = false;
				$(this).addClass("error");
			}
		});
		return validate;
	}

	//Сохранение обычных элементов с полями
	this.saveElement = function(obj) {
		var $holder = chapter.getElementHolder(obj);
		var arr = $holder.find('form').serializeArray();

		if (chapter.validateElement($holder))
		{
			$(arr).each(function() {
				var tmp_name = $(this)[0].name;

				$(this)[0].name = $(this)[0].name.replace(/_element_id_([0-9]+)/, '');
				tmp_name = tmp_name.split('[').join('_');
				tmp_name = tmp_name.split(']').join('');
				//console.log(tmp_name);
				//Проверяем, есть ли инстанс визредактора с таким именем
				//Если есть - берем значение из визредактора
				if (CKEDITOR.instances[tmp_name])
				{
					$(this)[0].value = CKEDITOR.instances[tmp_name].getData();
					if ($("#corrector_mode:checked").length == 0)
					{
						CKEDITOR.instances[tmp_name].destroy();
					}
				}
			});

			$.ajax({
						 url       : '/admin/elements/save/?id=' + $holder.attr('data-element_id') + '&type=' + $holder.attr('data-element_type'),
						 data      : arr,
						 type      : 'post',
						 async     : false,
						 beforeSend: function() {
						 },
						 error     : function(data) {
						 },
						 success   : function(data) {
							 if ($("#corrector_mode:checked").length == 0)
							 {
								 $holder.replaceWith(data);
							 }
							 markEdited();
						 }
					 });
		}
	}

	//Сохранение всех открытых элементов
	this.saveAllElements = function() {
		$('.element_holder[data-mode="admin"][data-edit="basic"]').each(function() {
			chapter.saveElement($(this));
		});

		$('.element_holder[data-mode="admin"][data-edit="inline"]').each(function() {
			chapter.saveInlineElement($(this));
			//После сохранения обязательно уничтожаем визредактор, т.к. визредактор создается при клике на элемент
			wysiwygManager.destroyEditor( $(this) );
		});
	}

	this.sortable_chapter_elements = function(ui) {
		//Сортировка элементов главы между собой
		$(".chapter_elements").sortable({
													  items               : '.element_holder',
													  cancel              : '.chapter_element',
                            handle : '.b-element__action_move',
													  axis                : 'y',
													  forcePlaceholderSize: true,
													  forceHelperSize     : true,
													  tolerance: 'pointer',

													  //При старте уничтожаем редактор
													  start               : function(event, ui) {
														  // console.log('start');
														 // wysiwygManager.destroyEditor(ui.item);
														  if (lockarticle) return false;
													  },
													  //При остановке создаем новый редактор
													  stop                : function(event, ui) {
														  chapter.sortable_chapter_elements_exec(this);
														  markEdited();
														//  wysiwygManager.replaceEditor(ui.item);
													  }
												  });
	}

	this.sortable_chapter_elements_exec = function(obj) {

		var list = [];
		$(obj).find(".element_holder").each(function(i) {
			list[i] = $(this).attr("data-element_id");
		});
		console.log(list);
		$.post("/admin/elements/sortable/", {list: list});
	}

	//Для заданого элемента возвращает контейнер, в котором размещаются все составные части заданого элемента
	this.getElementHolder = function(element) {
		return $(element).closest('.element_holder');
	}

	//Для заданого элемента возвращает свойство data-chapter-element контейнера, содержащего элемент.
	this.getElementChapter_Element = function(element) {
		return $(element).closest('.element_holder').attr('data-chapter-element');
	}
}

chapter.sortable_chapter_elements();


/*ELEMENT table*/
$(document).ready(function() {
	$('input[data-action="table_addfield"]').live('click', function() {
		var ch_order_num = chapter.getElementHolder($(this)).attr('data-chapter_order_num');
		var el_order_num = chapter.getElementHolder($(this)).attr('data-element_order_num');

		var key = new Date().getTime();
		$(this).before('<div><input type="text" class="span10" name="' + $(this).attr('data-type') + '[]" value="" /> <input class="btn btn-mini" type="button" name="" value="-" data-action="table_delete" /></div>');
	});

	$('input[data-action="table_delete"]').live('click', function() {
		chapter_element = chapter.getElementChapter_Element($(this));
		$(this).closest('div').remove();
		return false;
	});

});

$(function() {
	// Удаляем элемент
	$(".remove_element").live("click", function() {
		if (confirm("Вы действительно хотите удалить элемент?"))
		{
			var obj = $(this);
			$.get("/admin/elements/remove/", {id: $(this).attr("data-element_id")}, function(data) {
				if (data > 0)
				{
					chapter.getElementHolder(obj).remove();
					//obj.parent().remove();
					markEdited();
				}
				else alert('Не удалось удалить элемент');
			});
		}
		return false;
	});


	// Удаляем главу
	$(".remove_chapter").live("click", function() {
		if (confirm("Вы действительно хотите удалить главу со всеми элементами?"))
		{
			id = $(this).parent().find("a").attr("data-chapter_id");
			$(this).parent().remove();
			$("#chapters_list").find("div[data-chapter_id=" + id + "]").remove();
			$.get("/admin/chapters/remove/", {id: id}, function(data) {
				if (data == 0)
					alert('Не удалось удалить главу');
				markEdited();
				sortableChapters();
			});
		}
		return false;
	});


	function sortableChapters() {
		var list = [];
		$(".chapters_nav li a").each(function(i) {
			if ($(this).attr("data-chapter_id"))
			{
				list[i] = $(this).attr("data-chapter_id");
			}
		});
		$.post("/admin/chapters/sortable/", {list: list});
	}

	// Сортировка глав
	$(".chapters_nav").sortable({
											 cancel              : '.nav-header',
											 items               : 'li',
											 axis                : 'y',
											 distance: 15,
											 forcePlaceholderSize: true,
											 forceHelperSize     : true,
											 start               : function(event, ui) {
												 if (lockarticle) return false;
											 },
											 stop                : function(event, ui) {

												 markEdited();
												 sortableChapters();
											 }
										 });


	$(".save_chapter").live("focusout", function() {
		var chapter_id = $(this).attr("data-chapter_id");
		if (chapter_id)
		{
			var c_name = '';
			if ($(this).hasClass("chapter_name"))
			{
				c_name = "name";
			}

			if ($(this).hasClass("img_select"))
			{
				c_name = "image";
			}
			if (c_name)
			{
				markEdited();
				$.ajax({
							 url  : '/admin/chapters/save/',
							 type : "POST",
							 async: false,
							 data : {
								 chapter_id: chapter_id,
								 name      : c_name,
								 value     : $(this).val()
							 }
						 });
			}
		}
	});




	// Кнопки удалить и добавить сайт в элементе Точка гида.

	$(".js-remove-guide-site").live("click", function() {
		$(this).parents('.js-guide-site').remove();
	});

	$(".js-add-guide-site").live("click", function() {

		var $guideSites = $(this).parent('.js-guide-sites'),
				$newSite = $guideSites.find('.js-guide-site:first').clone();

		$newSite.find("input").val('');
		$newSite.find("input.site_link").val('http://');
		$newSite.find(".js-remove-guide-site").show();

		$guideSites.append($newSite);

	});


	$("#service_field_label").click(function() {
		$("#service_field").toggleClass("vis");
	});

	//Отмена изменений в элементах (значок в верхнем блоке)
	$("#edited").click(function() {
		if (confirm("Вы действительно хотите отменить все изменения?"))
		{
			location.href = "/admin/articles/undochanges/?article_id=" + $("#article_id").text();
		}
	});
});


setInterval(lock_article, 60000);
function lock_article() {
	if (lockarticle == false)
	{
		$.get('/admin/articles/updatelock/',
				{
					id: $(".article_ajax_save").attr('data-id'),
					article_draft_id: current_article_draft_id,
					createpage: createpage
				},
				function(data) {
					if (data != null && data.result == 'lock')
					{
						lockarticle = true;
						blockForm();
						alert(data.message);
						alert_msg(data.message);
					}
				}, 'json');
	}
}


function blockForm() {
	$(".tab-content").find("input,select,textarea").attr("disabled", "disabled");
	$("#elements_menu, .remove_element, .remove_chapter, #chapter_add_btn, .article_ajax_save").hide();
	$("div[contenteditable=true]").removeAttr("contenteditable");

	$(".preview_img").append("<span class='btn' id='save_image' style='margin-left:179px;'>Сохранить анонсную картинку</span>").find("input,select").removeAttr("disabled");
}


// Сохранение анонсной картинки в заблокированных статьях
$("#save_image").live('click', function()
{
	image = $("#ArticlesDraft_preview_img").val();
	$.post("/admin/articles/saveimage/", {'image':image,'article_id':article_id}, function(data){
	  if (data == 1)
	  {
		  alert('Сохранено');
	  }
		else
	  {
		  alert(data);
		  //alert('Ошибка');
	  }
	});
});


function markEdited() {
	if ($("#edited").hasClass("noedited"))
	{
		$("#edited").removeClass("noedited");
		$.post("/admin/articles/markEditing/", {id: $("#article_id").text()});
	}
}

function unMarkEdited() {
	$("#edited").addClass("noedited");
}

function alert_msg(msg)
{
	$("#alert").text(msg).show();
}


//----- CROP -----//
$(function() {
	  var crop;
	  var jcrop_api;
	  $("#crop_preview").click(function() {
		  if (typeof(jcrop_api) == 'object')
		  {
			  jcrop_api.destroy();
		  }
		  crop_init();
	  });
	  $("#submit_crop").click(function() {
		  if (checkCoords())
		  {
			  $.post(
				  '/admin/articles/crop/',
				  {'src': $("#ArticlesDraft_preview_img").val(),
					  'w': parseInt(crop.w),
					  'h': parseInt(crop.h),
					  'x': parseInt(crop.x),
					  'y': parseInt(crop.y)
				  },
				  function(data) {
					  $("#ArticlesDraft_preview_img").val(data);
					  jcrop_api.destroy();
					  $("#imgArticlesDraft_preview_img").hide();
				  }
			  );
		  }
	  })

	  function crop_init() {
		  var urlimage = $("#ArticlesDraft_preview_img").val();
		  if (urlimage)
		  {
			  $("#imgArticlesDraft_preview_img").show();
			  $("#imgArticlesDraft_preview_img img").attr("src", urlimage).removeAttr("style").Jcrop({
																																	  onSelect   : updateCoords,
																																	  setSelect  : [480, 286, 0, 0],
																																	  aspectRatio: 480 / 286
																																  }, function() {
				  jcrop_api = this;
			  });
		  }
		  else
		  {
			  alert('Необходимо сначала выбрать изображение');
		  }
	  }

	  function updateCoords(c) {
		  crop = c;
	  }

	  function checkCoords() {
		  console.log(crop);
		  if (parseInt(crop.w) > 0) return true;
		  alert('Please select a crop region then press submit.');
		  return false;
	  };
  }
);
//----- CROP -----//

	// Переключение элементы/главы

	$(function () {

		var $articleMenu = $('.js-article-menu');
				$menuItem = $articleMenu.find('.js-article-menu__item');

		$articleMenu.on('click', '.js-article-menu__button', function (e) {
			e.preventDefault();
			
			var $current = $(this);

			$current
				.addClass('active')
				.siblings()
				.removeClass('active')

			$menuItem
				.eq( $current.index() )
				.removeClass('hidden')
				.siblings('.js-article-menu__item')
				.addClass('hidden');

		});
		
	});


