var codemirror_rootpath = '/static/js/admin/';

CKEDITOR.editorConfig = function(config) {

	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	//config.skin = 'v2';
	config.contentsCss = CKEDITOR.basePath + 'content.css';
	// config.enterMode = CKEDITOR.ENTER_BR;
	config.enterMode = CKEDITOR.ENTER_P;

	config.docType = '<!DOCTYPE html>';
	config.autoParagraph = false;
	config.fillEmptyBlocks = false;
	config.enableTabKeyTools = true;
	config.format_tags = 'p;h1;h2;h3';
	config.floatSpaceDockedOffsetY = 30;
	//чтобы insert вставлял в <p>, а не <div>
	//config.forceEnterMode=true;	
	config.forcePasteAsPlainText = true;

	//config.extraPlugins = 'pastefromword,syntaxhighlight,sourcepopup,tableresize,onchange';
	//config.removePlugins = 'resize';
	config.extraPlugins = 'codemirror,onchange,tabletools,tableresize,autogrow,oembed';
	//config.autogrow = false;
	//config.autoGrow_onStartup = false;
	//config.autoGrow_minHeight = 300;
	//config.autoGrow_maxHeight = 600;

	//config.width = 600;
	config.height = 300;

	config.toolbar =
	[
		{ name: 'document', items: [ 'Source'] },
		{ name: 'clipboard', items: [ 'PasteText', 'PasteFromWord', 'RemoveFormat' ] },
		{ name: 'tools', items: [ 'Maximize'] },
		{ name: 'links', items: [ 'Link', 'Unlink']},
		{ name: 'images', items: [ 'Image', 'oembed'] },
		{ name: 'basicstyles', items: [ 'Bold', 'Italic' ] }
	];

	config.keystrokes =
	[
		[ CKEDITOR.ALT + 121 /*F10*/, 'toolbarFocus' ],
		[ CKEDITOR.ALT + 122 /*F11*/, 'elementsPathFocus' ],
		[ CKEDITOR.SHIFT + 121 /*F10*/, 'contextMenu' ],
		[ CKEDITOR.CTRL + 90 /*Z*/, 'undo' ],
		[ CKEDITOR.CTRL + 89 /*Y*/, 'redo' ],
		[ CKEDITOR.CTRL + CKEDITOR.SHIFT + 90 /*Z*/, 'redo' ],
		[ CKEDITOR.CTRL + 76 /*L*/, 'link' ],
		[ CKEDITOR.CTRL + 66 /*B*/, 'bold' ],
		[ CKEDITOR.CTRL + 73 /*I*/, 'italic' ]
	];
};


CKEDITOR.on('instanceReady', function(ev) {
	var generic = {
		indent          : true,
		breakBeforeOpen : true,
		breakAfterOpen  : true,
		breakBeforeClose: true,
		breakAfterClose : true
	}
	ev.editor.dataProcessor.writer.setRules('p', generic);
	ev.editor.dataProcessor.writer.setRules('td', generic);
});
