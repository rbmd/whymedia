var backurl;

$(document).ready(function() {

	$('[data-svg]').each(function(){
		var s = Snap($(this)[0]);
		Snap.load($(this).attr('data-svg'), function (f) {
			s.append(f);
		});
	});


	$('[data-image]').each(function() {
		$(this).css('background-image', 'url(' + $(this).attr('data-image') + ')')
	});


	sk = skrollr.init({
		smoothScrolling: false,
		forceHeight: false,
		mobileCheck: function() {
			return false;
		},
		beforerender: function(data) {
			// console.log(data)
		}
	});

	$('.menu__trigger').click(function(e) {
		$(this).toggleClass('menu__trigger--active');
	});

	$('.menu__link--push').click(function(e){
		var path = $(this).attr('href').split(lang_pref).join('').split('/').join('');
		$('.menu__trigger').removeClass('menu__trigger--active');
		// workBack();
		contactsBack();
		backurl = window.location.pathname;
		History.pushState(null, $(this).text(), $(this).attr('href'));
		processUrl(path);
		e.preventDefault();
	});


	$('.menu__logo').click(function(e){
		$("html, body").animate({ scrollTop: 0, duration: 700, queue: false });
		e.preventDefault();
	});


	$('.tile').click(function(e){
		$('.work__detail__video iframe').attr('src', '');
		backurl = window.location.pathname;
		History.pushState(null, $(this).text(), $(this).attr('href'));
		var path = $(this).attr('href').split(lang_pref).join('').split('/').join('');
		processUrl(path);
		e.preventDefault();
	});

	History.Adapter.bind(window, "statechange", function () {
		// var path = window.location.pathname.split('/').join('');
		// processUrl(path);
	});


	processUrl();


	addClassNameListener($('.menu__logo[data-index]'), function($o) {
		if ($o.hasClass('menu__link--active'))
		{
			if (location.pathname == lang_pref + '/works/' || location.pathname == lang_pref + '/showreel/')
			{
				backurl = window.location.pathname;
				History.pushState(null, $o.text(), $o.attr('href'));
			}
		}
	});


	addClassNameListener($('.menu__link[data-showreel]'), function($o) {
		if ($o.hasClass('menu__link--active'))
		{
			if (location.pathname == lang_pref + '/works/' || location.pathname == lang_pref + '/')
			{
				backurl = window.location.pathname;
				History.pushState(null, $o.text(), $o.attr('href'));
			}
		}
	});

	addClassNameListener($('.menu__link[data-work]'), function($o) {
		if ($o.hasClass('menu__link--active'))
		{
			backurl = window.location.pathname;
			History.pushState(null, $o.text(), $o.attr('href'));
		}
	});


	$('body').on('click', '.work__detail__play', function(){
		$('.work__detail').removeClass('work__detail--pay');
		$('.work__detail__video iframe').attr('src', $(this).attr('data-src'));
		$('.work__detail__video--inactive').removeClass('work__detail__video--inactive');
		$('.work__detail').addClass('work__detail--pay');
		resizeVideo();
	});

	$('.work__back').click(function(e) {

		workBack();

		e.preventDefault();
	});


	$('.menu__filter .menu__link').click(function(e){
		$('.menu__filter .menu__link').removeClass('menu__link--active');
		$(this).addClass('menu__link--active');
		var f = $(this).attr('data-type');
		
		$('.screen--layout .tile').removeAttr('style').removeClass('tile--off');
		$('[data-image]').each(function() {
			$(this).css('background-image', 'url(' + $(this).attr('data-image') + ')')
		});


		$('.screen--layout .tile').each(function() {
			// data-150-bottom-top="@class: tile <?= $pattern[$i] ?> tile--offscreen"
			// data--100-bottom-top="@class: tile <?= $pattern[$i] ?>"
			// $(this).attr('data--100-bottom-top', $(this).attr('data-orig--100-bottom-top'));
		});

		$('.screen--layout .tile:not([data-type="' + f + '"])').each(function() {
			console.log($(this))
			$(this).removeAttr('data-150-bottom-top');
			$(this).removeAttr('data--100-bottom-top');
		});

		if (f != "")
		{
			$('.screen--layout .tile:not([data-type="' + f + '"])').css('height', 0).addClass('tile--off');
		}

		$('.screen--layout .tile').removeClass('tile--1x');
		$('.screen--layout .tile').removeClass('tile--2x');
		$('.screen--layout .tile').removeClass('tile--3x');
		var j = 0;
		var $tls = $('.screen--layout .tile:not(.tile--off)');
		for (var i = 0; i < $tls.length; i++) {
			console.log(i)
			console.log(pattern[j])
			var t = $tls.get(i);
			$(t).attr('data-150-bottom-top', '@class: tile ' + pattern[j] + ' tile--offscreen');
			$(t).attr('data--100-bottom-top', '@class: tile ' + pattern[j]);
			$(t).addClass(pattern[j]);
			j++;
			if (j == pattern.length)
				j = 0;
		}

		$("html, body").animate({ scrollTop: $('#works').offset().top - 70, duration: 700, queue: false});

		sk.refresh();
	});


	$('.tile, .teammate__photo').on('mouseover', function(){
		$(this).find('video')[0].play();
	});

	$('.tile, .teammate__photo').on('mouseout', function(){
		$(this).find('video')[0].pause();
	});

	$('.showreel__video__play').click(function(){
		$('.showreel__video iframe').attr('src', $(this).attr('data-src'));
		$('.showreel__video--inactive').removeClass('showreel__video--inactive');
		$('.menu').addClass('menu--showreel-play');
		resizeVideo();
	});
	var path = window.location.pathname.split(lang_pref).join('').split('/').join('');
	processUrl(path);


	$('.showreel__close').click(function() {
		$('.showreel__video iframe').attr('src', '');
		$('.showreel__video').addClass('showreel__video--inactive');
		$('.menu').removeClass('menu--showreel-play');
	})



	$('.contacts__mob-menu__item').on('click', function(e) {
		$('.contacts__mob-menu__item').removeClass('contacts__mob-menu__item--active');
		$(this).addClass('contacts__mob-menu__item--active');
		$('.contact__contact, .contact__team').addClass('contact-hidden');
		$('.' + $(this).attr('data-target')).removeClass('contact-hidden')
	});


	$('.work__detail__play').mouseover(function() {
		$('.work__detail').addClass('work__detail--videohover');
	})

	$('.work__detail__play').mouseout(function() {
		$('.work__detail').removeClass('work__detail--videohover');
	})

	$('.work__close-video').click(function(e) {
		$('.work__detail__video').addClass('work__detail__video--inactive');
		$('.work__detail').removeClass('work__detail--pay');
		$('.work__detail__video iframe').attr('src', '');
	})

	// $('.work__tiles').click(function() {
	// 	work_page = 0
	// })

	

	$('.work__detail__label').click(function(e) {
		workBack();
		$('.menu__filter .menu__link[data-type="' + $(this).text() + '"]').click();
		$("html, body").scrollTop($('#works').offset().top - 70);
	});

	console.log('powered by rubymedia.ru')
});



// var body = document.body,
// 	timer;

// window.addEventListener('scroll', function() {
// 	clearTimeout(timer);
// 	if(body && body.classList && !body.classList.contains('no-hover')) {
// 		body.classList.add('no-hover')
// 	}
  
// 	timer = setTimeout(function(){
// 		if (body)
// 			body.classList.remove('no-hover')
// 	},700);
// }, false);
  

function processUrl (p) {
	var path = p || window.location.pathname.split('/').join('');
	console.log(path)
	switch (path) {
		case '':
			$("html, body").animate({ scrollTop: 0, duration: 1500, queue: false });
		break;
		case 'showreel':
			workBack();
			$("html, body").animate({ scrollTop: $('#'+path).offset().top, duration: 1500, queue: false});
		break;
		case 'works':
			workBack();
			$("html, body").animate({ scrollTop: $('#'+path).offset().top - 40, duration: 1500, queue: false});
		break;
		case 'contacts':
			$('.contact--hidden').removeClass('contact--hidden');
			$('.menu__link').removeClass('menu__link--active')
			$('.menu__link[data-contacts]').addClass('menu__link--active');

			// $('.screens').removeClass('screens--hidden');
			// $('.screen').removeClass('screen--hidden');
			// $('.screens').css('height', '100%');
			// $('.screens').css('opacity', '1');
			// $('.work__detail__video iframe').attr('src', '');

			$('.screens').animate({ opacity: 0, duration: 700, queue: false });
			$('.work').fadeOut(700);;
			setTimeout(function(){
				$('.work').addClass('work--hidden');
			},
			700);
		break;
		default:
			// if (work_page != undefined && work_page != 1)
			{
				$.getJSON(
					window.location.pathname,
					function(data){
						console.log(data)
						
						$('.screens').animate({ opacity: 0, duration: 700, queue: false });
						$('.work').fadeOut(700);;
						setTimeout(function(){
							$('.work').addClass('work--hidden');
						},
						700);
						
						setTimeout(function(){
								var $w = $('.work');
								$w.find('.work__detail__label').text(data.work.rubric);
								$w.find('.work__detail__name').text(data.work.name);
								$w.find('.work__detail__video').addClass('work__detail__video--inactive');
								
								if (data.work.video_vimeo)
								{
									$w.find('.work__detail__play').attr('data-src', 'https://player.vimeo.com/video/' + data.work.video_vimeo + '?autoplay=1&title=0&byline=0&portrait=0')
								}
								else
								{
									$w.find('.work__detail__play').attr('data-src', 'https://www.youtube.com/embed/' + data.work.video + '?rel=0&controls=1&showinfo=0&autoplay=1')
								}
								
								$w.find('.work__info__col:first .work__info__text').html(data.work.detail_text);

								if (data.work.client_name != '')
								{
									$w.find('.work__info__col:last .work__info__text a').text(data.work.client_name);
									$w.find('.work__info__col:last .work__info__text a').attr('href', data.work.client_url);
									$w.find('.work__info__col:last').removeClass('work__info__col--hidden');
								}
								else
								{
									$w.find('.work__info__col:last').addClass('work__info__col--hidden');
								}
								$w.find('.work__detail').css('background-image', 'url(' + data.work.pic + ')');
								// $w.find('.work__back').attr('href', (History.length > 0) ? History.savedStates[History.savedStates.length - 2].hash : lang_pref + '/');
								resizeVideo();
								$w.removeClass('work--hidden');
								$w.hide().fadeIn(700);

								$w.find('.work__tiles .tile:first').attr('href', lang_pref + '/works/' + data.prev.code + '/');
								$w.find('.work__tiles .tile:first').find('.tile__label').text(data.prev.rubric);
								$w.find('.work__tiles .tile:first').find('.tile__name').text(data.prev.name);
								$w.find('.work__tiles .tile:first').css('background-image', 'url(' + data.prev.pic + ')');
								$w.find('.work__tiles .tile:first').find('.tile__video source[type="video/mp4"]').attr('src', data.prev.video_mp4);
								$w.find('.work__tiles .tile:first').find('.tile__video source[type="video/webm"]').attr('src', data.prev.video_webm);
								$w.find('.work__tiles .tile:first').find('.tile__video')[0].load();

								$w.find('.work__tiles .tile:last').attr('href', lang_pref + '/works/' + data.next.code + '/');
								$w.find('.work__tiles .tile:last').find('.tile__label').text(data.next.rubric);
								$w.find('.work__tiles .tile:last').find('.tile__name').text(data.next.name);
								$w.find('.work__tiles .tile:last').css('background-image', 'url(' + data.next.pic + ')');
								$w.find('.work__tiles .tile:last').find('.tile__video source[type="video/mp4"]').attr('src', data.next.video_mp4);
								$w.find('.work__tiles .tile:last').find('.tile__video source[type="video/webm"]').attr('src', data.next.video_webm);
								$w.find('.work__tiles .tile:last').find('.tile__video')[0].load();

								$("html, body").animate({ scrollTop: 0, duration: 1500, queue: false });
								$('.screens').addClass('screens--hidden');
								$('.screens').height($w.height());
							},
							700
						)
					}
				)
			}
		break;
	}
}



$(window).resize(function() {
	resizeVideo();
})


function resizeVideo() {
	// $('.work__detail__video iframe').width($(window).width());
	// $('.work__detail__video iframe').height( 9 * $(window).width() / 16 );
	// $('.work__detail__video, .work__detail, .showreel__video, .showreel, .screen--showreel').height( 9 * $(window).width() / 16 );
	$('.work__detail__video, .work__detail, .showreel__video, .showreel, .screen--showreel').height( $(window).height() );
	
}



function addClassNameListener($o, callback) {
    var lastClassName = $o.attr('class');
    window.setInterval( function() {   
       var className = $o.attr('class');
        if (className !== lastClassName) {
            callback($o);   
            lastClassName = className;
        }
    },100);
}


function contactsBack()
{
	if (window.location.pathname != lang_pref + '/contacts/')
		return

	$('.menu__link[data-contacts]').removeClass('menu__link--active')
	$('.contact__close').parent().addClass('contact--hidden'); 

	$('.screens').removeClass('screens--hidden');
	$('.screen').removeClass('screen--hidden');
	$('.screens').css('height', '100%');
	$('.screens').animate({ opacity: 1, duration: 700, queue: false });

	// setTimeout(function(){
	// 	$("html, body").scrollTop(0);
	// },
	// 300);

	if (backurl && backurl != '')
	{
		History.pushState(null, 'Whymedia', backurl);
		backurl = ''
	}
	else
	{
		History.pushState(null, 'Whymedia', '/' + lang_pref);
	}

	// if (History.length > 0)
		// History.back();
	// else
		// $('.menu__logo').click();
}


function workBack() {
	$('.work__detail').removeClass('work__detail--pay');
	$('.screens').removeClass('screens--hidden');
	$('.screen').removeClass('screen--hidden');
	$('.screens').css('height', '100%');
	$('.screens').animate({ opacity: 1, duration: 700, queue: false });
	$('.work').fadeOut(700);

	setTimeout(function(){
		$('.work').addClass('work--hidden'); 
		$('.work__detail__video iframe').attr('src', '');
		sk.refresh();
		$("html, body").scrollTop($('#works').offset().top - 70);
	},
	300);
}


(function() {
  var delay = false;
  var dto = null;

  $(document).on('mousewheel DOMMouseScroll', function(event) {
    
    if(delay || dis == 1) {
    	event.preventDefault();
    	return;
    }

    if (!dto)
    	dto = setTimeout(function(){
    			delay = false;
    			clearTimeout(dto);
    			dto = null;
    		}, 1300)

    var path = window.location.pathname.split(lang_pref).join('').split('/').join('');

    var wd = event.originalEvent.wheelDelta || -event.originalEvent.detail;
// console.log(wd)
    if (wd < -10) 
    {
    	// down
    	if (path == '')
    	{
    		console.log('goto showreel')
    		$("html, body").animate({ scrollTop: $('#showreel').offset().top }, { duration: 1000, queue: false, easing: 'easeOutQuart' });
    		delay = true;
    	}

    	else if (path == 'showreel')
    	{
    		$('.showreel__close').click();
    		console.log('goto works')
    		$("html, body").animate({ scrollTop: $('#works').offset().top - 40 }, { duration: 1000, queue: false, easing: 'easeOutQuart' });
    		delay = true;
    	}
    }
    else if (wd > 10)
    {
    	// up
    	if (path == 'works' && $(window).scrollTop() <= $('#works').offset().top)
    	{
    		console.log('goto showreel')
    		$("html, body").animate({ scrollTop: $('#showreel').offset().top }, { duration: 1500, queue: false, easing: 'easeOutQuart' });
    		delay = true;
    	}

    	else if (path == 'showreel')
    	{
    		$('.showreel__close').click();
    		console.log('goto index')
    		$('.menu__pages').addClass('menu__pages--off')
    		$("html, body").animate({ scrollTop: $('#index').offset().top }, { duration: 1500, queue: false, easing: 'easeOutQuart' });
    		delay = true;
    	}
    }
  });
})();


var keys = {37: 1, 38: 1, 39: 1, 40: 1};
var dis;
function preventDefault(e) {
  e = e || window.event;
  e.stopPropagation()
  if (e.preventDefault)
      e.preventDefault();
  e.returnValue = false;  
}

function preventDefaultForScrollKeys(e) {
    if (keys[e.keyCode]) {
        preventDefault(e);
        return false;
    }
}

function disableScroll() {
dis = 1
  if (window.addEventListener) // older FF
      window.addEventListener('DOMMouseScroll', preventDefault, false);
  window.onwheel = preventDefault; // modern standard
  window.onmousewheel = document.onmousewheel = preventDefault; // older browsers, IE
  window.ontouchmove  = preventDefault; // mobile
  document.onkeydown  = preventDefaultForScrollKeys;
}

function enableScroll() {
	dis = 0
    if (window.removeEventListener)
        window.removeEventListener('DOMMouseScroll', preventDefault, false);
    window.onmousewheel = document.onmousewheel = null; 
    window.onwheel = null; 
    window.ontouchmove = null;  
    document.onkeydown = null;  
}

disableScroll();
$(window).load(function(){
	setTimeout(function() { 
		enableScroll(); 
		$('.screen--intro-loading').removeClass('screen--intro-loading');
		$('.menu__link--lang').removeClass('menu__link--hidden')
	}, 3000)
})