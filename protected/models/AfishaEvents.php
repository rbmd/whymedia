<?php
class AfishaEvents extends CActiveRecord
{
	public $rubrics;
	public $date_start;
	public $date_end;

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}


	public function tableName()
	{
		return 'afisha_events';
	}


	public function defaultScope()
	{
		return array(
			'condition' => 'is_delete=0'
		);
	}

	// Получить месяц начала события
	public function getMonth()
	{
		return intval(substr($this->date_start, 5, 2));
	}

	// Получить список месяцев события
	public function getMonths()
	{
		$month_start = intval(substr($this->date_start, 5, 2));
		$month_end   = intval(substr($this->date_end, 5, 2));
		if ($month_end == 0)
		{
			$month_end = 9;
		}
		$months = array();
		if ($month_start <= $month_end)
		{
			for ($i = $month_start; $i <= $month_end; $i++)
			{
				$months[] = $i;
			}
		}
		return $months;
	}


	// Получить дату начала события
	public function getDate()
	{
		$result = ($this->date_start != $this->date_end) ? 'с ' : '';
		$result .= intval(substr($this->date_start, 8, 2)) . ' ' . UtilsHelper::getMonthName(substr($this->date_start, 5, 2));
		return $result;
	}

	public function scopes()
	{
		return array(
			'indexed'       => array('index' => 'id'),
			'selectForList' => array(
				'select' => 't.*,GROUP_CONCAT(rubric_id SEPARATOR ",") AS rubrics,MIN(date_start) AS date_start,MAX(date_end) AS date_end',
				'order'  => 'date_active_start',
				'group'  => 't.id',
				'join'   => 'LEFT JOIN afisha_events_rubrics r ON (r.event_id=t.id) LEFT JOIN afisha_calendar c ON (t.id=c.event_id)'
			)
		);
	}


	public function rules()
	{
		return array(
			array('title,date_active_start', 'required'),
			array('date_active_start', 'type', 'type' => 'datetime', 'datetimeFormat' => 'yyyy-MM-dd HH:mm:ss'),
			array('link,address,text,video,instagram_tag', 'safe')
		);
	}


	public function attributeLabels()
	{
		return array(
			'title'             => 'Название',
			'link'              => 'Сайт',
			'address'           => 'Адрес',
			'image'             => 'Изображение',
			'date_active_start' => 'Дата старта',
			'date_publish'      => 'Дата отображения',
			'time'              => 'Время',
			'text'              => 'Текст',
			'video'             => 'Видео'
		);
	}

	public function afterSave()
	{
		// Загруженная обложка
		$image = CUploadedFile::getInstance($this, 'image');
		if ($image)
		{
			$this->saveImage($image->tempName);
		}
		return parent::afterSave();
	}


	public function saveImage($path)
	{
		$path = trim($path);
		if ($path)
		{
			$image = new Images($path);
			$image->resize(2000, 0);
			$image->save($this->getImageName('full'), 'jpg', 90);
			$image->resize(595, 0);
			$image->save($this->getImageName('standard'), 'jpg', 80);
			$image->resize(250, 250, true, true, false);
			$image->save($this->getImageName('preview'), 'jpg', 80);
			$image->resize(120, 120, true, true, false);
			if ($image->save($this->getImageName('preview_small'), 'jpg', 80))
			{
				$this->updateByPk($this->id, array('image' => 1));
				if (is_file($path))
				{
					unlink($path);
				}
			}
		}
	}


	public function getImage($type = 'full', $event)
	{
		$filename = $this->getImageName($type, $event['id']);
		return (isset($event['image']) && $event['image'] == 1) ? '/' . $filename : '';
	}


	public function getImageName($type = 'full', $id = false)
	{
		$event_id = $id > 0 ? $id : $this->id;
		$file     = $type;
		return 'media/bestsummer/events/' . implode('/', str_split($event_id)) . '/' . $file . '.jpg';
	}


	public function getFullInfo($id)
	{
		$model = $this->findByPk($id);
		if ($model == false)
		{
			return false;
		}
		foreach ($model as $key => $val)
		{
			$res[$key] = $val;
		}
		$res['image_path'] = ($model->image) ? '/' . $this->getImageName('standard', $id) : '/static/images/pub/bestsummer/placeholder_big.jpg';

		$dates        = Yii::app()->db->createCommand()
			->select('date_start,date_end,time')
			->from('afisha_calendar')
			->where('event_id=:event_id', array(':event_id' => $id))
			->queryAll();
		$res['dates'] = $dates;
		return $res;
	}


	public function getListByDates($date_start, $date_end, $rubric_id = 0)
	{
		$criteria = new CDbCriteria();

		if ($date_start && $date_end)
		{
			$criteria->mergeWith(array(
				'condition' => 'date_end >= :date_start AND date_start <= :date_end',
				'params'    => array(
					'date_start' => $date_start,
					'date_end'   => $date_end
				)
			));
			$criteria->mergeWith(array(
				'condition' => '(date_active_end > NOW() OR date_active_end IS NULL)'
			));

			if ($rubric_id > 0)
			{
				$criteria->mergeWith(array(
					'condition' => 'r.rubric_id=:rubric_id',
					'params'    => array('rubric_id' => $rubric_id)
				));
			}
		}
		return $this->selectForList()->findAll($criteria);
	}
}