<?php

/**
 * User: Rusakov Dmitry
 * Модель пользователей
 */
class User extends CActiveRecord
{
	const STATUS_NOACTIVE = 0;
	const STATUS_ACTIVE = 1;
	const STATUS_BANED = -1;

	/*
	 * Пусть куда сохраняем аватарку пользователя
	 */
	const UPLOAD_AVATAR_DIR = '/media/avatars';

	/*
	  * Размеры пережатия аватарки пользователя
	  */
	const PREVIEW_WIDTH = 150;
	const PREVIEW_HEIGHT = 150;
	const PREVIEW = '150x150';

	public $email;

	public $blog_code;
	public $blog_name;
	public $blog_description;
	// public $blog_cover;

	/**
	 * @var string Пароль, который вводится в форме при регистрации
	 */
	public $password = '';

	/**
	 * @var string Поле для "Введите пароль ещё раз" при регистрации.
	 */
	public $password2 = '';

	public $rememberMe = true;

	public $status = self::STATUS_NOACTIVE;

	// Список ролей пользователя. для выборки через CDbCriteria();
	public $rules;

	private $_identity = null;

	/*
	 * Здесь будем хранить стары аватар для удаления после заливки нового
	 */
	private $old_avatar = '';

	/**
	 * @var Код каптчи
	 */
	public $verifyCode;



	// public function __get($param)
	// {
	// 	if ($param == 'fullname')
	// 	{
	// 		return $this->firstname . ' ' . $this->lastname;
	// 	}
	// 	else
	// 	{
	// 		return parent::__get($param);
	// 	}
	// }

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return 'bg_users';
	}


	public function behaviors()
	{
		return array(
			'dateNormalizer' => array(
				'class' => 'application.extensions.RusDateNormalizerBehavior',
				'dateAttributes' => array(
					'birthday' => false,
				),
			),
		);
	}

	public function rules()
	{
		$rules = array(
			/*Правила, которые применяются ВСЕГДА по умолчанию*/
			array('username', 'length', 'min' => 1, 'max' => 255),
			array('lastname, firstname', 'length', 'min' => 1, 'max' => 64),
			array('lastname, firstname', 'match', 'pattern' => '/^[а-яеЁa-z0-9_\.-]+$/ui', 'message' => 'Поле содержит запрещённые символы!'),

			array('rating, comments_notify', 'numerical', 'integerOnly'=>true),

			array('profession', 'length', 'max' => 255),
			
			array('blog_code', 'length', 'min' => 5, 'max' => 25),
			array('blog_code', 'match', 'pattern' => '/^[a-z0-9_-]+$/', 'message' => 'Допустимые символы: a-z, 0-9, подчеркивание и тире'),
			array('blog_name', 'length', 'max' => 255),
			//array('blog_code, blog_name', '_check_blog'),
			array('blog_description', 'safe'),
			
			//array('username', 'match', 'pattern'=>'/^[а-яеЁa-z0-9@_\.-]+$/ui', 'message'=>'Поле содержит запрещённые символы!'),
			array('birthday', 'date', 'format' => 'dd/MM/yyyy', 'allowEmpty' => true, 'message' => 'Дата рождения указана некорректно!'),
			array('email', 'unique', 'message' => 'Пользователь с таким email уже зарегистрирован', 'on' => 'register'),
			array('username', 'unique', 'message' => 'Пользователь с таким Логином уже зарегистрирован!', 'on' => 'register'),
			array('country, city, instagram', 'safe'),

			/*Правила, которые добавляются только при регистрации*/
			array('email', 'email', /*'checkMX'=>true,*/
				'on' => 'register', 'message' => 'Неверный адрес электронной почты'
			),
			array('email', 'length', 'min' => 3, 'max' => 128, 'on' => 'register'),
			array('username, email, password, password2, firstname, lastname', 'required', 'on' => 'register', 'message' => 'Поле не заполнено'),
			array('password, password2', 'length', 'min' => 6, 'max' => 128, 'on' => 'register'),
			array('password2', 'compare', 'compareAttribute' => 'password', 'message' => 'Пароли должны совпадать', 'on' => 'register'),

			/*Правила, которые добавляются только при авторизации*/
			array('email, password', 'required', 'on' => 'auth', 'message' => 'Поле не заполнено'),
			array('email', 'length', 'min' => 1, 'max' => 128, 'on' => 'auth'),
			array('rememberMe', 'boolean', 'on' => 'auth'),
			array('password', '_auth_pass_method', 'on' => 'auth'),

			/*Правила, которые добавляются только для профайла*/
			array('email, firstname', 'required', 'on' => 'profile'),
			array('email', 'email', /*'checkMX'=>true,*/
				'on' => 'profile'
			),
			array('email', 'unique', 'message' => 'Пользователь с таким email уже зарегистрирован', 'on' => 'profile'),
			array('email', 'length', 'min' => 3, 'max' => 128, 'on' => 'profile'),
			array('password, password2', 'length', 'min'=>6, 'max'=>128, 'allowEmpty'=>true, 'on'=>'profile'),
			array('password2', 'compare', 'compareAttribute'=>'password', 'message' => 'Пароли должны совпадать', 'on'=>'profile'),

			/*Правила, которые добавляются для поиска по пользователмя из админки*/
			array('id, lastname, firstname, username, email, superuser', 'safe', 'on' => 'search'),

			/*Правила, которые добавляются для редактирования пользователей из админки*/
			array('email, username, firstname', 'required', 'on' => 'adminedit'),
			array('email', 'email', 'on' => 'adminedit'),
			array('email', 'length', 'min' => 3, 'max' => 128, 'on' => 'adminedit'),
			//array('avatar', '_check_file_exist', 'allowEmpty' => true, 'on'=>'adminedit'),
			array('avatar, description, activkey, createtime, lastvisit, superuser, status, external_type, external_id', 'safe', 'on' => 'adminedit'),

			array('avatar', 'file',
				'maxFiles'=>1,
				'maxSize'=> 1*1024*1024,
				'types'=>array('png', 'jpg'),
				'wrongType' => 'Неразрешённый формат файла. Разрешённые форматы: png, jpg',
				'tooLarge'=> 'Загружаемый файл не может превышать 1 Мегабайт',
				'allowEmpty' => true,
				'on'=>'profile'
			),

			/*Правила валидации при восстановлении пароля*/
			array('username', 'length', 'min' => 1, 'max' => 64, 'on' => 'recovery'),
			array('recovery_hash, recovery_time', 'safe', 'on' => 'recovery'),

			/*Правила валидации для сценария задания пользователем нового пароля после получения письма со ссылкой для восстановленя*/
			array('password, password2', 'length', 'min' => 6, 'max' => 128, 'allowEmpty' => false, 'on' => 'changepwd'),
			array('password2', 'compare', 'compareAttribute' => 'password', 'message' => 'Пароли должны совпадать', 'on' => 'changepwd'),

			array('blog_code', 'required', 'on' => 'editblog'),
		);

		//Для сценария регистрации, в случае аякс запросов на валидацию, НЕ отдаём правило для каптчи,
		//так как при аякс валидации каптча валидируется НЕверно!
		if (
			Yii::app()->request->isAjaxRequest &&
			($this->scenario == 'register' || $this->scenario == 'recovery') &&
			(Yii::app()->request->getParam('ajax') === 'register-form') || (Yii::app()->request->getParam('ajax') === 'recovery-form')
		) {
			return $rules;
		} else {
			//Так как каптча аяксом валидируется неверно, то отдаём это правило(каптча) ТОЛЬКО КОГДА POST-ЗАПРОС
			if ($this->scenario == 'register') {
				array_push(
					$rules,
					array(
						'verifyCode',
						'captcha',
						'skipOnError' => true,
						'enableClientValidation' => false,
						'captchaAction' => '/site/captcha',
						'on' => 'register'
					)
				);
			}

			if ($this->scenario == 'recovery') {
				array_push(
					$rules,
					array(
						'verifyCode',
						'captcha',
						'skipOnError' => true,
						'enableClientValidation' => false,
						'captchaAction' => '/site/captcha',
						'on' => 'recovery'
					)
				);
			}
		}

		return $rules;
	}

	public function _auth_pass_method($attribute, $params)
	{
		if (!$this->hasErrors()) // we only want to authenticate when no input errors
		{
			$identity = new UserIdentity($this->email, $this->password);

			$identity->authenticate();

			switch ($identity->errorCode) {
				// case UserIdentity::ERROR_STATUS_NOTACTIV:
				// 	$this->addError("email", 'Ваш аккаунт не активирован. <a href="/reactivate/?email=' . $this->email . '">Выслать письмо с активацией еще раз</a>');
				// 	break;

				case UserIdentity::ERROR_STATUS_BAN:
					$this->addError("email", 'Вы забаненны');
					break;

				case 0:
					break;

				default:
					$this->addError("email", 'Неверный логин или пароль');
					break;
			}
		}
	}

	public function _check_file_exist($attribute, $params)
	{
		if ($params['allowEmpty'] && $this->avatar == '') {
			return false;
		}
		if (!$this->hasErrors()) {
			$path = $this->getServerPath($this->avatar, self::PREVIEW) . '/' . $this->avatar;

			if (!file_exists($path)) {
				$this->addError("avatar", 'Указанного файла не существует');
			}
		}
	}


	public function _check_blog($attribute, $params)
	{
		if (!empty($this->blog_code) || !empty($this->blog_name) || !empty($this->blog_description))
		{
			$errors = $this->getErrors();

			if (empty($this->blog_name) && empty($errors['blog_name']))
			{
				$this->addError("blog_name", 'Укажите название блога');
				return false;
			}

			if (empty($this->blog_code) && empty($errors['blog_code']))
			{
				$this->addError("blog_code", 'Укажите адрес блога');
				return false;
			}

			if ($this->blog_code == 'authors' && empty($errors['blog_code']))
			{
				$this->addError("blog_code", 'Адрес недопустим для использования');
				return false;
			}
		}

		$criteria = new CDbCriteria();
		$criteria->condition = 'code = :blog_code AND user_id != :user_id';
		$criteria->params = array(':blog_code' => $this->blog_code, ':user_id' => $this->id);

		$b = Blog::model()->find($criteria);

		if (!empty($b))
			$this->addError("blog_code", 'Адрес занят');
	}

	/**
	 * Проверка пользователя на существование по логину или email
	 * @param $attribute
	 * @param $params
	 */
	public function _check_user_exist($attribute, $params)
	{
		if (!$this->hasErrors()) {
			$user = $this->find(
				'(`email`=:email OR `username`=:username) AND external_type = "" ',
				array(':username' => $this->username, ':email' => $this->username)
			);

			// if (empty($user)) {
				// $this->addError("username", 'Пользователя с таким логино/email не существует!');
			// }
		}
	}

	public function scopes()
	{
		return array(
			'active' => array(
				'condition' => 'status=' . self::STATUS_ACTIVE,
			),
			'notactvie' => array(
				'condition' => 'status=' . self::STATUS_NOACTIVE,
			),
			'banned' => array(
				'condition' => 'status=' . self::STATUS_BANED,
			),
			'superuser' => array(
				'condition' => 'superuser=1',
			)
		);
	}

	public function defaultScope()
	{
		return array(
			'select' => '*',
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array( //
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		$r = array(
			'id' => 'ID',
			'email' => 'Электронная почта',
			'password' => 'Пароль',
			'password2' => 'Повторите пароль',
			'username' => 'Логин',
			'firstname' => 'Имя',
			'rule' => 'Роль',
			'lastname' => 'Фамилия',
			'superuser' => 'Админ',
			'description' => 'Описание',
			'birthday' => 'Дата рождения',
			'verifyCode' => 'Код проверки',
			'rememberMe' => 'Запомнить меня',
			'avatar' => 'Аватар',
			'country' => 'Страна',
			'city' => 'Город',
			'blog_code' => 'Адрес блога',
			'blog_name' => 'Название блога',
			'blog_description' => 'Описание блога',
			'profession' => 'Профессия',
			'comments_notify' => 'Оповещение об ответах на комментарии',
		);

		if ($this->scenario == 'auth') {
			$r['email'] = 'Электронная почта';
		}

		return $r;
	}

	/**
	 * Logs in the user using the given username and password in the model.
	 * @return boolean whether login is successful
	 */
	public function login($pwd = null)
	{
		if ($this->_identity === null) {
			$this->_identity = new UserIdentity(trim($this->email), empty($pwd) ? trim($this->password) : trim($pwd));
			$this->_identity->authenticate();
		}

		if ($this->_identity->errorCode === UserIdentity::ERROR_NONE) {
			$duration = $this->rememberMe ? 3600 * 24 * 365 : 0; // 365 days = 1 Year

			Yii::app()->user->login($this->_identity, $duration);

			return true;
		} else {
			return false;
		}
	}

	protected function beforeSave()
	{
		if ($this->scenario == 'register') {
			$salt = UserIdentity::makesalt();
			$this->password = $salt . UserIdentity::encrypting($this->password, $salt);

			$this->status = self::STATUS_NOACTIVE;
			$this->createtime = new CDbExpression('NOW()');
			$this->lastvisit = new CDbExpression('NOW()');
		} 
		else if ($this->scenario == 'profile')
		{
			//Если меняем пароль, то надо сгенерировать хешь для нового пароля перед сохранением в БД
            if (!empty($this->password2))
            {
                $salt = UserIdentity::makesalt();
                $this->password = $salt.UserIdentity::encrypting($this->password, $salt);
            }

			$avatar = CUploadedFile::getInstance($this, 'avatar');

			if (!empty($avatar)) {

				$user_folder = md5($this->id);
				$avatar_filename = md5(time());
				$folder1 = substr($user_folder, 0, 2);
				$folder2 = substr($user_folder, 2, 2);

				$web_path = self::UPLOAD_AVATAR_DIR . '/original/' . $folder1 . '/' . $folder2;
				$path_to_save = Yii::getPathOfAlias('webroot') . $web_path;

				if (!file_exists($path_to_save))
					mkdir($path_to_save, 0777, true);

				if ($avatar->saveAs($path_to_save . '/' . $avatar_filename . '.' . $avatar->extensionName))
				{
					$this->avatar = $web_path . '/' . $avatar_filename . '.' . $avatar->extensionName;
					ThumbsMaster::getThumb($this->avatar, ThumbsMaster::$settings['260_260'], true);

					Yii::app()->cache->delete('User.getAvatar'.$this->id);
				}
			}
		}

		return parent::beforeSave();
	}


	protected function afterSave()
	{
		parent::afterSave();

		if ($this->scenario == 'createblog') {

			if (!empty($this->blog_code))
			{
				if (empty($this->blog))
				{
					$this->blog = new Blog();
				}

				$this->blog->user_id = $this->id;
				$this->blog->code = $this->blog_code;
				$this->blog->name = $this->blog_name;
				$this->blog->description = $this->blog_description;
				$this->blog->save();
				// if (!$this->blog->save())
					// die(json_encode($this->blog->getErrors()));

				Yii::app()->user->setState('blog_code', $this->blog->code);
			}

			//Обновляем данные в сессии, так как они могли измениться
			foreach ($this->attributes as $name => $value) {
				if (in_array($name, array('id', 'activkey', 'password'))) {
					continue;
				}

				Yii::app()->user->setState($name, is_numeric($value) ? $value + 0 : $value);
			}

			Yii::app()->user->setState('fio', $this->lastname . ' ' . $this->firstname);
		}



		if ($this->scenario == 'editblog') {

			if (!empty($this->blog_code))
			{
				$this->blog->code = $this->blog_code;
				$this->blog->name = $this->blog_name;
				$this->blog->description = $this->blog_description;
				$this->blog->save();
				// if (!$this->blog->save())
					// die(json_encode($this->blog->getErrors()));

				Yii::app()->user->setState('blog_code', $this->blog->code);
			}

			//Обновляем данные в сессии, так как они могли измениться
			foreach ($this->attributes as $name => $value) {
				if (in_array($name, array('id', 'activkey', 'password'))) {
					continue;
				}

				Yii::app()->user->setState($name, is_numeric($value) ? $value + 0 : $value);
			}

			Yii::app()->user->setState('fio', $this->lastname . ' ' . $this->firstname);
		}


		if ($this->scenario != 'adminedit')
		{
			if (!empty(Yii::app()->user))
			{
				//Обновляем данные в сессии, так как они могли измениться
				foreach ($this->attributes as $name => $value) {
					if (in_array($name, array('id', 'activkey', 'password'))) {
						continue;
					}

					Yii::app()->user->setState($name, is_numeric($value) ? $value + 0 : $value);
				}
			}
		}


		// ElasticNestedReindexQueue::addTask('authors', array('should' => array('match' => array('authors.id' => $this->id))));

		// $search = new \YiiElasticSearch\Search(Yii::app()->name, 'articles,posts');
		// $search->filter = array(
		// 	'nested' => array(
		// 		'path' => 'authors',
		// 		'query' => array(
		// 			'bool' => array('should' => array('match' => array('authors.id' => 21008)))
		// 		)
		// 	)
		// );

		// $result_set = Yii::app()->elasticSearch->search($search);
		// $search_results = $result_set->getResults();

		// foreach ($search_results as $p)
		// {
		// 	if ($p->getType() == 'posts')
		// 	{
		// 		$post = Post::model()->findByPk($p->getId());
		// 		$post->save();
		// 	}
		// }

	}

	/**
	 * Получить УНИКАЛЬНОЕ В ПРЕДЕЛАХ ПЛАНЕТЫ СЛУЧАЙНОЕ 32 байтное значение.
	 *
	 * @return string
	 */
	public final static function getUniqueValue()
	{
		return md5(uniqid(rand(), true));
	}

	/*
	  * Получить относительный путь до аватарки;
	  * $preview - получить оригинал или превьюшку указанного размера, например "150х150"
	  * Превьюшка должна существовать!
	  */
	public function getRelPath($photo_name, $preview = 'original')
	{
		$folder1 = substr($photo_name, 0, 2);
		$folder2 = substr($photo_name, 2, 2);
		$folder3 = substr($photo_name, 4, 2);

		$full_path = self::UPLOAD_AVATAR_DIR . "/$preview" . '/' . $folder1 . '/' . $folder2 . '/' . $folder3;

		return $full_path;
	}

	/*
	  * Получить абсолютный путь на сервере до аватарки
	  * $preview - получить оригинал или превьюшку указанного размера, например "150х150"
	  * Превьюшка должна существовать!
	  */
	public function getServerPath($photo_name, $preview = 'original')
	{
		return Yii::getPathOfAlias('webroot') . $this->getRelPath($photo_name, $preview);
	}

	/*
	  * Получить относительный ПОЛНЫЙ путь до аватарки на сайте, включая имя картинки
	  * $preview - получить оригинал или превьюшку указанного размера, например "150х150"
	  * Превьюшка должна существовать!
	  */
	public function getPhotoImgSrc($photo_name, $preview = 'original')
	{
		return Yii::app()->request->hostInfo . $this->getRelPath($photo_name, $preview) . '/' . $photo_name;
	}


	/**
	 * Вернуть полное имя пользователя
	 *
	 * @param int $id ID пользователя
	 * @return string
	 */
	public function getFullName($id = '')
	{
		if ($id) {
			$model = self::model()->findByPk($id);
		} else {
			$model = $this;
		}
		return $model->firstname . ' ' . $model->lastname;
	}


	/*
	 * Получить статус пользователя
	 * $user_id int
	 */
	public function getStatus($user_id = 0)
	{
		$user_id_for_sql = ($user_id) ? intval($user_id) : Yii::app()->user->id;
		$result = User::model()->find(array(
			'select' => 'status',
			'condition' => 'id=:id',
			'params' => array(':id' => $user_id_for_sql)
		));
		return $result['status'];
	}

	public static function getListForRole($role = '')
	{
		return Yii::app()->db->createCommand()
			->select('id, CONCAT(lastname, " ", firstname) AS name, email')
			->from('AuthAssignment a')
			->join('bg_users', 'bg_users.id = a.userid')
			->where('itemname=:role', array(':role' => $role))
			->order('lastname')
			->queryAll();
	}


	// Обновить последнее посещение
	public static function updateLastvisit($user_id)
	{
		self::model()->updateByPk($user_id, array(
			'lastvisit' => new CDbExpression('NOW()')
		));
	}



	public function updateRating()
	{
		$connection = Yii::app()->db;

		$sql = '
				select sum(r)
				
				from (

				select sum(rating) * 3 r from bg_posts 
					inner join bg_blogs on (bg_blogs.id = bg_posts.blog_id)
					where user_id = ' . $this->id . '

				union
				
				select sum(rating) r from bg_articles art
					inner join bg_articles_to_authors ata on (ata.article_id = art.id)
					inner join bg_authors au on (au.id = ata.author_id)
					where au.user_id = ' . $this->id . '

				union

				select sum(rating) r from bg_comments 
					where user_id = ' . $this->id . '

				) t';

		$rate = (int) $connection->createCommand($sql)->queryScalar();

		$sql = 'UPDATE bg_users SET rating = ' . $rate . ' WHERE id = ' . $this->id;
		$connection->createCommand($sql)->execute();
	}



	public function url()
	{
		return '/user/' . $this->id . '/';
	}



	public static function getAvatar($user_id)
	{
		$CACHE_KEY = 'User.getAvatar'.$user_id;
		$avatar = Yii::app()->cache->get($CACHE_KEY);

		if ($avatar === false)
		{
			$user = User::model()->findByPk($user_id);
			$avatar = !empty($user->avatar) ? $user->avatar : null;
			Yii::app()->cache->set($CACHE_KEY, $avatar, 3600 * 24 * 356);
		}

		return $avatar;
	}
}