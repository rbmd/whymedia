<?php
class SpecprojectsSantaUsers extends CActiveRecord
{
	public $verifyCode;
	public $whom_name;


	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return 'specprojects_santa_users';
	}

	public function rules()
	{
		return array(
			array('verifyCode', 'captcha', 'on' => array('register')),
			array('name, email, interests, gender,wish', 'required'),
			array('wish,born', 'safe'),
			array('email', 'email'),
			array('email', 'unique')
		);
	}


	public function attributeLabels()
	{
		return array(
			'id'         => 'ID',
			'name'       => 'Имя Фамилия',
			'born'       => 'Дата рождения',
			'email'      => 'E-mail',
			'gender'     => 'Пол',
			'interests'  => 'Интересы',
			'wish'       => 'Пожелания к подарку',
			'verifyCode' => ''
		);
	}

	public static function countActive()
	{
		$criteria            = new CDbCriteria;
		$criteria->condition = 'active=1';
		return self::model()->count($criteria);
	}


	public function afterValidate()
	{
		//$this->born = $_POST['SpecprojectsSantaUsers']['born_day'] . '.' . $_POST['SpecprojectsSantaUsers']['born_month'] . '.' . $_POST['SpecprojectsSantaUsers']['born_yar'];
		parent::afterValidate();
	}

}