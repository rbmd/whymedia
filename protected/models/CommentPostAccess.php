<?php

class CommentPostAccess
{
	public static function hasAccess($post_id, $user_id)
	{
		$post = Post::model()->cache(120)->findByPk($post_id);

		if (empty($post))
		{
			return false;
		}

		$CACHE_KEY = 'CommentPostAccess.hasAccess'.$post->blog_id.$user_id;
		$access = Yii::app()->cache->get($CACHE_KEY);

		if ($access === false)
		{

			$sql = '
					SELECT * 
					FROM bg_comment_blog_ban
					WHERE blog_id = ' . $post->blog_id . '
						AND user_id = :user_id
					';

			$command = Yii::app()->db->createCommand($sql);
			$command->bindParam(':user_id', $user_id);

			$result = $command->queryRow();

			$access = $result == false ? true : null;

			Yii::app()->cache->set($CACHE_KEY, $access, 60);
		}

		return $access;
	}

	// Бан автором пользователя в своем блоге
	public static function ban($blog_id, $user_id)
	{
		$sql = '
				INSERT INTO bg_comment_blog_ban (`blog_id`, `user_id`)
				VALUES ('.$blog_id.', '.$user_id.')
				ON DUPLICATE KEY UPDATE user_id = user_id;
			';

		$command = Yii::app()->db->createCommand($sql);
		$command->execute();

		Yii::app()->cache->delete('CommentPostAccess.hasAccess'.$blog_id.$user_id);
	}


	// Бан автором пользователя в своем блоге
	public static function unban($blog_id, $user_id)
	{
		$sql = '
				DELETE FROM bg_comment_blog_ban
				WHERE blog_id = ' . $blog_id . '
					AND user_id = ' . $user_id
			;

		$command = Yii::app()->db->createCommand($sql);
		$command->execute();

		Yii::app()->cache->delete('CommentPostAccess.hasAccess'.$blog_id.$user_id);
	}
}