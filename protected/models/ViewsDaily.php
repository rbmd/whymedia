<?php

/**
 * Ежедневная статистика по каждой статье
 */
class ViewsDaily extends CActiveRecord
{
	/*
	 * Время кеширования статистики в секундах
	 */
	const STATISTICS_TTL = 60;

    public $views = 0;
    public $views_all = 0;

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return 'bg_views_daily';
	}

	public function scopes()
	{
		return array(
			'Indexed'=>array(
				'index'=>'element_id',
				'select'=>array('*'),
			),
		);
	}

	/*Выбрать статистику для определённых элементов*/
	public function inList(array $elements_list)
	{
		if( empty($elements_list) )
		{
			return $this;
		}

		$this->getDbCriteria()->mergeWith(array(
			'condition' => 'element_id IN (' . implode(', ', $elements_list) . ')',
			'order'=>'element_id ASC',
		));

		return $this;
	}


	// Named scope - просмотры, относящиеся к конкретному телешоу (ко всем статьям с tree_id = $tree_id)
	// используется для выбора просмотров в рамках отдельных телешоу (просмотры всех выпусков в "Деньги")
	public function childOf($tree_id)
	{
		//Если на входе массив
		if(is_array($tree_id) && count($tree_id) > 0)
		{
			$this->getDbCriteria()->mergeWith(
				array(
					'condition' => 'tree_id IN ('.implode(',', $tree_id).')',
				)
			);
		}
		//Если на входе число
		elseif(is_numeric($tree_id))
		{
			$this->getDbCriteria()->mergeWith(
				array(
					'condition' => 'tree_id = ' . $tree_id,
				)
			);
		}
		
		return $this;
	}

	public function rules()
	{
		return array(
			//
		);
	}

	public function relations()
	{
		return array(
			//
		);
	}

	public function attributeLabels()
	{
		return array(
			//
		);
	}

	/*
	 * Получить статистику для списка статей за определённый день(дата).
	 *
	 * $elements - статьи, должны быть выбраны с участием скоупа Indexed(), например:
	 *    $articles = Articles::model()->with('tree')->Indexed()->findAll($criteria);
	 * $day - день(дата) за который надо выбрать статистику для указанных статей;
	 *    если день не указан, то берётся сегодняшнее число.
	 *    День(дата) должен быть указан в формате MySQL.
	 *
	 * Использовать так:
	 * (1)получить статистику с помощью этого метода, например:
	 *    $stat = ViewsFull::model()->getStatistic4Elements( $articles );
	 *    или так:
	 *    $stat = ViewsFull::model()->getStatistic4Elements( $articles, '2012-04-09' );
	 * (2)обращаться к статистике как $stat[id_статьи]->поле, например $stat['153963']->views
	 *
	 * Примечание: проверять на существование нужного элемента в $stat не требуется, так как если
	 * элемента не существует, то вернёт пустой объек ViewsFull, у которого свойства views и views_all будут == 0.
	 */
	public function getStatistic4Elements(array $elements, $day=null)
	{
		if( isset($elements[0]) )
		{
			throw new Exception('Нелья получать статистику для непроиндексированных объектов!');
		}

		if( empty($day) )
		{
			$day = date('Y-m-d');
		}

		$elements = array_keys($elements);

        if( empty($elements) )
        {
            return new EasyStat(array(),  __CLASS__);
        }

		$key = md5(implode(':', $elements) . '::' . $day);

		$result = Yii::app()->cache->get($key);

		if( empty($result) )
		{
			$result = new EasyStat( $this->inList($elements)->Indexed()->findAll('view_date=:day', array(':day'=>$day)), __CLASS__ );

			Yii::app()->cache->set($key, $result, self::STATISTICS_TTL);
		}

		return $result;
	}
}
?>