<?php
class InstagramTags extends CActiveRecord
{
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return 'bg_instagram_tags';
	}

	public function defaultScope()
	{
		return array(
			'condition' => 'is_delete=0'
		);
	}
}