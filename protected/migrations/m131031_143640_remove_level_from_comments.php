<?php

class m131031_143640_remove_level_from_comments extends CDbMigration
{
	public function up()
	{
		$this->dropColumn('bg_comments', 'level');
	}

	public function down()
	{
		$this->addColumn('bg_comments', 'level', 'INT(2) NOT NULL DEFAULT 0 AFTER root_id');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}