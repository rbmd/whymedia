<?php

class m140921_204715_block3_to_text extends CDbMigration
{
	public function up()
	{
		$this->dropColumn('chtpz_partners', 'block3_pic');
		$this->addColumn('chtpz_partners', 'block3_pic', 'TEXT NOT NULL DEFAULT "" AFTER block2_pic');
	}

	public function down()
	{

	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}