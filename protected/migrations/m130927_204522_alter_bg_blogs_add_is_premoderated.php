<?php

class m130927_204522_alter_bg_blogs_add_is_premoderated extends CDbMigration
{
	public function safeUp()
	{
		$this->addColumn('bg_blogs', 'is_premoderated', 'TINYINT(1) NOT NULL DEFAULT 1');
		$this->createIndex('is_premoderated', 'bg_blogs', 'is_premoderated');
	}

	public function safeDown()
	{
		$this->dropColumn('bg_blogs', 'is_premoderated');
	}
}