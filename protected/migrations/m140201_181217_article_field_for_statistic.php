<?php

class m140201_181217_article_field_for_statistic extends CDbMigration
{
	public function up()
	{
		$this->addColumn('bg_articles', 'detail_text_letters', 'INT(10) NOT NULL DEFAULT 0');

		$this->addColumn('bg_articles_draft', 'customer', 'VARCHAR(255) NULL ');
		$this->addColumn('bg_articles', 'customer', 'VARCHAR(255) NULL');


		$this->addColumn('bg_articles_draft', 'price', 'INT(10) NULL');
		$this->addColumn('bg_articles', 'price', 'INT(10) NULL');			

		$this->createIndex('customer', 'bg_articles', 'customer');	
	}

	public function down()
	{
		
		$this->dropColumn('bg_articles', 'detail_text_letters');

		$this->dropColumn('bg_articles_draft', 'customer');
		$this->dropColumn('bg_articles', 'customer');			

		$this->dropColumn('bg_articles_draft', 'price');
		$this->dropColumn('bg_articles', 'price');		

	}

}