<?php

class m140728_211626_fnl2014 extends CDbMigration
{
	public function up()
	{
		$this->createTable(
			'fnl2014',
			array(
				 'id'                => 'INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT',
				 'letter'        => 'VARCHAR(1)',
				 'name'           => 'VARCHAR(255)',
				 'logo'            => 'VARCHAR(255)',
				 'description'            => 'TEXT',
				 'photo'            => 'VARCHAR(255)',
			),
			'ENGINE=InnoDB CHARSET=utf8'
		);
	}

	public function down()
	{
		$this->dropTable('fnl2014');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}