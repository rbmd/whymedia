<?php

class m140201_204849_alter_bg_user_add_comments_notify extends CDbMigration
{
	public function safeUp()
	{
		$this->addColumn('bg_users', 'comments_notify', 'TINYINT(1) NOT NULL DEFAULT 1');
		$this->createIndex('comments_notify', 'bg_users', 'comments_notify');
	}

	public function safeDown()
	{
		$this->dropColumn('bg_users', 'comments_notify');
	}
}