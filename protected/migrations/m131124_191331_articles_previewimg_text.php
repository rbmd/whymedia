<?php

class m131124_191331_articles_previewimg_text extends CDbMigration
{
	public function up()
	{
		$this->addColumn('bg_articles_draft', 'preview_img_alt', 'TEXT NOT NULL DEFAULT "" AFTER preview_img');
		$this->addColumn('bg_articles', 'preview_img_alt', 'TEXT NOT NULL DEFAULT "" AFTER preview_img');		
	}

	public function down()
	{
		$this->dropColumn('bg_articles_draft', 'preview_img_alt');
		$this->dropColumn('bg_articles', 'preview_img_alt');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}