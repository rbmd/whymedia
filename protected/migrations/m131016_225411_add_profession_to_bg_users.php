<?php

class m131016_225411_add_profession_to_bg_users extends CDbMigration
{
	public function safeUp()
	{
		$this->addColumn('bg_users', 'profession', 'VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL');
	}

	public function safeDown()
	{
		$this->dropColumn('bg_users', 'profession');
	}
}