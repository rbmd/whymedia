<?php

class m131023_221854_create_bg_subscriptions extends CDbMigration
{
	public function safeUp()
	{
		$this->createTable(
			'bg_subscriptions',
			array(
				 'id'                 => 'INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT',
				 'watcher_id'         => 'INTEGER NOT NULL',
				 'author_id'          => 'INTEGER NOT NULL',
				 'watch_type'		  => 'INTEGER NOT NULL',
			)
		);

		$this->createIndex('watch_type', 'bg_subscriptions', 'watch_type');
		$this->createIndex('watcher_id', 'bg_subscriptions', 'watcher_id');
		$this->createIndex('author_id', 'bg_subscriptions', 'author_id');
		$this->createIndex('author_id,watcher_id,watch_type', 'bg_subscriptions', 'author_id,watcher_id,watch_type', true);
	}

	public function safeDown()
	{
		$this->dropTable('bg_subscriptions');
	}
}