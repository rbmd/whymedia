<?php

class m131015_130200_rating_table extends CDbMigration
{
	public function safeUp()
	{
		$this->createTable(
			'bg_articles_ratings',
			array(
				 'id'                => 'INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT',
				 'article_id'        => 'INTEGER NOT NULL',
				 'user_id'           => 'INTEGER NOT NULL',
				 'rating'            => 'INTEGER(2) NOT NULL',
			)
		);

		$this->createIndex('user_id', 'bg_articles_ratings', 'user_id');
		$this->createIndex('article_id', 'bg_articles_ratings', 'article_id');	

		$this->addColumn('bg_articles', 'rating', 'INTEGER NOT NULL DEFAULT 0 AFTER comments_count');	
	}

	public function safeDown()
	{
		$this->dropTable('bg_articles_ratings');
		$this->dropColumn('bg_articles', 'rating');
	}

}