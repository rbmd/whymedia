<?php

class m131114_214444_alter_bg_posts_add_comments_count extends CDbMigration
{
	public function safeUp()
	{
		$this->addColumn('bg_posts', 'comments_count', 'INTEGER NOT NULL DEFAULT 0');
		$this->createIndex('comments_count', 'bg_posts', 'comments_count');
	}

	public function safeDown()
	{
		$this->dropColumn('bg_posts', 'comments_count');
	}
}