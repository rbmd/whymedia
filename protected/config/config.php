<?php
require_once(__DIR__.'/../components/YiiElasticSearch-master/vendor/autoload.php');

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.


return array(
	'id'        => md5(php_uname().'asdgasdg(#)!fdb'),
	'basePath'          => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
	'name'              => 'RBK',
	'sourceLanguage'    => 'en_US',
	'language'          => 'ru',
	'charset'           => 'utf-8',
	'homeUrl'           => '/',
	'timeZone'          => "Europe/Moscow",
	'defaultController' => "Index",
	'layout'            => "index",

	// preloading 'log' component
	'preload'           => array(
		'log',
		//'less',
		),

	// autoloading model and component classes
	'import'            => array(
		'application.modules.admin.models.*',
		'application.models.*',
		'application.models.specprojects.*',
		'application.components.*',
		'application.widgets.elements.*',
		'application.widgets.elements.celements.*',
		'application.commands.*',
		'application.helpers.*',
		'application.modules.rights.*',
		'application.modules.rights.models.*',
		'application.modules.rights.components.*',
		'application.modules.rights.components.behaviors.*',
		'ext.nestedsetbehavior.NestedSetBehavior',
		'ext.imemcache.*',
		'ext.rbmcache.*',
		'ext.*',
		'ext.eoauth.*',
		'ext.eoauth.lib.*',
		'ext.lightopenid.*',
		'ext.eauth.*',
		'ext.eauth.services.*',
		),

	'modules'           => array(
		'admin',
		'rights' => array(
			#'superuserName'=> 'admin',
			'appLayout'     => 'application.modules.admin.views.layouts.index',
			'superuserName' => 'Admin',
			'debug'         => false
			)
		),

	// application components
	'components'        => array(



		'securityManager' => array(
			'validationKey' => '58e7f4a53ddb9b7435fa509d28be6162',
			),

		'elasticSearch' => array(
			'class' => 'YiiElasticSearch\Connection',
			'baseUrl' => 'http://localhost:9200/',
			),

		'cache'        => array(
				//'class'=>'system.caching.CMemCache',
			'class'   => 'application.extensions.imemcache.iMemCache',
			'servers' => array(
				array('host' => 'localhost', 'port' => 11211, 'weight' => 100),
				),
			),

		'user'         => array(
			'class'          => 'MyWebUser',
			'allowAutoLogin' => true,
			'loginUrl'       => '/login',
			),

		'request'      => array(
			'class'     => 'CHttpRequest',
			'scriptUrl' => '/',
			),

		'authManager'  => array(
			'class'        => 'RDbAuthManager',
				'defaultRoles' => array('Guest') // дефолтная роль
				),

		'facebook'     => array(
			'class'  => 'ext.yii-facebook-opengraph.SFacebook',
				'appId'  => '1241241', // needed for JS SDK, Social Plugins and PHP SDK
				'secret' => '', // needed for the PHP SDK
				'locale' => 'en_US', // override locale setting (defaults to en_US)
				//'jsSdk'=>true, // don't include JS SDK
				'async'  => true, // load JS SDK asynchronously
				//'jsCallback'=>false, // declare if you are going to be inserting any JS callbacks to the async JS SDK loader
				'status' => true, // JS SDK - check login status
				'cookie' => true, // JS SDK - enable cookies to allow the server to access the session
				//'oauth'=>true,  // JS SDK -enable OAuth 2.0
				//'xfbml'=>true,  // JS SDK - parse XFBML / html5 Social Plugins
				//'html5'=>true,  // use html5 Social Plugins instead of XFBML
				'ogTags' => array( // set default OG tags
					'title'       => 'Whymedia',
					'description' => '',
					'image'       => '',
					'type'        => 'article',
					),
				),

		'mail'         => array(
			'class' => 'application.components.Mail',
			'from'  => array('noreply@whymedia.ru' => 'whymedia')
			),

		'CURL'         => array(
			'class'   => 'ext.curl.Curl',
			'options' => array(
				'setOptions' => array(
					CURLOPT_SSL_VERIFYPEER => false,
					),
				),
			),

		'search'       => array(
			'class'             => 'application.extensions.DGSphinxSearch.DGSphinxSearch',
			'server'            => '127.0.0.1',
			'port'              => 9312,
			'maxQueryTime'      => 3000,
			'enableProfiling'   => 0,
			'enableResultTrace' => 0,
			),

					// Расширение требуется для eAuth с помощью которого будем авторизовать людей из соц сетей
		'loid'         => array(
			'class' => 'ext.lightopenid.loid',
			),

		'eauth'        => array(
			'class'    => 'ext.eauth.EAuth',
						// Использовать всплывающее окно вместо перенаправления на сайт провайдера.
			'popup'    => true,
						// Вы можете настроить список провайдеров и переопределить их классы
			'services' => array(
				'vkontakte' => array(
								//регистрация приложения: http://vkontakte.ru/editapp?act=create&site=1
					'class'         => 'VKontakteOAuthService',
					'client_id'     => '',
					'client_secret' => 'ryxb8HvurvXqMtsfqZo0',
					'title'         => 'Вконтакте',
					),

				'facebook'  => array(
								// регистрация приложения: https://developers.facebook.com/apps/
					'class'         => 'FacebookOAuthService',
					'client_id'     => '',
					'client_secret' => '183b1eac685a0779e2fca028c06646f2',
					),

				'twitter'   => array(
								// регистрация приложения: https://dev.twitter.com/apps/new
					'class'  => 'TwitterOAuthService',
					'key'    => '',
					'secret' => '8q4VuMmJaS8tsWU2qex9wzefiHK2bRso4Zh8zJBs',
					),

				'google'    => array(
								// регистрация приложения: https://code.google.com/apis/console/b/0/
					'class' => 'GoogleOpenIDService',
					'title' => 'GooglePlus',
					),
				),
			),

		'urlManager'   => array(
			'showScriptName' => false,
			'urlFormat'      => 'path',
			'rules'          => require('rules.php')
			),

		'db'           => array(
			'class'                 => 'CDbConnection',
			'connectionString'      => 'mysql:host=' . CONFIG_YII_DB_HOST . ';dbname=' . CONFIG_YII_DB_NAME,
			'emulatePrepare'        => false,
			'username'              => CONFIG_YII_DB_USER,
			'password'              => CONFIG_YII_DB_PASSWORD,
			'charset'               => 'utf8',
			'autoConnect'           => false,
			'schemaCachingDuration' => 0,

						// отключаем профайлер на продакшне
			'enableProfiling'       => false,
						// показываем значения параметров
			'enableParamLogging'    => true,
			),

		'imagemod'     => array(
			'class' => 'application.extensions.imagemodifier.CImageModifier',
			),

		'errorHandler' => array(
			'errorAction' => 'site/error',
			),

		'log'          => array(
			'class'  => 'CLogRouter',
			'routes' => array(
					/*array(
						'class'       => 'RainLogger',
						'levels'      => 'error',
						'logPath'     => $_SERVER['DOCUMENT_ROOT'] . '/protected/runtime',
								'maxFileSize' => 1000, #kbytes
								'maxLogFiles' => 100,
								),*/
			/*array(
				'class'         => 'CWebLogRoute',
								//'showInFireBug' => true,
								//'levels'        => 'error',
								//'enabled' => true,
					),*/
							/*array(
								'class'     => 'ext.yii-debug-toolbar.YiiDebugToolbarRoute',
								'ipFilters' => array('*',),
								),*/
							/*array(
								'class'  => 'CProfileLogRoute',
								'levels' => 'profile',
								),*/

							// Собираем и сохраняем статистику по времени выполнения контроллеров и виджетов
				/*array(
					'class'              => 'BgDBLogRoute',
					'levels'             => 'profile',
					'categories'         => 'bg.profiling.*',
					'autoCreateLogTable' => true,
					'logTableName'       => 'log',
					'connectionID'       => 'db',
					),*/

							// Сохраняем статистику по SQL запросам если включено профилирование
				/*array(
					'class'        => 'BgProfileLogRoute',
					'logTableName' => 'log',
					'levels'       => 'profile',
					'except'       => 'bg.profiling.*'
					),*/

				),
			),
	),

	// using Yii::app()->params['paramName']
	'params'            => array(
				'use_pre_auth'       => false, //использовать нашу прмежуточную авторизацию вместо http-авторизации
				'pre_auth_ignore_ip' => array('83.69.223.155'),
				// this is used in contact page
				'keepLoginTime'      => 3600,
				'adminEmail'         => 'registiy@gmail.com',
				'baseUrl'            => 'http://whymedia.ru',
				'workflow_rules'     => array(
					3 => 'canSetStatus_Draft',
					2 => 'canSetStatus_Ready',
					1 => 'canSetStatus_Published',
					),
				'seo'                => array(
					'title'       => 'WhyMedia',
					'description' => '',
					'keywords'    => '',
					),

				'instagram' => array(
					'user_id' => '',
					'access_token' => '',
					),
				),
);
