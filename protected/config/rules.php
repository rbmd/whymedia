<?php
//exit;
return array(

        '' => 'tree/index',
        '<section:(showreel|works|contacts)>' => 'tree/index',
        '<lang:(en|ge)>/<section:(showreel|works|contacts)>' => 'tree/index',
        #'articles/<id:[0-9]+>' => 'tree/work',
        #'articles/<lang:(en|ge|ru)>/<id:[0-9]+>' => 'tree/work',

        '<lang:(en|ge)>' => 'tree/index',
        'works/<code:[A-z0-9_-]+>' => 'tree/works',
        '<lang:(en|ge|ru)>/works/<code:[A-z0-9_-]+>' => 'tree/works',

        //Авторизация, профайл, восстановление пароля
        'reactivate' => 'site/reactivate',
        'activate/<code:[0-9cdefABCDEF]{32}>' => 'site/activate',
        'registration' => 'site/registration',
        'registration/confirm' => 'site/confirmRegistration',
        'recovery' => 'site/recovery',
        'recovery/code/<code:[0-9abcdefABCDEF]{32}>' => 'site/recovery',
        'recovery/confirmation' => 'site/recovery/success/1',
        'recovery/success' => 'site/recovery/success/2',
        'login' => 'site/login',
        'logout' => 'site/logout',
        'site/authexternals/<service:([A-z0-9_-]+)>' => 'site/authexternals',
        'site/authexternals/<service:([A-z0-9_-]+)>/<identity:([A-z0-9_\.:\/-]+)>' => 'site/authexternals',

       
        //Админка
        'admin/' => 'admin/default',
        'admin/rights' => 'rights/assignment/view',
        'admin/profiler/allrequests'=>'admin/profiler/allrequests',
        'admin/profiler/allwidgets'=>'admin/profiler/allwidgets',
        'admin/rights/<controller:\w+>/<action:\w+>' => 'rights/<controller>/<action>',




        //Предпросмотр статей
        '<preview:(preview)>/<id:[0-9]+>' => 'articles/detail',

        '<controller:\w+>/<action:\w+>' => '<controller>/<action>',

        //Все остальные страницы кидаем на статику
        #'(?!admin|blogs|atlas|site|profile|specprojects|specials|comments|test|api|ajax|elements)([\w-_/]+)' => 'site/static',


        //Стандартные правила роутинга, которые должны отрабатывать по умолчанию
        /*'<controller:\w+>' => '<controller>/index',
        '<controller:\w+>/<id:\d+>'=>'<controller>/view',
        '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
        '<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',*/
);