<?php
$config = require 'config.php';


$config_ext = array(
	'components' => array(
		'errorHandler' => array(
			'errorAction' => null,
		),
		
	),
	'params'            => array(
		'use_pre_auth'       => false,
	)
);


return CMap::mergeArray( $config, $config_ext );
