<?php
/**
 * Класс для работы с изображениями.
 *
 * img = new Image('pathtoimagefile.jpg');
 * img->resize(width, height, [crop], [rise]);
 * img->putImg(img, pos, x, y); //
 * img->putText(text, [pos], [x], [y], [color], [size], [font], [angle], [shadow]);
 * img->save(name, [format], [qua]);
 * img->prn([format], [qua]);
 * img->saveOriginal(name);
 *
 * @package Utils
 * @author a.parkhomenko <771067@gmail.com>
 * @version v0.2
 * @since 20.11.2010
 * @copyright (c) Siberian Framework
 */

class Images
{
	public
		$format, // Формат загруженного изображения
		$file; // Путь к файлу с исходным изображением
	protected
		$dst; // Изображение в процессе обработки


	/**
	 * Загрузка исходного изображения
	 *
	 * @param string $file Путь к исходному изображению
	 * @return void
	 */
	public function __construct($file = false)
	{
		if ($file)
		{
			$this->loadImage($file);
		}
	}


	/**
	 * Проверяет существование и формат файла с изображением и загружает его в память.
	 *
	 * @param string $file Имя изображения (файл)
	 */
	private function loadImage($file)
	{
		$this->file = $file; // Имя исходного файла

		if (!is_file($file)) // Проверка существования файла
		{
			//copy($file, '/tmp')
			//throw new Exception('Не найден файл: ' . $file);
		}

		$imginfo = getImageSize($file); // Информация об исходном изображении

		if (intval($imginfo[2]) == 0) // Если файл не является изображением
		{
			throw new Exception('Файл не является изображением');
		}

		if ($imginfo[2] == 1)
		{
			$this->dst    = @imageCreateFromGif($file);
			$this->format = 'gif';
		}
		elseif ($imginfo[2] == 2)
		{
			$this->dst    = @imageCreateFromJpeg($file);
			$this->format = 'jpeg';
		}
		elseif ($imginfo[2] == 3)
		{
			$this->dst = @imageCreateFromPng($file);
			imagesavealpha($this->dst, true);
			$this->format = 'png';
		}
		else
		{
			throw new Exception('Неверный формат изображения (' . $imginfo['mime'] . '). Возможные форматы: jpeg, gif, png.');
		}

		if (is_null($this->dst))
		{
			throw new Exception('Не найден файл: ' . $file);
		}
	}


	public function w()
	{
		return imageSX($this->dst);
	}


	public function h()
	{
		return imageSY($this->dst);
	}


	/**
	 * Изменение размера изображения. Подготовка
	 * Проверка данных об изменении размера изображения. Запуск метода реализующего изменение размера изображения
	 *
	 * @param int $w Ширина изображения
	 * @param int $h Высота изображения
	 * @param bool $crop Кроп. true - обрезать изображение, false - вписать изображение в размеры $w, $h
	 * @param string $bgcolor Цвет фона, в случае, если изображение вписывается внутрь палитры
	 * @param bool $rise Возможность увеличивать размеры исходного изображения
	 * @return bool
	 */
	public function resize($w, $h, $crop = false, $bgcolor = false, $rise = false)
	{
		if ($this->dst == false)
		{
			throw new Exception('Не загружено исходное изображение');
		}
		if (!is_int($w))
		{
			throw new Exception('Ширина изображения должна иметь целочисленный тип');
		}
		if (!is_int($h))
		{
			throw new Exception('Высота изображения должна иметь целочисленный тип');
		}
		if ($w < 0)
		{
			throw new Exception('Ширина изображения не может иметь отрицательное значение');
		}
		if ($h < 0)
		{
			throw new Exception('Высота изображения не может иметь отрицательное значение');
		}
		if (!is_bool($crop))
		{
			throw new Exception('Параметр Crop должен иметь логический тип (true/false)');
		}
		if (!is_bool($rise))
		{
			throw new Exception('Параметр Rise должен иметь логический тип (true/false)');
		}

		if ($this->dst = $this->resizeExec($w, $h, $crop, $bgcolor, $rise))
		{
			return true;
		}
	}


	/**
	 * Изменение размера изображения.
	 *
	 * Действия по изменению размера изображения
	 * Возвращает измененное изображение
	 *
	 * @param int $w Ширина изображения
	 * @param int $h Высота изображения
	 * @param bool $crop Кроп. true - обрезать изображение, false - вписать изображение в холст
	 * @return resource
	 */
	private function resizeExec($dstW, $dstH, $crop, $bgcolor, $rise)
	{
		$srcW = imageSX($this->dst); // Ширина исходного изображения
		$srcH = imageSY($this->dst); // Высота исходного изображения

		// Если ширина и высота имеют нулевое значение одновременно, возвращаем изображение без изменений
		if ($dstW == 0 && $dstH == 0)
		{
			return $this->dst;
		}

		/*
		 * Если не разрешено изменять размер изображения в большую сторону от исходного
		 * Приравниваем новые значения исходным
		 * По-умолчанию не разрешено.
		 */
		if ($rise == false && ($dstW != $dstH || $crop))
		{
			if ($srcW < $dstW)
			{
				$dstW = $srcW;
			}
			if ($srcH < $dstH)
			{
				$dstH = $srcH;
			}
		}

		$dstX = $dstY = $srcX = $srcY = 0; // Начальные размеры сдвигов изображений

		/*
		 * В случае, если ширина и высота изображения равны, и не задан цвет фона
		 * это число становится максимальным размером изображения по обоим измерениям.
		 */
		if ($dstW == $dstH && $bgcolor == false)
		{

			if ($srcW > $srcH)
			{
				if ($dstW > $srcW && $rise == false)
				{
					$dstW = $srcW;
				}
				$dstH = round($dstW * $srcH / $srcW);

			}
			else
			{
				if ($dstW > $srcH && $rise == false)
				{
					$dstH = $srcH;
				}
				$dstW = round($dstH * $srcW / $srcH);
			}
		}

		/*
		 * Если один из размеров изображения имеет нулевое значение:
		 * 1 отменяем разрешение обрезать изображение;
		 * 2 определяем его соответствующим размером исходного изображения (пропорционально).
		 */
		if ($dstW == 0 || $dstH == 0)
		{
			$crop = false;
			if ($dstW == 0)
			{
				$dstW = round($dstH * $srcW / $srcH);
			}
			if ($dstH == 0)
			{
				$dstH = round($dstW * $srcH / $srcW);
			}
		}

		$src = imageCreateTrueColor($dstW, $dstH); // Создаем холст для нового изображения

		if ($bgcolor) // Если задан цвет фона
		{
			$color = $this->getColor($bgcolor);
			imageFilledRectangle($src, 0, 0, $dstW, $dstH, imageColorAllocate($src, $color[0], $color[1], $color[2]));
		}

		if ($dstW / $dstH < $srcW / $srcH) // Если соотношения сторон исходного изображения меньше итогового
		{
			if ($crop)
			{
				$srcW = round($srcH * $dstW / $dstH);
				$srcX = (imageSX($this->dst) - $srcW) / 2;
			}

			else
			{
				$z    = $dstH;
				$dstH = round($dstW * $srcH / $srcW);
				$dstY = ($z - $dstH) / 2;
			}
		}

		else // Если соотношения сторон исходного изображения больше или равно итоговому
		{
			if ($crop)
			{
				$srcH = round($srcW / $dstW * $dstH);
				$srcY = (imageSY($this->dst) - $srcH) / 2;
			}

			else
			{
				$z    = $dstW;
				$dstW = round($dstH * $srcW / $srcH);
				$dstX = ($z - $dstW) / 2;
			}
		}

		// Копируем в холст исходное изображение с новыми размерами
		imageCopyResampled($src, $this->dst, $dstX, $dstY, $srcX, $srcY, $dstW, $dstH, $srcW, $srcH);
		return $src;
	}


	/**
	 * Возвращает ширину изображения
	 *
	 * @return int
	 */
	public function width()
	{
		return imageSX($this->dst);
	}


	/**
	 * Возвращает высоту изображения
	 *
	 * @return int
	 */
	public function height()
	{
		return imageSY($this->dst);
	}


	/**
	 * Сохранение оригинального изображения (включая EXIF)
	 *
	 * @param string $file Файл, для сохранения изображения
	 * @return bool
	 */
	public function saveOriginal($file)
	{
		if ($this->dst == false)
		{
			throw new Exception('Не загружено исходное изображение');
		}

		$pathA = pathinfo($file);
		// Если нет папки, пытаемся создать
		if (!is_dir($pathA['dirname']))
		{
			mkdir($pathA['dirname'], 0775, true);
			chmod($pathA['dirname'], 0775);
		}

		// Удаляем из файла расширение. Если есть
		$file = preg_replace('#\.(gif|png|jpg|jpeg)$#i', '', $file);

		// Получаем формат файла
		$format = ($this->format == 'jpeg') ? 'jpg' : $this->format;
		$file .= '.' . $format;

		// Если есть права на запись
		if ($this->writeableFile($file))
		{
			return copy($this->file, $file);
		}
		else
		{
			throw new Exception('Нет прав на запись для файла: ' . getcwd() . '/' . $file);
		}
	}


	/**
	 * Вывод изображения
	 *
	 * @param string $format Формат выходного файла
	 * @param ing $qua Качество изображения (для JPEG)
	 * @return bool
	 */
	public function prn($format = true, $qua = 80)
	{
		if ($this->dst == false)
		{
			throw new Exception('Не загружено исходное изображение');
		}
		return $this->printExec(null, $format, $qua);
	}


	/**
	 * Сохранение изображения
	 *
	 * @param string $file Имя выходного файла
	 * @param string $format Формат выходного файла
	 * @param ing $qua Качество изображения (для JPEG)
	 * @return bool
	 */
	public function save($file = '', $format = true, $qua = 80)
	{
		if ((!is_int($file) && !is_string($file)) || $file == '')
		{
			throw new Exception('Некорректно задано или не задано имя файла');
		}
		return $this->printExec($file, $format, $qua);
	}


	/**
	 * Выполнение вывода или сохранения изображения
	 *
	 * @param string $file Имя выходного файла
	 * @param string $format Формат выходного файла
	 * @param ing $qua Качество изображения (для JPEG)
	 * @return bool
	 */
	private function printExec($file, $format, $qua)
	{
		if (is_bool($format) || is_null($format))
		{
			$format = $this->format;
		}
		$format = strtolower($format);

		// Формат выходного файла
		if ($format != 'jpeg' && $format != 'jpg' && $format != 'gif' && $format != 'png')
		{
			throw new Exception('Неправильный формат выходного изображения. Возможные значения: jpeg, gif, png.');
		}

		// Если вывод файла
		if ($file == null)
		{
			header('Content-type: image/' . $format);
		}

		else // Если сохранение файла
		{
			$file = preg_replace('#\.(gif|png|jpg|jpeg)$#i', '', $file) . '.' . $format;

			$pathA = pathinfo($file);
			// Если нет папки, пытаемся создать
			if (!is_dir($pathA['dirname']))
			{
				mkdir($pathA['dirname'], 0775, true);
				chmod($pathA['dirname'], 0775);
			}

			if ($this->writeableFile($file) == false)
			{
				throw new Exception('Нет прав на запись для файла: ' . getcwd() . '/' . $file);
			}
		}

		$q = ($qua > 0 && $qua <= 100) ? $qua : true;

		switch ($format) // Определение типа изображения
		{
			case 'gif' :
				return imageGif($this->dst, $file); // Тип изображения Gif
			case 'png' :
				return imagePng($this->dst, $file); // Тип изображения Png
			case 'jpeg':
			case 'jpg':
				return imageJpeg($this->dst, $file, $q); // Тип изображения Jpeg
		}
	}


	/**
	 * Наложение дополнительного изображения на исходное
	 *
	 * Смещение изображения происходит с помощью параметров $x и $y
	 * Задаются в пикселях или процентах (%)
	 *
	 * @param string $file Имя накладываемого изображения
	 * @param ing $pos Позиция изображения (начиная с 1 (правый верхний угол) против часовой стрелки)
	 * @param string $x Смещение накладываемого изображения по горизонтали
	 * @param string $y Смещение накладываемого изображения по вертикали
	 * @return bool
	 */
	public function putImg($file, $pos = 2, $x = 0, $y = 0)
	{
		// Загрузка нового изображения
		$src = $this->loadImage($file);

		// Размеры нового изображения
		$w = imageSX($src[0]);
		$h = imageSY($src[0]);

		// Получить координаты исходного изображения, от которого будет накладываться изображение
		$p = $this->getPosition($pos, $x, $y, $w, $h);

		// Ноложение нового изображения на исходное
		return imageCopyResampled($this->dst, $src[0], $p[0], $p[1], 0, 0, $w, $h, $w, $h) ? true : false;
	}


	public function createTextImg($text, $color = '#ff0')
	{
		if (!is_string($text))
		{
			throw new Exception('Текст для изображения должен иметь строковый тип');
		}
		if (!isset($this->font['file']))
		{
			throw new Exception('Не загруженны данные о шрифте (метод loadFont)');
		}

		$font  = $this->font['file'];
		$size  = $this->font['size'];
		$angle = $this->font['angle'];

		// Размер и положение текста
		$sz = $this->imageTtfSize($size, $angle, $font, $text);

		//print_r($sz);

		$sz[0] += 20;
		$sz[1] += 8;

		$this->dst = imageCreateTrueColor($sz[0] + 100, $sz[1] + 100);

		if ($color == false)
		{
			$col = imageColorAllocate($this->dst, 255, 255, 255);
			imageFilledRectangle($this->dst, 0, 0, $sz[0], $sz[1], $col);
			imageColorTransparent($this->dst, $col);
		}

		else
		{
			$col = $this->getColor($color);
			$col = imageColorAllocate($this->dst, $col[0], $col[1], $col[2]);
			imageFilledRectangle($this->dst, 0, 0, $sz[0], $sz[1], $col);
		}


		$this->putText($text, 2);
	}


	/**
	 * Загрузка данных о шрифте
	 *
	 * @param string $file Путь к файлу со шрифтом
	 * @param string $color Цвет текста
	 * @param int $size Размер фрифта
	 * @param int $angle Угол наклона текста
	 * @param int $alpha Степень прозраности текста (от 0 до 127)
	 * @param string $shadow Цвет тени от текста
	 * @return none
	 *
	 */
	public function loadFont($file, $color = '#000', $size = false, $angle = 0, $alpha = 0, $shadow = false)
	{
		// Проверяем шрифт на работоспособность
		if (!is_string($file))
		{
			throw new Exception('Путь к файлу со шрифтом имеет не строковый тип');
		}
		elseif (!is_file($file))
		{
			throw new Exception($file . ' не является файлом');
		}
		elseif (!@imageTtfBBox($size, 0, $file, 'text'))
		{
			throw new Exception('Файл: ' . $file . ' не является допустимым изображением');
		}

		$this->font['file']   = $file;
		$this->font['color']  = is_string($color) ? $color : '#000';
		$this->font['size']   = is_int($size) ? $size : false;
		$this->font['alpha']  = is_int($alpha) ? $alpha : 0;
		$this->font['angle']  = is_int($angle) ? $angle : 0;
		$this->font['shadow'] = (!is_bool($shadow) && !is_string($shadow)) ? $shadow : false;
	}


	/**
	 * Наложение такста на исходное изображение
	 *
	 * @param string $text Текст
	 * @param ing $pos Позиция элемента
	 * @param string $x Смещение накладываемого текста по горизонтал
	 * @param string $y Смещение накладываемого текста по вертикали
	 * @param string $color Цвет текста
	 * @param int $size Размер фрифта
	 * @param int $angle Угол наклона текста
	 * @param int $alpha Степень прозраности текста (от 0 до 127)
	 * @param string $shadow Цвет тени от текста
	 * @param string $font Путь к файлу со шрифтом
	 * @return bool
	 */
	public function putText($text = 'Image', $pos = 0, $x = 0, $y = 0, $color = '0', $size = 10, $angle = 0, $alpha = 0, $shadow = false, $font = 'arial.ttf')
	{
		// Проверяем шрифт на работоспособность
		if (!@imageTtfBBox($size, 0, $font, $text))
		{
			return false;
		}

		if (!is_int($alpha))
		{
			$alpha = 0;
		}
		if (!is_int($angle))
		{
			$angle = 0;
		}

		// Если размер шрифта задан не целым числом, используем максимально возможный размер
		if (!is_int($size) || $size == false)
		{
			$size = $this->imageTtfGetMaxSize($angle, $font, $text, imageSX($this->dst), imageSY($this->dst));
		}

		// Получаем список цветов, разбитых на каналы [RGB]
		$clr = $this->getColor($color);

		// Цвет текста
		$color = imageColorAllocateAlpha($this->dst, $clr[0], $clr[1], $clr[2], $alpha);

		// Размер и положение текста
		$sz = $this->imageTtfSize($size, $angle, $font, $text);

		// Координаты текста
		$coord = $this->getPosition($pos, $x, $y, $sz[0], $sz[1]);
		$coord[0] += $sz[2];
		$coord[1] += $sz[3];

		// Если задана тень
		if ($shadow)
		{
			if ($shadow === true)
			{
				$shadow = '#000';
			}
			$sh     = $this->getColor($shadow);
			$sm     = ceil($size / 20); // Сдвиг тени относительно текста
			$shadow = imageColorAllocateAlpha($this->dst, $sh[0], $sh[1], $sh[2], $alpha);
			imageTtfText($this->dst, $size, $angle, $coord[0] + $sm, $coord[1] + $sm, $shadow, $font, $text);
		}
		return imageTtfText($this->dst, $size, $angle, $coord[0], $coord[1], $color, $font, $text) ? true : false;
	}


	/**
	 * Получить координаты накладываемого элемента
	 *
	 * @param ing $pos Позиция элемента
	 * @param string $x Смещение накладываемого элемента по горизонтал
	 * @param string $y Смещение накладываемого элемента по вертикали
	 * @param string $w Ширина накладываемого элемента
	 * @param string $h Высота накладываемого элемента
	 * @return list
	 */
	private function getPosition($pos, $x, $y, $w, $h)
	{
		$x1 = imageSX($this->dst); // Ширина исходного изображения
		$y1 = imageSY($this->dst); // Высота исходного изображения

		// Определение параметров смещения
		if (substr($x, -1) == '%')
		{
			$x = $x1 * intval($x) / 100;
		}
		if (substr($y, -1) == '%')
		{
			$y = $y1 * intval($y) / 100;
		}
		$x = intval($x);
		$y = intval($y);

		switch ($pos)
		{
			case 1  :
				$x += $x1 - $w;
				break;
			case 11 :
				$x += $x1 / 2 - $w / 2;
				break;

			case 2  :
				break;
			case 21 :
				$y += $y1 / 2 - $h / 2;
				break;

			case 3  :
				$y += $y1 - $h;
				break;
			case 31 :
				$x += $x1 / 2 - $w / 2;
				$y += $y1 - $h;
				break;

			case 4  :
				$x += $x1 - $w;
				$y += $y1 - $h;
				break;
			case 41 :
				$x += $x1 - $w;
				$y += $y1 / 2 - $h / 2;
				break;

			case 0  :
				$x += $x1 / 2 - $w / 2;
				$y += $y1 / 2 - $h / 2;
				break;
		}

		return array(round($x), round($y));
	}


	/**
	 * Возвращает список цветов в десятичном виде [RGB]
	 *
	 * @param string $color Цвет в 16-ричном формате, либо HTML-код
	 * @return list
	 */
	private function getColor($color = '#fff')
	{
		if (!is_string($color))
		{
			$color = '#fff';
		}
		$color = str_replace('#', '', $color);

		// Если код цвета имеет 3 символа (короткая запись)
		if (strlen($color) == 3)
		{
			for ($i = 0; $i < 3; $i++)
				$c[] = hexdec(str_repeat(substr($color, ($i), 1), 2));
		}
		else
		{
			for ($i = 0; $i < 3; $i++)
				$c[] = hexdec(substr($color, ($i * 2), 2));
		}

		return $c;
	}


	/**
	 * Проверка файла, на возможность записи в него.
	 *
	 * @param string $file Файл, для записи
	 * @return bool
	 */
	private function writeableFile($file)
	{
		if (is_file($file) && is_writeable($file) == false)
		{
			return false;
		}
		else
		{
			return true;
		}
	}


	/**
	 * Вычисляет размеры прямоугольника с горизонтальными и вертикальными сторонами,
	 * в который вписан указанный текст.
	 * Результирующий массив имеет структуру:
	 * array(
	 *   0  => ширина прямоугольника,
	 *   1  => высота прямоугольника,
	 *   2  => смещение начальной точки по X относительно левого верхнего угла прямоугольника,
	 *   3  => смещение начальной точки по Y
	 * )
	 *
	 * @param int $size Размер шрифта
	 * @param int $angle Угол наклона текста
	 * @param int $font Путь к файлу со шрифтом
	 * @param string $text Текст
	 * @return list
	 */
	private function imageTtfSize($size, $angle, $font, $text)
	{
		// Вычисляем охватывающий многоугольник
		// Вычисляем размер при НУЛЕВОМ угле поворота
		$horiz = imageTtfBBox($size, 0, $font, $text);

		// Вычисляим синус и косинус угла поворота
		$cos = cos(deg2rad($angle));
		$sin = sin(deg2rad($angle));
		$box = array();

		// Выполняем поворот каждой координаты
		for ($i = 0; $i < 7; $i += 2)
		{
			list ($x, $y) = array($horiz[$i], $horiz[$i + 1]);
			$box[$i]     = round($x * $cos + $y * $sin);
			$box[$i + 1] = round($y * $cos - $x * $sin);
		}

		$x = array($box[0], $box[2], $box[4], $box[6]);
		$y = array($box[1], $box[3], $box[5], $box[7]);

		// Вычисляем ширину, высоту и смещение начальной точки
		$width  = max($x) - min($x);
		$height = max($y) - min($y);
		return array($width, $height, 0 - min($x), 0 - min($y));
	}


	/**
	 * Метод возвращает наибольший размер шрифта, учитывая,
	 * что текст $text обязательно должен поместиться в исходное изображение
	 *
	 * @param int $angle Угол наклона текста
	 * @param int $font Путь к файлу со шрифтом
	 * @param string $text Текст
	 * @param int $width Ширина исходного изображения
	 * @param int $height Высота исходного изображения
	 * @return int
	 */
	public function imageTtfGetMaxSize($angle, $font, $text, $width, $height)
	{
		$min = 1;
		$max = $height;
		while (true)
		{
			// Рабочий размер - среднее между максимумом и минимумом.
			$size = round(($max + $min) / 2);
			$sz   = $this->imageTtfSize($size, $angle, $font, $text);
			if ($sz[0] > $width || $sz[1] > $height)
			{
				// Будем уменьшать максимальную ширину до те пор, пока текст не
				// "перехлестнет" многоугольник.
				$max = $size;
			}
			else
			{
				// Наоборот, будем увеличивать минимальную, пока текст помещается.
				$min = $size;
			}
			// Минимум и максимум сошлись друг к другу.
			if (abs($max - $min) < 2)
			{
				break;
			}
		}
		return $min;
	}


	/**
	 * Очистка до исходного изображения
	 *
	 * Повторно загружает исходное изображение в память
	 * Отменяет все действия по изменению исходного изображения:
	 * размеры, наложение другого изображения, текста и т.п.
	 * Необходимо при сохранении нескольких изображений из исходного
	 * @param void
	 * @return void
	 */
	public function clear()
	{
		$this->loadImage($this->file); // Загрузка изображения в память
	}


	/**
	 * Освобождает память от используемых ресурсов
	 *
	 * @param void
	 * @return void
	 *
	 */
	public function __destruct()
	{
		if (is_resource($this->dst))
		{
			imageDestroy($this->dst);
		}
	}


	/**
	 * "Скруглить" картинки
	 *
	 * @param $radius — радиус скругления в точках
	 */
	public function roundedCorners($radius = 0, $color = false)
	{
		/**
		 * Чем выше rate, тем лучше качество сглаживания и больше время обработки и
		 * потребление памяти.
		 *
		 * Оптимальный rate подбирается в зависимости от радиуса.
		 */
		$rate = 4;

		imagealphablending($this->dst, false);
		imagesavealpha($this->dst, true);

		$width  = imagesx($this->dst);
		$height = imagesy($this->dst);

		if ($radius == 0)
		{
			$radius = min($width, $height);
		}

		$rs_radius = $radius * $rate;
		$rs_size   = $rs_radius * 2;

		$corner = imagecreatetruecolor($rs_size, $rs_size);
		imagealphablending($corner, false);

		if ($color == false)
		{
			$trans = imagecolorallocatealpha($corner, 255, 255, 255, 127);
		}
		else
		{
			$aColor = $this->getColor($color);
			$trans = imagecolorallocatealpha($corner, $aColor[0], $aColor[1], $aColor[2], 0);
		}


		imagefill($corner, 0, 0, $trans);

		$positions = array(
			array(0, 0, 0, 0),
			array($rs_radius, 0, $width - $radius, 0),
			array($rs_radius, $rs_radius, $width - $radius, $height - $radius),
			array(0, $rs_radius, 0, $height - $radius),
		);

		foreach ($positions as $pos)
		{
			imagecopyresampled($corner, $this->dst, $pos[0], $pos[1], $pos[2], $pos[3], $rs_radius, $rs_radius, $radius, $radius);
		}

		$i   = -$rs_radius;
		$y2  = -$i;
		$r_2 = $rs_radius * $rs_radius;

		for (; $i <= $y2; $i++)
		{
			$y = $i;
			$x = sqrt($r_2 - $y * $y);
			$y += $rs_radius;
			$x += $rs_radius;

			imageline($corner, $x, $y, $rs_size, $y, $trans);
			imageline($corner, 0, $y, $rs_size - $x, $y, $trans);
		}
		foreach ($positions as $pos)
		{
			imagecopyresampled($this->dst, $corner, $pos[2], $pos[3], $pos[0], $pos[1], $radius, $radius, $rs_radius, $rs_radius);
		}
	}
}