<?php
/**
 * User: Alexander Parkhomenko <771067@gmail.com>
 * Date: 28.06.13
 * Time: 13:01
 */
class Mail
{
	public $from = 'noreply@kavpolit.com';

	//NE UDALAT!
	public function init(){}

	public function __construct($from = null)
	{
		if (isset($from))
			$this->from = $from;

		Yii::import('ext.swiftMailer.SwiftMailer');
	}

	public function send($to, $subj, $text)
	{
		if (is_array($to))
		{
			if (isset($to[0]))
			{
				if (isset($to[1]))
				{
					$oTo = array($to[0] => $to[1]);
				}
				else
				{
					$oTo = array($to[0]);
				}
			}
		}
		elseif (is_string($to))
		{
			$oTo = array($to);
		}

		$sm = new SwiftMailer();
		$sm->init();
		$transport = $sm->sendmailTransport("/usr/sbin/sendmail -t");
		$mailer    = $sm->mailer($transport);
		$message   = $sm
			->newMessage($subj)
			->setFrom($this->from)
			->setTo($oTo)
			->addPart($text, 'text/html');
		return $mailer->send($message);
	}
}