<?php
/**
 * User: Русаков Дмитрий
 * Date: 23.04.12
 * Time: 11:55
 *
 * Здесь централизованно храним все ключи, которые используем для кеширования в системе
 */
class keysSet
{
	const ARTICLES_LIST = 'ARTICLES_LIST'; /*Ключ для кеша Articles::model()->getArticlesList()*/
	const ARTICLES_DETAIL = 'ARTICLES_DETAIL'; /*Ключ для кеша детальной страницы статьи*/

	const COMMENTS_LIST = 'COMMENTS_LIST'; /*Ключ для кеша Comments::model()->getList()*/

	const AUTHORS_LIST = 'AUTHORS_LIST'; /*Ключ для кеша /authors/index/ */
	const AUTHORS_DETAIL = 'AUTHORS_DETAIL'; /*Ключ для кеша /authors/detail/ */

	const GALLERY_PLACE = 'GALLERY_PLACE'; /*Ключ для кеша /gallery/place/ */

	const TAGS_DETAIL = 'TAGS_DETAIL'; /*Ключ для кеша детальной страницы статьи*/

	const CACHE_KEY_TREE_NODE = 'CACHE_KEY_TREE_NODE'; /*Ключ для кеша статичных страниц (sitecontroller)*/

	const WIDGET_MAIN_ANNOUNCE = 'WIDGET_MAIN_ANNOUNCE'; /*Ключ для кеша MainannounceWidget*/
	const WIDGET_BEST_ARTICLES = 'WIDGET_BEST_ARTICLES'; /*Ключ для кеша BestArticlesWidget*/
	const WIDGET_NEWSLIST = 'WIDGET_NEWSLIST'; /*Ключ для кеша NewslistWidget*/
	const WIDGET_GUIDELIST = 'WIDGET_GUIDELIST'; /*Ключ для кеша GuidelistWidget*/
	const WIDGET_READALSO = 'WIDGET_READALSO'; /*Ключ для кеша ReadalsoWidget*/
	const WIDGET_VIDEOLIST = 'WIDGET_VIDEOLIST'; /*Ключ для кеша VideolistWidget*/
	const WIDGET_LASTSPECPROJECT = 'WIDGET_LASTSPECPROJECT'; /*Ключ для кеша Lastspecproject*/
	const WIDGET_RANDOMSPECPROJECTS = 'WIDGET_RANDOMSPECPROJECTS'; /*Ключ для кеша RandomspecprojectsWidget*/
	const WIDGET_AFISHA = 'WIDGET_AFISHA'; /*Ключ для кеша AfishaWidget*/
	const WIDGET_LASTMAGAZINE = 'WIDGET_LASTMAGAZINE'; /*Ключ для кеша LastmagazineWidget*/


	const WIDGET_BANNER = 'WIDGET_BANNER'; /*Ключ для кеша BannerWidget*/


	const CACHE_KEY_DO_PROFILE = 'CACHE_KEY_DO_PROFILE'; /*По ключу лежит флаг, включить режим профилирования или нет*/
	const CACHE_KEY_PROFILER_INTERVAL = 'CACHE_KEY_PROFILER_INTERVAL'; /*По ключу лежит интервал профайлера*/

}