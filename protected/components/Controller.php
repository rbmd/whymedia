<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
	/**
	 * @var null|string - Уникальный код запроса (для логирования)
	 */
	private static $_requestCode = null;

	public $layouts_map = array();

	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout = '//layouts/column1';
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu = array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs = array();
	public $rss_feeds = array(array('title' => 'Все материалы', 'code' => 'news'));

	public $pageKeywords = '';
	public $pageDescription = '';
	public $include_text = '';

	/**
	 * Текущий раздел сайта(для подсветки меню)
	 *
	 */
	public $section;

	/**
	 * @var bool - Здесь кешируем флаг включён или выключен профайл
	 *             (чтобы в рамках одного запроса каждый раз не лазить за этим флагом в мемкешед).
	 */
	private static $_do_profile = null;

	//Параметры кеширования страницы
	private $page_cache_params = array();


	public function __construct($id, $module = null)
	{
		

		/**
		 * Генерация уникального кода для запроса
		 */
		self::$_requestCode = User::getUniqueValue();

		parent::__construct($id, $module);

		$this->setPageTitle(Yii::app()->params['seo']['title']);
		$this->pageKeywords    = Yii::app()->params['seo']['keywords'];
		$this->pageDescription = Yii::app()->params['seo']['description'];
	}

	/**
	 * @return null|string - получить уникальный код текущего запроса
	 */
	public static function getRequestCode()
	{
		/**
		 * Если по каким-то причинам $_request_code не сгенерирован, например,
		 * если некий контроллер в приложении не наследует от Controller, то
		 * сгенерируем.
		 */
		if (empty(self::$_requestCode))
		{
			self::$_requestCode = User::getUniqueValue();
		}

		return self::$_requestCode;
	}

	public function filters()
	{
		$return = null;


		/*Кеширование страниц*/
		//Получаем контроллер и экшн для текущего запроса
		$c_a = Yii::app()->getUrlManager()->parseUrl(Yii::app()->request);

		//Проверяем, есть ли текущий экшн в настройках кеша
		if (isset($this->page_cache_params[$c_a]))
		{
			$return = array(
				'COutputCache',
				'duration'    => 0, //$this->page_cache_params[$c_a]['duration'],
				'varyByParam' => $this->page_cache_params[$c_a]['varyByParam'],
			);

			//Если установлена sql зависимость, добавляем ее
			#	if(isset($this->page_cache_params[$c_a]['dependency']))
			$return['dependency'] = array(
				'class'                 => 'CRBMDependency',
				'dependent_from_models' => array('Authors', 'Tags'),
				'cache_keys'            => array_merge($this->page_cache_params[$c_a]['varyByParam']),
				'c_a'                   => $c_a
			);
		}
		/*КОНЕЦ Кеширование страниц*/

		return array('preaccess', $return);

	}

	/**
	 * Установить seo-объект
	 * @param $seo_object - или ID в дереве или объект из которого бырать тайтл, кейвордс и дескрипшен
	 */
	public function setSeo($seo_object, array $params = array())
	{
		if (!empty($seo_object))
		{
			/*--------------------------------------------------------------------------------------------------*/
			//Кусок кода, который отвечает за всю "кухню" с SEO тагами(title, keywords, description)
			/*--------------------------------------------------------------------------------------------------*/
			//Временные переменные для хранения мета-тагов страницы.
			$pTitle       = '';
			$pKeywords    = '';
			$pDescription = '';

			//Если переданный seo-объект, - это id в дереве, то достанем seo-инфу из дерева
			if (is_numeric($seo_object))
			{
				$CACHE_KEY = Yii::app()->cache->buildKey(keysSet::CACHE_KEY_TREE_NODE, array($seo_object));
				$tree_node = Yii::app()->cache->get($CACHE_KEY);
				if ($tree_node === false)
				{
					$tree_node = Tree::model()->findByPk($seo_object, array('select' => array('seo_title', 'seo_keywords', 'seo_description', 'name')));
					Yii::app()->cache->set($CACHE_KEY, $tree_node, iMemCache::CACHE_DURATION_1_DAY);
					Yii::app()->cache->addDependency($CACHE_KEY, array('Tree' => array($seo_object)));
				}

				if (!empty($tree_node->seo_title))
				{
					$pTitle = $tree_node->seo_title;
				}
				if (!empty($tree_node->seo_keywords))
				{
					$pKeywords = $tree_node->seo_keywords;
				}
				if (!empty($tree_node->seo_description))
				{
					$pDescription = $tree_node->seo_description;
				}

				if (empty($this->section) || $this->section == 'index')
				{
					$this->section = 'news';
				}
				$this->rss_feeds = array(array('title' => $tree_node->name, 'code' => $this->section));
			}

			//Если установленный для контроллера seo-объект, - это объект некторого класса, то проверяем, содержит ли он
			//seo-информацию и если, да, то берём её.
			if (is_object($seo_object))
			{
				if ($seo_object->getAttribute('seo_title'))
				{
					if (!empty($seo_object->seo_title))
					{
						$pTitle = $seo_object->seo_title;
					}
				}
				else
				{
					if ($seo_object->getAttribute('name'))
					{
						if (!empty($seo_object->name))
						{
							$pTitle = $seo_object->name;
						}
					}
					elseif ($seo_object->getAttribute('title'))
					{
						if (!empty($seo_object->title))
						{
							$pTitle = $seo_object->title;
						}
					}
				}

				if ($seo_object->getAttribute('seo_keywords'))
				{
					if (!empty($seo_object->seo_keywords))
					{
						$pKeywords = $seo_object->seo_keywords;
					}
				}

				if ($seo_object->getAttribute('seo_description'))
				{
					if (!empty($seo_object->seo_description))
					{
						$pDescription = $seo_object->seo_description;
					}
				}
				else if ($seo_object->getAttribute('predetail_text'))
				{
					if (!empty($seo_object->predetail_text))
					{
						$pDescription = $seo_object->predetail_text;
					}
				}

				//Заполняем картинку для тегов фейсбука
				//для статей берем спец картинку
				if (get_class($seo_object) == 'Articles' && $seo_object->img('facebook'))
				{
					Yii::app()->facebook->ogTags['image'] = Yii::app()->params['baseUrl'] . $seo_object->img('facebook');
				}
				elseif ($seo_object->getAttribute('preview_img') && !empty($seo_object->preview_img))
				{
					Yii::app()->facebook->ogTags['image'] = Yii::app()->params['baseUrl'] . '/' . $seo_object->preview_img;
				}
				elseif ($seo_object->getAttribute('pic') && !empty($seo_object->pic))
				{
					Yii::app()->facebook->ogTags['image'] = Yii::app()->params['baseUrl'] . $seo_object->pic;
				}
			}

			//Если передан массив доп параметров, проверяем, есть ли в нем значения
			if (isset($params['title']))
			{
				$pTitle = $params['title'];
			}

			if (isset($params['keywords']))
			{
				$pKeywords = $params['keywords'];
			}

			if (isset($params['description']))
			{
				$pDescription = $params['description'];
			}

			//Заполняем мета теги страницы на основании SEO-данных, а если SEO-данные отсуствуют, то у статьи
			//так и останутся мета-теги по умолчанию из конфиг-секции params['seo'].
			if (!empty($pTitle))
			{
				$this->pageTitle = ($_SERVER['REQUEST_URI'] == '/') ? $pTitle : $pTitle . ' - ' . Yii::app()->params['seo']['title'];
			}
			if (!empty($pKeywords))
			{
				$this->pageKeywords = $pKeywords;
			}
			if (!empty($pDescription))
			{
				$this->pageDescription = $pDescription;
			}

			$ogtitle                                    = isset($params['ogtitle']) ? $params['ogtitle'] : $this->pageTitle;
			Yii::app()->facebook->ogTags['title']       = $ogtitle;
			Yii::app()->facebook->ogTags['description'] = html_entity_decode(str_replace(array("\r\n", "\r", "\n"), ' ', strip_tags($this->pageDescription)), ENT_NOQUOTES, 'UTF-8');
			/*--------------------------------------------------------------------------------------------------*/
		}
	}


	/**
	 * Фильтр промежуточной авторизации
	 * @param $filterChain
	 * @return bool
	 */
	public function filterPreaccess($filterChain)
	{
		//Фильтрация по ип
		$ips = Yii::app()->params['pre_auth_ignore_ip'];

		$ip  = Yii::app()->request->userHostAddress;

		//То, что надо исколючить из промежуточной авторизации
		$cond = $this->id == 'site' && $this->action->id == 'preauth';

		if (in_array($ip, $ips) || $cond || !Yii::app()->params['use_pre_auth'])
		{
			$filterChain->run();
			return true;
		}

		$session = Yii::app()->getSession();

		if (!isset($session['pre_auth']))
		{
			$this->redirect('/site/preauth/');
		}

		$filterChain->run();

		return true;
	}

	protected function afterRender($view, &$output)
	{
		parent::afterRender($view, $output);
		//Yii::app()->facebook->addJsCallback($js); // use this if you are registering any $js code you want to run asyc

		foreach ($this->rss_feeds as $feed)
		{
			//Yii::app()->clientScript->registerLinkTag('alternate', 'application/rss+xml', 'http://kavpolit.com/export/rss/' . $feed['code'] . '.xml', null, array('title' => 'Кавказская политика — ' . $feed['title']));
		}

		Yii::app()->facebook->initJs($output); // this initializes the Facebook JS SDK on all pages
		Yii::app()->facebook->renderOGMetaTags(); // this renders the OG tags
		return true;
	}



	/*Далее идут 5 метод, которые перекрываются относительно их базовых реализаций лишь с той целью,
	/*чтобы включить профилирование контроллеров и виджетов.*/
	/*Результаты профилирования кладутся в БД(это роутится в конфиге).*/
	protected function beforeAction($action)
	{
		if ($this::doProfile())
		{
			$c_a = (!empty($this->module) ? $this->module->id . '/' : '') . "{$this->id}/{$this->action->id}";

			Yii::beginProfile($c_a, 'bg.profiling.controllers');
		}

		return parent::beforeAction($action);
	}

	protected function afterAction($action)
	{
		parent::afterAction($action);

		if ($this::doProfile())
		{
			$c_a = (!empty($this->module) ? $this->module->id . '/' : '') . "{$this->id}/{$this->action->id}";

			Yii::endProfile($c_a, 'bg.profiling.controllers');
		}
	}

	public function widget($className, $properties = array(), $captureOutput = false)
	{
		if (YII_DEBUG)
		{
			// echo '<!-- WIDGET: '.$className.'-->';
		}

		$c_a = (!empty($this->module) ? $this->module->id . '/' : '') . "{$this->id}/{$this->action->id}";

		if ($this::doProfile())
		{
			Yii::beginProfile($c_a . ':' . $className, 'bg.profiling.widgets');
		}

		$r = parent::widget($className, $properties, $captureOutput);

		if ($this::doProfile())
		{
			Yii::endProfile($c_a . ':' . $className, 'bg.profiling.widgets');
		}

		return $r;
	}

	public function beginWidget($className, $properties = array())
	{
		/*

		Вложенные виджеты не которые используют в шаблонах beginWidget() и endWidget() не логируем, ввиду того, что под это YII не заточен!
        То есть в станадртный механизм обработки логов от которого мы наследум свой класс не передаётся информация о том, является ли текущий
        виджет вложенным в какой-то другой.

		if( $this->doProfile() )
		{
			$c_a = (!empty($this->module) ? $this->module->id . '/' : '') ."{$this->id}/{$this->action->id}";

			Yii::beginProfile($c_a . ':' . $className, 'bg.profiling.widgets');
		}*/

		return parent::beginWidget($className, $properties);
	}

	public function endWidget($id = '')
	{
		$r = parent::endWidget($id);

		/*
		Вложенные виджеты не которые используют в шаблонах beginWidget() и endWidget() не логируем, ввиду того, что под это YII не заточен!
        То есть в станадртный механизм обработки логов от которого мы наследум свой класс не передаётся информация о том, является ли текущий
        виджет вложенным в какой-то другой.

		if( $this->doProfile() )
		{
			$c_a = (!empty($this->module) ? $this->module->id . '/' : '') ."{$this->id}/{$this->action->id}";

			Yii::endProfile($c_a . ':' . get_class($r), 'bg.profiling.widgets');
		}*/

		return $r;
	}

	/**
	 * @return bool - возвращает включен ли режим профилирования или нет
	 */
	public static function doProfile()
	{
		if (self::$_do_profile !== null)
		{
			if (self::$_do_profile === true)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		$CACHE_KEY = Yii::app()->cache->buildKey(keysSet::CACHE_KEY_DO_PROFILE);

		$do_profile = Yii::app()->cache->get($CACHE_KEY);

		if ($do_profile === false || $do_profile == 0)
		{
			//Кешируем результат на время обработки запроса
			self::$_do_profile = false;

			return false;
		}

		//Кешируем результат на время обработки запроса
		self::$_do_profile = true;

		return true;
	}

	/**
	 * @static
	 *
	 * Переключение текущего режима профилирования на противоположный:
	 * если профилирование выключено, то оно будет включено и, наоборот,
	 * если профилирование включено, то оно будет выключено.
	 *
	 */
	public static function SwitchProfilingMode()
	{
		$CACHE_KEY = Yii::app()->cache->buildKey(keysSet::CACHE_KEY_DO_PROFILE);

		//Если профилирование выключено, включим его
		if (!self::doProfile())
		{
			//Чистим таблицу от старых логов, перед стартом профилирования
			/*
			$connection = Yii::app()->db;
			$sql = "TRUNCATE TABLE " . Profiler::model()->tableName();
			$command = $connection->createCommand($sql);
			$command->query();
			*/

			Yii::app()->cache->set($CACHE_KEY, 1, iMemCache::CACHE_DURATION_INFINITY);
		}
		//Если профилирование включено, выключим его
		else
		{
			Yii::app()->cache->set($CACHE_KEY, 0, iMemCache::CACHE_DURATION_INFINITY);

			Yii::app()->cache->delete($CACHE_KEY);
		}
	}

	/********БЛОК ФУНКЦИЙ РАБОТЫ С КЕШЕМ В ШАБЛОНЕ*******************/


	/**
	 *  Устанавливает параметры кеширования для заданой страницы
	 * @param string $c_a - строка вида article/detail
	 * @param int $duration - время кеширования в секундах
	 * @param array $varyByParam - массив участвующих в генерации ключа переменных.
	 */
	public function setPageCacheParams($c_a, $duration, $varyByParam = array(), $dependency = array())
	{

		$this->page_cache_params[$c_a]['duration']    = $duration;
		$this->page_cache_params[$c_a]['varyByParam'] = $varyByParam;

		/*
		$controller_id = $this->id;
		$action_id = $this->getAction()->id;
		
		//Название вызванного метода контроллера
		$method_name = 'action'.$action_id;		
	
		//Получаем параметры используемого метода
		$method = new ReflectionMethod($this, $method_name); 
		$method_parameters = $method->getParameters();
		
		//Создаем массив разрешенных параметров и включаем в него параметры вызванного метода
		if(is_array($method_parameters))
		{
			foreach($method_parameters as $param)
			{
				$accepted_get_params[] = $param->getName();
			}
		}
		
		//Постраничку разрешаем везде
		$accepted_get_params[] = 'p';
		
		
		//Все переменные из GET
		$get_params = $this->getActionParams();
		
		//Берем значения разрешенных переменных и записываем в ключи для генерации кеша
		foreach($accepted_get_params as $aparam)
		{
			if(isset($get_params[$aparam]))
			{
				$cache_keys[] = $aparam.':'.$get_params[$aparam];
			}
		}
		
		$cache_keys[] = $controller_id;
		$cache_keys[] = $action_id;
		
		$cache_keys_string = implode('|', $cache_keys);
		
		$cache_key = md5($cache_keys_string);
		
		$this->page_cache_params = $accepted_get_params;
		*/

		#UtilsHelper::debug($this->getActionParams());
		#UtilsHelper::debug($accepted_get_params);	
		#UtilsHelper::debug($cache_keys);
		#UtilsHelper::debug($cache_keys_string);

		#UtilsHelper::debug($cache_key);

	}

}