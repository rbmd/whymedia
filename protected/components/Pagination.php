<?
class Pagination extends CLinkPager
{
	public $width = 300;
	//хак для постранички на главной, делаем +1 ко всем страницам при выводе
	public $index_page = false;

	public function run()
	{
		if($this->index_page)
		{
			if(isset($_GET['page']) && ($_GET['page'] == 0 || $_GET['page'] == -1) )
				$page = 0;
			else
				$page = $this->pages->currentPage + 2;
		} else {
			$page = $this->pages->currentPage + 1;
		}

		#var_dump($_GET['page']);

		$total_pages = ceil($this->pages->itemCount / $this->pages->pageSize);

		if ($page - $this->width > 0)
		{
			$start = $page - $this->width;
		}
		else
		{
			$start = 1;
		}
		
		if ($start + $this->width * 2 <= $total_pages)
		{
			$end = $start + $this->width * 2;
		}
		else
		{
			$end = $total_pages + ( ($this->index_page) ? 1 : 0 );
			$start = $end - $this->width * 2;
			
			if ($start < 1)
				$start = 1;
		}

		if ($page - 1 > 0)
			$move_left = $page - 1;
		else
			$move_left = null;
		
		if ($page + 1 <= $end)
			$move_right = $page + 1;
		else $move_right = null;
		
		$this->render(
			'application.widgets.paginator.views.generic',
			array(
				'page' => $page,
				'start' => $start,
				'end' => $end,
				'move_left' => $move_left,
				'move_right' => $move_right,
				'total_pages' => $total_pages,
			)
		);
	}
}
?>