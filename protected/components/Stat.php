<?php
class Stat
{
	static public function viewAdd($id, $entity_name = 'Articles')
	{
		//Сначала обновляем общий счётчик просмотров сущности.
		$updated = ViewsFull::model()->updateCounters(
			array('views' => !RobotsHelper::isBot() + 0, 'views_all' => 1),
			'element_id=:id AND entity_name=:entity_name',
			array(':id' => $id, ':entity_name' => $entity_name)
		);



		//(если статистики по сущности ещё нет, то добавляем в табличку)
		if (empty($updated))
		{
			$entity              = new ViewsFull();
			$entity->element_id  = $id;
			$entity->entity_name = $entity_name;
			$entity->entity 	 = ViewsFull::entityHelper($entity_name);
			$entity->views       = 1;
			$entity->views_all   = 1;
			$entity->save();
		}
		
		/*
		//Теперь обновляем счётчик просмотров сущности по дням.
		$updated = ViewsDaily::model()->updateCounters(
			array('views' => !RobotsHelper::isBot() + 0, 'views_all' => 1),
			"entity_name=:entity_name AND element_id=:id AND view_date=DATE(NOW())",
			array(':id' => $id, ':entity_name' => $entity_name)
		);

		//(если статистики по сущности ещё нет, то добавляем в табличку)
		if (empty($updated))
		{
			$entity              = new ViewsDaily();
			$entity->element_id  = $id;
			$entity->entity_name = $entity_name;
			$entity->views       = 1;
			$entity->views_all   = 1;
			$entity->view_date   = new CDbExpression('DATE(NOW())');
			$entity->save();
		}
		*/
	}
}