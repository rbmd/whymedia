<?php
class TreeController extends Controller
{
	public $layout = 'index';

	public $inter = '';

	public $lang = 'en';

	public function beforeAction($action)
	{
		$inter['all']['en'] = 'All';
		$inter['all']['ru'] = 'Все';
		$inter['all']['ge'] = 'ყველაფერი';

		$inter['advert']['en'] = 'Advert';
		$inter['advert']['ru'] = 'Реклама';
		$inter['advert']['ge'] = 'რეკლამა';

		$inter['event']['en'] = 'Event';
		$inter['event']['ru'] = 'События';
		$inter['event']['ge'] = 'ღონისძიებები';

		$inter['showreel']['en'] = 'showreel';
		$inter['showreel']['ru'] = 'Шоурил';
		$inter['showreel']['ge'] = 'showreel';

		$inter['work']['en'] = 'Work';
		$inter['work']['ru'] = 'Работы';
		$inter['work']['ge'] = 'ნამუშევრები';

		$inter['contacts']['en'] = 'Contacts';
		$inter['contacts']['ru'] = 'Контакты';
		$inter['contacts']['ge'] = 'კონტაქტი';

		$inter['about']['en'] = 'About';
		$inter['about']['ru'] = 'О проекте';
		$inter['about']['ge'] = 'პროექტის შესახებ';

		$inter['back_to_works']['en'] = 'Back to works';
		$inter['back_to_works']['ru'] = 'Назад к работам';
		$inter['back_to_works']['ge'] = 'უკან ნამუშევრებთან';

		$inter['more_projects']['en'] = 'More';
		$inter['more_projects']['ru'] = 'Еще';
		$inter['more_projects']['ge'] = 'მეტი';

		$inter['clients']['en'] = 'Clients';
		$inter['clients']['ru'] = 'Клиенты';
		$inter['clients']['ge'] = 'კლიენტები';

		$inter['client']['en'] = 'Client';
		$inter['client']['ru'] = 'Клиент';
		$inter['client']['ge'] = 'Client';

		$inter['next']['en'] = 'Next';
		$inter['next']['ru'] = 'Следующая';
		$inter['next']['ge'] = 'Next';

		$inter['previous']['en'] = 'Previous';
		$inter['previous']['ru'] = 'Предыдущая';
		$inter['previous']['ge'] = 'Previous';

		$inter['copyright']['en'] = '&copy; 2015, why.company. All Rights Reserved.';
		$inter['copyright']['ru'] = '&copy; 2015, why.company. Все права защищены.';
		$inter['copyright']['ge'] = '&copy; 2015, why.company. ყველა უფლება დაცულია.';

		$inter['watch_project']['en'] = 'watch project';
		$inter['watch_project']['ru'] = 'смотреть проект';
		$inter['watch_project']['ge'] = 'უყურეთ პროექტს';

		$inter['watch_showreel']['en'] = 'watch showreel';
		$inter['watch_showreel']['ru'] = 'смотреть шоурил';
		$inter['watch_showreel']['ge'] = 'watch showreel';

		$this->inter = $inter;

		return parent::beforeAction($action);

	}

	public function actionIndex($lang = 'ru', $section = null)
	{
		$this->lang = $lang;

		$p = Page::model()->findAll();

		foreach($p as $page)
		{
			$pages[$page->page][$page->param] = $page;
		}

		$criteria = new CDbCriteria();
		$criteria->order = 'date_active_start DESC';
		$rubrics = Rubric::model()->findAll($criteria);


		$criteria = new CDbCriteria();
		$criteria->condition = 'first_in_ge=0';
		$criteria->order = 'date_active_start DESC';

		if($lang == 'ge')
		{
			$criteria->order = 'first_in_ge DESC, date_active_start DESC';
			$criteria->condition = 'first_in_ge=1';
		}


		$works = Work::model()->active()->findAll($criteria);
		$lang_pref = array('ge' => '/ge', 'ru' => '', 'en' => '/en');

		$this->render('index', array('rubrics' => $rubrics, 'inter' => $this->inter, 'lang' => $lang, 'section' => $section, 'lang_pref' => $lang_pref[$lang], 'pages' => $pages, 'works' => $works, ) );	
	}

	public function actionWorks($lang = 'ru', $code = null)
	{
		if(is_null($code))
		{
			die(1);
		}

		$this->lang = $lang;

		$w = Work::model()->active()->with('rubric')->findByAttributes(array(), 'code=:code', array(':code' =>$code));
		$work['name'] = $w->{'name_'.$lang};
		$work['rubric'] = $w->rubric->{'name_'.$lang};
		$work['detail_text'] = $w->{'detail_text_'.$lang};
		$work['client_name'] = $w->client_name;
		$work['client_url'] = $w->client_url;
		$work['video'] = $w->video;
		$work['video_vimeo'] = $w->video_vimeo;
		$work['pic'] = $w->pic;

		$json['work'] = $work;


		$criteria = new CDbCriteria();
		$criteria->order = 't.date_active_start DESC';
		$rubrics = Rubric::model()->findAll($criteria);


		$criteria = new CDbCriteria();
		$criteria->params = array(':das' => $w->date_active_start);
		$criteria->order = 't.date_active_start ASC';
		$first = Work::model()->active()->with('rubric')->find($criteria);

		$criteria = new CDbCriteria();
		$criteria->params = array(':das' => $w->date_active_start);
		$criteria->order = 't.date_active_start DESC';
		$last = Work::model()->active()->with('rubric')->find($criteria);


		$criteria = new CDbCriteria();
		$criteria->condition = 't.date_active_start > :das';
		$criteria->params = array(':das' => $w->date_active_start);
		$criteria->order = 't.date_active_start ASC';
		$n = Work::model()->active()->with('rubric')->find($criteria);

		if (!empty($n))
		{
			$next['id'] = $n->id;
			$next['name'] = $n->{'name_'.$lang};
			$next['rubric'] = $n->rubric->{'name_'.$lang};
			$next['detail_text'] = $n->{'detail_text_'.$lang};
			$next['client_name'] = $n->client_name;
			$next['client_url'] = $n->client_url;
			$next['video'] = $n->video;
			$next['video_webm'] = $n->cell_video_webm;
			$next['video_mp4'] = $n->cell_video_mp4;
			$next['video_vimeo'] = $n->video_vimeo;
			$next['pic'] = $n->pic;
			$next['code'] = $n->code;
			$json['next'] = $next;
		}
		else
		{
			$next['id'] = $first->id;
			$next['name'] = $first->{'name_'.$lang};
			$next['rubric'] = $first->rubric->{'name_'.$lang};
			$next['detail_text'] = $first->{'detail_text_'.$lang};
			$next['client_name'] = $first->client_name;
			$next['client_url'] = $first->client_url;
			$next['video'] = $first->video;
			$next['video_webm'] = $first->cell_video_webm;
			$next['video_mp4'] = $first->cell_video_mp4;
			$next['video_vimeo'] = $first->video_vimeo;
			$next['pic'] = $first->pic;
			$next['code'] = $first->code;
			$json['next'] = $next;
		}

		$criteria = new CDbCriteria();
		$criteria->condition = 't.date_active_start < :das';
		$criteria->params = array(':das' => $w->date_active_start);
		$criteria->order = 't.date_active_start DESC';
		$p = Work::model()->active()->with('rubric')->find($criteria);

		if (!empty($p))
		{
			$prev['id'] = $p->id;
			$prev['name'] = $p->{'name_'.$lang};
			$prev['rubric'] = $p->rubric->{'name_'.$lang};
			$prev['detail_text'] = $p->{'detail_text_'.$lang};
			$prev['client_name'] = $p->client_name;
			$prev['client_url'] = $p->client_url;
			$prev['video'] = $p->video;
			$prev['video_webm'] = $p->cell_video_webm;
			$prev['video_mp4'] = $p->cell_video_mp4;
			$prev['video_vimeo'] = $p->video_vimeo;
			$prev['pic'] = $p->pic;
			$prev['code'] = $p->code;
			$json['prev'] = $prev;
		}
		else
		{
			$prev['id'] = $last->id;
			$prev['name'] = $last->{'name_'.$lang};
			$prev['rubric'] = $last->rubric->{'name_'.$lang};
			$prev['detail_text'] = $last->{'detail_text_'.$lang};
			$prev['client_name'] = $last->client_name;
			$prev['client_url'] = $last->client_url;
			$prev['video'] = $last->video;
			$prev['video_webm'] = $last->cell_video_webm;
			$prev['video_mp4'] = $last->cell_video_mp4;
			$prev['video_vimeo'] = $last->video_vimeo;
			$prev['pic'] = $last->pic;
			$prev['code'] = $last->code;
			$json['prev'] = $prev;
		}



		if (Yii::app()->request->isAjaxRequest)
		{
			header('Content-type: application/json');
			echo CJSON::encode($json);
			Yii::app()->end();
		}
		else
		{
			$p = Page::model()->findAll();

			foreach($p as $page)
			{
				$pages[$page->page][$page->param] = $page;
			}


			$criteria = new CDbCriteria();
			$criteria->order = 'date_active_start DESC';
			$works = Work::model()->active()->findAll($criteria);

			$lang_pref = array('ge' => '/ge', 'ru' => '', 'en' => '/en');

			$this->render('index', array('rubrics' => $rubrics,'inter' => $this->inter, 'lang' => $lang, 'lang_pref' => $lang_pref[$lang], 'pages' => $pages, 'works' => $works, 'detail' => $json) );	
		}
		#var_dump($work);
	}

}