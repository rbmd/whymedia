<?
class UserController extends Controller
{
	public $layout = 'inner';


	
	public function actionIndex($user_id)
	{
		$CACHE_KEY = 'UserController.actionIndex'.$user_id.':'.Yii::app()->request->getParam('page');
		$_render = Yii::app()->cache->get($CACHE_KEY);

		if ($_render === false)
		{
			$_render['user'] = User::model()->with(array('blog'))->findByPk( $user_id );

			if (empty($_render['user']))
				throw new CHttpException(404);

			$_render['posts'] = array();
			$_render['pages'] = null;


			if (!empty($_render['user']->blog))
			{
				$criteria = new CDbCriteria();
				$criteria->condition = 'blog_id = :blog_id AND t.is_draft = 0 AND t.active = 1';
				$criteria->params = array(':blog_id' => $_render['user']->blog->id);
				$criteria->order = 't.date_create DESC';

				$count = Post::model()->count( $criteria );

				$_render['pages'] = new CPagination($count);
				$_render['pages']->pageSize = 10;
				$_render['pages']->applyLimit($criteria);

				$_render['posts'] = Post::model()->with(array('blog'))->Indexed()->findAll( $criteria );
			}

			Yii::app()->cache->set($CACHE_KEY, $_render, 60);
		}

		if (empty($_render['user']))
			throw new CHttpException(404);

		$stat = ViewsFull::model()->getStatistic4Elements($_render['posts'], 'post');

		$this->render('index', array('user' => $_render['user'], 'posts' => $_render['posts'], 'pages' => $_render['pages'], 'stat' => $stat));
	}




	public function actionArticles($user_id)
	{
		$CACHE_KEY = 'UserController.actionArticles'.$user_id.':'.Yii::app()->request->getParam('page');
		$_render = Yii::app()->cache->get($CACHE_KEY);

		if ($_render === false)
		{
			$_render['user'] = User::model()->with(array('blog'))->findByPk( $user_id );

			if (empty($_render['user']))
				throw new CHttpException(404);
			
			$_render['articles'] = array();
			$_render['pages'] = null;

			$criteria = new CDbCriteria();
			$criteria->condition = 'user_id = :user_id AND active = 1';
			$criteria->params = array(':user_id' => $user_id);

			$author = Authors::model()->find($criteria);

			if (empty($author))
				throw new CHttpException(404);

			$criteria = new CDbCriteria();
			$criteria->index = 'id';
			$criteria->order = 'date_active_start DESC';

			$count = Articles::model()->authorIdIs($author->id)->count($criteria);

			$_render['pages'] = new CPagination($count);
			$_render['pages']->pageSize = 10;
			$_render['pages']->applyLimit($criteria);

			$_render['articles'] = Articles::model()->authorIdIs($author->id)->Indexed()->findAll($criteria);

			Yii::app()->cache->set($CACHE_KEY, $_render, 60);
		}

		$stat = ViewsFull::model()->getStatistic4Elements($_render['articles']);

		$this->render('articles', array('user' => $_render['user'], 'articles' => $_render['articles'], 'stat' => $stat, 'pages' => $_render['pages']));
	}




	public function actionComments($user_id)
	{
		$CACHE_KEY = 'UserController.actionComments'.$user_id.':'.Yii::app()->request->getParam('page');
		$_render = Yii::app()->cache->get($CACHE_KEY);

		if ($_render === false)
		{
			$_render['user'] = User::model()->with(array('blog'))->findByPk( $user_id );

			if (empty($_render['user']))
				throw new CHttpException(404);

			$criteria = new CDbCriteria();
			$criteria->select = '*';
			$criteria->condition = 't.user_id = :user_id AND t.active=1';
			$criteria->params = array(':user_id' => $user_id);
			$criteria->order = 't.date_publish DESC';


			$_render['count'] = Comments::model()->count( $criteria );

			$_render['pages'] = new CPagination($_render['count']);
			$_render['pages']->pageSize = 10;
			$_render['pages']->applyLimit($criteria);

			$_render['comments'] = Comments::model()->with(array('Article' => array('with' => array('authors')), 'Post' => array('with' => array('blog' => array('with' => array('user'))))))->findAll($criteria);

			Yii::app()->cache->set($CACHE_KEY, $_render, 60);
		}

		$this->render('comments', array('user' => $_render['user'], 'comments' => $_render['comments'], 'pages' => $_render['pages'], 'count' => $_render['count']));
	}



	public function actionSubscriptions($user_id)
	{
		$CACHE_KEY = 'UserController.actionSubscriptions'.$user_id.':'.Yii::app()->request->getParam('page');
		$_render = Yii::app()->cache->get($CACHE_KEY);

		if ($_render === false)
		{
			$_render['user'] = User::model()->with(array('blog'))->findByPk( $user_id );

			if (empty($_render['user']))
				throw new CHttpException(404);

			$criteria = new CDbCriteria();
			$criteria->condition = 'watcher_id = :watcher_id';
			$criteria->params = array(':watcher_id' => $user_id);

			$_render['subscriptions'] = Subscription::model()->with(array('Author' => array('with' => array('blog'))))->findAll($criteria);

			Yii::app()->cache->set($CACHE_KEY, $_render, 60);
		}

		$this->render('subscriptions', array('user' => $_render['user'], 'subscriptions' => $_render['subscriptions']));
	}



	public function actionSubscribers($user_id)
	{
		$CACHE_KEY = 'UserController.actionSubscribers'.$user_id.':'.Yii::app()->request->getParam('page');
		$_render = Yii::app()->cache->get($CACHE_KEY);

		if ($_render === false)
		{
			$_render['user'] = User::model()->with(array('blog'))->findByPk( $user_id );

			if (empty($_render['user']))
				throw new CHttpException(404);
		
			$criteria = new CDbCriteria();
			$criteria->condition = 'author_id = :author_id';
			$criteria->params = array(':author_id' => $user_id);

			$_render['subscribers'] = Subscription::model()->with(array('Watcher' => array('with' => array('blog'))))->findAll($criteria);

			Yii::app()->cache->set($CACHE_KEY, $_render, 60);
		}

		$this->render('subscribers', array('user' => $_render['user'], 'subscribers' => $_render['subscribers']));
	}




}