<?php
class InstagramWidget extends CWidget
{
	public function run()
	{
		$CACHE_KEY = 'InstagramWidget:run';

		$data = Yii::app()->cache->get($CACHE_KEY);

		if ($data === false)
		{
			$url = 'https://api.instagram.com/v1/users/' . Yii::app()->params['instagram']['user_id'] . '/media/recent/?access_token=' . Yii::app()->params['instagram']['access_token'];
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_TIMEOUT, 20);
			
			$result = curl_exec($ch);
			curl_close($ch);

			$data = json_decode($result);

			Yii::app()->cache->set($CACHE_KEY, $data, 600);
		}

		if (empty($data))
			return;

		try {
			if (count($data->data) > 5)
			{
				$data->data = array_slice($data->data, 0, 5);
			}
			
			$i = array_rand($data->data);
			$image = @$data->data[$i]->images->low_resolution->url;
			$link = @$data->data[$i]->link;

			if (empty($image) || empty($link))
				throw new Exception("Error Processing Request", 1);

			$this->render('rightcol1', array('link' => $link, 'image' => $image));
		}
		catch (Exception $e)
		{
			// var_dump($e->getMessage());
		}
	}
}
?>





