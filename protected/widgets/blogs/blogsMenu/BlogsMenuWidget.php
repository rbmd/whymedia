<?

class BlogsMenuWidget extends CWidget 
{
	public $tpl;

	public function run()
	{
		$CACHE_KEY = 'BlogsMenuWidget'.Yii::app()->user->id;
		$_render = Yii::app()->cache->get($CACHE_KEY);

		if ($_render === false)
		{
			$posts = Post::model()->count(array('condition' => 'active = 1 AND is_draft = 0'));
			$bloggers = Blog::getBloggersCount();

			if (!Yii::app()->user->isGuest)
			{
				$user = User::model()->with(array('blogger_subscriptions'))->findByPk(Yii::app()->user->id);
				$subscriptions = count($user->blogger_subscriptions);
			}
			else
			{
				$subscriptions = 0;
			}

			$_render['posts'] = $posts;
			$_render['bloggers'] = $bloggers;
			$_render['subscriptions'] = $subscriptions;

			Yii::app()->cache->set($CACHE_KEY, $_render, 60);
		}

		$this->render($this->tpl, $_render);
	}
}