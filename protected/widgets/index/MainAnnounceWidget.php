<?
class MainAnnounceWidget extends CWidget 
{

	public function run()
	{
		$items = Articles::model()->Indexed()->visible()->isMainAnnounce()->recently(4)->with(array('tree', 'authors'))->findAll();

		$this->render('mainannounce', array('items' => $items, ) );
	}
}