<header class="section-title _optional">
	<h2><a href="#tab-news">Новости</a></h2>
</header><!-- /.section-title -->

<!-- Здесь висит ограниченное количество новостей из сайдбара, отображаются только для узких мониторов -->
<div class="anons-list _all-news _optional" id="tab-news">
	<?
	$current_time = time();

	foreach ($items as $item)
	{
		?>
		<div class="anons">
			<div class="date"><?= Yii::app()->dateFormatter->format('HH:mm', $item->date_publish) ?></div>
			<a href="<?= $item->url() ?>" class="title"><?= $item->name ?></a>
		</div>
		<?
	}
	?>

	<a class="all-link" href="#">Все новости</a>

</div><!-- /.anons-list._all-news -->