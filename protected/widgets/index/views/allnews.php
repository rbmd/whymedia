<div class="list-block list-block-white">
	<div class="list-block-white__before"></div>
	<div class="list-block-white__after"></div>
	<div class="scroll-nest">
		<div class="wrapper wrapper-adaptive">
			<div class="grid">

				<?	
			
				foreach($items as $item)
				{					

				?>
				<a href="<?=$item->url()?>" class="shortcut">
					<img src="<?=ThumbsMaster::getThumb($item->preview_img, ThumbsMaster::$settings['85_85'])?>" alt="" class="shortcut__img">
					<span class="shortcut__info">
						<span class="shortcut__info-date"><?= UtilsHelper::timeToNiceString($item->date_publish)?></span>
						<span class="shortcut__info-name">
							<?=$item->name?>							
						</span>
					</span>
				</a>

				<?
				}
				?>


			</div>
		</div>
	</div>

	<div class="service-link-nest">
		<a href="javascript:void(0)" class="service-link service-link-more" data-icon-name="plusCross" onclick="return false;">
			<span class="service-link__name">Еще новости</span>
		</a>

		<a href="/news/" class="service-link service-link-all" data-icon-name="angle">
			Все новости
		</a>
	</div>
</div>