<div class="navdots">
	<ul></ul>
</div>

<?
foreach($items as $item){?>
<a href="<?=$item->url()?>" class="intro intro--dark" style="background-image: url(<?=$item->preview_img?>);">
	<div class="wrapper wrapper-adaptive wrapper-relative">
		<div class="intro__text">
			<div class="intro__text-title">
				<?=$item->name?>
			</div>
			<div class="intro__text-txt">
				<?=$item->preview_text?>
			</div>
		</div>
	</div>
</a>
<?}?>