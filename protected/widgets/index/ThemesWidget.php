<?
class ThemesWidget extends CWidget
{

	public function init()
	{

	}

	public function run()
	{


		$CACHE_KEY = 'MainarticlesWidget'.serialize(ArticlesPk::getArray());
		$_render = Yii::app()->cache->get($CACHE_KEY);	

		if($_render === false)
		{		



			$themes = IndexpageSeries::model()->with(array('series'))->order('order_num ASC')->findAll();

			foreach($themes as $theme_key => $theme)
			{
				if(!isset($first))
				{
					$count = 5;
					$first = true;
				}
				else
					$count = 3;

				//->pkNotIn(ArticlesPk::getArray())
				if(isset($theme->series))
					$items_holder[$theme_key] = Articles::model()->Indexed()->visible()->recently($count)->with(array('tree'))->inSeries($theme->series->id)->findAll();

			}

			$_render['themes'] = $themes;
			$_render['items_holder'] = $items_holder;

			Yii::app()->cache->set($CACHE_KEY, $_render, 60);
		}

		$this->render('themes', array('themes' => $_render['themes'], 'items_holder' => $_render['items_holder']));
	}

}