<?
class AllnewsWidget extends CWidget
{
	public $tpl = 'allnews';

	public $count = 12;

	public function init()
	{

	}

	public function run()
	{

		$CACHE_KEY = 'AllnewsWidget'.$this->tpl.$this->count;
		$_render = Yii::app()->cache->get($CACHE_KEY);		

		if($_render === false)
		{
			$criteria        = new CDbCriteria;
			#$criteria->condition = 'date_active_start >= (NOW() - INTERVAL 20 DAY)';
			$items = Articles::model()->Indexed()->parentIs(Tree::TREE_NEWS_ID)->visible()->recently($this->count)->findAll($criteria);

			
			$_render['items'] = $items;

			Yii::app()->cache->set($CACHE_KEY, $_render, 60);

		}


		$this->render($this->tpl, array('items' => $_render['items']) );
		
	}

}