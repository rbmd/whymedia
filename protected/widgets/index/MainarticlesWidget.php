<?
class MainarticlesWidget extends CWidget
{
	public $tpl = 'mainarticles';

	public function init()
	{

	}

	public function run()
	{

		$CACHE_KEY = 'MainarticlesWidget'.serialize(ArticlesPk::getArray());
		$_render = Yii::app()->cache->get($CACHE_KEY);	

		if($_render === false)
		{

			//Главная статья
			$main_item = Articles::model()->Indexed()->visible()->isMainAnnounce()->recently(1)->with(array('tree', 'authors'))->findAll();

			//Добавляем выбранные элементы в глобальный массив выбранных
			ArticlesPk::add($main_item);

			//Статьи с "показывать на морде"
			$items = Articles::model()->Indexed()->visible()->isShowOnIndexPage()->recently(5)->with(array('tree', 'authors'))->pkNotIn(ArticlesPk::getArray())->findAll();


			$_render['main_item'] = $main_item;
			$_render['items'] = $items;

			Yii::app()->cache->set($CACHE_KEY, $_render, 60);
		}

		$stat = ViewsFull::model()->getStatistic4Elements($_render['main_item'] + $_render['items']);

		ArticlesPk::add($_render['main_item']);
		ArticlesPk::add($_render['items']);

		$this->render($this->tpl, array('main_item' => array_pop($_render['main_item']), 'items' => $_render['items'], 'stat' => $stat));
	}

}