<?
class TreeWidget extends CWidget
{

	public function init()
	{

	}

	public function run()
	{

		$CACHE_KEY = 'TreeWidget';
		$_render = Yii::app()->cache->get($CACHE_KEY);	

		if($_render === false)
		{
			$tree = IndexpageTree::model()->with(array('tree'))->order('order_num ASC')->findAll();

			foreach($tree as $t_key => $t)
			{
				//вернуть в выборку когда попросят :)
				//pkNotIn(ArticlesPk::getArray())->

				$items_holder[$t_key] = Articles::model()->Indexed()->visible()->recently(5)->with(array('tree'))->parentIs($t->tree_id)->findAll();

			}

			$_render['tree'] = $tree;
			$_render['items_holder'] = $items_holder;

			Yii::app()->cache->set($CACHE_KEY, $_render, 60);
		}
		



		$this->render('tree', array('tree' => $_render['tree'], 'items_holder' => $_render['items_holder']));
	}

}