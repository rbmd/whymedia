<?php // Если страница одна, не показываем пагинатор
if ($end > 1)
{
?>
<li>
	<div class="paging">
		<?
		if(!is_null($prevPage))
		{				
			?>
			<a href="<?=$prevPage?>" class="prev">← Предыдущая</a>
			<?
		}
		
		for ($p = $start; $p <= $end; $p++)
		{
			$aPage = ($p == 1) ? array('page' => null) : array('page' => $p);
			$base_page = UtilsHelper::mergeGetRequest($aPage);		
			if($p == $page)
				$current = 'curent';
			else
				$current = '';
			?>
			<a href="<?=$base_page?>" class="num <?=$current?>"><?=$p?></a>
			<?
		}

		if(!is_null($nextPage))
		{					
			?>
			<a href="<?=$nextPage?>" class="prev">Следующая →</a>
			<?
		}

		?>						 
	</div>
</li>

<?php } ?>