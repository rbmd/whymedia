<?
class MaterialsBySerieWidget extends CWidget
{
	//ID серии для получения списка материалов
	public $series_id;

	public $tree_id;

	public $count = 3;

	public function init()
	{
		$this->series_id = intval($this->series_id);
		$this->tree_id = intval($this->tree_id);
	}

	public function run()
	{
		$items = null;
		$serie = null;
		$tree = null;

		$CACHE_KEY = 'MaterialsBySerieWidget'.$this->series_id.$this->tree_id.$this->count.serialize(ArticlesPk::getArray());
		$_render = Yii::app()->cache->get($CACHE_KEY);

		if($_render === false)
		{
			if($this->series_id > 0)
			{
				$serie = Series::model()->findByPk($this->series_id);
				$items = Articles::model()->visible()->with(array('tree'))->pkNotIn(ArticlesPk::getArray())->inSeries($this->series_id)->Indexed()->recently($this->count)->findAll();
			}

			if($this->tree_id > 0 && is_null($items) )
			{
				$tree = Tree::model()->findByPk($this->tree_id);
				$items = Articles::model()->visible()->with(array('tree'))->pkNotIn(ArticlesPk::getArray())->parentIs($this->tree_id)->Indexed()->recently($this->count)->findAll();
			}

			$_render['serie'] = $serie;
			$_render['tree'] = $tree;
			$_render['items'] = $items;

			Yii::app()->cache->set($CACHE_KEY, $_render, 60);

		}

		if(!is_null($_render['items']) )
		{
			$this->render('materialsbyserie',array('items' => $_render['items'], 'serie' => $_render['serie'], 'tree' => $_render['tree']));
		}

	}
}