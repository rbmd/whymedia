<div class="wrapper wrapper-adaptive">

	<div class="footer__heading">Другие статьи по теме:</div>

	<div class="grid">
		<?foreach($items as $item){?>
		<a href="<?=$item->url()?>" class="tile tile-1-1" data-image="<?=ThumbsMaster::getThumb($item->preview_img, ThumbsMaster::$settings['240_240'])?>">
			<img src="<?=ThumbsMaster::getThumb($item->preview_img, ThumbsMaster::$settings['240_240'])?>" alt="/test/2.jpg">
			<span class="tile__info">
				<span class="tile__info-date"><?=Yii::app()->dateFormatter->format('dd MMMM', $item->date_active_start)?></span>
				<span class="tile__info-section tile__info-section--<?=$item->tree->code?>"><?=$item->tree->name?></span>
				<span class="tile__info-name"><?=$item->name?></span>
				<span class="tile__info-txt"><?=$item->preview_text?></span>
				<span class="tile__info-read">Читать</span>
			</span>
		</a>
		<?}?>
	</div>
	<?/*
	<a href="?11" class="load-more">
		<span class="load-more__icon" data-icon-name="laceIcon"></span>
		<span class="load-more__name">Еще материал</span>
	</a>
	*/?>
</div>