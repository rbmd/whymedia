<?
/*
Виджет выводит форму с параметрами для сортировки статей в списках. 

Сама сортировка осуществляется в соответствующих контроллерах или виджетах.

*/


class ListRegionSelectorWidget extends CWidget 
{
	//Типы сортировки статей (популярные, последние)
	private $types;

	//Разделы для mediatypes (передаются из вьюхи mediatypesController.detail() )
	public $mediatypes;

	//Текущий раздел (передается из вьюхи mediatypesController.detail() )
	public $mediatype;


	//Шаблон
	public $tpl;

	public function init()
	{
		$this->types = array('popular' => 'Популярные', 'last' => 'Последние');

	}

	public function run()
	{
		//Для раздела /mediatypes/ свой фильтр без регионов
		if($this->tpl == 'mediatypesTopmenu')
			$this->render('mediatypesTopmenu', array( 'mediatypes' => $this->mediatypes, 'mediatype' => $this->mediatype, ) );
		else
		{

			$currentRegion = Yii::app()->request->getParam('region', 0);
			$currentType = Yii::app()->request->getParam('type', 'last');

			//Если указан несуществующий тип статей
			if(!isset($this->types[$currentType]))
				throw new CHttpException(404);

			$CACHE_KEY = 'ListRegionSelectorWidget';
			$regions = Yii::app()->cache->get($CACHE_KEY);

			if($regions === false)
			{
				$regions = Regions::model()->indexed()->findAll();
				Yii::app()->cache->set($CACHE_KEY, $regions, 60);
			}
			
			//Если указан несуществующий регион
			if($currentRegion != 0 && !array_key_exists($currentRegion, $regions) )
				throw new CHttpException(404);


			$this->render('listregionselector', array('regions' => $regions, 'currentRegion' => $currentRegion, 'types' => $this->types, 'currentType' => $currentType));
		}
	}

}