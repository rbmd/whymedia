<?
$aid = yii::app()->controller->action->id;
?>
<div class="b-filter">
	<ul class="b-filterlist _tabs">
		<li class="tablink <?= $aid == 'index' ? 'active' : '' ?>"><a href="/user/<?= $user->id ?>/">Посты <span>(<?= $posts ?>)</span></a></li>
		<?
		if (!empty($articles))
		{
			?><li class="tablink <?= $aid == 'articles' ? 'active' : '' ?>"><a href="/user/<?= $user->id ?>/articles/">статьи <span>(<?= $articles ?>)</span></a></li><?
		}
		?>
		<li class="tablink <?= $aid == 'comments' ? 'active' : '' ?>"><a href="/user/<?= $user->id ?>/comments/">комментарии <span>(<?= $comments ?>)</span></a></li>
		<li class="tablink <?= $aid == 'subscriptions' ? 'active' : '' ?>"><a href="/user/<?= $user->id ?>/subscriptions/">подписки <span>(<?= $subscriptions ?>)</span></a></li>
		<li class="tablink <?= $aid == 'subscribers' ? 'active' : '' ?>"><a href="/user/<?= $user->id ?>/subscribers/">подписчики <span>(<span id="subscriptions_cnt"><?= $subscribers ?></span>)</span></a></li>
	</ul>
</div>