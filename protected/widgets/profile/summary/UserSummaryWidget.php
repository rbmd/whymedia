<?php
class UserSummaryWidget extends CWidget
{
	public $user_id;

	public function init() { }

	public function run()
	{
		$user = User::model()->cache(60)->findByPk($this->user_id);

		if (!Yii::app()->user->isGuest)
		{
			$criteria = new CDbCriteria();
			$criteria->condition = 'author_id = ' . $this->user_id . ' AND watcher_id = ' . Yii::app()->user->id;
			$subscription = Subscription::model()->find($criteria);
		}
		
		$watcher_is_subscribed = !empty($subscription);

		$this->render('user', array('user' => $user, 'watcher_is_subscribed' => $watcher_is_subscribed));
	}
}