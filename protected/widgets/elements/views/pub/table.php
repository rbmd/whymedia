<div class="b-article-element b-article-element_shiftable b-article-element-good">
<?php if (mb_strlen($E->fields->video) > 0) { ?>
<div class="b-article-element-good__video">
	<?=UtilsHelper::handlerIframe($E->fields->video)?>
</div>
<?php } ?>
<? if (mb_strlen($E->fields->image_path) > 0) : ?>
	<table class="b-article-element-good__layout">
		<tbody>
			<tr>
				<td class="b-article-element-good__info">
					<img src="<?=$E->fields->image_path?>" alt="<?=htmlspecialchars($E->fields->name)?>" class="b-article-element-good__image" width="180" />
					<p class="b-article-element-good__location-name g-font-title-3"><?=$E->fields->facility_name?></p>
					<? if ($E->fields->metro) { ?>
						<p class="b-article-element-good__location-subway"><i class="g-icon g-icon_subway"></i>&nbsp;<?=$E->fields->metro?></p>
					<? } ?>
					<p class="b-article-element-good__location-address g-font-small-5"><?=$E->fields->address?></p>
					<p class="b-article-element-good__location-additional g-font-small-5"><?=$E->fields->additional_info?></p>
					<? if ($E->fields->site) { ?>
						<noindex>
						<a href="<?=$E->fields->site?>" target="_blank" rel="nofollow" class="b-article-element-good__site g-font-small-5">
						<?=($E->fields->site_text) ? $E->fields->site_text : $E->fields->site ?>
						</a>
						</noindex>
					<? } ?>
				</td>
				<td class="b-article-element-good__gap"></td>
				<td class="b-article-element-good__main">
					<div class="b-article-element-good__head">
						<p class="b-article-element-good__title g-font-title-2"><?=$E->fields->name?></p>
						<? if (mb_strlen($E->fields->price) > 0) : ?><span class="b-article-element-good__price g-color-text g-font-title-2"><?=$E->fields->price?></span><? endif; ?>
					</div>
					<p class="b-article-element-good__description"><?=$E->fields->description?></p>
					<?
						if (!is_null($E->fields->bold)) {
							foreach ($E->fields->bold as $bo) {
					?>
						<p class="b-article-element-good__additional-bold"><?=$bo?></p>
					<?
							}
						}
					?>
					<?
						if (isset($E->fields->italic)) {
							foreach ($E->fields->italic as $it) {
					?>
						<p class="b-article-element-good__additional-italic g-font-small-5"><?=$it?></p>
					<?
						}
					}
					?>
				</td>
			</tr>
		</tbody>
	</table>
<? else : ?>
	<table class="b-article-element-good__layout">
		<tbody>
			<tr>
				<td class="b-article-element-good__main">
					<div class="b-article-element-good__head">
						<p class="b-article-element-good__title g-font-title-2"><?=$E->fields->name?></p>
						<?if(mb_strlen($E->fields->price) > 0){?><span class="b-article-element-good__price g-color-text g-font-title-2"><?=$E->fields->price?></span><?}?>
					</div>
					<p class="b-article-element-good__description"><?=$E->fields->description?></p>
					<?php
						if (isset($E->fields->bold)) {
							foreach ($E->fields->bold as $bo) {
					?>
						<p class="b-article-element-good__additional-bold"><?=$bo?></p>
					<?php
						}
					}
					?>
					<?php
						if (isset($E->fields->italic)) {
							foreach ($E->fields->italic as $it) {
					?>
						<p class="b-article-element-good__additional-italic g-font-small-5"><?=$it?></p>
					<?php
						}
					}
					?>
				</td>
				<td class="b-article-element-good__gap"></td>
				<td class="b-article-element-good__info">
					<p class="b-article-element-good__location-name g-font-title-3"><?=$E->fields->facility_name?></p>
					<? if (mb_strlen($E->fields->metro)) { ?>
					<p class="b-article-element-good__location-subway g-color-text">
						<i class="g-icon g-icon_subway_red"></i>&nbsp;<?=$E->fields->metro?>
					</p>
					<? } ?>
					<p class="b-article-element-good__location-address g-font-small-5"><?=$E->fields->address?></p>
					<p class="b-article-element-good__location-additional g-font-small-5"><?=$E->fields->additional_info?></p>
					<? if ($E->fields->site) { ?>
						<noindex>
						<a href="<?=$E->fields->site?>" target="_blank" rel="nofollow" class="b-article-element-good__site g-font-small-5">
							<?=($E->fields->site_text) ? $E->fields->site_text : $E->fields->site ?>
						</a>
						</noindex>
					<? } ?>
				</td>
			</tr>
		</tbody>
	</table>
<? endif; ?>
</div>