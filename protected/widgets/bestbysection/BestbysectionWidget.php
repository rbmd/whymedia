<?
class BestbysectionWidget extends CWidget
{

	public function init()
	{

	}

	public function run()
	{
		$nodes = array_keys( Tree::model()->tree_nodes_for_articles );

		shuffle($nodes);

		$CACHE_KEY = 'BestbysectionWidgetCache';

		$_render = Yii::app()->cache->get($CACHE_KEY);

		if($_render == false)
		{
			foreach($nodes as $tree_id)
			{

				$tree[$tree_id] = Tree::model()->findByPk($tree_id);

				//Популярные статьи к заданому разделу
				$criteria = new CDbCriteria();
				$criteria->order = 'rating DESC';
				$criteria->limit = 4;
				$items[$tree_id] = Articles::model()->visible()->with(array('tree'))->parentIs($tree_id)->Indexed()->findAll($criteria);

		        $_render['tree'] = $tree;
		        $_render['items'] = $items;

		        Yii::app()->cache->set($CACHE_KEY, $_render, 300);
		    }

		}

		//Вытаскиваем статистику для выбранных статей
		foreach($nodes as $tree_id)
		{
       		$stat[$tree_id] = ViewsFull::model()->getStatistic4Elements($_render['items'][$tree_id] );

       	}

	    $nodes_main[] = array_pop($nodes);
	    $nodes_main[] = array_pop($nodes);

		$this->render('bestbysection', array('items' => $_render['items'], 'stat' => $stat, 'nodes_main' => $nodes_main, 'nodes' => $nodes, 'tree' => $_render['tree'], ) );
	}

}