<?
	// $poll->type==Votes::POLL_TYPE_SINGLE
	#$class = 'b-article-element b-article-element_shiftable b-article-element-poll';
	$voted = $poll->showResultsToUser();
	if ($voted) {
		#$class .= ' b-article-element-poll_state_voted';
	} else {
		#$class .= ' b-article-element-poll_state_not-voted';
	}

	if ($voted) {
		$this->controller->renderPartial('application.widgets.polls.views.results.viewpoint_results', array('poll'=>$poll) );
	} else {
	?>

	<script>
		$('input.viewpointVoteButton').click(function(){
			
			$(this).parent().find('input[type=radio]').attr('checked', 'checked');
			
			var answers = $('#form_poll_<?=$poll->id?> input').serialize();

			$.ajax({
				url: "/polls/vote/<?=$poll->id?>/",
				dataType: 'json',
				data: answers,
				beforeSend: function(){
					
				},
				success: function(data){
					$('#poll_<?=$poll->id?>').html(data.poll_results);
				}
			});				
		});
	</script>
	<form id="form_poll_<?=$poll->id?>" class="personsBlock" method="POST">
		<div class="_greybg columns">
			<?
			foreach($poll->items as $item_id => $item)
			{
				?>
				<div class="col _w49">
					<div class="personitem">
						<div class="voit">
							<img src="<?=$item->img?><?#=ThumbsMaster::getThumb($item->img, ThumbsMaster::$settings[$thumb_size])?>" alt="" width="210">
							<input type="radio" class="" name="answers[]" value="<?=$item_id?>" style="display:none"/>
							<input type="button" value="Голосовать" class="button _blue-button viewpointVoteButton">
						</div>
						<div class="textblock">
							<div class="shadow"></div>
							<h3><?=$item->name?></h3>
							<p><?=$item->description?></p>
						</div>								
						
					</div>				
				</div>
				<?
			}
			?>
		</div>
	</form>



	
<?
}
?>