<?/*
<form class="voitForm">
	<div class="voit _greybg">
			<p class="ui-custom-radio">
				<input type="radio" value="" name="var" id="r1" style="position: absolute; left: -9999px;">
				<span class="radio-replacer selected" data-name="var" data-id="r1"></span>
				<label for="r1">Да. Таким образом мы сможем наладить порядок в обществе и избежать мнгих проблем</label>
			</p>
			<div class="ui-custom-radio"><input type="radio" value="" name="var" id="r2" style="position: absolute; left: -9999px;"><div class="radio-replacer" data-name="var" data-id="r2"></div><label for="r2">Нет. Думаю это покушение на личную жизнь, а значит противозаконно</label></div>
			<div class="ui-custom-radio"><input type="radio" value="" name="var" id="r3" style="position: absolute; left: -9999px;"><div class="radio-replacer" data-name="var" data-id="r3"></div><label for="r3">Мне все равно. Делайте что хотите – меня в первую очередь прослушайте</label></div>
			<input type="submit" value="Голосовать" class="button _blue-button">
	</div>
	
</form>
*/?>

<?
if( $poll->showResultsToUser() )
{
	$this->controller->renderPartial('application.widgets.polls.views.results.poll_results', array('poll'=>$poll) );

} else {
?>

<div id="poll_<?=$poll->id?>">
					
	<form id="form_poll_<?=$poll->id?>" class="voitForm" action="" method="POST">


		<div class="voit _greybg">
			<p><?= $poll->name ?></p>
				
				<?if($poll->type == 'select'){?>
					<?
					foreach($poll->items as $item_id => $item) {
						?>
						<p class="ui-custom-radio">
							<input type="radio" value="<?= $item_id ?>" name="answers[]" id="r_<?= $item_id ?>" style="position: absolute; left: -9999px;">
							<label for="r_<?= $item_id ?>"><?= $item->name ?></label>
						</p>
						<? 
					} 
					?>
				<?} else {?>
					<?
					foreach($poll->items as $item_id => $item) {
						?>
						<p class="ui-custom-checkbox">
							<input type="checkbox" value="<?= $item_id ?>" name="answers[]" id="r_<?= $item_id ?>" style="position: absolute; left: -9999px;">
							<label for="r_<?= $item_id ?>"><?= $item->name ?></label>
						</p>
					<? 
					} 
					?>			
				<?}?>


				<input id="pollSubmit<?=$poll->id?>" class="button _blue-button" type="button" value="Голосовать" />
				
				<script>
					$('#pollSubmit<?=$poll->id?>').click(function(){
						var answers = $('#form_poll_<?=$poll->id?> input').serialize();
						$.ajax({
							url: "/polls/vote/<?=$poll->id?>/",
							dataType: 'json',
							data: answers + '&inArticle=1',
							beforeSend: function(){
								
							},
							success: function(data){
								$('#poll_<?=$poll->id?>').replaceWith(data.poll_results);
							}
						});					
					});
				</script>

				
				



		</div>

	</form>

</div>

<script type="text/javascript">
	$('#poll_<?=$poll->id?> .ui-custom-radio').radioReplace();
</script>
<?
}
?>