<?
if( $poll->showResultsToUser() )
{
	$this->controller->renderPartial('application.widgets.polls.views.results.poll_rightcol_results', array('poll'=>$poll) );

} else {
?>

<div id="poll_<?=$poll->id?>" class="sidebox">
					
	<form id="form_poll_<?=$poll->id?>" action="" method="POST">

		<div class="poll">
			
			<div class="question">
				<?=$poll->name?>
			</div>

			<div class="answers">

				
				<?if($poll->type == 'select'){?>
					<?
					foreach($poll->items as $item_id => $item) {
						?>
						<div class="b-article-element-poll__option">
							<label>
								<input type="radio" name="answers[]" value="<?=$item_id?>" />
								<span><?=$item->name?></span>
							</label>
						</div>
					<? 
					} 
					?>
				<?} else {?>
					<?
					foreach($poll->items as $item_id => $item) {
						?>
						<div class="b-article-element-poll__option">
							<label>
								<input type="checkbox" name="answers[]" value="<?=$item_id?>" />
								<span><?=$item->name?></span>
							</label>
						</div>
					<? 
					} 
					?>			
				<?}?>

				<input type="hidden" name="pollFromRightCol" value="1" />
				<input id="pollSubmit<?=$poll->id?>" type="button" value="Голосовать" />
				
				<script>
					$('#pollSubmit<?=$poll->id?>').click(function(){
						var answers = $('#form_poll_<?=$poll->id?> input').serialize();
						$.ajax({
							url: "/polls/vote/<?=$poll->id?>/",
							dataType: 'json',
							data: answers,
							beforeSend: function(){
								
							},
							success: function(data){
								$('#poll_<?=$poll->id?>').replaceWith(data.poll_results);
							}
						});					
					});
				</script>

				
				

			</div>

		</div>

	</form>

</div>
<?
}
?>


<?


return;


	$voted = $poll->showResultsToUser();
	

	if ($voted) {
		$this->controller->renderPartial('application.views.polls.poll_results', array('poll'=>$poll) );
	} else {
	?>
	<div class="b-article-element b-article-element_shiftable b-article-element-poll b-article-element-poll_state_not-voted">
		<i class="g-icon g-icon_poll"></i>
		<p class="b-article-element-poll__title"><?=$poll->name?></p>
		<!-- Visible when not voted: poll options -->
		<div class="b-article-element-poll__options">
			<form id="form_poll_<?=$poll->id?>" action="" method="POST">
			<?if($poll->type == 'select'){?>
				<?
				foreach($poll->items as $item_id => $item) {
					?>
					<div class="b-article-element-poll__option">
						<label>
							<input type="radio" class="b-article-element-poll__option__control" name="answers[]" value="<?=$item_id?>" />
							<span class="b-article-element-poll__option__title g-font-default-2"><?=$item->name?></span>
						</label>
					</div>
				<? 
				} 
				?>
			<?} else {?>
				<?
				foreach($poll->items as $item_id => $item) {
					?>
					<div class="b-article-element-poll__option">
						<label>
							<input type="checkbox" class="b-article-element-poll__option__control" name="answers[]" value="<?=$item_id?>" />
							<span class="b-article-element-poll__option__title g-font-default-2"><?=$item->name?></span>
						</label>
					</div>
				<? 
				} 
				?>			
			<?}?>
			</form>
			
			<input type="button" class="b-article-element-poll__vote" value="Голосовать" />
			
			<script>
				$('.b-article-element-poll__vote').click(function(){
					var answers = $('#form_poll_<?=$poll->id?> input').serialize();
					$.ajax({
						url: "/polls/vote/<?=$poll->id?>/",
						dataType: 'json',
						data: answers,
						beforeSend: function(){
							
						},
						success: function(data){
							$('#poll_<?=$poll->id?>').html(data.poll_results);
						}
					});					
				});
			</script>
			
			<?php $restricted = $poll->is_ip_restricted(); ?>
		</div>
	</div>
<?php
	}
?>