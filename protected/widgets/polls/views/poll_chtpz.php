<div id="poll_<?=$poll->id?>">
<?
if ($poll->showResultsToUser())
{
	$this->controller->renderPartial('application.widgets.polls.views.results.poll_chtpz_results', array('poll'=>$poll, 'light' => (!empty($light) && $light == true)) );

} 
else 
{
	?>
	<form id="form_poll_<?=$poll->id?>" action="" method="POST">
		<?
		if ($poll->type == 'select')
		{
			foreach($poll->items as $item_id => $item) 
			{
				?>
				<label class="poll__label">
					<input type="radio" class="poll__input" name="answers[]" value="<?=$item_id?>">
					<span class="poll__label__name"><?=$item->name?></span>
				</label>
				<? 
			} 
		}
		else
		{
			foreach ($poll->items as $item_id => $item) 
			{
				?>
				<label class="poll__label">
					<input type="checkbox" class="poll__input" name="answers[]" value="<?=$item_id?>">
					<span class="poll__label__name"><?=$item->name?></span>
				</label>
				<? 
			} 		
		}
		?>
	</form>
	<?
}
?>
</div>

<script>
	$('#form_poll_<?=$poll->id?> input').click(function(){
		var answers = $('#form_poll_<?=$poll->id?> input').serialize();
		$.ajax({
			url: "/polls/vote/<?=$poll->id?>/" + "<?= !empty($light) ? '?light=true' : '' ?>",
			dataType: 'json',
			data: answers,
			beforeSend: function(){
				
			},
			success: function(data){
				$('#poll_<?=$poll->id?>').html(data.poll_results);
			}
		});					
	});
</script>