<?php
foreach($poll->items as $item_id => $item) 
{
	if($poll->voices_count > 0)
	{
		$percent = number_format(($item->voices_count / $poll->voices_count) * 100, 0);
		$voices_count = $item->voices_count;
	}
	else 
	{
		$percent = 0;		
		$voices_count = 0;
	}
	?>
	<div class="poll__result-name">
		<?=$item->name?> &mdash; <?=$percent?>%
	</div>

	<div class="poll__result-bar">
		<div class="poll__result-bar__fill" style="width: <?=$percent?>%;"></div>
	</div>
	<?php
}