<?php
/**
 * User: Rusakov Dmitry
 * Виджет голосования
 */

class PollWidget extends CWidget
{
	/**
	 * @var poll_id - какое голосование показывать; если null то показывается самое последнее разрешённое к показу
	 */
	public $poll_id = null;

	//Если указано, то выбираем голосование заданого типа (см model Polls::$vote_poll_types)
	public $pollTypeToSelect = null;

	//Если true, то используем шаблоны отображения внутри статьи, если фолс - правая колонка
	public $inArticle = false;

	public $light = false;

	public function init()
	{
	}

	public function run()
	{
		$poll           = null;
		$not_from_cache = false;

		$CACHE_KEY = 'poll_' . $this->poll_id.$this->pollTypeToSelect;
		$poll      = Yii::app()->cache->get($CACHE_KEY);

		#if ($poll == false)
		{
			//Если не задано какой конкретно опрос брать, берём самый свежиый, достпный для показа
			$criteria        = new CDbCriteria;
			$criteria->order = "date_active_start DESC";
			$criteria->limit = 1;

			$poll = Polls::model()->
				with(array(
					'items',
					'users_voted_count',
					'voices_count',
				))->
				visible()->pollTypeIs($this->pollTypeToSelect)->pkIs($this->poll_id)->find($criteria);

			#if(isset($this->pollTypeToSelect))
			#	$poll = $poll->;


		#	$poll = $poll->find($criteria);
			$not_from_cache = true;
			Yii::app()->cache->set($CACHE_KEY, $poll, 60);
		}


		if (!empty($poll))
		{

			//Для точки зрения другой шаблон
			/*if($poll->poll_type == 1)
				$tpl = 'viewpoint';
			elseif($this->inArticle == true)
				$tpl = 'poll';
			else */
			{
				$tpl = 'poll_chtpz';
				
				//Если мы находимся в статье, то нужно проверить, не привязан ли опрос к этой статье(и скрыть его в случае, если привязан) КРИВОЕ РЕШЕНИЕ! НУЖНО ПОПРАВИТЬ
				if(Yii::app()->controller->id == 'articles' && !is_null(Yii::app()->controller->article_id) )
				{
					$e = Elements::model()->articlePk(Yii::app()->controller->article_id)->typeIs('poll')->find();
					if(!is_null($e))
						return;
				}				

			}


			$this->render($tpl, array('poll' => $poll, 'light' => $this->light));
		}
	}
}