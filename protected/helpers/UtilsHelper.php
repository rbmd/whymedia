<?
define("CURRENT_TIME", date('Y-m-d H:i:s'));
/*
Любые небольшие функции для админки
*/
class UtilsHelper
{

	/*
	НАЧАЛО
	Блок функций получения транслитерованого кода для URL из заданой строки
	*/
	public static function transliterate($str)
	{
		return self::str2url($str);
	}

	//Создает из транслитерованой строки валидный для использования в url код
	private static function str2url($str) {
		$str = strip_tags($str);
		// переводим в транслит
		$str = self::rus2translit($str);

		// в нижний регистр
		$str = mb_strtolower($str);

		// заменям все ненужное нам на "-"
		$str = preg_replace('~[^a-z0-9_]+~u', '_', $str);

		$str = str_replace(array('laquo', 'raquo'), '', $str);
		$str = preg_replace('~[_]+~u', '_', $str);
		$str = substr($str, 0, 50);

		// удаляем начальные и конечные '-'
		$str = trim($str, "_");


		return $str;
	}

	//Функция превращает кириллицу в транслит
	private static function rus2translit($str) {
		$converter = array(
			'а' => 'a',   'б' => 'b',   'в' => 'v',
			'г' => 'g',   'д' => 'd',   'е' => 'e',
			'ё' => 'e',   'ж' => 'zh',  'з' => 'z',
			'и' => 'i',   'й' => 'y',   'к' => 'k',
			'л' => 'l',   'м' => 'm',   'н' => 'n',
			'о' => 'o',   'п' => 'p',   'р' => 'r',
			'с' => 's',   'т' => 't',   'у' => 'u',
			'ф' => 'f',   'х' => 'h',   'ц' => 'c',
			'ч' => 'ch',  'ш' => 'sh',  'щ' => 'shh',
			'ь' => '',  'ы' => 'y',   'ъ' => '',
			'э' => 'e',   'ю' => 'yu',  'я' => 'ya',

			'А' => 'A',   'Б' => 'B',   'В' => 'V',
			'Г' => 'G',   'Д' => 'D',   'Е' => 'E',
			'Ё' => 'E',   'Ж' => 'Zh',  'З' => 'Z',
			'И' => 'I',   'Й' => 'Y',   'К' => 'K',
			'Л' => 'L',   'М' => 'M',   'Н' => 'N',
			'О' => 'O',   'П' => 'P',   'Р' => 'R',
			'С' => 'S',   'Т' => 'T',   'У' => 'U',
			'Ф' => 'F',   'Х' => 'H',   'Ц' => 'C',
			'Ч' => 'Ch',  'Ш' => 'Sh',  'Щ' => 'Shh',
			'Ь' => '',  'Ы' => 'Y',   'Ъ' => '',
			'Э' => 'E',   'Ю' => 'Yu',  'Я' => 'Ya',
		);
		return strtr($str, $converter);
	}
	/*
	КОНЕЦ
	*/


	public static function getBackurl($inc_query_str = false)
	{
		$backurl = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] && !in_array(strtolower($_SERVER['HTTPS']),array('off','no'))) ? 'https' : 'http';
		$backurl .= '://'.$_SERVER['HTTP_HOST'];
		$backurl .= $_SERVER['REQUEST_URI'];
		if (!empty($_SERVER['PATH_INFO'])) $myUrl .= $_SERVER['PATH_INFO'];
		if ($inc_query_str && count($_GET))
		{
			$backurl .= '?'.http_build_query($_GET);
		}

		return $backurl;
	}


	// Перевод секунд в длительности вида 00:00:00
	public static function humanizeDuration($secs, $required = array('m', 's'))
	{
		$vals = array(
						'w' => (int) ($secs / 86400 / 7),
						'd' => $secs / 86400 % 7,
						'h' => $secs / 3600 % 24,
						'm' => $secs / 60 % 60,
						's' => $secs % 60,
					);

		$ret = array();

		$added = false;
		foreach ($vals as $k => $v)
		{
			if ($v > 0 || $added || in_array($k, $required))
			{
				$added = true;
				$ret[] = $v > 9 ? $v : '0'.$v;
			}
		}

		return join(':', $ret);
	}


	// Склонение множественных
	public static function pluralize($n, $form0, $form1, $form2)
	{
		$w = array($form0, $form1, $form2);
		return $w[self::pl_numeral($n)];
	}

	private static function pl_numeral($n)
	{
		$n = abs($n);

		return ($n%10==1 && $n%100!=11) ? 0 :
			(($n%10>=2 && $n%10<=4 && ($n%100<10 || $n%100>=20)) ? 1 : 2);
	}


	//Вычисляет empty для выражения, так как стандартный php-empty Не позволяет это делать
    public static function isEmpty($val)
    {
        return empty($val);
    }



    // Красивый print_r =)
    public static function debug($message, $title='', $color='#a00'){
        $d = debug_backtrace();
        if(isset($d[0])){
            $f = $d[0]['file'];
            $l = $d[0]['line'];
        }
        echo '<table border="0" cellpadding="5" cellspacing="0" style="border:1px solid '.$color.'; z-index: 9999;"><tr><td>';
        if(isset($f) && isset($l)){
            echo '<p style="background-color: #ddd">'.($f).': Line '.($l).'</p>';
        }
        if (strlen($title)>0)
        {
            echo '<p style="color: '.$color.';font-size:11px;font-family:Verdana;">['.$title.']</p>';
        }

        if (is_array($message) || is_object($message))
        {
            echo '<pre style="color:'.$color.';font-size:11px;font-family:Verdana;">'; print_r($message); echo '</pre>';
        }
        else
        {
            echo '<p style="color:'.$color.';font-size:11px;font-family:Verdana;">'.$message.'</p>';
        }

        echo '</td></tr></table>';
    }

    /**
     * Возвращает массив с правами текущего пользователя
     *
     * @static
     * @return array - массив с правами текущего пользователя
     */
    public static function roles()
    {
        return array_keys( Rights::getAssignedRoles(Yii::app()->user->id) );
    }




    /**
    * Построение адресной с троки с учетом существующего GET запроса
    * $variables - массив вида ('page' => 3, 'sort' => 'desc')
	* Текущая QUERY_STRING объединяется с $variables
	* @return string - |текущий путь| + |query string|, ex. |/story/| + |?sort=abc&page=5|
	*/
    public static function mergeGetRequest( $variables = array() )
    {
    	$query = array();
    	parse_str(Yii::app()->request->getQueryString(), $query);

    	foreach ($variables as $key => $value)
    	{
    		if ($value == '' || !isset($value))
    			unset($query[$key]);
			else
				$query[$key] = $value;
    	}

    	$result = http_build_query($query);

    	$prefix = strlen(Yii::app()->request->pathInfo) > 0 ? '/' : '';

    	if(strlen($result) > 0){
    		return $prefix.Yii::app()->request->pathInfo.'/?'.$result;
    	}
    	else {
    		return $prefix.Yii::app()->request->pathInfo.'/';
    	}
    }

	protected static $_ruMonths = array('', 'января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря');
	protected static $_ruMonthsNominative = array('', 'январь', 'февраль', 'март', 'апрель', 'май', 'июнь', 'июль', 'август', 'сентябрь', 'октябрь', 'ноябрь', 'декабрь');


	public static function getMonthName($num, $nominative = false)
	{
		if ($nominative)
		{
			return self::$_ruMonthsNominative[intval($num)];
		}
		else
		{
			return self::$_ruMonths[intval($num)];
		}
	}


	/**
     * Функция получает дату и находит разницу между текущим моментом и этой датой,
     * полученный результат склоняется и возвращается в текстовом формате в виде красивой строки, например "2 часа назад"
     *
     * @static
     * @param $date - дата, в формате MySQL, разницу с которой будем считать
     * @return string - временной промежуток от текущего момента до $date в текстовом формате
     */
    public static function timeToNiceString($date)
    {
		 $time = strtotime($date);
		 //echo $date;
		 // UNIX-time сегодня в 00 часов
		 $current_time = strtotime(substr(CURRENT_TIME, 0, 11) . '00:00:00');

		 // Если сегодня
		 if (substr(CURRENT_TIME, 0, 10) == substr($date, 0, 10))
		 {
			 return substr($date, 11, 5);
		 }
		 // Если вчера
		 elseif ($current_time - $time < 86400 && $current_time - $time >= 0)
		 {
		 	return substr($date, 11, 5) . ' вчера';
		 }
		 // Если в этом году
		 elseif (substr(CURRENT_TIME, 0, 4) == substr($date, 0, 4))
		 {
			 return substr($date, 8, 2) . ' ' . self::$_ruMonths[intval(substr($date, 5, 2))];
		 }
		 // Если в другом году
		 else
		 {
			 return substr($date, 8, 2) . ' ' . self::$_ruMonths[intval(substr($date, 5, 2))] . ' ' . substr($date, 0, 4);
		 }
    }





    /**
     * Функция подготовливает запрос введённый пользователем для передачи Сфинксу.
     * А именно убираются лишние пробелы + добавляются wildcards
     * @static
     *
     * @param $query - поисковый запрос от пользователя
     * @return string - обработанный поисковый запрос для Сфинкса
     */
    public static function prepareQuery($query)
    {
        $result = '';

        //Удаляем дублирующиеся пробелы внутри текста и концевые пробелы
        $query = preg_replace('/\s+/ui', ' ', strip_tags(trim($query)));

        //Разбиваем поисковый запрос по пробелам и для каждого слова в массиве добавляем wildcards перед и после,
        //причём слова из одной буквы заменяем пустой строкой
        $result = array_map(function($elem){ if(mb_strlen($elem)>1) {return "*{$elem}*";} return '';}, explode(' ', $query));

        //Берём максимум 8 слов
        $result = array_slice($result, 0, 8);

        //Снова склеиваем массив в строку
        $result = implode(' ', $result);

        //Убираем  дублирующиеся пробелы полученные в результате склеивания пустых строк на которые были заменены одиночные символы
        $result = preg_replace('/\s+/ui', ' ', trim($result));

        return $result;
    }


	public static function removeDirectory($dirname)
	{
		if (is_dir($dirname)) $dir_handle = opendir($dirname);
		if (!$dir_handle) return false;
		while($file = readdir($dir_handle))
		{
			if ($file != '.' && $file != '..')
			{
				if (!is_dir($dirname . "/" . $file))
					unlink($dirname . "/" . $file);
				else
					self::removeDirectory($dirname . '/' . $file);
			}
		}
		closedir($dir_handle);
		return rmdir($dirname);
	}


	// Добавить параметр wmode=transparent в iframe
	public static function handlerIframe($str)
	{
		if (strpos($str, '<iframe') !== false)
		{
			if (preg_match_all('#<iframe.+src=["\']{1}(http://www.youtube.com/[^ ]+?)["\']{1}\ .+>#', $str, $arr))
			{
				foreach ($arr[1] as $val)
				{
					if (strpos($val, 'wmode=transparent') === false)
					{
						$separator = strpos($val, '?') ? '&' : '?';
						$str       = str_replace($val, $val . $separator . 'wmode=transparent', $str);
					}
				}
			}
		}
		return $str;
	}

    /**
     * Распечатать print_r($data) в строку
     *
     * @static
     * @param $data
     * @return string
     */
    public static function inspect($data)
    {
        ob_start();
        print_r($data);
        $result = ob_get_contents();
        ob_end_clean();

        return $result;
    }


	// Обработка ссылок на внешнии сайты
	public static function processingExternalLink($text)
	{
		return preg_replace_callback(
			'~<a([\s\S]+?)href="(.+?)"~im',
			function ($match)
			{
				$url = $match[2];
				$fl  = mb_substr($url, 0, 1);
				// Internal on site or hash-based
				// Slon, BG and tvrain
				// Already has this attribute
				if ($fl == '#' || $fl == '/' ||
					preg_match('~^(?:http:\/\/)?(?:www\.)?(slon|bg|tvrain)\.ru~im', $url) != false ||
					mb_stripos($match[1], 'rel="nofollow"') !== false
				)
				{
					return $match[0];
				}
				// External link
				return sprintf('<a%srel="nofollow" href="%s"', $match[1], $match[2]);
			},
			$text
		);
	}
}