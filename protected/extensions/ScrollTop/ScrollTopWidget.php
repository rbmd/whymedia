<?php
class ScrollTopWidget extends CWidget
{

    /**
     * @var string
     */
    public $label = '^top';

    public $scriptOptions = array();
    /**
     * @var array
     */
    public $linkOptions = array();

    /**
     * @return void
     */
    public function init() {
        if ( empty($this->linkOptions['id']) ) {
            $this->linkOptions['id'] = $this->getId();
        }

        $this->registerClientScripts();

        echo CHtml::link($this->label, '#', $this->linkOptions);
    }


    public function registerClientScripts() {
        $a=CHtml::asset(dirname(__FILE__).'/assets/');

        /** @var $cs CClientScript */
        $cs=Yii::app()->clientScript;

        $cs->registerCssFile(
            Yii::app()->assetManager->publish(
                Yii::getPathOfAlias('ext.ScrollTop.assets').'/scrolltotop.css'
            )
        );

        $cs->registerCoreScript('jquery');
        $cs->registerScriptFile($a.'/jquery.scrolltop.js');


        $jsonOptions = CJavaScript::encode($this->scriptOptions);

        // register the jquery and   plugin code
        $id = $this->linkOptions['id'];

        $cs->registerScript(__CLASS__ . '#' . $id,"$(\"#{$id}\").topLink({$jsonOptions});");
    }
}