(function($) {


	$.fn.autocmplt = function(options) {

		//Блок со всем содержимым
		var container = $(this);

		var autocmplt = this;

		var DEFAULTS = {

			url                : '', //url ajax запроса
			ajax_request_delay : 300, //Задержка между повторным ajax-запросом
			min_query_length   : 2, //Минимальная длина ввода для отсылки ajax-запроса
			max_helper_items   : 30, //Максимальное количество элементов в подсказке
			hidden_element_name: "autocmplt[]", //Название для создаваемого hidden элемента(который будет передаваться в пост или гет запросе)
			max_items          : 10,	//Максимальное количество добавленных элементов
			allowNewItems      : false, // Возможность добавлять свои новые тэги
			param              : 'value', // Параметр, который будет записан в массив (key или value)
			css                : {
				searchInput  : 'autocmplt_input',
				selectedItems: 'autocmplt_items'
			}
		};

		var KEYS = {
			ENTER     : 13,
			ARROW_UP  : 38,
			ARROW_DOWN: 40
		}

		//Блок хранения добавленных значений
		var $itemsBlock = $("<div />", {
			'class': DEFAULTS.css.selectedItems
		});

		//Базовый input для ввода значений
		var $searchInput = $('<input/>', {
			'class': DEFAULTS.css.searchInput,
			'type' : 'text'
		});

		//Блок для подсказки и input
		var inputhelper_block = $('<div class="autocmplt_inputhelper"></div>');

		//Счетчик запросов в таймере, чтобы не делать на каждый keyup запрос
		var ajax_timeout_counter = 0;

		var focus_timeout_id;

		this.init = function() {
			//Перекрываем стандартные опции пользовательскими
			autocmplt.options = $.extend(DEFAULTS, options);

			autocmplt.options.container_id = container.attr('id');

			$searchInput.focus(function() {
				container.addClass('active');
			});

			$searchInput.keypress(function(e) {
				//Отменяем обработку для enter
				//console.log(e.keyCode);
				if (e.keyCode === KEYS.ENTER)
					e.preventDefault();
			});

			$searchInput.keyup(function(e) {
				e.preventDefault();

				if (autocmplt.options.allowNewItems && e.keyCode === KEYS.ENTER)
				{
					var newItemText = $(this).val();
					autocmplt.addItem({
												"key"  : newItemText,
												"value": newItemText
											});
				}
				if (e.keyCode == KEYS.ARROW_UP || e.keyCode == KEYS.ARROW_DOWN)
				{
					$(this).blur();
					$("#autocmplt_helper_" + container.attr('id')).attr("tabindex", 0).focus();

					$("#autocmplt_helper_" + container.attr('id')).find('.autocmplt_helper_element').first().addClass('selected');


					return false;
				}
				else
				{
					ajax_timeout_counter++;

					if ($searchInput.val().length >= autocmplt.options.min_query_length)
					{
						//Разрешаем запросы не чаще чем options.ajax_request_delay
						var ajax_timeout_counter_diff = ajax_timeout_counter;

						setTimeout(function() {
							if (ajax_timeout_counter_diff == ajax_timeout_counter)
							{
								var $searchInput_val = autocmplt.ucFirstAllWords($searchInput.val());
								if (autocmplt.options.url)
								{
									$.ajax({
												 url     : autocmplt.options.url,
												 type    : 'GET',
												 data    : {'s': $searchInput_val },
												 dataType: 'json',
												 success : function(data) {
													 //console.log(data);
													 var i = 0;

													 var $searchInput_value = $searchInput_val;

													 //массив для передачи в блок подсказки
													 var ret = {};
													 ret.highlighted = [];
													 ret.values = [];
													 ret.keys = [];

													 //Проходим по всем возвращенным словам и ищем вхождения для подсветки
													 for (k in data)
													 {
														 if (data[k] == undefined)
														 {
															 continue;
														 }
														 i++;
														 if (i > autocmplt.options.max_helper_items) break;

														 var symbols = data[k].value.match(new RegExp($searchInput_value, "i"));

														 if (symbols !== null)
														 {
															 temp = data[k].value.split(symbols);
															 ret.highlighted.push(temp.join('<b>' + symbols + '</b>'));
															 ret.values.push(data[k].value);
															 ret.keys.push(data[k].key);
														 }

													 }

													 //отображаем подсказку
													 autocmplt.showHelper(ret);

												 }
											 });
								}
							}
						}, DEFAULTS.ajax_request_delay);


					}

				}

			});

			$(container).on('keydown', "#autocmplt_helper_" + container.attr('id'), function(e) {
				if (e.keyCode == KEYS.ARROW_UP || e.keyCode == KEYS.ARROW_DOWN)
				{
					e.preventDefault();
				}
			});

			$(container).on('keyup', "#autocmplt_helper_" + container.attr('id'), function(e) {
				e.preventDefault();

				var $cur_elem = $("#autocmplt_helper_" + container.attr('id')).find('a.selected');
				if ($cur_elem.length > 0)
				{
					if (e.keyCode == KEYS.ENTER)
					{
						var key = $cur_elem.data('key'),
							value = $cur_elem.data('value');
						autocmplt.addItem({
													'key'  : key,
													'value': value
												});


					}

					if (e.keyCode == KEYS.ARROW_DOWN && $cur_elem.next().length > 0)
					{
						$cur_elem.next().addClass('selected');
						$cur_elem.removeClass('selected');
					}
					else
						if (e.keyCode == KEYS.ARROW_UP && $cur_elem.prev().length > 0)
						{
							$cur_elem.prev().addClass('selected');
							$cur_elem.removeClass('selected');
						}
				}
			});

			$(document).on('mouseup', function(e) {
//      console.log(e.target)


				if ($(container).has(e.target).length === 0)
				{
					$('.autocmplt_helper').hide();
				}


			})

			$itemsBlock.attr('id', 'autocmplt_' + autocmplt.options.container_id + '_items');

			//Добавляем блок, в котором будут лежать выбранные элементы
			container.append($itemsBlock);

			//Добавляем блок, в котором будет лежать input для ввода и всплывающая подсказка
			inputhelper_block.append($searchInput);
			//Создаем input для ввода данных
			container.append(inputhelper_block);
		}

		//Публичный метод добавления. Вызов через $('#element').trigger('add', {'key': 12, 'value': 'Title'});
		$(container).on('add', function(event, data) {
//			autocmplt.addItem($('<div data-key="'+data.key+'" data-value="'+data.value+'"></div>') );
			autocmplt.addItem(data);
		});

		//Публичный метод удаления. Вызов через $('#element').trigger('delete', {'key': 12});
		$(container).on('delete', function(event, data) {
			$itemsBlock.find('.autocmplt_item[data-key=' + data.key + ']').remove();
		});


		//Добавляем
		this.addItem = function(data) {

			//console.log(data);

			var item = $('<div class="autocmplt_item"></div>');

			var item_content = $('<span />');

			var value;
			if (autocmplt.options.param == 'key')
			{
				value = data.key;
			}
			else
			{
				value = data.value;
			}

			var hidden_item = $('<input />', {
				'type' : 'hidden',
				'name' : autocmplt.options.hidden_element_name,
				'value': value
			});


			var del_item = $('<a/>', {
				'href' : '#',
				'class': 'autocmplt_item_delete'
			});

			item.attr({
							 'data-key'  : data.key,
							 'data-value': data.value
						 });

			item_content.append(decodeURIComponent(data.value)).append(del_item);

			//При клике на del_item должно происходить удаление
			del_item.click(function(e) {
				e.preventDefault();
				autocmplt.removeItem($(this))
			});

			item.append(item_content).append(hidden_item);

			$itemsBlock.append(item);

			container.removeClass('active');

			autocmplt.checkItems();
		}

		//Удаляем
		this.removeItem = function(t) {
			t.closest('.autocmplt_item').remove();

			autocmplt.checkItems();
		}

		//Проверяет количество уже добавленных итемов
		this.checkItems = function() {
			//Скрываем базовый инпут, если добавлено больше лимита
			if ($itemsBlock.find('.autocmplt_item').length >= autocmplt.options.max_items)
			{
				$searchInput.hide();
			}
			else
			{
				$searchInput.show();
			}

			//Скрываем блок подсказки
			if (!container.hasClass('active'))
			{
				$('#autocmplt_helper_' + container.attr('id')).remove();
			}

			$searchInput.val('');

		}

		//Отображает блок подсказки
		this.showHelper = function(ret) {
			//delete old helper
			$('#autocmplt_helper_' + container.attr('id')).remove();

			//Всплывающий хелпер
			var helper = $('<div id="autocmplt_helper_' + container.attr('id') + '" class="autocmplt_helper"></div>');

			for (k in ret.highlighted)
			{

				var helper_element = $('<a/>', {
					'class'     : 'autocmplt_helper_element',
					'data-key'  : encodeURIComponent(ret.keys[k]),
					'data-value': encodeURIComponent(ret.values[k]),
					'href'      : '#',
					'html'      : ret.highlighted[k]
				});

				//При клике на элемент должно происходить добавление в список
				helper_element.click(function(e) {
					e.preventDefault();
					autocmplt.addItem({
												"key"  : $(this).data('key'),
												"value": decodeURIComponent($(this).data('value'))
											});
				});

				// Первый элемент по-умолчанию выделен
				/*
				 if (helper_element_selected == undefined){
				 var helper_element_selected = true;
				 helper_element.addClass('selected');
				 }*/
				helper.append(helper_element);
			}

			inputhelper_block.append(helper);
		}

		this.ucFirstAllWords = function(str) {
			var pieces = str.split(" ");
			for (var i = 0; i < pieces.length; i++)
			{
				var j = pieces[i].charAt(0).toUpperCase();
				pieces[i] = j + pieces[i].substr(1);
			}
			return pieces.join(" ");
		}
		this.init();
		return this;
	}
})(jQuery);