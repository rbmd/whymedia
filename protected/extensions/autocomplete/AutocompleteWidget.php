<?php
class AutocompleteWidget extends CInputWidget
{
	public $data;
	public $url = false;
	public $maxItems = 50;
	public $allowNewItems = false;

	public function init()
	{
		parent::init();

		$this->registerClientScript();
		$this->render('index', array(
			'id'            => 'autocomplete_' . md5(microtime()),
			'data'          => $this->data,
			'name'          => $this->name,
			'param'         => ($this->allowNewItems) ? 'value' : 'key',
			'url'           => $this->url,
			'maxItems'      => $this->maxItems,
			'allowNewItems' => $this->allowNewItems,
		));
	}


	// Register CSS and Script.
	protected function registerClientScript()
	{
		$assets  = dirname(__FILE__) . '/assets';
		$baseUrl = Yii::app()->assetManager->publish($assets);
		Yii::app()->clientScript->registerCssFile($baseUrl . '/autocmplt.css');
		Yii::app()->clientScript->registerScriptFile($baseUrl . '/autocmplt.js', CClientScript::POS_BEGIN);
		Yii::app()->clientScript->registerScriptFile('/static/js/admin/jquery-ui.min.js');
	}
}