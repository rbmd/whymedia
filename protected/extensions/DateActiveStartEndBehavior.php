<?php
/**
 * Бихейвер добавляет возможность скорректировать время кеширования списка объектов, исходя из того, что у каждого
 * из них есть время начала показа и время конца показа на сайте.
 *
 * User: Dmitry Rusakov
 * Date: 28.04.12
 * Time: 18:45
 */
class DateActiveStartEndBehavior extends CActiveRecordBehavior
{
    /**
     * Очень большое целое число, которое будет трактоваться как бесконечное время кеширования.
     */
    const BIG_INT = 1000000000;

    /**
     * @var string - поле объекта, в котором хранится дата начала показа сущности на сайте
     */
    public $date_active_start = 'date_active_start';

    /**
     * @var string - поле объекта, в котором хранится дата завершения показа сущности на сайте
     */
    public $date_active_end = 'date_active_end';

    /**
     * @var CDbCriteria - критерий, которому должны удовлетворять объекты для того, чтобы уже можно было отслеживать
     * время их показа. Грубо говоря, если у объекта снята галка активность, то и смотреть время начала/конца его показа
     * бесполезно, так как он всё равно неактивен.
     */
    public $condition = null;

    /**
     * Скорректировать время кеширования для сущностей той модели, к которой подключен бихейвер на основе анализа
     * сущностей время отображения которых ещё не наступило и сущностей время отображения на сайте которых завершается
     *
     * @param $desired_time_to_cache  - желаемое время на которое требуется закешировать массив объектов
     * @return int - возвращает время на которое в действительности можно закешировать данный массив объектов
     */
    public function correctCacheTime($desired_time_to_cache, $new_condition=null)
    {
        if($desired_time_to_cache === iMemCache::CACHE_DURATION_INFINITY)
        {
            $desired_time_to_cache = self::BIG_INT;
        }

        $table = $this->owner->tableName();

        $ext = $new_condition === null ? $this->condition : $new_condition;
        $ext = !empty($ext) ? " AND id IN (SELECT id FROM `$table` WHERE $ext)" : '';

        $connection = Yii::app()->db;

        $sql = "SELECT min(t.k)
                FROM
                (
                    SELECT (UNIX_TIMESTAMP({$this->date_active_start}) - UNIX_TIMESTAMP(NOW())) as k
                    FROM `$table`
                    WHERE
                      (date_active_start IS NOT NULL) AND
                      (UNIX_TIMESTAMP({$this->date_active_start}) - UNIX_TIMESTAMP(NOW())) > 0 AND
                      (UNIX_TIMESTAMP({$this->date_active_start}) - UNIX_TIMESTAMP(NOW()) < $desired_time_to_cache)
                      $ext

                    UNION

                    SELECT (UNIX_TIMESTAMP({$this->date_active_end}) - UNIX_TIMESTAMP(NOW())) as k
                    FROM `$table`
                    WHERE
                      (date_active_end IS NOT NULL) AND
                      (UNIX_TIMESTAMP(NOW()) > UNIX_TIMESTAMP({$this->date_active_start})) AND
                      (UNIX_TIMESTAMP({$this->date_active_end}) >= UNIX_TIMESTAMP(NOW())) AND
                      (UNIX_TIMESTAMP({$this->date_active_end}) < UNIX_TIMESTAMP(NOW() + interval $desired_time_to_cache second))
                      $ext
                ) t";

        $command = $connection->createCommand($sql);

        $result = $command->queryScalar();

        //Если у нас нет ни одного объекта, который надо показать/скрыть в ближайшее время < желаемого, то используем
        //жеаемое время кеширования.
        if( $result === null )
        {
            $result = $desired_time_to_cache;
        }
        //Если у нас получилось так, что некий объект надо показать/скрыть через 0 секунд, то надо вернуть 1,
        //чтобы мемкешед не закешировал на вечно(у него 0 - это вечное кеширование).
        elseif( $result == 0)
        {
            $result = 1;
        }
        //Если кеш надо будет сбросить через время меньшее, чем желаемое, то приведём полученный резкльтат к инту
        else{
            $result += 0;
        }

        return ($result === self::BIG_INT) ? iMemCache::CACHE_DURATION_INFINITY : $result;
    }
}