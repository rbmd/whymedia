
<select <?=CHtml::renderAttributes($this->htmlOptions)?> >
	<option value="">-- Выбрать раздел --</option>
    <? foreach($tree as $t) : ?>
        <option value="<?=$t->id?>" <? if($t->id == $this->model->$attribute) : ?>selected="selected"<? endif ?> >
            <?=str_repeat('&nbsp;&nbsp;&nbsp;&nbsp;', $t->clevel - $base_level - 1)?>
            <?=$t->name?>
        </option>
    <? endforeach ?>
</select>
