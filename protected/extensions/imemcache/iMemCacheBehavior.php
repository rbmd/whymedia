<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Dmitry Rusakov
 * Date: 24.04.12
 * Time: 16:51
 *
 * Бихейвер, который надо подключить к моделе, чтобы она сбрасывала ключи в кеше, исходя из зависимостей,
 * которые определены с помощью iMemCache->addDependency()
 *
 */
class iMemCacheBehavior extends CActiveRecordBehavior
{
    const CLONE_OBJECT_METHOD_NAME = 'cloneObject';

    /**
     * @var $old_object - значение старого объекта (до сохранения)
     */
    protected $_old_object = null;

    /**
     * @var string - имя поля, которое является уникальным идентификатором
     * для объектов модели к которой подключаем бихейвер
     */
    public $entity_id = 'id';

    /**
     * @return - получение старого значения объекта (до сохранения)
     */
    public function getOldObject()
    {
        return $this->_old_object;
    }

    /**
     * Вспомогателная функция правильного клонирования объекта.
     *
     * Алгоритм работы: если объек реализует свою собсвенную стратегию клонирования, то используем её,
     * в противном случае используем стратегию клонирования по умолчанию.
     *
     * @param $object
     * @return mixed|null
     */
    private function _duplicate($object)
    {
		 return;
        if( empty($object->id) )
        {
            return null;
        }

        //Если объект реализут свою собсвенную стратегию клонирования, то используем её.
        if( method_exists($object, self::CLONE_OBJECT_METHOD_NAME) )
        {
            return call_user_func_array ( array($object, self::CLONE_OBJECT_METHOD_NAME) , array() );
        }

        //В противном случае сначала клонируем объенкт средствами PHP,
        $new_object = clone $object;
        //а затем клонируем все атрибуты ActiveRecord.
        $new_object->attributes = $object->attributes;

        return $new_object;
    }

    /**
     * Обработчик события чтения объекта из БД
     * @param CEvent $event
     */
    public function afterFind($event)
    {
        parent::afterFind($event);

        //Сохраняем изначальный объект(до его возможного пересохранения)
        $this->_old_object = $this->_duplicate( $event->sender );
    }

    /**
     * Обработчик сохранения и добавления
     * @param $event
     */
    public function afterSave($event)
    {
        parent::afterSave($event);

        $model_name = get_class($event->sender);

        $id = $this->owner->getAttribute($this->entity_id);

        Yii::app()->cache->destroyCacheBasedOnDependencies(
            $model_name,
            array($id),
            $event->sender
        );

        //Если это не вновь созданный объект, у которого просто не может быть "старого значения"(до сохранения),
        //то обновляем изначальное состояние.
        if( !$event->sender->isNewRecord ){
            //Сохраняем изначальный объект(до его возможного пересохранения)
            $this->_old_object = $this->_duplicate( $event->sender );
        }
    }

    /**
     * Обработчик удаления
     * @param $event
     */
    public function afterDelete($event)
    {
        parent::afterDelete($event);

        $model_name = get_class($event->sender);

        $id = $this->owner->getAttribute($this->entity_id);

        Yii::app()->cache->destroyCacheBasedOnDependencies(
            $model_name,
            array($id),
            $event->sender
        );
    }
}