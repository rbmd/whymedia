<?php
// Запускать так:
// /usr/local/bin/php /home/bg.ru/www/protected/console.php blogs updateinstagram
// /usr/local/bin/php /home/bg.ru/www/protected/console.php blogs updateviewspost
class BlogsCommand extends CConsoleCommand
{
	public $user;

	// обновим посты из инстаграма
	public function actionUpdateInstagram()
	{
		$timestart = microtime(true);
		Yii::app()->setImport(array(
			'application.modules.blogs.components.*',
			'application.modules.blogs.models.*'
		));

		$instagram = new BlogsInstagram();
		$regions = Yii::app()->db->createCommand()->select('id, instagram_tags')->from('blogs_regions')->queryAll();
		foreach ($regions as $region)
		{
			$tags = explode(',', $region['instagram_tags']);
			foreach ($tags as $tag)
			{
				$tag = trim($tag);
				if ($tag)
				{
					echo $region['id'] . ' - ' . $tag . "\n";
					$instagram->addPost($tag, $region['id']);
				}
			}
		}
		$this->downloadImages();
		echo round(microtime(true) - $timestart, 4);
	}

	// загрузим незагруженные картинки из инстаграма
	public function downloadImages()
	{
		$posts = BlogsPostAdmin::model()->findAll('instagram_id IS NOT NULL AND cover=0 AND comment != ""');
		if ($posts)
		{
			echo "Загрузим картинки\n";
			foreach ($posts as $post)
			{
				$post->saveImage($post->comment);
				echo $post->id . "\n";
			}
		}
	}


// обновим количество просмотров и комментариев к постам
	public
	function actionUpdateViewsPost()
	{
		$time = microtime(true);
		$list = Articles::model()->findAll('link LIKE "http://bg.ru/blogs/posts/%" OR link LIKE "http://bg.ru/bestsummer2013/events/%"');
		foreach ($list as $item)
		{
			if (preg_match('#http://bg.ru/(blogs|bestsummer2013).+?([0-9]+)#', $item->link, $aLink))
			{
				$entity_name = ($aLink[1] == 'blogs') ? 'Blogs' : 'BestSummer';
				$entity_id   = $aLink[2];
				$res         = Yii::app()->db->createCommand('SELECT views,views_all FROM bg_views_full WHERE entity_name="' . $entity_name . '" AND element_id=' . $entity_id)->queryRow();
				if ($res)
				{
					$comments = intval(Comments::model()->count('entity_name=:name AND entity_id=:id AND active=1', array(
						'name' => $entity_name,
						'id'   => $entity_id
					)));
					Articles::model()->updateByPk($item->id, array('comments_count' => $comments));
					echo $item->id . '  ' . $item->link . ' - ' . $res['views'] . ' - ' . $comments . "\n";
					if ($ee = Yii::app()->db->createCommand('SELECT COUNT(*) FROM bg_views_full WHERE entity_name="Articles" AND element_id=' . $item->id)->queryScalar())
					{
						Yii::app()->db->createCommand()->update('bg_views_full', array(
							'views'     => $res['views'],
							'views_all' => $res['views_all']
						), 'entity_name="Articles" AND element_id=' . $item->id);
					}
					else
					{
						Yii::app()->db->createCommand()->insert('bg_views_full', array(
							'views'       => $res['views'],
							'views_all'   => $res['views_all'],
							'entity_name' => 'Articles',
							'element_id'  => $item->id
						));
					}
				}
			}
		}
		echo "\n\n";
		echo microtime(true) - $time;
		echo "\n\n";
	}
}