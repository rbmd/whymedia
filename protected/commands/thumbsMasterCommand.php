<?
// /usr/bin/php /home/kp1.codemedia.ru/www/protected/console.php thumbsmaster update

class ThumbsMasterCommand extends CConsoleCommand
{
	public function actionUpdate()
	{
		$lock = Yii::app()->cache->get("ThumbsMasterCommandLock");

		if ($lock !== false)
			exit(0);

		Yii::app()->cache->set("ThumbsMasterCommandLock", 1, 60);


		echo "\n\r======= ThumbsMasterCommand->actionUpdate started =======\n\r";

		$tasks = ThumbsMasterTask::model()->findAll(array('condition' => 'attempts < 60', 'limit' => 100, 'order' => 'attempts ASC, id ASC'));
		foreach ($tasks as $t)
		{
			$t->apply();
		}

		echo "\n\r======= ThumbsMasterCommand->actionUpdate finished =======\n\r";
	}
}
?>