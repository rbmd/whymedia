<!DOCTYPE html>
<html>
<head>
	<? $reset = '14'; ?>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, minimal-ui">
	<meta name="keywords" content="">
	<meta name="description" content="">
	<meta name="google-site-verification" content="WpycnwvF1L5LjN95SfHT3I8i4-Cu3MoF7Rj_vQ-3qdU" />
	<title><?=$this->pageTitle?></title>

	<link rel="stylesheet" type="text/css" href="/static/css/app.css?<?= $reset ?>">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="/static/css/social-likes_flat.css?<?= $reset ?>">
	<link rel="icon" type="image/png" href="/favicon.png?<?= $reset ?>" />

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script type="text/javascript" src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
	<script src="/static/js/snap.svg-min.js"></script>
	<script src="/static/js/skrollr.min.js"></script>
	<script src="/static/js/jquery.history.js"></script>
	<script src="/js/social-likes.min.js"></script>
	<script src="/static/js/app.js?<?= $reset ?>"></script>

	<script src="https://use.typekit.net/zsa6pao.js"></script>
	<script>try{Typekit.load({ async: true });}catch(e){}</script>

	<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	ga('create', 'UA-70247028-1', 'auto');
	ga('send', 'pageview');

	</script>


</head>