<div class="footer">
	<a href="/" class="footer__logo" data-svg="/static/svg/y.svg"></a>
	<div class="footer__copy">
		<?=$this->inter['copyright'][$this->lang]?>
	</div>

	<div class="footer__contacts">
		<div class="footer__contacts__item">+7 915 263–38–33</div>
		<a href="mailto:whydirect@gmail.com" class="footer__contacts__item">whydirect@gmail.com</a>
		<div class="footer__contacts__item">
			<a href="http://facebook.com/whymedia"><i class="fa fa-facebook"></i></a>
			<a href="http://www.youtube.com/user/whytapes"><i class="fa fa-youtube"></i></a>
		</div>
	</div>
</div>