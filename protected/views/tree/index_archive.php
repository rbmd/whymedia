<div class="wrap">
	<div class="grid">
		<div class="grid-flow">
		<?
		foreach($materials as $m)
		{
			{
				?>
				<a href="<?=$m->url()?>" class="grid-item grid-item--flow">
					<span class="grid-item__date"><?=Yii::app()->dateFormatter->format('d MMMM', $m->date_active_start)?></span>
					<span class="grid-item__name"><?=$m->name?></span>
				</a>
				<?
			}
		}
		?>
		</div>
	</div>
</div>
<?
$this->widget('Pagination',
				array(
					'pages' => $pages,
					'index_page' => true,
				)
			);
?>