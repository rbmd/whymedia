<?
$pattern = array('tile--3x', 'tile--1x', 'tile--1x', 'tile--1x', 'tile--1x', 'tile--3x', 'tile--2x', 'tile--2x');
?>

<meta property="og:title" content="<?= !empty($detail) ? $detail['work']['name'] : 'Why.media' ?>">
<meta property="og:description" content="<?= !empty($detail) ? $detail['work']['detail_text'] : '' ?>">
<meta property="og:image" content="<?= !empty($detail) ? 'http://why.company' . $detail['work']['pic'] : '' ?>">

<script>
	window.lang_pref = '<?=$lang_pref?>';
	var pattern = <?= json_encode($pattern) ?>;
</script>

<a id="index"></a>
<div class="menu menu--transparent" 
	data-anchor-target=".screen--layout" 
	data-bottom-top="@class: menu menu--transparent" 
	data-100-top="@class: menu">

	<a href="<?= $lang_pref ?>/" 
		data-index
		data-anchor-target="#index"
		data-top="@class: menu__logo menu__link--push menu__link--active"
		data--100-top="@class: menu__logo menu__link--push"
		class="menu__logo menu__link--push" data-svg="/static/svg/y.svg"></a>

	<div class="menu__trigger">
		<i class="fa fa-bars"></i>
	</div>
	
	<div class="menu__filter">
		<div class="menu__link menu__link--active menu__link--ondark" data-type=""><?=$inter['all'][$lang]?></div>

		<?foreach($rubrics as $rubric){?>
			<div class="menu__link menu__link--ondark" data-type="<?=$rubric->{'name_'.$lang}?>"><?=$rubric->{'name_'.$lang}?></div>
		<?}?>
		<?/*
		<div class="menu__link menu__link--ondark" data-type="ADVERT"><?=$inter['advert'][$lang]?></div>
		<div class="menu__link menu__link--ondark" data-type="EVENT"><?=$inter['event'][$lang]?></div>
		<div class="menu__link menu__link--ondark" data-type="AXTIVISM">Axtivism</div>
		*/?>
	</div>


	<div class="menu__pages"
		data-anchor-target="#index"
		data-top="@class: menu__pages menu__pages--off"
		data--100-top="@class: menu__pages"
		<?/*
		data-anchor-target="#showreel"
		data-bottom-top="@class: menu__pages menu__pages--off"
		data-top-bottom="@class: menu__pages"
		*/?>
		>

		<a href="<?= $lang_pref ?>/showreel/" 
			data-showreel
			data-anchor-target=".screen--showreel"
			data-center-top="@class: menu__link menu__link--push menu__link--active"
			data-center-bottom="@class: menu__link menu__link--push"
			data-bottom-top="@class: menu__link menu__link--push"
			class="menu__link menu__link--push"><?=$inter['showreel'][$lang]?></a>
		<a href="<?= $lang_pref ?>/works/" 
			data-work
			data-anchor-target=".screen--layout"
			data-center-top="@class: menu__link menu__link--push menu__link--active"
			data-center-bottom="@class: menu__link menu__link--push"
			data-bottom-top="@class: menu__link menu__link--push"
			class="menu__link menu__link--push"><?=$inter['work'][$lang]?></a>
		<a href="<?= $lang_pref ?>/contacts/" 
			data-contacts
			class="menu__link menu__link--push"><?=$inter['contacts'][$lang]?></a>
		<div class="menu__link menu__link--lang menu__link--hidden">
			<?=$lang?>
			<div class="menu__link--lang__languages">
				<?
				if ($lang != 'en')
				{
					?><a href="/en/">EN</a><?
				}
				?>

				<?
				if ($lang != 'ge')
				{
					?><a href="/ge/">GE</a><?
				}
				?>

				<?
				if ($lang != 'ru')
				{
					?><a href="/">RU</a><?
				}
				?>
			</div>
		</div>
	</div>
</div>


<div class="contact contact--hidden">
	<div class="contact__close" onclick="contactsBack();"></div>
	<div class="contacts__mob-menu">
		<div class="contacts__mob-menu__item contacts__mob-menu__item--active" data-target="contact__contact"><?=$inter['contacts'][$lang]?></div>
		<div class="contacts__mob-menu__item" data-target="contact__team"><?=$inter['about'][$lang]?></div>
	</div>
	<div class="contact__team contact-hidden">
		<div class="contact__team__head"><?=$inter['about'][$lang]?></div>
		<?#=$pages['about']['text_'.$lang]->content?>
		
		<?
		if($lang == 'ge')
			$about_lang = 'en';
		else
			$about_lang = $lang;
		?>

		<div class="contact__team__text">
			<?=$pages['about']['text_'.$lang]->content?>
		</div>

		<div class="contact__team__people">
			<div class="teammate">
				<?if(strlen($pages['about']['photo1']->content) > 0){?>
					<div class="teammate__photo" data-image="<?=$pages['about']['photo1']->content?>"></div>
				<?} else {?>
					<div class="teammate__photo" data-image="<?=$pages['about']['photo1']->content?>">
						<video preload="false" loop="loop">
							<source src="<?=$pages['about']['photo1_mp4']->content?>" type="video/mp4">
							<source src="<?=$pages['about']['photo1_webm']->content?>" type="video/webm">
						</video>
					</div>
				<?}?>
				<?=$pages['about']['photo1_text_'.$about_lang]->content?>
				
			</div>

			<div class="teammate">
				<?if(strlen($pages['about']['photo2']->content) > 0){?>
					<div class="teammate__photo" data-image="<?=$pages['about']['photo2']->content?>"></div>
				<?} else {?>
					<div class="teammate__photo" data-image="<?=$pages['about']['photo2']->content?>">
						<video preload="false" loop="loop">
							<source src="<?=$pages['about']['photo2_mp4']->content?>" type="video/mp4">
							<source src="<?=$pages['about']['photo2_webm']->content?>" type="video/webm">
						</video>
					</div>
				<?}?>
				<?=$pages['about']['photo2_text_'.$about_lang]->content?>
				
			</div>
		</div>
		
	</div>
	<div class="contact__contact">
		<div class="contact__contact__head"><?=$inter['contacts'][$lang]?></div>
		<div class="contact__contact__text">
			<?=$pages['contacts']['text_'.$lang]->content?>
		</div>
		<div class="contact__contact__soc">
			<a href="http://facebook.com/whymedia"><i class="fa fa-facebook"></i></a>
			<a href="http://vk.com/whymedia"><i class="fa fa-vk"></i></a>
			<a href="http://www.youtube.com/user/whytapes"><i class="fa fa-youtube"></i></a>
		</div>
	</div>
</div>


<?
if (!empty($detail))
{
	?>
	<script>
	var work_page = 1;
	</script>
	<div class="work">
		<div class="work__detail" data-image="<?=$detail['work']['pic']?>">
			<div class="work__wrap">
				<div class="work__detail__video work__detail__video--inactive">
					<div class="work__close-video"></div>
					<iframe width="100%" height="100%" src="" frameborder="0" allowfullscreen></iframe>
				</div>
				<div class="work__detail__label"><?= $detail['work']['rubric'] ?></div>
				<div class="work__detail__name"><?= $detail['work']['name'] ?></div>
				<?
				if (!empty($detail['work']['video']))
				{
					?>
					<div class="work__detail__play" data-src="https://www.youtube.com/embed/<?= $detail['work']['video'] ?>?rel=0&amp;controls=1&amp;showinfo=0&amp;autoplay=1"></div>
					<?
				}
				else if (!empty($detail['work']['video_vimeo']))
				{
					?>
					<div class="work__detail__play" data-src="https://player.vimeo.com/video/<?= $detail['work']['video_vimeo'] ?>?autoplay=1&title=0&byline=0&portrait=0"></div>
					<?
				}
				?>
				<div class="work__share">
					<span class="work__share__text">Share:</span>

					<div class="social-likes" data-counters="no">
					    <div class="facebook" title="Share link on Facebook"></div>
					    <div class="vkontakte" title="Share link on Vkontakte"></div>
					</div>
				</div>
			</div>
			<div class="work__info">
				<div class="work__info__col">
					<div class="work__info__title">
						<?=$inter['about'][$lang]?>:
					</div>
					<div class="work__info__text">
						<?= $detail['work']['detail_text'] ?>
					</div>
				</div>

				<div class="work__info__col <?= empty($detail['work']['client_name']) ? 'work__info__col--hidden' : '' ?>">
					<div class="work__info__title">
						<?= $inter['client'][$lang]?>:
					</div>
					<div class="work__info__text">
						<a href="<?= $detail['work']['client_url'] ?>"><?= $detail['work']['client_name'] ?></a>
					</div>
				</div>
			</div>
		</div>
		<a href="<?= $lang_pref ?>/works/" class="work__back"><?=$inter['back_to_works'][$lang]?></a>
		<div class="work__more"><?=$inter['more_projects'][$lang]?></div>
		<div class="work__tiles">
			<div class="tiles">
				<?
				if (!empty($detail['prev']))
				{
					?>
					<a href="<?= $lang_pref ?>/works/<?=$detail['prev']['code']?>/" class="tile tile--2x" data-image="<?=$detail['prev']['pic']?>">
						<video preload="false" loop="loop" class="tile__video">
							<source src="<?= $detail['prev']['video_mp4'] ?>" type="video/mp4">
							<source src="<?= $detail['prev']['video_webm'] ?>" type="video/webm">
						</video>
						<span class="tile__action"><?=$inter['previous'][$lang]?></span>
						<span class="tile__label"><?= $detail['prev']['rubric'] ?></span>
						<span class="tile__name"><?= $detail['prev']['name'] ?></span>
					</a>
					<?
				}
				?>
				<?
				if (!empty($detail['next']))
				{
					?>
					<a href="<?= $lang_pref ?>/works/<?=$detail['next']['code']?>/" class="tile tile--2x" data-image="<?=$detail['next']['pic']?>">
						<video preload="false" loop="loop" class="tile__video">
							<source src="<?= $detail['next']['video_mp4'] ?>" type="video/mp4">
							<source src="<?= $detail['next']['video_webm'] ?>" type="video/webm">
						</video>
						<span class="tile__action"><?=$inter['next'][$lang]?></span>
						<span class="tile__label"><?= $detail['next']['rubric'] ?></span>
						<span class="tile__name"><?= $detail['next']['name'] ?></span>
					</a>
					<?
				}
				?>
			</div>
		</div>
	</div>
	<?
}
else
{
	?>
	<div class="work work--hidden">
		<div class="work__detail" data-image="">
			<div class="work__wrap">
				<div class="work__detail__video work__detail__video--inactive">
					<div class="work__close-video"></div>
					<iframe width="100%" height="100%" src="" frameborder="0" allowfullscreen></iframe>
				</div>
				<div class="work__detail__label"></div>
				<div class="work__detail__name"></div>
				<div class="work__detail__play" data-src=""></div>
				<div class="work__share">
					<span class="work__share__text">Share:</span>

					<div class="social-likes" data-counters="no">
					    <div class="facebook" title="Share link on Facebook"></div>
					    <div class="vkontakte" title="Share link on Vkontakte"></div>
					</div>
				</div>
			</div>
			<div class="work__info">
				<div class="work__info__col">
					<div class="work__info__title">
						<?=$inter['about'][$lang]?>:
					</div>
					<div class="work__info__text">
					</div>
				</div>
				<div class="work__info__col">
					<div class="work__info__title">
						<?=$inter['client'][$lang]?>:
					</div>
					<div class="work__info__text">
						<a href=""></a>
					</div>
				</div>
			</div>
		</div>
		<a href="<?= $lang_pref ?>/works/" class="work__back"><?=$inter['back_to_works'][$lang]?></a>
		<div class="work__more"><?=$inter['more_projects'][$lang]?></div>
		<div class="work__tiles">
			<div class="tiles">
				<a href="" class="tile tile--2x" data-image="">
					<video preload="false" loop="loop" class="tile__video">
						<source src="" type="video/mp4">
						<source src="" type="video/webm">
					</video>
					<span class="tile__action"><?=$inter['previous'][$lang]?></span>
					<span class="tile__label"></span>
					<span class="tile__name"></span>
				</a>
					
				<a href="" class="tile tile--2x" data-image="">
					<video preload="false" loop="loop" class="tile__video">
						<source src="" type="video/mp4">
						<source src="" type="video/webm">
					</video>
					<span class="tile__action"><?=$inter['next'][$lang]?></span>
					<span class="tile__label"></span>
					<span class="tile__name"></span>
				</a>
			</div>
		</div>
	</div>
	<?
}
?>


<div class="screens">
	<div class="screen <?= !empty($detail) ? 'screen--hidden' : '' ?> screen--intro screen--intro-loading">
		<div class="screen--intro__why" data-svg="/static/svg/why.svg"></div>
		<div class="screen--intro__y">
			<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
				 width="283.464px" height="283.465px" viewBox="0 0 283.464 283.465" xml:space="preserve">
				<g>
					<path fill="#ED1A3B" d="M196.153,171.751c12.453,0,22.09,10.805,22.09,22.089c0,12.926-9.164,23.977-22.09,23.977
						c-14.338,0-23.982-10.817-23.982-23.977C172.171,182.556,181.815,171.751,196.153,171.751z"/>
					<path fill="#fff" d="M207.388,65.647l-53.904,90.212v61.957h-34.787v-61.957L65.222,65.647h39.773l32.609,57.608l32.182-57.608
						H207.388z"/>
				</g>
			</svg>
		</div>
		<div class="screen--intro__text">
			<div class="screen--intro__text__m">
				<?=$pages['main']['text_'.$lang]->content?>
			</div>
		</div>
		<div class="screen--intro__scroll">scroll</div>
	</div>


	<a id="showreel"></a>
	<div class="screen <?= !empty($detail) ? 'screen--hidden' : '' ?> screen--showreel">
		<div class="showreel" 
			data-anchor-target=".screen--showreel" 
			data-bottom-top="@class: showreel" 
			data-bottom="@class: showreel showreel--relative" 
			data-top-bottom="@class: showreel showreel--relative showreel--offscreen"
			<?= !empty($pages['showreel']['photo_background']->content) ? 'data-image="'.$pages['showreel']['photo_background']->content.'"' : '' ?>>

			<div class="showreel__video showreel__video--inactive">
				<div 
					data-anchor-target=".screen--intro"
					data-bottom-top="opacity: 1.0;" 
					data-bottom="opacity: 0.0;" 
					data-top-bottom="opacity: 1.0;"
					class="showreel__watch">
					<?=$pages['showreel']['text_'.$lang]->content?>
				</div>
				<div class="showreel__close"></div>
				<div class="showreel__video__play" 
					data-anchor-target=".screen--intro"
					data-bottom-top="opacity: 1.0;" 
					data-bottom="opacity: 0.0;" 
					data-top-bottom="opacity: 1.0;"
					data-src="https://www.youtube.com/embed/<?=$pages['showreel']['video']->content?>?rel=0&amp;controls=1&amp;showinfo=0&amp;autoplay=1"></div>
				<?
				if (!empty($pages['showreel']['video_mp4']->content) && !empty($pages['showreel']['video_webm']->content))
				{
					?>
					<video preload="true" loop="loop" autoplay="1">
						<source src="<?=$pages['showreel']['video_mp4']->content?>?>" type="video/mp4">
						<source src="<?=$pages['showreel']['video_webm']->content?>" type="video/webm">
					</video>
					<?
				}
				?>
				<iframe width="100%" height="100%" src="" frameborder="0" allowfullscreen></iframe>
			</div>
		</div>
	</div>


	<a id="works"></a>
	<div class="screen <?= !empty($detail) ? 'screen--hidden' : '' ?> screen--layout">
		<div class="tiles">
			<?
			$i = 0;
			foreach ($works as $work)
			{
				?>
				<a href="<?= $lang_pref ?>/works/<?=$work->code?>/" 
					class="tile <?= $pattern[$i] ?> tile--offscreen" 
					data-type="<?= $work->rubric->{'name_'.$lang} ?>"
					data-150-bottom-top="@class: tile <?= $pattern[$i] ?> tile--offscreen"
					data--100-bottom-top="@class: tile <?= $pattern[$i] ?>"
					data-orig-150-bottom-top="@class: tile <?= $pattern[$i] ?> tile--offscreen"
					data-orig--100-bottom-top="@class: tile <?= $pattern[$i] ?>"
					data-image="<?=$work->pic?>">
					<video preload="false" loop="loop" class="tile__video">
						<source src="<?= $work->cell_video_mp4 ?>" type="video/mp4">
						<source src="<?= $work->cell_video_webm ?>" type="video/webm">
					</video>
					<span class="tile__action"><?=$this->inter['watch_project'][$this->lang]?></span>
					<span class="tile__label"><?=$work->rubric->{'name_'.$lang}?></span>
					<span class="tile__name"><?=$work->{'name_'.$lang}?></span>
				</a>
				<?

				$i++;

				if (empty($pattern[$i]))
				{
					$i = 0;
				}
			}
			?>
		</div>


		<?/*
		<h1>SHOWREEL</h1>

		<?=$pages['showreel']['text_'.$lang]->content?>
		<br>
		youtube video id: <?=$pages['showreel']['video']->content?>


		<h1>WORKS</h1>

		<h1>ABOUT</h1>

		<?=$pages['about']['text_'.$lang]->content?>


		<h1>CONTACTS</h1>

		<?=$pages['contacts']['text_'.$lang]->content?>
		*/?>
	</div>

	<?= Yii::app()->controller->renderPartial('application.views.layouts.parts.footer') ?>
</div>