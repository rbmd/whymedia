<h1>Введите новый пароль</h1>

<div class="sign-up-form">

<?$form=$this->beginWidget('CActiveForm', array(
	'id' => 'changepwd-form',
	'enableAjaxValidation' => true,
	'enableClientValidation' => true,
	'action' => "/recovery/code/$code/",
	'clientOptions'=>array('validateOnSubmit'=>true, 'validateOnChange'=>true)
));
?>


<div class="form-wrapper">

	<div class="row">
		<label><?= $model->attributeLabels()['password'] ?> <sup>*</sup></label>
		<span class="field-wrapper">
			<?= $form->passwordField($model, 'password', array('class' => 'field', 'style' => 'width: 300px')) ?>
			<?= $form->error($model, 'password', array('class' => 'error', 'style' => 'width: 300px')) ?>
		</span>
	</div>


	<div class="row">
		<label><?= $model->attributeLabels()['password2'] ?> <sup>*</sup></label>
		<span class="field-wrapper">
			<?= $form->passwordField($model, 'password2', array('class' => 'field', 'style' => 'width: 300px')) ?>
			<?= $form->error($model, 'password2', array('class' => 'error', 'style' => 'width: 300px')) ?>
		</span>
	</div>


	<div class="row">
		<label for=""></label>
		<div class="field-wrapper">
			<?= CHtml::submitButton('Изменить пароль', array('class' => 'button _blue-button')) ?>
		</div>
	</div>
</div>

<?php $this->endWidget(); ?>

</div>