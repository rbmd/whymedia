<?php

class PollsController extends Controller
{
	public $layout = 'index';

	public function __construct($id,$module=null)
	{
		parent::__construct($id,$module);

		//Подрубаем css со свойствами для валидации полей формы создания лекции
		Yii::app()->clientScript->registerCssFile(
			Yii::app()->assetManager->publish(
				'static/css/form.css'
			)
		);
	}

    public function beforeRender($action) {
        return true;
    }
	
	public function actionIndex()
	{
	
        $criteria = new CDbCriteria();		
		$criteria->order = 'id DESC';
		
		$count = Polls::model()->count($criteria);
	 
		$pages = new CPagination($count);
		$pages->pageSize = 20;
		$pages->applyLimit($criteria);	
		
		$votes = Polls::model()->findAll($criteria);

		$this->render( 'index', array('votes' => $votes, 'pages' => $pages) );
		
	}
	
	public function actionCreate()
	{
		$model = new Polls();
		
		//Выдать сообщения об ошибке в случае аякс валидации
		if (Yii::app()->request->isAjaxRequest && Yii::app()->request->getParam('ajax') === 'votes-form')
		{
			echo CActiveForm::validate($model);

			Yii::app()->end();
		}
		
		if(isset($_POST['Polls']))
		{			
			$model->attributes = $_POST['Polls'];
			$model->voteitems = isset($_POST['PollItems']) ? $_POST['PollItems'] : array();
            $model->voteitemsanswers = isset($_POST['PollItemsAnswers']) ? $_POST['PollItemsAnswers'] : array();

			if ($model->save())
			{
				if( !isset($_POST['apply']) )
					$this->redirect("/admin/polls/");
				else
					$this->redirect('/admin/polls/edit/?id='.$model->id, true);
			}

		}
	
		$this->render('edit', array('model' => $model, 'action' => 'create') );
	}
	
	public function actionEdit($id)
	{
		if($id > 0)
		{
			$model = Polls::model()->FindByPk($id);

			//Выдать сообщения об ошибке в случае аякс валидации
			if (Yii::app()->request->isAjaxRequest && Yii::app()->request->getParam('ajax') === 'votes-form')
			{
				echo CActiveForm::validate($model);

				Yii::app()->end();
			}

			if(isset($_POST['Polls']))
			{
				$model->attributes = $_POST['Polls'];
				$model->voteitems = isset($_POST['PollItems']) ? $_POST['PollItems'] : array();
                $model->voteitemsanswers = isset($_POST['PollItemsAnswers']) ? $_POST['PollItemsAnswers'] : array();

				if($model->save())
				{
                    if( !isset($_POST['apply']) )
                    {
                        $this->redirect('/admin/polls/');
                    }
                    else{
                        $this->refresh();
                    }
				}
			}

			$this->render('edit', array('model' => $model, 'action' => 'edit' ) );
		}
	}

    /*
    * Удаление опроса
    */
    public function actionDelete()
    {
        if( Yii::app()->request->isAjaxRequest )
        {
            $id = Yii::app()->request->getParam('id');

            if( !empty($id) )
            {
                $transaction = Polls::model()->dbConnection->beginTransaction();

                try{
                    PollItems::model()->deleteAll('vote_id=:id', array(':id'=>$id));
                    PollVoices::model()->deleteAll('vote_id=:id', array(':id'=>$id));
                    Polls::model()->findByPk($id)->delete();

                    $transaction->commit();
                }
                catch(Exception $e)
                {
                    $transaction->rollBack();

                    throw $e;
                }
            }

            Yii::app()->end();
        }

        $this->redirect('/admin/polls/');
    }

    //Возвращает шаблон варианта ответа голосования при добавлении в админке конктретной голосовалки нового варианта
    public function actionAjaxAddPollItem()
    {
    	$item = new PollItems();
    	$item->id = md5(microtime(true));
    	$this->renderPartial('admin.views.polls.parts.item', array('item' => $item));
    }
}