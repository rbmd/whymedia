<?php

class RubricsController extends Controller
{
	public $layout = 'index';
	
	public function actionIndex()
	{
		$criteria = new CDbCriteria();
		$criteria->order = 'date_active_start DESC';
		
		$articles = Rubric::model()->findAll($criteria);

		

		$this->render('index', array('articles' => $articles, ) );
	}

	public function actionArticles()
	{
		

		$this->render('articles', array('articles' => $articles) );
	}
	
	public function actionCreate()
	{
		$model = new Rubric();
		
		if(isset($_POST['Rubric']))
		{			
			$model->attributes = $_POST['Rubric'];
			
			if ($model->save())
			{
				if( !isset($_POST['apply']) )
					$this->redirect("/admin/rubrics/");
				else
					$this->redirect('/admin/rubrics/edit/?id='.$model->id, true);
			}

		}
	
		$this->render('edit', array('model' => $model, 'action' => 'create') );
	}
	
	public function actionEdit($id)
	{
		if($id > 0)
		{
			$model = Rubric::model()->FindByPk($id);
			
			if(isset($_POST['Rubric']))
			{
				$model->attributes = $_POST['Rubric'];

				if($model->save())
				{
                    if( !isset($_POST['apply']) )
                    {
                    	Yii::app()->request->redirect('/admin/rubrics/');
                    }
                    else
					{
                        $this->redirect('/admin/rubrics/edit/?id='.$model->id, true);
                    }
				}
			}
			
			

			$this->render('edit', array('model' => $model, 'action' => 'edit'  ) );		
		}
	}


    public function actionDelete()
    {
        if( Yii::app()->request->isAjaxRequest )
        {
            $id = Yii::app()->request->getParam('id');

            if( !empty($id) )
            {
                $transaction = Rubric::model()->dbConnection->beginTransaction();

                try{
                    
                    Rubric::model()->findByPk($id)->delete();

                    $transaction->commit();
                }
                catch(Exception $e)
                {
                    $transaction->rollBack();

                    throw $e;
                }
            }

            Yii::app()->end();
        }

        $this->redirect('/admin/rubrics/');
    }
}