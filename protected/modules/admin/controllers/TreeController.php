<?
class TreeController extends Controller
{
	public $layout = 'index';

	/*
	 * Список категорий, запрещённых для удаления. Задается в конструкторе
	 */
	public $deny_to_delete = array(0); 

	
	public function __construct($id,$module=null)
	{
		parent::__construct($id,$module);
		Yii::app()->clientScript->registerScriptFile(Yii::app()->assetManager->publish('static/js/admin/wysiwyg_manager.js'), CClientScript::POS_END);
		
		//Подрубаем css со свойствами для валидации полей формы создания лекции
		Yii::app()->clientScript->registerCssFile(
			Yii::app()->assetManager->publish(
				'static/css/form.css'
			)
		);

		//Запрещаем удалять все ноды дерева, которые существовали до запуска проекта
		$this->deny_to_delete = range(0, 39);
	}

	


	
	public function actionIndex()
	{
		$t = Tree::model()->findAll(array('order' => 'cleft') );

		$this->render(
			'index',
			array(
				'tree' => $t,
				'deny_to_delete_list' => $this->deny_to_delete
			)
		);
	}
	
	public function actionCreate()
	{
		$model = new Tree();

		//Выдать сообщения об ошибке в случае аякс валидации
		if (Yii::app()->request->isAjaxRequest && Yii::app()->request->getParam('ajax') === 'tree-form')
		{
			echo CActiveForm::validate($model);

			Yii::app()->end();
		}

		if (isset($_POST['Tree']))
		{
			$parent_node = Tree::model()->findByPk($_POST['Tree']['parent_id']);
			
			if( !is_null($parent_node) && preg_match('/[a-z0-9_-]+/i', trim($_POST['Tree']['code'])) )
			{
				$new_node = new Tree;
				$new_node->name = $_POST['Tree']['name'];
				$new_node->code = trim($_POST['Tree']['code']);
				$new_node->preview_text = $_POST['Tree']['preview_text'];
				$new_node->url = $parent_node->url.'/'.$_POST['Tree']['code'];
				$new_node->seo_title = isset($_POST['Tree']['seo_title']) ? trim($_POST['Tree']['seo_title']) : '';
				$new_node->seo_keywords = isset($_POST['Tree']['seo_keywords']) ? trim($_POST['Tree']['seo_keywords']) : '';
				$new_node->seo_description = isset($_POST['Tree']['seo_description']) ? trim($_POST['Tree']['seo_description']) : '';

				$new_node->appendTo($parent_node);

				if( !isset($_POST['apply']) )
					$this->redirect("/admin/tree/");
				else
					$this->redirect('/admin/tree/edit/?id='.$new_node->id, true);
			}
			
		}

		$t = Tree::model()->findAll(array('order' => 'cleft') );
		$this->render(
			'edit',
			array(
				'tree' => $t,
				'action' => 'create',
				'parent_id' => -1,
				'model' => $model,
				'deny_to_delete_list' => $this->deny_to_delete
			)
		);
	}
	
	public function actionEdit($id)   
	{	
		$model = Tree::model()->FindByPk($id+0);

		if ( empty($model) )
			throw new CHttpException(404);

		//Выдать сообщения об ошибке в случае аякс валидации
		if (Yii::app()->request->isAjaxRequest && Yii::app()->request->getParam('ajax') === 'tree-form')
		{
			echo CActiveForm::validate($model);

			Yii::app()->end();
		}

		if (isset($_POST['Tree']) && $id > 0)
		{
			//Обновляем информацию
			$model->attributes = $_POST['Tree'];
			if ($id == 1) $model->code = '';

			//Меняем родителя
			$parent = Tree::model()->findByPk($_POST['Tree']['parent_id']);
			if ($id != $_POST['Tree']['parent_id'] && $model->parent->id != $_POST['Tree']['parent_id'])
			{
				$model->moveAsLast($parent);
			}

			if( $model->validate() )
			{
				$model->saveNode();

				if( !isset($_POST['apply']) )
				{
					$this->redirect('/admin/tree/');
				}
				else
				{
					$this->refresh();
				}
			}
		}

		//Получаем полный путь до текущего узла
		$descendants = $model->ancestors()->findAll();

		$t = Tree::model()->findAll(array('order' => 'cleft') );

		$this->render(
			'edit',
			array(
				'action' => 'edit',
				'tree' => $t,
				'model' => $model,
				'descendants' => $descendants,
				'parent_id' => ($model->parent) ? $model->parent->id : -1,
				'deny_to_delete_list' => $this->deny_to_delete
			)
		);
	}
	
	public function actionDeleteArticle($id)
	{
		if(in_array($id, $this->deny_to_delete))
		{
			$this->redirect('/admin/tree/');
		}
			
		$x = Tree::model()->findByPk($id);

		//Нельзя удалять узел, у которого есть дети
		if( !$x->isLeaf() )
		{
			$this->redirect('/admin/tree/');
		}
		
		if(!is_null($x))
		{
			$x->deleteNode();
			$this->redirect('/admin/tree/');
		}
		else
			return 0;
	}


	public function actionGetTree($filter = '', $attachToModel = '', $variableName = '')
	{
		$filter = urldecode($filter);
		if (Yii::app()->request->isAjaxRequest) 
		{
			$criteria = new CDbCriteria();
			if (strlen($filter) > 0) {
				$criteria->condition = 'id = :id OR name LIKE :name';
				$criteria->params = array(':id' => $filter, ':name' => '%'.$filter.'%');
			}
			$criteria->order = 'id DESC';
			
			$count = Tree::model()->count($criteria);
		
			$pages = new CPagination($count);
			$pages->pageSize = 9;
			$pages->applyLimit($criteria);

			$output = array();
			$tree_all = Tree::model()->findAll($criteria);
			foreach ($tree_all as $tva)
			{
				$output[] = array('id' => $tva->id, 'name' => $tva->name);
			}

			$this->layout = 'ajax';
			$this->render(
							'gettree',
							array(
								'output' => $output,
								'filter' => $filter,
								'attachToModel' => $attachToModel,
								'variableName' => $variableName,
								'pages' => $pages,
							)
						);
		}
	}

    /**
     * Переместить узел дерева вниз или вверх.     *
     * ID узла дерева и узла куда надо переместить приходят в get-параметрах
     *
     * @param string $direction - направление перемещения: up - вверх, down - вниз
     */
    public function actionMove($direction = null)
    {
        if( empty($direction) )
        {
            exit(0);
        }

        $id = Yii::app()->request->getParam('id');

        $src_node  = Tree::model()->findByPk($id);

        if(!empty($src_node))
        {
            if($direction == 'up')
            {
                $prev_node = $src_node->getPrevSibling();

                if( !empty($prev_node) )
                {
                    $src_node->moveBefore($prev_node);
                }
            }

            if($direction == 'down')
            {
                $next_node = $src_node->getNextSibling();

                if( !empty($next_node) )
                {
                    $src_node->moveAfter($next_node);
                }
            }
        }


        exit(0);
    }


	//Возвращает json массив по введенному названию серии
	public function actionGetByName($s)
	{
		$q = new CDbCriteria();
		$q->compare('LOWER(name)',strtolower($s),true); 
		
		$tree = Tree::model()->findAll($q);
		
		$arr = array();
		
		if(!is_null($tree))
		{
			$i = 0;
			
			foreach($tree as $t)
			{
				$arr[$i]['id'] = $t->id;
				$arr[$i]['key'] = $t->id;
				$arr[$i]['value'] = $t->name;
				$i++;
			}
		}
		
		echo CJSON::Encode($arr);
		Yii::app()->end();
	}	

}
?>