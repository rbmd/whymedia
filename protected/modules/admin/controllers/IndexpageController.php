<?
class IndexpageController extends Controller 
{
	public $layout = 'index';

	public function actionIndex()
	{
		$s = array('video' => array(), 'main_infographic' => array(), 'main_photogallery' => array(), 'main_interview' => array(), 'first_double_article' => array(), 'second_double_article' => array(), 'third_double_article' => array(), 'fourth_double_article' => array() );
		//Управление последовательностью cтатей на главной
		$materials = IndexpageSeries::model()->with(array('materials' => array('joinType' => 'INNER JOIN') ))->order('order_num ASC')->findAll();
		foreach($materials as $material)
		{
			#var_dump($material->place);
			$s[$material->place][] = array('id' => $material->materials->id, 'name' => $material->materials->name);
		}

		$this->render('index', array('materials' => $s, ));
	}


	public function actionSavematerials()
	{
		if(Yii::app()->request->isPostRequest)
		{
			$materials = (Yii::app()->request->getPost('materials_id'));

			//ОБЕРНУТЬ В ТРАНЗАКЦИЮ!
			Yii::app()->db->createCommand()->truncateTable(IndexpageSeries::model()->tableName());
			#var_dump($series);
			#var_dump($materials);
			#exit;
			if(is_array($materials))
			{
				foreach($materials as $material_place => $material)
				{
					if(is_array($material))
					{
						foreach($material as $material_order_num => $material_id)
						{
							$s = new IndexpageSeries();
							$s->place = $material_place;
							$s->material_id = $material_id;
							$s->order_num = $material_order_num;
							$s->save();
						}
					}
					
				}
			}

			$this->redirect('/admin/indexpage/');
		}
	}

	public function actionSavetree()
	{
		if(Yii::app()->request->isPostRequest)
		{
			$tree = (Yii::app()->request->getPost('tree_id'));

			//ОБЕРНУТЬ В ТРАНЗАКЦИЮ!
			Yii::app()->db->createCommand()->truncateTable(IndexpageTree::model()->tableName());
			#var_dump($series);
	
			foreach($tree as $tree_order_num => $tree_id)
			{
				$s = new IndexpageTree();
				$s->tree_id = $tree_id;
				$s->order_num = $tree_order_num;
				$s->save();
			}

			$this->redirect('/admin/indexpage/');
		}
	}


}