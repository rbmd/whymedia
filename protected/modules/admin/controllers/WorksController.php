<?php

class WorksController extends Controller
{
	public $layout = 'index';
	
	public function actionIndex()
	{
		$criteria = new CDbCriteria();
		$criteria->order = 't.date_active_start DESC';
		
		$articles = Work::model()->with('rubric')->findAll($criteria);

		$this->render('index', array('articles' => $articles) );
	}

	public function actionArticles()
	{
		

		$this->render('articles', array('articles' => $articles) );
	}
	
	public function actionCreate()
	{
		$model = new Work();
		$model->date_active_start = date('Y-m-d H:i:s');
		if(isset($_POST['Work']))
		{			
			$model->attributes = $_POST['Work'];
			
			if ($model->save())
			{
				if( !isset($_POST['apply']) )
					$this->redirect("/admin/works/");
				else
					$this->redirect('/admin/works/edit/?id='.$model->id, true);
			}

		}

		$tree = $this->getRubric();
		$galleries = $this->getGalleries();
		$polls = $this->getPolls();
	
		$this->render('edit', array('model' => $model, 'action' => 'create',  'tree' => $tree, 'galleries' => $galleries, 'polls' => $polls, ) );
	}
	
	public function actionEdit($id)
	{
		if($id > 0)
		{
			$model = Work::model()->FindByPk($id);
			
			if(isset($_POST['Work']))
			{
				$model->attributes = $_POST['Work'];

				if($model->save())
				{
                    if( !isset($_POST['apply']) )
                    {
                    	Yii::app()->request->redirect('/admin/works/');
                    }
                    else
					{
                        $this->redirect('/admin/works/edit/?id='.$model->id, true);
                    }
				}
			}
			
			$tree = $this->getRubric();
			$galleries = $this->getGalleries();
			$polls = $this->getPolls();
			

			$this->render('edit', array('model' => $model, 'action' => 'edit', 'tree' => $tree, 'galleries' => $galleries, 'polls' => $polls, ) );		
		}
	}

	private function getRubric()
	{
		$themes_ac = Rubric::model()->findAll(array('order' => 'id DESC'));
		foreach ($themes_ac as $theme)
		{
			$themes[$theme->id] = $theme->name_ru;
		}
		return $themes;
	}

	private function getGalleries()
	{
		$galleries_ac = Galleries::model()->findAll(array('order' => 'id DESC'));
		$galleries[]  = '--';
		foreach ($galleries_ac as $gallery)
		{
			$galleries[$gallery->id] = $gallery->name;
		}
		return $galleries;
	}

	private function getPolls()
	{
		$polls_ac = Polls::model()->findAll(array('order' => 'id DESC'));
		$polls[]  = '--';
		foreach ($polls_ac as $poll)
		{
			$polls[$poll->id] = $poll->name;
		}
		return $polls;
	}


    /*
     * Удаление тега
     */
    public function actionDelete()
    {
        if( Yii::app()->request->isAjaxRequest )
        {
            $id = Yii::app()->request->getParam('id');

            if( !empty($id) )
            {
                $transaction = Work::model()->dbConnection->beginTransaction();

                try{
                    
                    Work::model()->findByPk($id)->delete();

                    $transaction->commit();
                }
                catch(Exception $e)
                {
                    $transaction->rollBack();

                    throw $e;
                }
            }

            Yii::app()->end();
        }

        $this->redirect('/admin/materials/');
    }

    //Возвращает json массив по введенному названию серии
	public function actionGetByName($s)
	{
		$q = new CDbCriteria();
		$q->compare('LOWER(name)',strtolower($s),true); 
		
		$series = Work::model()->findAll($q);
		
		$arr = array();
		
		if(!is_null($series))
		{
			$i = 0;
			
			foreach($series as $s)
			{
				$arr[$i]['id'] = $s->id;
				$arr[$i]['key'] = $s->id;
				$arr[$i]['value'] = $s->name;
				$i++;
			}
		}
		
		echo CJSON::Encode($arr);
		Yii::app()->end();
	}	
}