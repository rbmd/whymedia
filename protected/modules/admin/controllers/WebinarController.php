<?
class WebinarController extends Controller 
{
	public $layout = 'index';

	public function actionIndex()
	{
		if(Yii::app()->request->isPostRequest)
		{
			

			$webinar = Webinar::model()->findByPk(1);
			$webinar->attributes = Yii::app()->request->getPost('Webinar');
			$webinar->save();
			
			$this->redirect('/admin/webinar/');
		}

		$webinar = Webinar::model()->findByPk(1);

		$this->render('index', array('webinar' => $webinar, ));
	}


	public function actionSave()
	{
		if(Yii::app()->request->isPostRequest)
		{
			$webinar_announce = Yii::app()->request->getPost('webinar_announce');

			$webinar = new Webinar();
			$webinar->webinar_id = Yii::app()->request->getPost('webinar_id');
			$webinar->webinar_announce_pic = $webinar_announce['pic'];
			$webinar->webinar_announce_day = $webinar_announce['day'];
			$webinar->save();
			
			
			$this->redirect('/admin/webinar/');
		}
	}

	


}