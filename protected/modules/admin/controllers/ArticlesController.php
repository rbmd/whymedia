<?php

class ArticlesController extends Controller
{
	public $layout = 'index';
	public $action_type = '';

	public function __construct($id, $module = null)
	{
		parent::__construct($id, $module);
		$this->setPageTitle('Статьи');
		//Подрубаем css со свойствами для валидации полей формы создания лекции
		Yii::app()->clientScript->registerCssFile(Yii::app()->assetManager->publish('static/css/form.css'));
		Yii::app()->clientScript->registerCssFile('/static/css/admin/jcrop/jquery.Jcrop.min.css');
		Yii::app()->clientScript->registerScriptFile(Yii::app()->assetManager->publish('static/js/admin/guidegmap.js'), CClientScript::POS_END);
		//Yii::app()->clientScript->registerScriptFile(Yii::app()->assetManager->publish('static/js/admin/wysiwyg_manager.js'), CClientScript::POS_END);
		Yii::app()->clientScript->registerScriptFile(Yii::app()->assetManager->publish('static/js/admin/jquery.scrollTo.min.js'), CClientScript::POS_END);
		Yii::app()->clientScript->registerScriptFile(Yii::app()->assetManager->publish('static/js/admin/jquery.doubletap.js'), CClientScript::POS_END);
		Yii::app()->clientScript->registerScriptFile(Yii::app()->assetManager->publish('static/js/admin/jquery.doubletap.js'), CClientScript::POS_END);
		Yii::app()->clientScript->registerScriptFile(Yii::app()->assetManager->publish('static/js/admin/jquery.autosize-min.js'), CClientScript::POS_END);
		Yii::app()->clientScript->registerScriptFile(Yii::app()->assetManager->publish('static/js/admin/article_edit.js'), CClientScript::POS_END);
		Yii::app()->clientScript->registerScriptFile(Yii::app()->assetManager->publish('static/js/admin/jscrop/jquery.color.js'), CClientScript::POS_END);
		Yii::app()->clientScript->registerScriptFile(Yii::app()->assetManager->publish('static/js/admin/jscrop/jquery.Jcrop.min.js'), CClientScript::POS_END);

	}


	/**
	 * Восстановить статью из ArticleDraft
	 * @param int $id article_draft_id, который сделать рабочим
	 */
	public function actionRestore($id = null)
	{
		$this->redirect('/admin/articles/edit/?id=' . ArticlesDraft::restore($id) . '#pane=chapters');
	}


	/**
	 * Вернуть несохраненные изменения (главы и элементы)
	 * @param int $article_id ID статьи
	 */
	public function actionUndoChanges($article_id)
	{
		if (intval($article_id))
		{
			$id = ArticlesDraft::getCurrentDraft($article_id)->id;
			$this->redirect('/admin/articles/edit/?id=' . ArticlesDraft::restore($id) . '#pane=chapters');
		}
	}


	// При загрузке страницы создания статьи сразу создаются записи в bg_articles и bg_articles_draft для назначения ID
	public function actionCreate()
	{
		$model = new Articles('new_article');

		if ($model->save())
		{
			$date = date('Y-m-d H:i:s');

			$articlesDraft                    = new ArticlesDraft('new_article');
			$articlesDraft->date_create       = $date;
			$articlesDraft->date_publish      = $date;
			$articlesDraft->date_active_start = $date;
			$articlesDraft->to_rss = 1;
			$articlesDraft->is_dummy          = 1;
			$articlesDraft->article_id        = $model->id;
			$articlesDraft->save();

			// Глава по-умолчанию
			$chapter                   = new ChaptersDraft();
			$chapter->name             = 'Новая глава';
			$chapter->order_num        = 1;
			$chapter->article_id       = $articlesDraft->article_id;
			$chapter->article_draft_id = $articlesDraft->id;
			$chapter->save();

			// Элемент по-умолчанию
			$element                   = new ElementsDraft();
			$element->type             = 'wysiwyg';
			$element->chapter_id       = $chapter->id;
			$element->article_id       = $articlesDraft->article_id;
			$element->article_draft_id = $articlesDraft->id;
			$element->save();

			$this->redirect('/admin/articles/edit/?id=' . $model->id, true);
		}
	}


	// Сохранение статьи
	public function actionSave()
	{
		$timestart = microtime(true);
		// Запишем сюда результат работы метода для вывода json
		$result = array();

		if (!Yii::app()->request->isPostRequest)
		{
			throw new CHttpException(400);
		}

		$id    = intval(Yii::app()->request->getPost('id'));
		$model = ArticlesDraft::getCurrentDraft($id);

		$post_article = Yii::app()->request->getPost(get_class($model));

		if ($post_article == false)
		{
			throw new CHttpException(400);
		}

		//Обрабатываем карту гидов
		$guidegmap_post            = Yii::app()->request->getPost('GuideGMap');
		$post_article['guidegmap'] = CJSON::encode($guidegmap_post);

		// Создаем новый экемпляр драфта на основе последнего актуального
		$model_new               = new ArticlesDraft();
		$model_new->old_draft_id = $model->id; // ID предыдущего драфта. Нужен, чтобы привязать к новому драфту главы и элементы
		$model_new->attributes   = $post_article;
		$model_new->x_timestamp  = new CDbExpression('NOW()');
		$model_new->user_id      = Yii::app()->user->id;
		$model_new->is_dummy     = 1;
		$model_new->article_id   = $model->article_id;

		// Дополнительные настройки
		$extrapost = Yii::app()->getRequest()->getPost('extra');
		$extra     = json_decode($model->extra_params, true);
		unset($extra['blogs_digest']);
		if (is_array($extrapost))
		{
			foreach ($extrapost as $key => $val)
			{
				$extra[$key] = $val;
			}
		}
		$model_new->extra_params = FnlHelper::utf_json_encode($extra);//json_encode($extra, JSON_UNESCAPED_UNICODE);


		// Если статья заблокирована другим редактором
		if (WorkflowLock::isLockArticle($model->article_id))
		{
			$result = $this->createMessageForLock($model);
		}
		// Проверяем сохраняемую статью на актуальность
		elseif ($this->checkToSave($model->id) == false)
		{
			$result = $this->createMessageForReSave($model);
		}
		// Статья не проходит валидацию
		elseif (!$model_new->validate())
		{
			$aErrors = array();
			foreach ($model_new->getErrors() as $errors)
				$aErrors[] = $errors[0];

			$result = array(
				'result'  => 'error',
				'message' => implode("\n", $aErrors)
			);
		}
		// Сохранение
		else
		{
			//Оборачиваем все сохранение в транзакцию
			$transaction = $model_new->dbConnection->beginTransaction();
			try
			{
				// сбрасываем флаг is_dummy для всех редакций текущей статьи
				Yii::app()->db->createCommand()->update($model->tableName(), array(
					'is_dummy' => 0,
					'edited'   => 0
				), 'article_id=:id', array(':id' => $model->article_id));


				$model_new->save(); // Результат не проверяем, т.к. выше проверяли validate();

				Articles::model()->restoreFromDraft($model_new->id);


				$transaction->commit();

				// Если изменили ответственного за статью, добавляем это в блокировку, для отправки имейла
				if ($model->assignee != $post_article['assignee'])
				{
					WorkflowLock::setAssignee($model->article_id, $post_article['assignee']);
				}

				$result = array(
					'result'           => 'success',
					'article_draft_id' => $model_new->id
				);
			} catch (Exception $e)
			{
				$transaction->rollback();
			}
		}

		header('Content-type:application/json');
		echo json_encode($result);
		ArticlesSaveLog::saveArticle($model, '', Yii::app()->getRequest()->getParam('createpage'), microtime(true) - $timestart);
		Yii::app()->end();
	}


	public function actionEdit($id, $articles_draft_id = 0)
	{
		$this->action_type = 'edit';

		if ($articles_draft_id)
		{
			$this->action_type = 'hist';
			$model             = ArticlesDraft::model()->find('id=:id', array(':id' => $articles_draft_id));
		}
		else
		{
			$model = ArticlesDraft::model()->find('article_id=:article_id AND is_dummy=1', array(':article_id' => $id));
		}

		// Если в ArticlesDraft нет записи о статье, создаем ее из Articles
		// Это возможно для старых, никогда не редактируемых раньше статей
		if (is_null($model))
		{
			$model = ArticlesDraft::createdFromOrigin($id);
		}

		if (empty($model))
		{
			throw new CHttpException(404);
		}


		$this->setPageTitle($model->name);

		/*----Блок, реализующий блокировку редактируемой статьи---*/
		$workflow_lock = WorkflowLock::model()->findByAttributes(
			array('article_id' => $model->article_id)
		);

		//Проверяем, что данная статья ещё не заблокирована, чтобы не случайно не поставить блокировку дважды
		if (empty($workflow_lock))
		{
			$workflow_lock_e             = new WorkflowLock();
			$workflow_lock_e->article_id = $id;
			$workflow_lock_e->user_id    = Yii::app()->user->id + 0;
			$workflow_lock_e->lock_date  = new CDbExpression('NOW()');
			$workflow_lock_e->save();
		}
		//Если блокировка уже стоит,
		else
		{
			//то, если это тот же самый юзер обновляем блокировку,
			if ($workflow_lock->user_id == Yii::app()->user->id + 0)
			{
				$workflow_lock->lock_date = new CDbExpression('NOW()');
				$workflow_lock->save();
				$workflow_lock = 0;
			}
			//а если это другой пользоваватель пытается получить доступ
			else
			{
				$user = User::model()->findByPk($workflow_lock->user_id);
				//Подготовим сообщение о том, кто заблокировал статью.
				$workflow_lock->user_name = empty($user) ? 'anonymous' : ProfileHelper::getFIO($workflow_lock->user_id);
			}
		}
		/*--------------------------------------------------------*/

		$series = $model->series;

		//Получаем массив activerecord авторов с ключами являющимися типом авторов.
		$authors = Authors::model()->authorsByArticleIdAT($id);

		//
		$magazines_ac = Magazine::model()->findAll(array('order' => 'issue_abs DESC'));
		$magazines[]  = '--';
		foreach ($magazines_ac as $mag)
		{
			$magazines[$mag->id] = $mag->issue_abs;
		}

		$tree = Tree::model()->findAll(array('order' => 'cleft'));
		$tags = ArticlesToTags::model()->article($model->article_id);
		$regions = ArticlesToRegions::model()->articleIs($model->article_id);

		$relations = ($model->relations) ? Yii::app()->db->createCommand()->select('id, name')->from('bg_articles')->where('id IN(' . $model->relations . ')')->limit(5)->queryAll() : array();

		$this->render('edit', array('model'     => $model,
											 'action'    => $this->action_type,
											 'tags'      => $tags,
											 'tree'      => $tree,
											 'authors'   => $authors,
											 'magazines' => $magazines,
											 'series'    => $series,
											 'themeweek' => ThemeWeek::model()->find('code=:code', array(':code' => $model->theme_week_code)),
											 'lock'      => $workflow_lock,
											 'relations' => $relations,
											 'regions'	=> $regions,
		));

	}


	/**
	 * Генерирует сообщение для пользователя о том, что статья устарела
	 *
	 * @param $model ArticlesDraft
	 * @param string $msg
	 * @return array
	 */
	private function createMessageForReSave($model, $msg = '')
	{
		$result = array(
			'result'  => 'lock',
			'message' => "Эта статья была пересохранена\n" . $model->x_timestamp . " пользователем " . User::model()->getFullName($model->user_id) . "\n\nВаши данные неактуальны\nПерезагрузите страницу"
		);
		ArticlesSaveLog::saveArticle($model, 'lock ' . $msg, $model->user_id . ' - ' . User::model()->getFullName($model->user_id) . ' (' . Yii::app()->getRequest()->getParam('article_draft_id') . '-' . $model->id . ') ' . Yii::app()->getRequest()->getParam('createpage'));
		return $result;
	}

	private function createMessageForLock($model, $msg = '')
	{
		$result = array(
			'result'  => 'lock',
			'message' => "Эта статья была заблокирована\n" . $model->x_timestamp . " пользователем " . User::model()->getFullName($model->user_id)
		);
		return $result;
	}


	// Проверяем пересохраняемую версию статьи на актуальность
	private function checkToSave($article_draft_id)
	{
		if ($article_draft_id == Yii::app()->getRequest()->getParam('article_draft_id'))
		{
			return true;
		}
		else
		{
			return false;
		}
	}


	// Возвращает json массив по введенному названию
	public function actionGetByName($s)
	{
		$q = new CDbCriteria();
		$q->compare('LOWER(name)', strtolower($s), true);
		$articles = Articles::model()->recently(20)->findAll($q);
		$arr      = array();
		if (!is_null($articles))
		{
			$i = 0;
			foreach ($articles as $s)
			{
				$arr[$i]['id']    = $s->id;
				$arr[$i]['key']   = $s->id;
				$arr[$i]['value'] = $s->name;
				$i++;
			}
		}
		echo CJSON::Encode($arr);
		Yii::app()->end();
	}


	/**
	 * Устанавливает блокировку статьи (вызывается из js)
	 * @param int $id ArticlesDraftID
	 */
	public function actionUpdateLock($id)
	{
		$result = array();

		$model = ArticlesDraft::getCurrentDraft($id);
		if ($this->checkToSave($model->id) == false)
		{
			$result = $this->createMessageForReSave($model, 'UpdateLock');
		}
		else
		{
			$workflow_lock = WorkflowLock::model()->findByAttributes(array('article_id' => $id));
			if ($workflow_lock)
			{
				// Если заблокирована другим пользователем
				if ($workflow_lock->user_id != Yii::app()->user->id)
				{
					$result = array(
						'result'  => 'lock',
						'message' => "Эта статья была заблокирована\n" . $workflow_lock->lock_date . " пользователем " . User::model()->getFullName($workflow_lock->user_id)
					);
				}
				else
				{
					$workflow_lock->lock_date = date('Y-m-d H:i:s');
					$workflow_lock->save();
				}
			}
		}
		echo CJSON::Encode($result);
		Yii::app()->end();
	}


	// Снятие блокировки со статьи(например при нажатии на "Отмена" в редактировании статьи)
	public function actionBreaklock($article_id = 0)
	{
		// Снимаем  блокировку
		WorkflowLock::breakLock($article_id, Yii::app()->user->id);
		//Перенаправляем пользователя на список всех статей
		$this->redirect('/admin/articles/');
	}


	// Удалить статью и все сущности с ней связанные
	public function actionDeleteArticle()
	{
		if (Yii::app()->request->isAjaxRequest)
		{
			$article_id = Yii::app()->request->getParam('id');
			if (!empty($article_id))
			{
				$transaction = Articles::model()->dbConnection->beginTransaction();
				try
				{
					ArticlesToAuthors::model()->deleteAll('article_id=:id', array(':id' => $article_id));
					ArticlesToTags::model()->deleteAll('article_id=:id', array(':id' => $article_id));
					WorkflowLock::model()->deleteAll('article_id=:id', array(':id' => $article_id));
					Articles::model()->findByPk($article_id)->delete();
					$transaction->commit();
				} catch (Exception $e)
				{
					$transaction->rollBack();
					throw $e;
				}
			}
			Yii::app()->end();
		}
		$this->redirect('/admin/articles');
	}


	//Получение списка статей для различных аякс запросов
	public function actionGetList($filter = '', $attachToModel = '', $variableName = '')
	{
		$filter = urldecode($filter);
		if (Yii::app()->request->isAjaxRequest)
		{
			$criteria = new CDbCriteria();
			if (strlen($filter) > 0)
			{
				$criteria->condition = 'id = :id OR name LIKE :name';
				$criteria->params    = array(':id' => $filter, ':name' => '%' . $filter . '%');
			}
			$criteria->order = 'id DESC';

			$count = Articles::model()->count($criteria);

			$pages           = new CPagination($count);
			$pages->pageSize = 9;
			$pages->applyLimit($criteria);

			$output  = array();
			$stories = Articles::model()->findAll($criteria);
			foreach ($stories as $tva)
			{
				$output[] = array('id' => $tva->id, 'name' => $tva->name);
			}

			$this->layout = 'ajax';
			$this->render(
				'getlist',
				array(
					'output'        => $output,
					'filter'        => $filter,
					'attachToModel' => $attachToModel,
					'variableName'  => $variableName,
					'pages'         => $pages,
				)
			);
		}
	}


	// Помечаем статью меткой "Отредактировано"
	public function actionMarkEditing()
	{
		$id = intval(Yii::app()->getRequest()->getPost('id'));
		if ($id > 0)
		{
			echo ArticlesDraft::model()->updateAll(array('edited' => 1), 'article_id=:id AND is_dummy=1', array('id' => $id));
		}
	}


	// Помечаем статью меткой "Отредактировано"
	public function actionSaveImage()
	{
		$id                   = intval(Yii::app()->getRequest()->getPost('article_id'));
		$image                = Yii::app()->getRequest()->getPost('image');
		$article              = ArticlesDraft::getCurrentDraft($id);
		$article->preview_img = $image;

		// сохраняем картинку
		$article->saveImages();

		// обновляем поле с картинкой
		ArticlesDraft::model()->updateByPk($article->id, array('preview_img' => $image));

		// пересохраняем рабочую копию
		Articles::model()->restoreFromDraft($article->id);
		echo 1;
	}


	public function beforeRender($action)
	{
		return true;
	}


	public function actionCrop()
	{
		$jpeg_quality = 90;

		//$_POST['src'] = 'media/upload/images/kids/2013/january/21jan/anons_crop_' . time() . '.jpg';

		$original = Yii::app()->getRequest()->getPost('src');
		$ext      = substr(strrchr(Yii::app()->getRequest()->getPost('src'), '.'), 1);
		$file_new = preg_replace('#_crop_([0-9]{10})#', '', substr($original, 0, strrpos($original, '.'))) . '_crop_' . time() . '.jpg';
		$file_old = $_SERVER['DOCUMENT_ROOT'] . $original;

		$info = getImageSize($file_old);
		if (is_array($info))
		{
			$ext = str_replace('image/', '', $info['mime']);
			$w = Yii::app()->getRequest()->getPost('w');
			$h = Yii::app()->getRequest()->getPost('h');
			$x = Yii::app()->getRequest()->getPost('x');
			$y = Yii::app()->getRequest()->getPost('y');

			switch ($ext)
			{
				case "jpg":
					$img_r = imagecreatefromjpeg($file_old);
					break;
				case "jpeg":
					$img_r = imagecreatefromjpeg($file_old);
					break;
				case "png":
					$img_r = imagecreatefrompng($file_old);
					break;
				case "gif":
					$img_r = imagecreatefromgif($file_old);
					break;
			}
			$dst_r = ImageCreateTrueColor($w, $h);

			imageinterlace($dst_r, 1);

			imagecopyresampled($dst_r, $img_r, 0, 0, $x, $y, $w, $h, $w, $h);

			header('Content-type: image/jpeg');
			imagejpeg($dst_r, $_SERVER['DOCUMENT_ROOT'] . $file_new, $jpeg_quality);

			echo $file_new;
		}
	}


	public function actionIndex($id = null, $parent_id = null, $name = null, $is_best = null, $is_guide = null, $is_main_guide = null, $noactive = null)
	{
		$criteria         = new CDbCriteria();
		$criteria->order  = 't.date_active_start DESC';
		$criteria->select = 't.*, tree.id, tree.name';

		//filter
		if ($id)
		{
			$criteria->addCondition('t.id = ' . (int)$id);
		}

		if ($parent_id && $parent_id > 1)
		{
			$criteria->addCondition('t.parent_id = ' . (int)$parent_id);
		}

		if ($name)
		{
			$name = urldecode($name);
			$criteria->addSearchCondition('t.name', $name);
		}

		if ($is_best)
		{
			$criteria->addCondition('t.is_best=' . $is_best);
		}

		if ($is_guide)
		{
			$criteria->addCondition('t.is_guide=' . $is_guide);
		}

		if ($is_main_guide)
		{
			$criteria->addCondition('t.is_main_guide=' . $is_main_guide);
		}
		if ($noactive)
		{
			$criteria->addCondition('t.active=0');
		}

		$count = Articles::model()->count($criteria);

		$pages           = new CPagination($count);
		$pages->pageSize = 80;
		$pages->applyLimit($criteria);

		$news = Articles::model()->Indexed()->with('tree', 'lock')->findAll($criteria);
		$stat = ViewsFull::model()->getStatistic4Elements($news);
		$this->render('index', array('news' => $news, 'stat' => $stat, 'pages' => $pages));
	}

	public function actionStatistic($send_csv = null, $id = null, $parent_id = null, $name = null, $is_best = null, $region_id = null, $is_main_guide = null, $noactive = null, $date_publish_from = null, $date_publish_to = null, $date_create_from = null, $date_create_to = null, $customer = null, $author_id = null)
	{
		$criteria         = new CDbCriteria();
		$criteria->order  = 't.date_active_start DESC';
		$criteria->select = 't.*, tree.id, tree.name';
		$criteria->addCondition("t.name != ''");

		//filter
		if ($id)
		{
			$criteria->addCondition('t.id = ' . (int)$id);
		}

		if ($parent_id && $parent_id > 1)
		{
			$criteria->addCondition('t.parent_id = ' . (int)$parent_id);
		}

		if ($name)
		{
			$name = urldecode($name);
			$criteria->addSearchCondition('t.name', $name);
		}

		if ($is_best)
		{
			$criteria->addCondition('t.is_best=' . $is_best);
		}

		if ($is_main_guide)
		{
			$criteria->addCondition('t.is_main_guide=' . $is_main_guide);
		}
		if ($noactive)
		{
			$criteria->addCondition('t.active=0');
		}

		if ($region_id > 0)
		{
			$criteria->scopes = array('inRegion' => $region_id);
		}

		if ($date_publish_from)
		{
			$criteria->addCondition('t.date_publish >= :date_publish_from');
			$criteria->params['date_publish_from'] = $date_publish_from;
		}

		if ($date_publish_to)
		{
			$criteria->addCondition('t.date_publish <= :date_publish_to');
			$criteria->params['date_publish_to'] = $date_publish_to;
		}

		if ($date_create_from)
		{
			$criteria->addCondition('t.date_publish >= :date_create_from');
			$criteria->params['date_create_from'] = $date_create_from;
		}

		if ($date_create_to)
		{
			$criteria->addCondition('t.date_publish <= :date_create_to');
			$criteria->params['date_create_to'] = $date_create_to;
		}

		if ($customer)
		{
			$criteria->addSearchCondition('customer', $customer);
			//$criteria->params['customer'] = $customer;
		}

		if ($author_id > 0)
		{
			$criteria->scopes = array('authorIdIs' => $author_id);
		}		

		$count = Articles::model()->count($criteria);

		if(is_null($send_csv))
		{
			$pages           = new CPagination($count);
			$pages->pageSize = 80;
			$pages->applyLimit($criteria);
		}

		$regions = Regions::model()->findAll();

		
		$authors = Authors::model()->bySurnameName()->findAll();

		$news = Articles::model()->Indexed()->with('tree', 'lock', 'regions')->findAll($criteria);
		$stat = ViewsFull::model()->getStatistic4Elements($news);

		if(!is_null($send_csv) )
		{
			header('Content-Type: text/csv; charset=windows-1251' );
            header('Content-Disposition: attachment;filename=statistic.csv');
			$this->renderPartial('statistic_csv', array('news' => $news, 
											'stat' => $stat, 											
											'regions' => $regions,));
		
		}
		else
		{
			$this->render('statistic', array('news' => $news, 
											'stat' => $stat, 
											'pages' => $pages,
											'regions' => $regions,
											'authors' => $authors,
											));
		}
	}
}