<?php

class Articles extends CActiveRecord
{
	/**
	 * Название поискового индекса для статей в поисковом движке "Sphinx"
	 */
	const SPHINX_SEARCH_INDEX = "articles";

	/**
	 * Сколько должно быть результатов на одну страницу при поиске по статьям с помощью "Sphinx".
	 */
	const SPHINX_SEARCH_ITEMS_PER_PAGE = 16;

	const COMMENTS_COUNT_TTL = 60; //Кешируем на 24 часа, кеш также сбрасывается, когда приходит новый коммент

	public $authorsids;
	public $presentersids;
	public $guestsids = array();
	public $tagList = array();
	public $sendToTwitter;


	//Если false, то отрабатывает afterSaveProcessElements();
	public $save_only_article = false;

	/**
	 * Типы плиток, ключ соответствующий типу плитки хранится в поле ceil_type
	 */
	public $ceil_types = array(0 => 'Стандартный', 1 => 'Плитка-цитата', 2 => 'Архив', 3 => 'Реклама');


	/**
	 * Типы листалок глав, ключ соответствующий типу плитки хранится в поле chapter_paging_type
	 */
	public $chapter_paging_types = array(0 => 'Главы с изображением', 1 => 'Главы без изображения', 2 => 'Пагинатор', 3 => 'Без листалки', 4 => 'Без первой страницы');


	protected function afterConstruct()
	{
		parent::afterConstruct();
	}


	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}


	public function tableName()
	{
		return 'bg_articles';
	}

	public function behaviors()
	{
		return array(
			/*'dateNormalizer' => array(
					 'class' => 'application.extensions.RusDateNormalizerBehavior',
					 'dateAttributes' => array(
						  'date_active_start' => true,
						  'date_active_end' => true,
						  'date_broadcast_start' => true,
					 ),
				),

				'TTL' => array(
					 'class' => 'application.extensions.DateActiveStartEndBehavior',
					 'date_active_start' => 'date_active_start',
					 'date_active_end' => 'date_active_end',
					 'condition' => 'active = 1',
				),*/

			'searchable' => array(
				'class' => 'YiiElasticSearch\SearchableBehavior',
			),

			'iMemCacheBehavior' => array(
				'class'     => 'application.extensions.imemcache.iMemCacheBehavior',
				'entity_id' => 'id'
			),

			'TTL'               => array(
				'class'             => 'application.extensions.DateActiveStartEndBehavior',
				'date_active_start' => 'date_active_start',
				'date_active_end'   => 'date_active_end',
				'condition'         => 'active = 1',
			),

		);
	}


	public function scopes()
	{
		return array(
			'Indexed' => array(
				'index' => 'id',
			),
			'active'  => array(
				'condition' => 't.active = 1',
			),
			'visible' => array(
				'condition' => 't.active=1 AND NOW() >= t.date_active_start',
			),
			'toRss'   => array(
				'condition' => 'to_rss=1',
			),
			'isHowto' => array(
				'condition' => 'is_howto=1',
			)
		);
	}

	/**
	 * Скоуп. Исключает из запроса заданые айдишники
	 */
	public function PknotIn($ids = null)
	{
		if (is_array($ids))
		{
			$this->getDbCriteria()->mergeWith(array(
					'condition' => 't.id NOT IN(' . implode(', ', $ids) . ')',
				)
			);
			return $this;
		}
		return $this;
	}

	/**
	 * Скоуп. Выбирает статьи с галкой "Гид"
	 */
	public function isGuide()
	{
		$this->getDbCriteria()->mergeWith(array('condition' => 'is_guide=1'));
		return $this;
	}

	/**
	 * Скоуп. Выбирает статьи с галкой "Главный гид"
	 */
	public function lastMainGuide()
	{
		$this->getDbCriteria()->mergeWith(array(
				'condition' => 'is_guide=1 AND is_main_guide=1',
				'limit'     => 1,
				'order'     => 't.date_active_start DESC'
			)
		);
		return $this;
	}

	/**
	 * Скоуп. Выбирает статьи с галкой "Новость"
	 */
	public function isNews()
	{
		$this->getDbCriteria()->mergeWith(array('condition' => 'is_news=1'));
		return $this;
	}

	/**
	 * Скоуп. Выбирает статьи с галкой "Новость"
	 */
	public function isMiniNews($is = true)
	{

		$this->getDbCriteria()->mergeWith(array(
				'condition' => 'is_mininews=' . (($is) ? 1 : 0),
			)
		);
		return $this;
	}

	/**
	 * Скоуп. Выбирает статьи с определенным parent_id
	 */
	public function parentIs($parent_id = 0)
	{
		$this->getDbCriteria()->mergeWith(array('condition' => 'parent_id=' . $parent_id));
		return $this;
	}

	/**
	 * Скоуп. Выбирает статьи без определенного parent_id
	 */
	public function parentNotIs($parent_id = 0)
	{
		$this->getDbCriteria()->mergeWith(array('condition' => 'parent_id!=' . $parent_id));
		return $this;
	}


	/**
	 * Скоуп. Еще статьи из серии
	 */
	public function inSeries($series_id = 0)
	{
		$this->getDbCriteria()->mergeWith(array('condition' => 'series_id=' . $series_id));
		return $this;
	}

	/**
	 * Скоуп. Принадлежность статьи к региону
	 */
	public function inRegion($region_id = 0)
	{
		if($region_id > 0)
		{
			$this->getDbCriteria()->mergeWith(array(
				'select'    => 't.*',
				'join'      => 'INNER JOIN bg_articles_to_regions ar ON ar.article_id=t.id',
				'condition' => 'ar.region_id=' . $region_id,
				'order'     => 't.date_active_start DESC',
			));
		}
		return $this;
	}

	/**
	 * Скоуп. Выбирает лучшие статьи
	 */
	public function isBest()
	{
		$this->getDbCriteria()->mergeWith(array('condition' => 'is_best=1'));
		return $this;
	}


	public function inThemeWeek($code = '')
	{
		$this->getDbCriteria()->mergeWith(array(
			'condition' => 'theme_week_code=:code',
			'params'=>array(':code'=>$code)
			));
		return $this;
	}


	/**
	 * Скоуп. Выбирает главные анонсы
	 */
	public function isMainAnnounce()
	{
		$this->getDbCriteria()->mergeWith(array('condition' => 'show_main_announce=1'));
		return $this;
	}

	/**
	 * Скоуп. Выбирает главные разделов
	 */
	public function isMainAnnounceSection()
	{
		$this->getDbCriteria()->mergeWith(array('condition' => 'show_main_announce_section=1'));
		return $this;
	}

	/**
	 * Скоуп. Статьи с флагом Показывать на морде
	 */
	public function isShowOnIndexPage()
	{
		$this->getDbCriteria()->mergeWith(array('condition' => 'show_on_indexpage=1'));
		return $this;
	}


	/* Скоуп: Выбрать статьи, относящиес к определённому тегу*/
	public function tagIs($tag_id)
	{
		$this->getDbCriteria()->mergeWith(array(
			'select'    => 't.*',
			'join'      => 'INNER JOIN bg_articles_to_tags at ON at.article_id=t.id',
			'condition' => 'at.tag_id=' . $tag_id,
			'order'     => 't.date_active_start DESC',
		));
		return $this;
	}

	/*Скоуп: Выбрать статьи, относящиес к определённому автору*/
	public function authorIdIs($author_id)
	{
		$this->getDbCriteria()->mergeWith(array(
			'select'    => 't.*',
			'join'      => 'INNER JOIN bg_articles_to_authors at ON at.article_id=t.id',
			'condition' => 'at.author_id=' . $author_id,
			'order'     => 't.date_active_start DESC',
		));
		return $this;
	}

	/**
	 * Скоуп. Выбирает статьи с галочкой видео
	 */
	public function hasVideo()
	{
		$this->getDbCriteria()->mergeWith(array('condition' => 'has_video=1'));
		return $this;
	}

	/**
	 * Скоуп. Выбирает статьи с галочкой фото
	 */
	public function hasGallery()
	{
		$this->getDbCriteria()->mergeWith(array('condition' => 'has_gallery=1'));
		return $this;
	}


	/**
	 * Скоуп. Выбирает статьи с галочкой Трансляция
	 */
	public function hasTranslation()
	{
		$this->getDbCriteria()->mergeWith(array('condition' => 'has_translation=1'));
		return $this;
	}	

	// Выбор N последних
	public function recently($limit = 1)
	{
		$this->getDbCriteria()->mergeWith(array(
			'order' => 't.date_active_start DESC',
			'limit' => $limit,
		));
		return $this;
	}


	/**
	 * Скоуп выбора новостей
	 */
	public function news($count = 30)
	{
		$this->getDbCriteria()->mergeWith(
			array(
				'condition' => 'parent_id = :parent_id',
				'order'     => 'date_active_start DESC',
				'limit'     => $count,
				'params'    => array(':parent_id' => Tree::TREE_NEWS_ID),
			)
		);
		return $this;
	}

	// Скоуп. Выбор самых комментируемых
	// @param $days - дней назад от текущего времени
	// @param $limit - количество записей
	public function mostCommented($days, $limit = 1)
	{
		$this->getDbCriteria()->mergeWith(array(
			'condition' => 't.date_active_start >= CURDATE() - INTERVAL :days DAY',
			'order'     => 't.comments_count DESC',
			'limit'     => $limit,
			'params'    => array(':days' => $days),
		));
		return $this;
	}

	/**
	 * Скоуп. Выбор по привязке к журналу
	 */
	public function magazineIs($magazine_id)
	{
		$this->getDbCriteria()->mergeWith(
			array(
				'condition' => 'magazine_id = :magazine_id',
				'order'     => 'date_active_start DESC',
				'params'    => array(':magazine_id' => $magazine_id),
			)
		);
		return $this;
	}


	/**
	 * Скоуп. Сортировка по заданому полю
	 */
	public function order($order)
	{
		$this->getDbCriteria()->mergeWith(array('order' => $order));
		return $this;
	}


	public function relations()
	{
		return array(
			'tree'              => array(self::BELONGS_TO, 'Tree', 'parent_id'),
			#'presenters' => array(self::MANY_MANY, 'Authors', 'bg_articles_to_authors(article_id, author_id)', 'condition' => 'presenters_presenters.type="presenter"', 'joinType' => 'LEFT JOIN'),
			#'author'  => array(self::HAS_MANY, 'Authors', 'bg_articles_to_authors(article_id, author_id)', 'select' => 't.*', 'joinType' => 'LEFT JOIN'),
			'ArticlesToAuthors' => array(self::HAS_MANY, 'ArticlesToAuthors', 'article_id'),
			'authors'           => array(self::HAS_MANY, 'Authors', array('author_id' => 'id'), 'through' => 'ArticlesToAuthors'),

			'chapters'          => array(self::HAS_MANY, 'Chapters', 'article_id'),
			'chapter'           => array(self::HAS_ONE, 'Chapters', 'article_id'),
			'tags'              => array(self::MANY_MANY, 'Tags', 'bg_articles_to_tags(article_id, tag_id)'),

			'ArticlesToRegions' => array(self::HAS_MANY, 'ArticlesToRegions', 'article_id'),
			'regions'           => array(self::HAS_MANY, 'Regions', array('region_id' => 'id'), 'through' => 'ArticlesToRegions'),

			'series'            => array(self::BELONGS_TO, 'Series', 'series_id'),
			'lock'              => array(self::HAS_ONE, 'WorkflowLock', 'article_id'),
			'statistics'        => array(self::HAS_ONE, 'ViewsFull', 'element_id'),
		);
	}


	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria = new CDbCriteria;
		$criteria->compare('id', $this->id);
		$criteria->compare('name', $this->name, true);
		$criteria->compare('main_today', $this->main_today);
		$criteria->compare('main_popular', $this->main_popular);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	public function url()
	{
		if ($this->link)
		{
			return $this->link;
		}
		if (array_key_exists($this->parent_id, Tree::model()->tree_nodes_for_articles))
		{
			//kavpolit style
			return "/articles/{$this->code}-{$this->id}/";
			//bg style
			//return "/" . Tree::model()->tree_nodes_for_articles[$this->parent_id] . "/{$this->code}-{$this->id}/";
		}
		return '/404.php';
	}


	protected function beforeSave()
	{
		//Если новая запись - проставляем время создания
		if (empty($this->date_create))
		{
			$this->date_create = new CDbExpression('NOW()');
		}
		return parent::beforeSave();
	}



	/**
	 * @return int - возвращает количество комментариев к этой статье,
	 * кеширует результат на COMMENTS_COUNT_TTL секунд.
	 * Не берём собственное поле $this->comments_count объекта, так как сам текущий объект
	 * может быть закеширован в кеше на время сущесвенно большее COMMENTS_COUNT_TTL.
	 */
	public function comments_cnt()
	{
		$key    = "article_comments_count_{$this->id}";
		$result = Yii::app()->cache->get($key);
		if ($result === false)
		{
			#$article = self::model()->findByPk($this->id, array('select' => array('t.comments_count')));

			$c = Comments::model()->visible()->entityNameIs('articles')->entityIdIs($this->id)->count();
			if (empty($c))
			{
				return null;
			}
			$result = $c;
			Yii::app()->cache->set($key, $result, self::COMMENTS_COUNT_TTL);
		}
		return $result;
	}


	/*Видна ли статья на сайте и заполнено ли у неё поле "Сюжет"*/
	public function isVisibleAndStoryDefined()
	{
		$series_id = $this->series_id;
		return $this->isVisible() && !empty($series_id);
	}

	/*Видна ли статья на сайте и поле "Сюжет" равно указазанному*/
	public function isVisibleAndStoryIs($series_id)
	{
		return $this->isVisible() && $this->series_id + 0 == $series_id + 0;
	}


	/*Статья видна на сайте и среди её автора есть указанный*/
	public function isVisibleAndPresenterIs($presenter_id)
	{
		$presenters = $this->presenters;
		foreach ($presenters as $presenter)
		{
			if ($presenter->id == $presenter_id)
			{
				return $this->isVisible() && true;
			}
		}
		return false;
	}


	/**
	 * Возвращает истину, если статья относится к выбранной категории (поддереву сайта) и видима на сайте
	 *
	 * @param $item_id - id категории в дереве
	 * @return bool
	 */
	public function scopeIs($tree_id)
	{
		return ($this->parent_id == $tree_id) && $this->isVisible();
	}


	/**
	 * Возвращает истину, если данная статья относится к тегу $tag_id
	 *
	 * @param $tag_id
	 * @return bool
	 */
	public function tagIdIs($tag_id)
	{
		$r = ArticlesToTags::model()->findByAttributes(array('article_id' => $this->id, 'tag_id' => $tag_id));
		if (!empty($r))
		{
			return true && $this->isVisible();
		}
		return false;
	}


	public function img($size = 'original')
	{
		if (0 < strlen($this->preview_img))
		{
			$folder = str_replace('-', '/', substr($this->date_create, 0, 10));
			$ext    = substr($this->preview_img, strrpos($this->preview_img, '.') + 1);
			if ('original' == $size)
			{
				$img = '/media/upload/images/zold/' . $folder . '/' . $this->id . '.' . $ext;
			}
			elseif ('circle' == $size)
			{
				$img = '/media/images/circle/' . $folder . '/' . $this->id . '.png';
			}
			//elseif ('romb' == $size) $img = '/media/images/romb/' . $folder . '/' . $this->id . '.png';
			elseif ('facebook' == $size)
			{
				$img = '/media/images/facebook/' . $folder . '/' . $this->id . '.' . $ext;
			}
			else
			{
				$img = '/media/images/' . $size . '/' . $folder . '/' . $this->id . '.' . $ext;
			}

			if (isset($img))
			{
				return $img . '?' . strtotime($this->x_timestamp);
			}
			else
			{
				return false;
			}

		}
		return false;
	}


	//Возвращает json decode свойство guidegmap для отображения в публичке. Проверяет на "является ли гидом"
	public function getGuidegmap()
	{
		if ($this->is_guide == 1)
		{
			return CJSON::decode($this->guidegmap);
		}
		return false;
	}

	/**
	 * Получаем список статей для заданого раздела
	 * Если задан _GET['page'], он будет учтен в выборке автоматически
	 * @param string $section - раздел сайта. См Tree::model()->$tree_nodes_for_articles для разделов из дерева. Для главной страницы - index. Для гидов - guides
	 * @return array(items - active record массив статей, stat - статистика просмотров)
	 **/
	public function getArticlesList($section)
	{
		/*
		На главных страницах разделов необходимо запоминать массив уже выбранных статей, чтобы на следующих страницах не показывать эти статьи.
		Поэтому кладем в мемкеш и учитываем в дальнейшем выборке
		*/
		$CACHE_KEY       = Yii::app()->cache->buildKey('ARTICLES_LIST_INDEX_ARTICLES_PK', array(), 'SECTION_' . $section);
		$ArticlesPkArray = Yii::app()->cache->get($CACHE_KEY);
		if ($ArticlesPkArray === false)
		{
			$ArticlesPkArray = ArticlesPk::getArray();

			if (is_null(Yii::app()->request->getParam('page')))
			{
				//Кеш вечный
				Yii::app()->cache->set($CACHE_KEY, $ArticlesPkArray, 60);

				Yii::app()->cache->addDependency($CACHE_KEY, array(
						'Articles@getIsActive' => array(),
					)
				);
			}
		}
		#var_dump($ArticlesPkArray);

		//Основные параметры выборки
		$basic_list = Articles::model()->isMiniNews(false)->with('tree')->Indexed()->visible()->pkNotIn($ArticlesPkArray)->order('date_active_start DESC');

		//Если указанная секция есть в массиве разделов сайта, берем ее id и находим элементы, относящиеся к этой секции
		$tree_key = array_search($section, Tree::model()->tree_nodes_for_articles);
		if (false !== $tree_key)
		{
			$basic_list->parentIs($tree_key);
		}
		elseif ($section == 'how-to')
		{
			$basic_list->isHowto();
		}
		//Для гидов выбираем только гиды
		elseif ($section == 'guides')
		{
			$basic_list->isGuide();
		}


		//Постраничка
		$criteria         = new CDbCriteria();
		$basic_list_count = clone $basic_list;

		$CACHE_KEY = Yii::app()->cache->buildKey(keysSet::ARTICLES_LIST, array(), 'PAGES_COUNT_' . $section);

		$count = Yii::app()->cache->get($CACHE_KEY);
		if ($count === false)
		{
			$count = $basic_list_count->count($criteria);
			Yii::app()->cache->set($CACHE_KEY, $count, 60);
			Yii::app()->cache->addDependency($CACHE_KEY, array(
					'Articles@getIsActive' => array(),
				)
			);
		}


		// Для главной необходимо 19 плиток. для аяксовых 18. чтобы последняя строка плиток была заполнена полностью
		$pageSize = intval(Yii::app()->request->getParam('page')) > 1 ? 18 : 19;

		$pages           = new CPagination($count);
		$pages->pageSize = $pageSize;
		$pages->applyLimit($criteria);


		// Т.к. limit отличаеся на 1. пропускаем на 1 запись больше для того, чтобы избежать дублирования статей
		if (intval(Yii::app()->request->getParam('page')) > 1)
		{
			$criteria->offset++;
		}

		//Если запрошенная страница больше общего количества страниц
		if (intval(Yii::app()->request->getParam('page')) > $pages->pageCount)
		{
			if (Yii::app()->request->isAjaxRequest)
			{
				return false;
			}
			else
			{
				throw new CHttpException(404);
			}
		}

		$CACHE_KEY = Yii::app()->cache->buildKey(keysSet::ARTICLES_LIST, array(), 'PAGE_NUMBER_' . $pages->CurrentPage . $section);
		$items     = Yii::app()->cache->get($CACHE_KEY);

		if ($items === false)
		{
			$items = $basic_list->findAll($criteria);

			Yii::app()->cache->set($CACHE_KEY, $items, 60);

			Yii::app()->cache->addDependency($CACHE_KEY, array(
					'Articles@getIsActive' => array(),
				)
			);
		}

		if (!is_null($items))
		{
			$stat = ViewsFull::model()->getStatistic4Elements($items);
			return array('items' => $items, 'stat' => $stat);
		}
		return false;
	}

	/* CACHE FUNCTIONS */
	public function getIsActive()
	{
		return $this->active == 1;
	}

	public function getIsShowMainAnnounceSection()
	{
		return $this->show_main_announce_section == 1;
	}

	public function getIsShowMainAnnounce()
	{
		return $this->show_main_announce == 1;
	}

	public function getIsBest()
	{
		return $this->is_best == 1;
	}

	public function getIsActiveAndIsBest()
	{
		return $this->getIsActive() && $this->getIsBest();
	}

	public function getIsNews()
	{
		return $this->is_news == 1;
	}

	public function getIsGuide()
	{
		return $this->is_guide == 1;
	}

	public function getHasVideo()
	{
		return $this->has_video == 1;
	}


	// Востанавливаем статью из ArticlesDraft по article_draft_id
	public static function restoreFromDraft($article_draft_id)
	{
		$draft                      = ArticlesDraft::model()->findByPK($article_draft_id);
		$article                    = Articles::model()->findByPK($draft->article_id);
		$article->save_only_article = true;

		//Переменная для сохранения всех глав в одну строку и подсчета количества символов в статье
		$full_detail_text = '';

		foreach ($draft as $key => $val)
		{
			if ($key == 'is_dummy' || $key == 'edited' || $key == 'comments_count' || $key == 'article_id' || $key == 'user_id' || $key == 'id' || $key == 'date_create')
			{
				continue;
			}
			$article->$key = $val;
		}

		Chapters::model()->deleteAll('article_id = :id', array(':id' => $draft->article_id));
		Elements::model()->deleteAll('article_id = :id', array(':id' => $draft->article_id));
		$chapters = ChaptersDraft::model()->findAll('article_draft_id=:id AND `hist`=0', array(':id' => $draft->id));
		foreach ($chapters as $chapter)
		{
			$newChapter  = new Chapters();
			$textChapter = array();
			foreach ($chapter as $key => $val)
			{
				if ($key == 'article_draft_id' || $key == 'id' || $key == 'hist')
				{
					continue;
				}
				$newChapter->$key = $val;
			}
			$newChapter->save();
			// Список элементов для главы
			$criteria            = new CDbCriteria;
			$criteria->condition = 'chapter_id=:id AND `hist`=0';
			$criteria->params    = array(':id' => $chapter->id);
			$criteria->order     = 'order_num';
			$elements            = ElementsDraft::model()->findAll($criteria);
			foreach ($elements as $element)
			{
				$newElement = new Elements();
				foreach ($element as $key => $val)
				{
					if ($key == 'article_draft_id' || $key == 'id' || $key == 'hist')
					{
						continue;
					}
					$newElement->$key = $val;
				}
				$newElement->chapter_id = $newChapter->id;
				$newElement->save();
				
				$params['elementType'] = $element->type;
				$params['elementId'] = $element->id;
				$params['content'] = $element->content;
				$params['renderMode'] = 'pub';
				$e = ElementBuilder::build($params);				
				$element_text = $e->render(true, true);
				
				//
				if($element->type == 'wysiwyg')
					$full_detail_text .= $element_text;
				#$element_text = CElements::instance()->type($element->type)->parseToDb($draft->article_id, json_decode($element->content, true));
				//Все это перешло в обработчик afterRender в соответствующий класс элемента
				/*if ($element->type == 'wysiwyg')
				{
					$element_text = preg_replace("!(&quot;)(?=[a-zA-Z0-9АаБбВвГгДдЕеЁёЖжЗзИиЙйКкЛлМмНнОоПпРрСсТтУуФфХхЦцЧчШшЩщЪъЬьЫыЭэЮюЯя])!s", "&laquo;", $element_text);
					$element_text = preg_replace("!([a-zA-Z0-9АаБбВвГгДдЕеЁёЖжЗзИиЙйКкЛлМмНнОоПпРрСсТтУуФфХхЦцЧчШшЩщЪъЬьЫыЭэЮюЯя\?\!., ])(&quot;)([^Ёё]{0,1})!", "\\1&raquo;\\3", $element_text);
					$element_text = str_replace(' - ',' &ndash; ', $element_text);
				}*/
				$textChapter[] = $element_text;
			}
			$newChapter->detail_text = UtilsHelper::processingExternalLink(implode('', $textChapter));
			$newChapter->save();
		}

		//Общее количество символов в статье (для статистики)
		$article->detail_text_letters = strlen(strip_tags($full_detail_text) );

		$article->save();
	}


	// Возвращаем объект со связанными статьями
	public function getRelationArticles($id)
	{
		$item = self::model()->findByPk($id);
		if ($item->relations)
		{
			$items = self::model()->visible()->Indexed()->findAll('id IN(' . $item->relations . ')');
			$ar    = explode(',', $item->relations);
			foreach ($ar as $key)
			{
				if (isset($items[$key]))
				{
					$items_sort[$key] = $items[$key];
				}
			}
			if (isset($items_sort))
			{
				return $items_sort;
			}
		}
		return array();
	}






	public function beforeDelete()
	{
		Comments::model()->deleteAllByAttributes(array('entity_name' => 'Articles', 'entity_id' => $this->id));

		return parent::beforeDelete();
	}

	/**
	* Возвращает цену за 1000 символов из настроек сайта или из параметров статьи(если прописана)
	*
	*/
	public function getPrice()
	{
		$price = ($this->price > 0) ? $this->price : Settings::model()->getParam('articles_price_per_letters');
		return intval($price);
	}

	/**
	* Возвращает стоимость текущей статьи с учетом настроек сайта(articles_price_per_letters в настройках)
	* и установленной цены за 1000 символов на эту статью в параметрах статьи.
	* Подсчет количства символов происходит при сохранении статьи в модели Articles::restoreFromDraft()
	*/
	public function getTotalPrice()
	{
		$totalPrice = ($this->detail_text_letters / 1000) * $this->getPrice();

		return $totalPrice;
	}

	/**
	* Возвращает дату первого проставления галочки Опубликовано(active)
	*
	*/
	public function getFirstPublishDate()
	{
		$draft = ArticlesDraft::model()->findByAttributes(array('article_id' => $this->id, 'active' => 1), array('order' => 'id ASC') );
		if(!is_null($draft))
			return $draft->date_create;
	}





	public function getElasticIndex()
    {
        return 'fnl';
    }

	public function getElasticType()
    {
        return 'articles';
    }

	/**
	 * @param DocumentInterface $document the document where the indexable data must be applied to.
	 */
	public function populateElasticDocument(YiiElasticSearch\Document $document)
	{
		// if (get_class($this) == 'Articles')
		{
			$a = Articles::model()->findByPK($this->id);

			
			$tags = array();
			if (!empty($a))
			{
				if (!empty($a->tags))
				{
					foreach ($a->tags as $t)
					{
						$tags[] = $t->name;
					}
				}
			}

			$authors = array();
			$authors_txt = array();
			if (!empty($a))
			{
				if (!empty($a->authors))
				{
					foreach ($a->authors as $t)
					{
						$authors[] = array(
										'id' => $t->id,
										'name' => $t->name . ' ' . $t->surname,
										'image' => $t->preview_img,
										'url' => $t->url(),
									);

						$authors_txt[] = $t->name . ' ' . $t->surname;

					}
				}
			}



			$document->setId($this->id);
			$document->active     = intval($this->active && strtotime($this->date_active_start) < time()) ;
			$document->name     = $this->name;
			$document->parent_id     = $this->parent_id;
			$document->section     = !empty($this->tree->name) ? $this->tree->name : '';
			$document->tags  = $tags;
			
			if (!empty($authors))
				$document->authors  = $authors;

			$document->authors_txt  = $authors_txt;
			$document->date_active_start  = $this->date_active_start;
			$document->url = !empty($a) ? $a->url() : '/404.php';
			$document->predetail_text  = $this->predetail_text;
			$document->preview_text  = $this->preview_text;
			$document->preview_img  = $this->preview_img;

			if ($document->active == 0)
			{
				ElasticScheduledReindexQueue::addTask('articles', $this->id, $this->date_active_start);
			}
		}
	}



}




