<?php

/**
 * This is the model class for table "bg_photos".
 *
 * The followings are the available columns in table 'bg_photos':
 * @property integer $id
 * @property integer $gallery_id
 * @property string $name
 * @property string $alt
 * @property string $path
 * @property string $detail_text
 * @property integer $order_num
 * @property string $copyright
 */
class GalleryPhotos extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return GalleryPhotos the static model class
	 */

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}


	public function tableName()
	{
		return 'bg_photos';
	}


	public function rules()
	{
		return array(
			array('gallery_id, order_num', 'numerical', 'integerOnly' => true),
			array('name, alt, path, copyright', 'length', 'max' => 255),
			array('detail_text', 'length', 'max' => 1000),
			array('detail_text', 'safe'),
			array('id, gallery_id, name, alt, path, detail_text, order_num, copyright', 'safe', 'on' => 'search'),
		);
	}


	public function afterSave()
	{
		if (isset($this->id))
		{
			$tmpDir = Yii::app()->getBasePath() . '/../assets/upload';

			// Если новая картинка галереи
			if (file_exists($tmpDir . $this->path))
			{
				$ext  = substr($this->path, strrpos($this->path, '.') + 1);
				$path = Yii::getPathOfAlias('webroot') . '/media/galleries/';
				$file = $this->gallery_id . '/' . $this->id . '.' . $ext;

				$img = new Images($tmpDir . $this->path);
				/*
				$img->resize(1600, 1600);
				$img->save($path . 'large/' . $file, $ext, 95);
				$img->clear();
				$img->resize(0, 560, false, '#ffffff', true);
				$img->save($path . '840x560/' . $file, $ext, 95);

				$img->resize(0, 440, false, '#ffffff', true);
				$img->save($path . '660x440/' . $file, $ext, 95);
				*/
				$img->save($path . 'large/' . $file, $ext, 100);

				$Image       = self::model()->findByPk($this->id);
				$Image->path = '/' . $this->gallery_id . '/' . $this->id . '.' . $ext;
				$Image->save();
				unlink($tmpDir . $this->path);
			}
		}
		parent::afterSave();
	}


	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id'          => 'ID',
			'gallery_id'  => 'Gallery',
			'name'        => 'Название',
			'alt'         => 'alt/title',
			'path'        => 'Загрузить изображения',
			'detail_text' => 'Детальное описание',
			'order_num'   => 'Порядковый номер',
			'copyright'   => 'Авторство'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('gallery_id', $this->gallery_id);
		$criteria->compare('name', $this->name, true);
		$criteria->compare('alt', $this->alt, true);
		$criteria->compare('path', $this->path, true);
		$criteria->compare('detail_text', $this->detail_text, true);
		$criteria->compare('order_num', $this->order_num);
		$criteria->compare('copyright', $this->order_num);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	/**
	 * Скоуп. Указываем галерею
	 */
	public function galleryIdIs($gallery_id = null)
	{
		$this->getDbCriteria()->mergeWith(
			array(
				'condition' => 'gallery_id = :gallery_id',
				'params'    => array(':gallery_id' => $gallery_id),
			)
		);

		return $this;
	}

	/**
	 * Скоуп. Указываем order_num
	 */
	public function orderNumIs($order_num = null)
	{
		$this->getDbCriteria()->mergeWith(
			array(
				'condition' => 'order_num = :order_num',
				'params'    => array(':order_num' => $order_num),
			)
		);

		return $this;
	}

	public function img($size = 'original')
	{
		if (0 < strlen($this->path))
		{
			if ('original' == $size)
			{
				$img = "/media/galleries" . $this->path;
			}
			else
			{
				$img = "/media/galleries/" . $size . $this->path;
			}

			if (isset($img))
			{
				return $img;
			}
			else
			{
				return false;
			}
		}
		return false;
	}

	/**
	 * Скоуп. Сортировка по заданому полю
	 */
	public function order($order)
	{
		$this->getDbCriteria()->mergeWith(array('order' => $order));
		return $this;
	}
}