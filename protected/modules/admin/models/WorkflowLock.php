<?php
class WorkflowLock extends CActiveRecord
{
	public $user_name = '';

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}


	public function tableName()
	{
		return 'bg_workflow_lock';
	}


	public function rules()
	{
		return array(
			array('article_id, user_id, lock_date', 'required'),
			array('article_id, user_id, assignee', 'numerical', 'integerOnly' => true),
			//array('lock_date', 'date', 'format'=>'yyyy-MM-dd hh:mm:ss'),
		);
	}

	public static function isLockArticle($article_id)
	{
		$lock = self::model()->find('article_id=:article_id AND user_id!=:user_id', array(
			'article_id' => $article_id,
			'user_id'    => Yii::app()->user->id
		));
		if (count($lock))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	// Переназначаем ответственного
	public static function setAssignee($article_id, $assignee)
	{
		$workflow_lock = self::model()->find('article_id=:id', array(':id' => $article_id));
		if ($workflow_lock)
		{
			$workflow_lock->assignee = $assignee;
			$workflow_lock->save();
		}
	}


	// Снимаем блокировку
	public static function breakLock($article_id = 0, $user_id = 0)
	{
		$lock = self::model()->findByAttributes(array('article_id' => $article_id));

		//Проверяем, что блокировка существует и что она созданна тем же пользователем, который её и удаляет
		if (!empty($lock) && ($lock->user_id == $user_id || $user_id == 0))
		{
			if ($lock->assignee > 0)
			{
				$userto   = User::model()->findByPK($lock->assignee);
				$userfrom = User::model()->findByPK($lock->user_id);
				$article  = Articles::model()->findByPK($article_id);
				$subj     = 'Вы назначены ответственным: ' . $article->name;
				$text     = $article->name . ' ' . Yii::app()->params['baseUrl'] . '/admin/articles/edit/?id=' . $article_id . '<br/><br/>' . $article->comment;
				$text .= '<br/><br/>';
				$text .= $article->active ? 'Опубликовано' : ' Не опубликовано';
				$text .= '<br/><br/>Дата начала активности: ' . $article->date_active_start;
				$text .= '<br/><br/>Ответственный: [' . $lock->user_id . '] ' . $userto->firstname . ' ' . $userto->lastname;
				$text .= '<br/><br/>Вас назначил ответственным: [' . $lock->user_id . '] ' . $userfrom->firstname . ' ' . $userfrom->lastname;

				// Если назначенный является корректором
				$right = Yii::app()->db->createCommand('SELECT itemname FROM AuthAssignment WHERE `userid`=' . $userto->id . ' AND `itemname`="corrector"')->queryScalar();
				if ($right)
				{
					$correctorList = User::getListForRole('corrector');
					foreach ($correctorList as $corrector)
					{
						Yii::app()->mail->send(array($corrector['email'], $corrector['name']), $subj, $text);
					}
				}
				else
				{
					Yii::app()->mail->send(array($userto->email, $userto->firstname . ' ' . $userto->lastname), $subj, $text);
				}
			}
			//Снимаем блокировку
			$lock->delete();
		}
	}
}