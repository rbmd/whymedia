<?php
class Work extends CActiveRecord
{
	public $cell_sizes = array('Маленькая','Средняя', 'Большая');


	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'works';
	}



	public function relations()
	{
		return array(
			'rubric'              => array(self::BELONGS_TO, 'Rubric', 'rubric_id'),

		);
	}

	public function scopes()
	{
		return array(
			'Indexed' => array(
				'index' => 'id',
			),
			'active'  => array(
				'condition' => 't.active = 1',
			),
		);
	}

	public function attributeLabels()
	{
		$atr = array(
			'id' => 'ID',
			'code' => 'Синоним для URL',
			'active' => 'Показывать на сайте',
			'name_ru' => 'Название RU',
			'name_en' => 'Название EN',
			'name_ge' => 'Название GE',
			'lead' => 'Подзаголовок',
			'detail_text_ru' => 'Текст RU',
			'detail_text_en' => 'Текст EN',
			'detail_text_ge' => 'Текст GE',
			'date_active_start' => 'Дата на сайте',
			'pic' => 'Изображение',
			'pic_small' => 'Изображение для главной',
			'pic_desc' => 'Подпись для изображения',
			'pic_inner' => 'Изображение для внутряка',
			'pic_inner_desc' => 'Ссылка для изображения',
			'tree_id' => 'Рубрика',
			'gallery_id' => 'Фотогалерея',
			'poll_id' => 'Опрос',
			'ad_link_text' => 'Текст рекламной ссылки',
			'ad_link_url' => 'Рекламная ссылка',
			'cell_size' => 'Размер ячейки',
			'rubric_id' => 'Рубрика',
			'client_name' => 'Клиент',
			'client_url' => 'Ссылка на сайт клиента',
			'custom_url' => 'Произвольная ссылка',
			'video' => 'Youtube video ID',
			'video_vimeo' => 'Vimeo video ID',
			'cell_video_mp4' => 'Video mp4',
			'cell_video_webm' => 'Video webm',
			'first_in_ge' => 'Выводить с приоритетом в грузинской версии'
		);

		$parent_atr = parent::attributeLabels();		
		return $atr + $parent_atr;
	}

	
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		$rules = array(
			array('name_ru, code', 'required'),
            array('first_in_ge, active, predetail_text, date_active_start, detail_text_ru, detail_text_en, detail_text_ge, lead, left_col_text, cell_size, rubric_id, gallery_id, poll_id, ad_link_text, ad_link_url', 'safe'),
			array('cell_video_mp4, cell_video_webm, name_ru, video, video_vimeo, name_en, name_ge, pic, pic_small, pic_desc, pic_inner, pic_inner_desc, client_name, client_url, custom_url', 'length', 'max'=>255),
		);
		
		$parent_rules = parent::rules();
		$res = array_merge($rules, $parent_rules);
		return $res;
	}

	public function url()
	{
		return '/articles/'.$this->id.'/';
	}



}