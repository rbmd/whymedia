<?php
class Rubric extends CActiveRecord
{


	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'rubrics';
	}



	public function attributeLabels()
	{
		$atr = array(
			
			'name_ru' => 'Название RU',
			'name_en' => 'Название EN',
			'name_ge' => 'Название GE',			
			'date_active_start' => 'Дата для сортировки',
			

		);

		$parent_atr = parent::attributeLabels();		
		return $atr + $parent_atr;
	}

	
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		$rules = array(
			array('name_ru', 'required'),
			array('name_ru, name_en, name_ge, date_active_start', 'length', 'max'=>255),
		);
		
		$parent_rules = parent::rules();
		$res = array_merge($rules, $parent_rules);
		return $res;
	}





}