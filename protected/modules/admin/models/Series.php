<?php

/**
 * This is the model class for table "bg_story".
 *
 * The followings are the available columns in table 'bg_story':
 * @property integer $id
 * @property string $code
 * @property string $name
 * @property string $preview_text
 */
class Series extends KeywordsItem
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Story the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'bg_series';
    }

    public function behaviors()
    {
        return array(

        );
    }

    public function scopes()
    {
        return array(
            /*Убирать нельзя, иначе sitemap.xml не сгенерится */
            'visible'=>array(
            ),
            'indexed'=>array(
                'index'=>'id',
            ),
        );
    }

	// Скоуп select
	public function select($str = 'id')
	{
		$this->getDbCriteria()->mergeWith(array(
			'select' => $str,
		));

		return $this;
	}		
	
	// Скоуп выбора по полю name
	public function nameIs($str = '')
	{
		$this->getDbCriteria()->mergeWith(array(
			'condition' => 'name=:name',
			'params' => array(':name' => $str),
		));

		return $this;
	}		
	
    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        $rules = array(
            array('name', 'required'),
            array('code, name, preview_img', 'length', 'max'=>255),
            array('code, seo_title, seo_keywords, seo_description', 'safe'),
            array('id, code, name', 'safe', 'on'=>'search')
        );
		
		$parent_rules = parent::rules();
		$res = array_merge($rules, $parent_rules);
		return $res;
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'articles' => array(self::HAS_MANY, 'Articles', 'series_id')
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        $atr = array(
            'id' => 'ID',
            'name' => 'Название',
            'code' => 'Символьный код',
            'preview_img' => 'Изображение',
        );
		
		$parent_atr = parent::attributeLabels();		
		return $atr + $parent_atr;
    }

    protected function afterValidate()
    {
        parent::afterValidate();
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('code',$this->code,true);
        $criteria->compare('name',$this->name,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }
	
	public function getSeriesList()
	{
			$seriesids = array();
			$seriesids[''] = '';
			$series = Series::Model()->findAll();
			foreach($series as $seria) {
				$seriesids[$seria->id] = $seria->name;
			}
			
			return $seriesids;
	}


    //Возвращает количество статей привязанных к серии
    public function articlesCount($series_id = null)
    {
        if(is_null($series_id))
            $series_id = $this->id;


        $CACHE_KEY = 'serieArticlesCount'.$series_id;
        $cnt = Yii::app()->cache->get($CACHE_KEY);

        if($cnt === false)
        {
            //Считаем новую сумму голосов
            $condition = 'series_id=:series_id';
            $params = array(':series_id' => $series_id, );
            $cnt = Articles::model()->count($condition, $params);  

            Yii::app()->cache->set($CACHE_KEY, $cnt, 60);           
        }
       

        return $cnt;  
    }


    /**
     * @return string - вернуть ссылку на страницу этого сюжета
     */
    public function url()
    {
        return "/series/{$this->id}/";
    }
}
?>