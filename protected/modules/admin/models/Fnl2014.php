﻿<?php
class Fnl2014 extends CActiveRecord
{


	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'fnl2014';
	}



	

	
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		$rules = array(
			array('name', 'required'),
            array('description', 'safe'),
			array('name, letter, logo, photo', 'length', 'max'=>255),
		);
		
		$parent_rules = parent::rules();
		$res = array_merge($rules, $parent_rules);
		return $res;
	}


	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		$atr = array(
			'id' => 'ID',
			'name' => 'Название',
            'code' => 'Символьный код',
			'is_guide_tag' => 'Тег гида',
            'detail_text' => 'Описание тега',
		);
		
		$parent_atr = parent::attributeLabels();		
		return $atr + $parent_atr;
	}


}