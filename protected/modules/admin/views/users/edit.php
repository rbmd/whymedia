<?php
if (isset($model->id)) : ?>
         <h2>Редактирование пользователя, id=<?=$model->id?></h2>
<?php else : ?>
         <h2>Создание пользователя</h2>
<?php endif; ?>

<div class="form">
    <?php $form=$this->beginWidget('CActiveForm', array(
        'action' => isset($model->id) ? '/admin/users/edit/id/'.$model->id . '/': '/admin/users/edit/',
        'id'=>'adminedit-form',
        //'enableClientValidation'=>true,
        //'enableAjaxValidation'=>true,
        'clientOptions'=>array(
            //'validateOnSubmit'=>true,
        ),
        'htmlOptions'=>array('enctype'=>'multipart/form-data'),
    )); ?>

    <p class="note">Поля, отмеченные <span class="required">*</span> обязательны для заполнения.</p>

    <div class="control-group">
        <?php echo $form->labelEx($model,'email'); ?>
        <?php echo $form->textField($model,'email'); ?>
        <?php echo $form->error($model,'email'); ?>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx($model,'username'); ?>
        <?php echo $form->textField($model,'username'); ?>
        <?php echo $form->error($model,'username'); ?>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx($model,'lastname'); ?>
        <?php echo $form->textField($model,'lastname'); ?>
        <?php echo $form->error($model,'lastname'); ?>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx($model,'firstname'); ?>
        <?php echo $form->textField($model,'firstname'); ?>
        <?php echo $form->error($model,'firstname'); ?>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx($model,'profession'); ?>
        <?php echo $form->textArea($model,'profession'); ?>
        <?php echo $form->error($model,'profession'); ?>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx($model, 'birthday'); ?>
        <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model' => $model,
            'attribute'=>'birthday',
            'options'=>array(
                'showAnim'=>'fold',
                'dateFormat' => 'dd/mm/yy',
                'minDate'=>'-90y',
                'yearRange' => '1917:c',
                'defaultDate' => '-20y',
                'changeMonth' => 'true',
                'changeYear' => 'true',
            ),
            'htmlOptions'=>array(
                //'style'=>'height:20px;',
            )
        ));?>
        <?php echo $form->error($model, 'birthday'); ?>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx($model,'avatar'); ?>
        <?php echo $form->textField($model,'avatar'); ?>
        <?php echo $form->error($model,'avatar'); ?>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx($model, 'country'); ?>
        <?php $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
            'model'=>$model,
            'attribute'=>'country',
            'source'=> array_keys(Country::model()->findAll(array('select'=>'name_ru', 'index'=>'name_ru'))),
            'htmlOptions'=>array(
                'size'=>'20'
            ),
        ));?>
        <?php echo $form->error($model, 'country'); ?>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx($model, 'city'); ?>
        <?php echo $form->textField($model, 'city'); ?>
        <?php echo $form->error($model, 'city'); ?>
    </div>

	<div class="control-group">
		<?php echo $form->labelEx($model, 'description'); ?>
		<?php echo $form->textArea($model, 'description'); ?>
		<?php echo $form->error($model, 'description'); ?>
	</div>

    <div class="control-group">
        <?php echo $form->labelEx($model, 'activkey'); ?>
        <?php echo $form->textField($model, 'activkey'); ?>
        <?php echo $form->error($model, 'activkey'); ?>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx($model, 'createtime'); ?>
        <?php echo $form->textField($model, 'createtime'); ?>
        <?php echo $form->error($model, 'createtime'); ?>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx($model, 'lastvisit'); ?>
        <?php echo $form->textField($model, 'lastvisit'); ?>
        <?php echo $form->error($model, 'lastvisit'); ?>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx($model, 'superuser'); ?>
        <?php echo $form->textField($model, 'superuser'); ?>
        <?php echo $form->error($model, 'superuser'); ?>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx($model, 'status'); ?>
        <?php echo $form->textField($model, 'status'); ?>
        <?php echo $form->error($model, 'status'); ?>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx($model, 'external_type'); ?>
        <?php echo $form->textField($model, 'external_type'); ?>
        <?php echo $form->error($model, 'external_type'); ?>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx($model, 'external_id'); ?>
        <?php echo $form->textField($model, 'external_id'); ?>
        <?php echo $form->error($model, 'external_id'); ?>
    </div>

    <?= $form->errorSummary($model, 'Исправьте, пожалуйста, следующие ошибки:', null, array('class' => 'alert alert-error')) ?>
    
    <div class="row buttons">
        <?php echo CHtml::submitButton('Сохранить', array('class'=>'btn btn-success')); ?>
        <?php if (isset($model->id)) echo CHtml::submitButton('Применить', array('class' => 'btn btn-primary', 'name'=>'apply',)); ?>
        <?php if (isset($model->id)) echo CHtml::button('Отменить', array('class' => 'btn', 'id'=>'cancel_btn', 'onclick'=>'window.location.href = "/admin/users"; return false;')); ?>
    </div>

    <?php $this->endWidget(); ?>

    <hr/>

    <?php 
    if (isset($model->id)){
        $form=$this->beginWidget('CActiveForm', array(
            'action' => isset($model->id) ? '/admin/users/edit/id/'.$model->id . '/': '/admin/users/edit/',
            'id'=>'adminedit-form',
            //'enableClientValidation'=>true,
            //'enableAjaxValidation'=>true,
            'clientOptions'=>array(
                //'validateOnSubmit'=>true,
            ),
            'htmlOptions'=>array('enctype'=>'multipart/form-data'),
        )); ?>
            <div class="row buttons">
                <?php echo CHtml::label('Сменить пароль на: ', 'new_pwd', array('style'=>'font-weight: bold;'));?>
                <?php echo CHtml::textField('new_pwd', $new_pwd, array()); ?>
                <?php echo CHtml::submitButton('Сохранить', array('class'=>'btn btn-success')); ?>
                <br/>
                <?if( isset($pwd_error) ):?>
                    <span style="color:#bb0010; font-weight: bold;"><?php echo CHtml::encode($pwd_error)?></span>
                <?endif?>
            </div>
    <?php
        $this->endWidget();
    }
    ?>
</div>