<a href="/admin/tree/create/" class="btn btn-success">Создать категорию</a>
<br/><br/>

<?/*
<div id="tree">
</div>
<script type="text/javascript">
	$(function () {
		$.jstree._themes = '/static/css/admin/jstree-themes/'
		$("#tree").jstree({ 
			themes: {
				theme : '',
			},
			json_data : { 
				data: [<?= json_encode($tree[0]->getTree()) ?> ]
			},
			plugins : [ 'themes', 'json_data', 'cookies' ]
		}).bind('loaded.jstree', function() { $('#tree a').addClass('btn'); });		

	});
</script>
*/?>



<table class="table">
  <thead>
  	<tr>
  		<th>&nbsp;</th>
  		<th>&nbsp;</th>
  		<th>#</th>
  		<th>Название</th>
  		<th>Символьный код</th>
  		<th>Полный путь</th>
        
  		<th>&nbsp;</th>
  	</tr>
  </thead>
<?
foreach($tree as $key => $t)
{
	?>
	<tr id="node-<?= $t->id ?>" data-level="<?= $t->clevel ?>">
		<td style="white-space: nowrap;">            
			<a onclick="MoveNode('up', <?=$t->id?>); return false;" title="up" class="btn btn-mini"><i class="icon-arrow-up"></i></a>
            <a onclick="MoveNode('down', <?=$t->id?>); return false;" title="down" class="btn btn-mini"><i class="icon-arrow-down"></i></a>
		</td>
		<td style="text-align: center;">
			<span class="label label-<?=(($t->active == 1) ? 'success' : 'important')?>">&nbsp;&nbsp;</span>
		</td>
		<td><?=$t->id?></td>
		<td>
            <?=str_repeat('------', $t->clevel-1) ?>
            <a class="tree_node_edit" href="/admin/tree/edit/?id=<?=$t->id?>"><?=$t->name?><?if(!empty($t->eng_name)):?>&nbsp;(<span style="background-color: #ffb6c1;"><?=$t->eng_name?></span>)<?endif?></a>
        </td>
		<td><?=$t->code?></td>
		<td><a href="/<?=$t->url?>" target="_blank"><?=$t->url?></a></td>            
		<?
		if($t->id == 1)
		{
			?>
			<td>&nbsp;</td>
			<?
		} else {
		?>		
		<td><?/*
            <? if( !in_array($t->id, $deny_to_delete_list) && $t->isLeaf() ) : ?>
            <?= CHtml::link('Удалить', "/admin/tree/deletenode/?id=$t->id", array('class'=>'tree_node_delete', 'confirm'=>'Вы уверены, что хотите удалить эту категорию?')) ?>
            <? endif ?>
			*/?>
        </td>
		<?
		}
		?>
	</tr>
	<?
}
?>
</table>

<?php
Yii::app()->getClientScript()->registerScript('tree_table_dragable', "

    function MoveNode(direction, id)
    {
        $.ajax({
            type: 'POST',
            url: '/admin/tree/move/direction/' + direction + '/',
            data: 'id=' + id,
            dataType: 'json',
            complete: function(){ },
            success: function(data, textStatus, jqXHR)
            {
            	var node = $('#node-' + id);
            	
            	var rel;
            	if (direction == 'up')
            		rel = node.prevAll('tr[data-level='+node.data('level')+']:first');
            	else
            		rel = node.nextAll('tr[data-level='+node.data('level')+']:first');
            		
            	
            	{
	            	var node_childs = node.nextUntil('tr[data-level='+node.data('level')+']').filter(function() {  return $(this).data('level') > node.data('level'); });
	            	var rel_childs = rel.nextUntil('tr[data-level='+rel.data('level')+']').filter(function() {  return $(this).data('level') > rel.data('level'); });
	            	
	            	if (direction == 'up')
	            		node.insertBefore(rel);
	            	else
	            		rel.insertBefore(node);
	            		
	            	var marker = node;
	            	node_childs.each(function() {
	            		$(this).insertAfter(marker);
	            		marker = $(this);
	            	});
	            	
	            	marker = rel;
	            	rel_childs.each(function() {
	            		$(this).insertAfter(marker);
	            		marker = $(this);
	            	});
	            }
            },
            error: function(XHR, textStatus, errorThrown){alert(\"Произошла непредвиденная ошибка! \"+XHR.responseText);},
        });
    }

	", CClientScript::POS_END);
?>