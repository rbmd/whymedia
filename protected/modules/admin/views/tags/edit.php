<h1><?
if($action == 'create')
{
	?>Создание тега<?
}
else
{
	?>Тег #<?=$model->id?><?
}
?>
</h1>
<div class="form">
	<?
		$form = $this->beginWidget('CActiveForm', array(
			'action' => !empty($model->id) ? "?id={$model->id}" : '',
			'id' => 'tags-form',
			'enableClientValidation' => false,
			'enableAjaxValidation' => false,
			'clientOptions' => array(
				'validateOnSubmit' => false, 
			),
			'htmlOptions' => array(
								'class' => 'form-horizontal',
							),
		));
	?>

		<fieldset>
			<div class="control-group">
				<?= $form->labelEx($model, 'name', array('class' => 'control-label')) ?>
				<div class="controls">
					<div class="row">
						<div class="span8">
							<?= $form->textField($model, 'name', array('data-sync-to' => 'slug', 'class' => 'span8')) ?>
							<span class="help-inline"><?= $form->error($model, 'name') ?></span>
						</div>
					</div>
				</div>
			</div>

			<div class="control-group">
				<?= $form->labelEx($model, 'code', array('class' => 'control-label')) ?>
				<div class="controls">
					<div class="row">
						<div class="span8">
							<div class="input-append"><?
								echo $form->textField($model, 'code', array('data-sync-from' => 'slug', 'class' => 'span7'));
								$cookie_var = Yii::app()->request->cookies['transliterate_lock_state'];
								?><button data-action="trigger-lock" data-sync-to="slug" class="btn icon" onclick="dynamicClick($(this)); return false;"><?= isset($cookie_var->value[ Yii::app()->request->requestUri ]) ? $cookie_var->value[ Yii::app()->request->requestUri ] : 'w' ?></button>
							</div>
							<span class="help-inline"><?= $form->error($model, 'code') ?></span>
						</div>
					</div>
				</div>
			</div>
			
			<div class="control-group">
				<?= $form->labelEx($model, 'is_guide_tag', array('class' => 'control-label')) ?>
				<div class="controls">
					<?= $form->checkBox($model, 'is_guide_tag') ?>
					<span class="help-inline"><?= $form->error($model, 'is_digitilized') ?></span>
				</div>
			</div>	
			
    		<div class="control-group">
				<?= $form->labelEx($model, 'detail_text', array('class' => 'control-label')) ?>
				<div class="controls">
					<div class="row">
						<div class="span8">
							<?= $form->textField($model, 'detail_text', array('class' => 'span8')) ?>
							<span class="help-inline"><?= $form->error($model, 'detail_text') ?></span>
						</div>
					</div>
				</div>
			</div>

			<div class="control-group">
				<?= $form->labelEx($model, 'seo_title', array('class' => 'control-label')) ?>
				<div class="controls">
					<div class="row">
						<div class="span8">
							<?= $form->textField($model, 'seo_title', array('class' => 'span8')) ?>
							<span class="help-inline"><?= $form->error($model, 'seo_title') ?></span>
						</div>
					</div>
				</div>
			</div>

			<div class="control-group">
				<?= $form->labelEx($model, 'seo_keywords', array('class' => 'control-label')) ?>
				<div class="controls">
					<div class="row">
						<div class="span8">
							<?= $form->textField($model, 'seo_keywords', array('class' => 'span8')) ?>
							<span class="help-inline"><?= $form->error($model, 'seo_keywords') ?></span>
						</div>
					</div>
				</div>
			</div>

			<div class="control-group">
				<?= $form->labelEx($model, 'seo_description', array('class' => 'control-label')) ?>
				<div class="controls">
					<div class="row">
						<div class="span8">
							<?= $form->textArea($model, 'seo_description', array('class' => 'span8')) ?>
							<span class="help-inline"><?= $form->error($model, 'seo_description') ?></span>
						</div>
					</div>
				</div>
			</div>
		</fieldset>



	<?= CHtml::errorSummary($model, 'Исправьте, пожалуйста, следующие ошибки:', null, array('class' => 'alert alert-error')) ?>
	
	<div class="submit">
        <? echo CHtml::submitButton('Сохранить', array('class' => 'btn btn-success')); ?>
        <? echo CHtml::submitButton('Применить', array('class' => 'btn btn-primary', 'name'=>'apply',)); ?>
        <? echo CHtml::button('Отменить', array('class' => 'btn', 'id'=>'cancel_btn')); ?>
	</div>

    <div style="float:right;">
        <? echo CHtml::ajaxButton(
        'Удалить',
        '/admin/tags/delete',
        array(
            'data' => "id={$model->id}",
            'beforeSend' => 'function(){}',
            'complete' => 'function(){}',
            'success' => 'function(){alert("Тег успешно удалён!"); window.location.href="/admin/tags";}',
            'error' => 'function(){alert("Во время удаления тега произошла непредвиденная ошибка!");}',
        ),
        array(
            'class' => 'btn btn-danger',
            'id'=>'delete_btn',
            'confirm'=>'Вы уверены, что хотите безвозвратно удалить этот тег?',
        )
    ); ?>
    </div>
	 
<? $this->endWidget(); ?>


	<?
	if($action == 'edit')
	{
	?>
	<div>
		<h3>Статьи, содержащие тег</h3>
		<?
		foreach($articles as $v)
		{
		?>
			<a href="/admin/articles/edit/?id=<?=$v->id?>"><?=$v->name?></a><br />
		<?
		}
		?>
	 </div>
	<?
	}
	?>
</div>

<?php
Yii::app()->getClientScript()->registerScript('cancel_btn_tags', '
    $("#cancel_btn").click( function(e) {
        window.location.href = "/admin/tags/";
    });
', CClientScript::POS_READY)
?>