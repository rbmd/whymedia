<h1><?
if($action == 'create')
{
	?>Создание работы<?
}
else
{
	?>Работа #<?=$model->id?><?
}
?>
</h1>
<div class="form">
	<?
		$form = $this->beginWidget('CActiveForm', array(
			'action' => !empty($model->id) ? "?id={$model->id}" : '',
			'id' => 'tags-form',
			'enableClientValidation' => false,
			'enableAjaxValidation' => false,
			'clientOptions' => array(
				'validateOnSubmit' => false, 
			),
			'htmlOptions' => array(
								'class' => 'form-horizontal',
							),
		));
	?>

		<fieldset>

			<div class="control-group">
				<?= $form->labelEx($model, 'active', array('class' => 'control-label')) ?>

				<div class="controls">
					<?= $form->checkBox($model, 'active') ?>
					<span class="help-inline"><?= $form->error($model, 'active') ?></span>
				</div>
			</div>

			<div class="control-group">
				<?= $form->labelEx($model, 'first_in_ge', array('class' => 'control-label')) ?>

				<div class="controls">
					<?= $form->checkBox($model, 'first_in_ge') ?>
					<span class="help-inline"><?= $form->error($model, 'first_in_ge') ?></span>
				</div>
			</div>

			<div class="control-group">
				<?= $form->labelEx($model, 'name_ru', array('class' => 'control-label')) ?>
				<div class="controls">
					<div class="row">
						<div class="span8">
							<?= $form->textField($model, 'name_ru', array('data-sync-to' => 'slug', 'class' => 'span8')) ?>
							<span class="help-inline"><?= $form->error($model, 'name_ru') ?></span>
						</div>
					</div>
				</div>
			</div>

			<div class="control-group">
				<?= $form->labelEx($model, 'name_en', array('class' => 'control-label')) ?>
				<div class="controls">
					<div class="row">
						<div class="span8">
							<?= $form->textField($model, 'name_en', array('data-sync-to' => 'slug', 'class' => 'span8')) ?>
							<span class="help-inline"><?= $form->error($model, 'name_en') ?></span>
						</div>
					</div>
				</div>
			</div>

			<div class="control-group">
				<?= $form->labelEx($model, 'name_ge', array('class' => 'control-label')) ?>
				<div class="controls">
					<div class="row">
						<div class="span8">
							<?= $form->textField($model, 'name_ge', array('data-sync-to' => 'slug', 'class' => 'span8')) ?>
							<span class="help-inline"><?= $form->error($model, 'name_ge') ?></span>
						</div>
					</div>
				</div>
			</div>

			<div class="control-group">
				<?= $form->labelEx($model, 'code', array('class' => 'control-label')) ?>
				<div class="controls">
					<div class="row">
						<div class="span8">
							<?= $form->textField($model, 'code', array('data-sync-to' => 'slug', 'class' => 'span8')) ?>
							<span class="help-inline"><?= $form->error($model, 'code') ?></span>
						</div>
					</div>
				</div>
			</div>



			<div class="control-group control-group_x2">
				<?= $form->labelEx($model, 'date_active_start', array('class' => 'control-label')) ?>
				<div class="controls">
					<? $this->widget('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker', array(
					'model'     => $model,
					'attribute' => 'date_active_start',
					'mode'      => 'datetime',
					'language'  => 'ru',
					'options'   => array(
						//'dateFormat' => 'dd/mm/yy',
						'dateFormat' => 'yy-mm-dd',
						'timeFormat' => 'hh:mm:ss',
					),
				));?>
					<span class="help-inline"><?= $form->error($model, 'date_active_start') ?></span>
				</div>
			</div>

			<div class="control-group">
				<?= $form->labelEx($model, 'video', array('class' => 'control-label')) ?>
				<div class="controls">
					<div class="row">
						<div class="span8">
							<?= $form->textField($model, 'video', array('data-sync-to' => 'slug', 'class' => 'span8')) ?>
							<span class="help-inline"><?= $form->error($model, 'video') ?></span>
						</div>
					</div>
				</div>
			</div>

			<div class="control-group">
				<?= $form->labelEx($model, 'video_vimeo', array('class' => 'control-label')) ?>
				<div class="controls">
					<div class="row">
						<div class="span8">
							<?= $form->textField($model, 'video_vimeo', array('data-sync-to' => 'slug', 'class' => 'span8')) ?>
							<span class="help-inline"><?= $form->error($model, 'video_vimeo') ?></span>
						</div>
					</div>
				</div>
			</div>

			<div class="control-group">
				<?= $form->labelEx($model, 'cell_video_mp4', array('class' => 'control-label')) ?>
				<div class="controls">
					<div class="row">
						<div class="span8">
							<?= $form->textField($model, 'cell_video_mp4', array('data-sync-to' => 'slug', 'class' => 'span8')) ?>
							<script type="text/javascript">
								$(function() {
									ckfinderManager.create('<?=get_class($model)?>_cell_video_mp4');
								});
							</script>
							<span class="help-inline"><?= $form->error($model, 'cell_video_mp4') ?></span>
						</div>
					</div>
				</div>
			</div>

			<div class="control-group">
				<?= $form->labelEx($model, 'cell_video_webm', array('class' => 'control-label')) ?>
				<div class="controls">
					<div class="row">
						<div class="span8">
							<?= $form->textField($model, 'cell_video_webm', array('data-sync-to' => 'slug', 'class' => 'span8')) ?>
							<script type="text/javascript">
								$(function() {
									ckfinderManager.create('<?=get_class($model)?>_cell_video_webm');
								});
							</script>
							<span class="help-inline"><?= $form->error($model, 'cell_video_webm') ?></span>
						</div>
					</div>
				</div>
			</div>

			<div class="control-group">
				<?= $form->labelEx($model, 'rubric_id', array('class' => 'control-label')) ?>
				<div class="controls">
					<?echo CHtml::activeDropDownList($model, 'rubric_id', $tree, array('class' => 'b-admin__select'));?>
					<span class="help-inline"><?= $form->error($model, 'rubric_id') ?></span>
				</div>
			</div>

			<div class="control-group">
				<?= $form->labelEx($model, 'cell_size', array('class' => 'control-label')) ?>
				<div class="controls">
					<?echo CHtml::activeDropDownList($model, 'cell_size', $model->cell_sizes, array('class' => 'b-admin__select'));?>
					<span class="help-inline"><?= $form->error($model, 'rubric_id') ?></span>
				</div>
			</div>

			<div class="control-group ">
				<?php echo $form->labelEx($model, 'client_name', array('class' => 'control-label')); ?>
				<div class="controls">
					<?=$form->textField($model, 'client_name', array('class' => 'span8'))?>
					<span class="help-inline"><?php echo $form->error($model, 'client_name'); ?></span>
				</div>
			</div>

			<div class="control-group ">
				<?php echo $form->labelEx($model, 'client_url', array('class' => 'control-label')); ?>
				<div class="controls">
					<?=$form->textField($model, 'client_url', array('class' => 'span8'))?>
					<span class="help-inline"><?php echo $form->error($model, 'client_url'); ?></span>
				</div>
			</div>



			<div class="control-group ">
				<?php echo $form->labelEx($model, 'pic', array('class' => 'control-label')); ?>
				<div class="controls">

					<?=$form->textField($model, 'pic', array('class' => 'b-admin__text span8'))?>
					<script type="text/javascript">
						$(function() {
							ckfinderManager.create('<?=get_class($model)?>_pic');
						});
					</script>

					<span class="help-inline"><?php echo $form->error($model, 'pic'); ?></span>
				</div>

			</div>

			<div class="control-group ">
				<?php echo $form->labelEx($model, 'pic_small', array('class' => 'control-label')); ?>
				<div class="controls">

					<?=$form->textField($model, 'pic_small', array('class' => 'b-admin__text span8'))?>
					<script type="text/javascript">
						$(function() {
							ckfinderManager.create('<?=get_class($model)?>_pic_small');
						});
					</script>

					<span class="help-inline"><?php echo $form->error($model, 'pic_small'); ?></span>
				</div>

			</div>


			<div class="control-group">
				<?= $form->labelEx($model, 'custom_url', array('class' => 'control-label')) ?>
				<div class="controls">
					<div class="row">
						<div class="span8">
							<?= $form->textField($model, 'custom_url', array('data-sync-to' => 'slug', 'class' => 'span8')) ?>
							<span class="help-inline"><?= $form->error($model, 'custom_url') ?></span>
						</div>
					</div>
				</div>
			</div>


	
			
  		<div class="control-group">
			<?= $form->labelEx($model, 'detail_text_ru', array('class' => 'control-label')) ?>
			<div class="controls bordered-inline-editor">
				<div class="row">
					<div class="span8">
						<?= $form->textarea($model, 'detail_text_ru', array('class' => 'span12', 'rows'=>10)) ?>
						<span class="help-inline"><?= $form->error($model, 'detail_text_ru') ?></span>
					</div>
				</div>
			</div>
		</div>

  		<div class="control-group">
			<?= $form->labelEx($model, 'detail_text_en', array('class' => 'control-label')) ?>
			<div class="controls bordered-inline-editor">
				<div class="row">
					<div class="span8">
						<?= $form->textarea($model, 'detail_text_en', array('class' => 'span12', 'rows'=>10)) ?>
						<span class="help-inline"><?= $form->error($model, 'detail_text_en') ?></span>
					</div>
				</div>
			</div>
		</div>


  		<div class="control-group">
			<?= $form->labelEx($model, 'detail_text_ge', array('class' => 'control-label')) ?>
			<div class="controls bordered-inline-editor">
				<div class="row">
					<div class="span8">
						<?= $form->textarea($model, 'detail_text_ge', array('class' => 'span12', 'rows'=>10)) ?>
						<span class="help-inline"><?= $form->error($model, 'detail_text_ge') ?></span>
					</div>
				</div>
			</div>
		</div>


      <script>
        $(document).ready(function() {
           wysiwygManager.createEditor('<?=get_class($model)?>_detail_text', {config: 'config.js', inlineEditor: true});
        });
      </script>

      


		</fieldset>



	<?= CHtml::errorSummary($model, 'Исправьте, пожалуйста, следующие ошибки:', null, array('class' => 'alert alert-error')) ?>
	
	<div class="submit">
        <? echo CHtml::submitButton('Сохранить', array('class' => 'btn btn-success')); ?>
        <? echo CHtml::submitButton('Применить', array('class' => 'btn btn-primary', 'name'=>'apply',)); ?>
        <? echo CHtml::button('Отменить', array('class' => 'btn', 'id'=>'cancel_btn')); ?>
	</div>

    <div style="float:right;">
        <? echo CHtml::ajaxButton(
        'Удалить',
        '/admin/works/delete/',
        array(
            'data' => "id={$model->id}",
            'beforeSend' => 'function(){}',
            'complete' => 'function(){}',
            'success' => 'function(){alert("Элемент успешно удалён!"); window.location.href="/admin/works/";}',
            'error' => 'function(){alert("Во время удаления элемента произошла непредвиденная ошибка!");}',
        ),
        array(
            'class' => 'btn btn-danger',
            'id'=>'delete_btn',
            'confirm'=>'Вы уверены, что хотите безвозвратно удалить этот элемент?',
        )
    ); ?>
    </div>
	 
<? $this->endWidget(); ?>

</div>

<?php
Yii::app()->getClientScript()->registerScript('cancel_btn_tags', '
    $("#cancel_btn").click( function(e) {
        window.location.href = "/admin/works/";
    });
', CClientScript::POS_READY)
?>