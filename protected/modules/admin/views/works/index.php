<h1>Работы</h1>
<a href="/admin/works/create/" class="btn btn-success">Создать </a>
<br/><br/>

<table class="table">
    <thead>
        <th>ID</th>        
        <th>Рубрика</th>
        <th class="span10">Название</th>
        <th>Дата на сайте</th>
    </thead>
<?
foreach($articles as $n)
{
?>
	<tr>
		<td>#<?=$n->id?></td>
        <td><?if(isset($n->rubric)){?><?=$n->rubric->name_ru?><?}?></td>
		<td><a href="/admin/works/edit/?id=<?=$n['id']?>"><?=$n->name_ru?></a></td>	
        <td><?=$n->date_active_start?></td>
	</tr>
<?
}
?>
</table>
