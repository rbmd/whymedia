<div class="col span7" style="text-align: center;">

    <div style="padding-bottom: 10px"><span style="font-weight: bold;">СТАТИСТИКА ПО КОНТРОЛЛЕРАМ</span></div>

    <table style="width:100%; border: 1px solid #000000;">

        <thead style="color: #f5f5f5; background-color: #000000;font-weight: bold;">
            <tr>
                <td style="">Контроллер</td>
                <td style="width: 53px;">Кол-во</td>
                <td style="width: 100px;padding-left: 10px;padding-right: 10px;">Максимальное время, сек</td>
                <td style="width: 200px;padding-left: 10px;padding-right: 10px;">Среднее время (контроллер+виджеты), сек</td>
                <td style="width: 200px;padding-left: 10px;padding-right: 10px;">Среднее время (только контроллер, без виджетов), сек</td>
            </tr>
        </thead>

        <tbody>
            <?$i=0;?>
            <?foreach($controllers as $controller => $data):?>

                <? $all_widgets_time = Profiler::getTotalWidgetsTimePerController($controller);?>

                <tr style="background-color: <?if($i%2):?>#87cefa;<?else:?>#0e90d2;<?endif?>">
                    <td style="text-align: left;padding-left: 5px;"><a style="color: #000000;" href="/admin/profiler/allrequests/?c=<?=$controller?>"><?=$controller?></a></td>
                    <td><?=$data->cnt?></td>
                    <td><?=sprintf("%.3f", round($data->max_exec_time, 3))?></td>
                    <td><span style="font-weight: bold;"><?=sprintf("%.3f", round($data->avg_exec_time, 3))?></span></td>
                    <td><span style="font-weight: bold;"><?=sprintf("%.3f", round($data->avg_exec_time, 3) - round($all_widgets_time, 3))?></span></td>
                </tr>
<?/*
                <tr>
                    <td colspan="4">
                        <table style="width: 100%">
                            <!--
                            <thead style="color: #f5f5f5; background-color: #000000; font-size:12px; font-weight: normal;">
                            <tr>
                                <td style="width: 25px;"></td>
                                <td style="">Виджет</td>
                                <td style="width: 50px;">Кол-во</td>
                                <td style="width: 70px;padding-left: 10px;padding-right: 10px;">Сред время</td>
                            </tr>
                            </thead>
                            -->
                            <tbody>
                            <?foreach($data->widgets as $widget => $tdata):?>
                                <tr style="background-color: #ffbd40;">
                                    <td style="width: 20px;"></td>
                                    <td style="text-align: left"><?=$widget?></td>
                                    <td style="width: 53px;"><?=$tdata->cnt?></td>
                                    <td style="width:200px;padding-left: 10px;padding-right: 10px;"><?=sprintf("%.3f", round($tdata->avg_exec_time, 3))?></td>
                                    <td style="width:215px;">-</td>
                                </tr>
                            <?endforeach?>
                            </tbody>
                        </table>
                        <table style="width: 100%">
                            <tr style="background-color: #ffbd40;">
                                <td style="width: 20px;"></td>
                                <td style="text-align: left;font-weight: bold;">ИТОГОВОЕ ВРЕМЯ ВЫПОЛНЕНИЯ ВСЕХ ВИДЖЕТОВ:</td>
                                <td style="font-weight: bold;width:494px;"><?=sprintf("%.3f", round($all_widgets_time, 3))?></td>
                            </tr>
                        </table>
                    </td>
                </tr>
*/?>
            <?++$i;?>
            <?endforeach?>
        </tbody>

    </table>
</div>

<div style="text-align: center;margin-top: 40px;">

    <div style="padding-bottom: 10px"><span style="font-weight: bold;">СТАТИСТИКА ПО ВИДЖЕТАМ</span></div>

    <table style="width:100%; border: 1px solid #000000;">

        <thead style="color: #f5f5f5; background-color: #000000;font-weight: bold;">
        <tr>
            <td style="">Виджет</td>
            <td style="width: 60px;">Кол-во</td>
            <td style="width: 100px;padding-left: 10px;padding-right: 10px;">Максимальное время, сек</td>
            <td style="width: 100px;padding-left: 10px;padding-right: 10px;">Среднее время, сек</td>
        </tr>
        </thead>

        <tbody>
            <?$i=0;?>
            <?foreach($widgets as $widget => $data):?>
                <tr style="background-color: <?if($i%2):?>#87cefa;<?else:?>#0e90d2;<?endif?>">
                    <td style="text-align: left;padding-left: 5px;"><a style="color:black;" href="/admin/profiler/allwidgets/?w=<?=$widget?>"><?=$widget?></a></td>
                    <td><?=$data->cnt?></td>
                    <td><?=sprintf("%.3f", round($data->max_exec_time, 3))?></td>
                    <td><?=sprintf("%.3f", round($data->avg_exec_time, 3))?></td>
                </tr>
                <?++$i;?>
            <?endforeach?>
        </tbody>

    </table>
</div>

<br/>
<br/>