<h3>Параметры главной страницы</h3>

<div style="width: 400px; float: left">
	<form action="/admin/indexpage/savematerials/" method="POST">
		<div class="control-group">
			<label class="control-label">Главное интервью</label>
			<div class="controls">
				<?php
				$this->widget('AutocompleteWidget', array(
					'data'     => $materials['main_interview'],
					'name'     => 'materials_id[main_interview][]',
					'maxItems' => 1,
					'url'      => '/admin/materials/getbyname/',
				));?>
			</div>
		</div>

		<div class="control-group">
			<label class="control-label">Фотогалерея</label>
			<div class="controls">
				<?php
				$this->widget('AutocompleteWidget', array(
					'data'     => $materials['main_photogallery'],
					'name'     => 'materials_id[main_photogallery][]',
					'maxItems' => 1,
					'url'      => '/admin/materials/getbyname/',
				));?>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">Инфографика</label>
			<div class="controls">
				<?php
				$this->widget('AutocompleteWidget', array(
					'data'     => $materials['main_infographic'],
					'name'     => 'materials_id[main_infographic][]',
					'maxItems' => 1,
					'url'      => '/admin/materials/getbyname/',
				));?>
			</div>
		</div>

		<div class="control-group">
			<label class="control-label">Первая двойная статья</label>
			<div class="controls">
				<?php
				$this->widget('AutocompleteWidget', array(
					'data'     => $materials['first_double_article'],
					'name'     => 'materials_id[first_double_article][]',
					'maxItems' => 1,
					'url'      => '/admin/materials/getbyname/',
				));?>
			</div>
		</div>

		<div class="control-group">
			<label class="control-label">Вторая двойная статья</label>
			<div class="controls">
				<?php
				$this->widget('AutocompleteWidget', array(
					'data'     => $materials['second_double_article'],
					'name'     => 'materials_id[second_double_article][]',
					'maxItems' => 1,
					'url'      => '/admin/materials/getbyname/',
				));?>
			</div>
		</div>

		<div class="control-group">
			<label class="control-label">Третья двойная статья</label>
			<div class="controls">
				<?php
				$this->widget('AutocompleteWidget', array(
					'data'     => $materials['third_double_article'],
					'name'     => 'materials_id[third_double_article][]',
					'maxItems' => 1,
					'url'      => '/admin/materials/getbyname/',
				));?>
			</div>
		</div>

		<div class="control-group">
			<label class="control-label">Четвертая двойная статья</label>
			<div class="controls">
				<?php
				$this->widget('AutocompleteWidget', array(
					'data'     => $materials['fourth_double_article'],
					'name'     => 'materials_id[fourth_double_article][]',
					'maxItems' => 1,
					'url'      => '/admin/materials/getbyname/',
				));?>
			</div>
		</div>

				<div class="control-group">
			<label class="control-label">Видео</label>
			<div class="controls">
				<?php
				$this->widget('AutocompleteWidget', array(
					'data'     => $materials['video'],
					'name'     => 'materials_id[video][]',
					'maxItems' => 1,
					'url'      => '/admin/materials/getbyname/',
				));?>
			</div>
		</div>

		<input type="submit" name="save" value="Сохранить" />
	</form>
</div>

